package me.randomHashTags.RandomPackage.utils.placeholders;

import me.clip.placeholderapi.external.EZPlaceholderHook;
import me.randomHashTags.RandomPackage.RPPlayerData;
import me.randomHashTags.RandomPackage.RandomPackage;
import me.randomHashTags.RandomPackage.api.Titles;
import org.bukkit.entity.Player;

public class Placeholders extends EZPlaceholderHook{

    private static final Titles titles = Titles.getTitles();
    public Placeholders(RandomPackage rp) {
        super(rp, "randompackage");
    }

    @Override
    public String onPlaceholderRequest(Player player, String identifier) {
        final RPPlayerData pdata = player != null ? RPPlayerData.get(player.getUniqueId()) : null;
        if(pdata == null) {
            return "";
        } else if(identifier.equals("ah_listings")) {
            return Integer.toString(pdata.auctions.size());
        } else if(identifier.equals("ah_bin")) {
            return Integer.toString(pdata.collectionbin.size());

        } else if(identifier.equals("coinflip_wins")) {
            return Integer.toString(pdata.coinflipWins);
        } else if(identifier.equals("coinflip_won$")) {
            return Double.toString(pdata.coinflipWonCash);
        } else if(identifier.equals("coinflip_losses")) {
            return Integer.toString(pdata.coinflipLosses);
        } else if(identifier.equals("coinflip_lost$")) {
            return Double.toString(pdata.coinflipLostCash);
        } else if(identifier.equals("coinflip_notifications")) {
            return Boolean.toString(pdata.receivesCoinflipNotifications);

        } else if(identifier.equals("duel_elo")) {
            return Integer.toString(pdata.duelELO);
        } else if(identifier.equals("duel_wins")) {
            return Integer.toString(pdata.duelWins);
        } else if(identifier.equals("duel_losses")) {
            return Integer.toString(pdata.duelLosses);
        } else if(identifier.equals("duel_invites")) {
            return Boolean.toString(pdata.receivesDuelInvites);

        } else if(identifier.equals("filter")) {
            return Boolean.toString(pdata.activeFilter);

        } else if(identifier.equals("fund_deposits")) {
            return Double.toString(pdata.fundDeposits);

        } else if(identifier.equals("jackpot_wins")) {
            return Integer.toString(pdata.jackpotWins);
        } else if(identifier.equals("jackpot_tickets")) {
            return Integer.toString(pdata.jackpotTicketsPurchased);
        } else if(identifier.equals("jackpot_won$")) {
            return Double.toString(pdata.jackpotWinnings);
        } else if(identifier.equals("jackpot_notifications")) {
            return Boolean.toString(pdata.receivesJackpotNotifications);

        } else if(identifier.equals("chat_title")) {
            return !titles.isEnabled || pdata.activeTitle == null ? "" : pdata.getFormattedActiveTitle(true);
        } else if(identifier.equals("tab_title")) {
            return !titles.isEnabled || pdata.activeTitle == null ? "" : pdata.getFormattedActiveTitle(false);
        }
        return null;
    }
}