package me.randomHashTags.RandomPackage.utils.supported;

import org.bukkit.Bukkit;
import org.bukkit.plugin.RegisteredServiceProvider;

import net.milkbowl.vault.chat.Chat;
import net.milkbowl.vault.economy.Economy;
import net.milkbowl.vault.permission.Permission;

public class VaultAPI {
	
	private static VaultAPI instance;
	public static final VaultAPI getVaultAPI() {
		if(instance == null) instance = new VaultAPI();
		return instance;
	}
	
	public static Economy economy = null;
	public static Chat chat = null;
	public static Permission perms = null;
	public boolean setupEconomy() {
        RegisteredServiceProvider<Economy> economyProvider = Bukkit.getServer().getServicesManager().getRegistration(Economy.class);
        if(economyProvider != null) { economy = economyProvider.getProvider(); } return (economy != null);
    }
	public boolean setupChat() {
		RegisteredServiceProvider<Chat> rsp = Bukkit.getServer().getServicesManager().getRegistration(Chat.class);
        chat = rsp != null ? rsp.getProvider() : null;
        return chat != null;
	}
	public boolean setupPermissions() {
        RegisteredServiceProvider<Permission> rsp = Bukkit.getServer().getServicesManager().getRegistration(Permission.class);
        perms = rsp != null ? rsp.getProvider() : null;
        return perms != null;
    }
}
