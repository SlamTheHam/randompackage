package me.randomHashTags.RandomPackage.utils.universal;

import java.io.File;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.SimpleDateFormat;
import java.util.*;

import me.randomHashTags.RandomPackage.api.CustomEnchants;
import me.randomHashTags.RandomPackage.utils.GivedpItem;
import org.bukkit.*;
import org.bukkit.FireworkEffect.Type;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.*;
import org.bukkit.entity.Skeleton.SkeletonType;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.inventory.meta.FireworkMeta;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.PotionMeta;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.PluginManager;
import org.bukkit.potion.Potion;
import org.bukkit.potion.PotionData;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.potion.PotionType;

import me.randomHashTags.RandomPackage.RandomPackage;
import me.randomHashTags.RandomPackage.RandomPackageAPI;
import me.randomHashTags.RandomPackage.utils.classes.customenchants.CustomEnchant;
import org.bukkit.scheduler.BukkitScheduler;


@SuppressWarnings("deprecation")
public class UVersion {
	public final RandomPackage randompackage = RandomPackage.getPlugin;
	public final String v = Bukkit.getVersion();
	public final boolean isLegacy = v.contains("1.8");
	public ItemStack item = new ItemStack(Material.APPLE, 1);
	public ItemMeta itemMeta = item.getItemMeta();
	public ArrayList<String> lore = new ArrayList<>();
	
	public final Random random = new Random();
	public final BukkitScheduler scheduler = Bukkit.getScheduler();
	private static UVersion instance;
	public static final UVersion getInstance() {
	    if(instance == null) instance = new UVersion();
	    return instance;
	}
	public final PluginManager pluginmanager = Bukkit.getPluginManager();

    public void save(String folder, String file) {
        File f = null;
        final File d = randompackage.getDataFolder();
        if(folder != null && !folder.equals(""))
            f = new File(d + File.separator + folder + File.separator, file);
        else
            f = new File(d + File.separator, file);
        if(!f.exists()) {
            f.getParentFile().mkdirs();
            randompackage.saveResource(folder != null && !folder.equals("") ? folder + File.separator + file : file, false);
        }
    }
	
	public ItemStack getItemInHand(LivingEntity entity) {
        if(entity == null) return null;
		if(v.contains("1.8")) return entity.getEquipment().getItemInHand();
		else                        return entity.getEquipment().getItemInMainHand();
	}
	public ItemStack getItemInHand(LivingEntity entity, ItemStack lookforitem) {
		if(v.contains("1.8"))                                             return entity.getEquipment().getItemInHand();
		else if(entity.getEquipment().getItemInMainHand().getItemMeta().equals(lookforitem.getItemMeta())
				&& entity.getEquipment().getItemInMainHand().getData().getData() == lookforitem.getData().getData())  return entity.getEquipment().getItemInMainHand();
		else if(entity.getEquipment().getItemInOffHand().getItemMeta().equals(lookforitem.getItemMeta())
				&& entity.getEquipment().getItemInOffHand().getData().getData() == lookforitem.getData().getData())   return entity.getEquipment().getItemInOffHand();
		else return null;
	}
	public void removeItem(Player player, ItemStack itemstack, int amount) {
		int nextslot = getNextSlot(player, itemstack);
		for(int i = 1; i <= amount; i++) {
			if(nextslot >= 0) {
				final ItemStack is = player.getInventory().getItem(nextslot);
				if(is.getAmount() == 1) {
					player.getInventory().setItem(nextslot, new ItemStack(Material.AIR));
					nextslot = getNextSlot(player, itemstack);
				} else {
					is.setAmount(is.getAmount() - 1);
				}
			}
		}
		player.updateInventory();
	}
	private int getNextSlot(Player player, ItemStack itemstack) {
    	final PlayerInventory inv = player.getInventory();
		for(int i = 0; i < inv.getSize(); i++) {
			item = inv.getItem(i);
			if(item != null && item.getData().getData() == itemstack.getData().getData() && item.getType().equals(itemstack.getType())
					&& item.getItemMeta().equals(itemstack.getItemMeta())) {
				return i;
			}
		}
		return -1;
	}
	public int getTotalAmount(Inventory inventory, Material material, byte data) {
		int amount = 0;
		for(ItemStack is : inventory.getContents()) if(is != null && is.getType().equals(material) && is.getData().getData() == data) amount += is.getAmount();
		return amount;
	}
	public int getTotalExperience(Player player) {
		final double levelxp = LevelToExp(player.getLevel()), nextlevelxp = LevelToExp(player.getLevel() + 1), difference = nextlevelxp - levelxp;
		final double p = (levelxp + (difference * player.getExp()));
		return (int) Math.round(p);
	}
	public void setTotalExperience(Player player, int total) {
		player.setTotalExperience(0);
		player.setExp(0f);
		player.setLevel(0);
		player.giveExp(total);
	}
	private double LevelToExp(int level) {
		return level <= 16 ? (level * level) + (level * 6) : level <= 31 ? (2.5 * level * level) - (40.5 * level) + 360 : (4.5 * level * level) - (162.5 * level) + 2220;
	}
	public String toReadableDate(Date d, String format) {
		return new SimpleDateFormat(format).format(d);
	}
	public PotionEffect getPotionEffect(LivingEntity entity, PotionEffectType type) {
		if(v.contains("1.8")) {
			for(PotionEffect e : entity.getActivePotionEffects())
				if(e.getType().equals(type))
					return e;
			return null;
		} else {
			return entity.getPotionEffect(type);
		}
	}
	public List<Location> getChunkLocations(Chunk chunk) {
		final List<Location> l = new ArrayList<>();
		final int x = chunk.getX()*16, z = chunk.getZ()*16;
		final World world = chunk.getWorld();
		for(int xx = x; xx < x+16; xx++)
		    for(int zz = z; zz < z+16; zz++)
		    	l.add(new Location(world, xx, 0, zz));
		return l;
	}
	public PotionEffectType getPotionEffectType(String input) {
		if(input != null && input != "") {
			input = input.toUpperCase();
			if(input.contains("STRENGTH")) return PotionEffectType.INCREASE_DAMAGE;
			else if(input.contains("MINING_FATIGUE")) return PotionEffectType.SLOW_DIGGING;
			else if(input.contains("SLOWNESS")) return PotionEffectType.SLOW;
			else if(input.contains("HASTE")) return PotionEffectType.FAST_DIGGING;
			else if(input.contains("JUMP")) return PotionEffectType.JUMP;
			else if(input.contains("INSTANT_H")) return PotionEffectType.HEAL;
			else if(input.contains("INSTANT_D")) return PotionEffectType.HARM;
			else {
				for(PotionEffectType p : PotionEffectType.values()) {
					if(p != null && input.equals(p.getName())) {
						return p;
					}
				}
				return null;
			}
		} else return null;
	}
	public ItemStack addVanillaEnchants(ItemStack is) {
		if(is != null && is.hasItemMeta() && is.getItemMeta().hasLore()) {
		    final CustomEnchants customenchants = CustomEnchants.getCustomEnchants();
			final HashMap<Enchantment, Integer> enchants = new HashMap<>();
			itemMeta = is.getItemMeta(); lore.clear();
			for(String string : itemMeta.getLore()) {
				if(string.toLowerCase().startsWith("venchants{")) {
					for(String s : string.split("\\{")[1].split("}")[0].split(";")) {
						enchants.put(getEnchantment(s), getRemainingInt(s));
					}
				} else if(string.toLowerCase().startsWith("rpenchants{")) {
					for(String s : string.split("\\{")[1].split("}")[0].split(";")) {
						CustomEnchant e = CustomEnchant.valueOf(s);
						if(e != null) {
							int l = getRemainingInt(s);
							l = l != -1 ? l : random.nextInt(e.getMaxLevel());
							if(l != 0 || l == 0 && !customenchants.levelZeroRemoval)
								lore.add(e.getRarity().getApplyColors() + e.getName() + " " + RandomPackageAPI.getAPI().toRoman(l != 0 ? l : 1));
						}
					}
				} else if(string.toLowerCase().startsWith("e:")) {
					enchants.put(getEnchantment(string.split(":")[1]), getRemainingInt(string));
				} else
					lore.add(ChatColor.translateAlternateColorCodes('&', string));
			}
			itemMeta.setLore(lore); lore.clear();
			is.setItemMeta(itemMeta);
			for(Enchantment e : enchants.keySet()) is.addUnsafeEnchantment(e, enchants.get(e));
		}
		return is;
	}
	
	public Color getColor(String path) {
		if(path == null) return null;
		else if(path.equalsIgnoreCase("aqua")) return Color.AQUA;
		else if(path.equalsIgnoreCase("black")) return Color.BLACK;
		else if(path.equalsIgnoreCase("blue")) return Color.BLUE;
		else if(path.equalsIgnoreCase("fuchsia")) return Color.FUCHSIA;
		else if(path.equalsIgnoreCase("gray")) return Color.GRAY;
		else if(path.equalsIgnoreCase("green")) return Color.GREEN;
		else if(path.equalsIgnoreCase("lime")) return Color.LIME;
		else if(path.equalsIgnoreCase("maroon")) return Color.MAROON;
		else if(path.equalsIgnoreCase("navy")) return Color.NAVY;
		else if(path.equalsIgnoreCase("olive")) return Color.OLIVE;
		else if(path.equalsIgnoreCase("orange")) return Color.ORANGE;
		else if(path.equalsIgnoreCase("purple")) return Color.PURPLE;
		else if(path.equalsIgnoreCase("red")) return Color.RED;
		else if(path.equalsIgnoreCase("silver")) return Color.SILVER;
		else if(path.equalsIgnoreCase("teal")) return Color.TEAL;
		else if(path.equalsIgnoreCase("white")) return Color.WHITE;
		else if(path.equalsIgnoreCase("yellow")) return Color.YELLOW;
		else return null;
	}
	public void spawnFirework(Firework firework, Location loc) {
		if(firework != null) {
			Firework fw = loc.getWorld().spawn(new Location(loc.getWorld(), loc.getX()+0.5, loc.getY(), loc.getZ()+0.5), Firework.class);
			fw.setFireworkMeta(firework.getFireworkMeta());
		}
	}
	public Firework spawnFirework(Type explosionType, Color trailColor, Color explodeColor, int power) {
		Firework fw = Bukkit.getWorlds().get(0).spawn(Bukkit.getWorlds().get(0).getSpawnLocation(), Firework.class);
		FireworkMeta fwm = fw.getFireworkMeta();
		fwm.setPower(power);
		fwm.addEffect(FireworkEffect.builder().trail(true).flicker(true).with(explosionType).withColor(trailColor).withFade(explodeColor).withFlicker().withTrail().build());
		fw.setFireworkMeta(fwm);
		return fw;
	}
	public Entity getEntity(UUID uuid) {
		if(uuid != null) {
			if(v.contains("1.8") || v.contains("1.9")) {
				for(World w : Bukkit.getWorlds())
					for(LivingEntity le : w.getLivingEntities())
						if(uuid.equals(le.getUniqueId())) return le;
			} else {
				return Bukkit.getEntity(uuid);
			}
		}
		return null;
	}
	public int getRemainingInt(String string) {
		string = ChatColor.stripColor(ChatColor.translateAlternateColorCodes('&', string)).replaceAll("\\p{L}", "").replaceAll("\\s", "").replaceAll("\\p{P}", "").replaceAll("\\p{S}", "");
		return string == null || string.equals("") ? -1 : Integer.parseInt(string);
	}
	public Double getRemainingDouble(String string) {
		string = ChatColor.stripColor(ChatColor.translateAlternateColorCodes('&', string).replaceAll("\\p{L}", "").replaceAll("\\p{Z}", "").replaceAll("\\.", "d").replaceAll("\\p{P}", "").replaceAll("\\p{S}", "").replace("d", "."));
		return string == null || string.equals("") ? -1.00 : Double.parseDouble(string.contains(".") && string.split("\\.").length > 1 && string.split("\\.")[1].length() > 2 ? string.substring(0, string.split("\\.")[0].length() + 3) : string);
	}
	/*
	 * 
	 * Credit to Sahil Mathoo from StackOverFlow at
	 * https://stackoverflow.com/questions/8154366/how-to-center-a-string-using-string-format 
	 * 
	 */
	public String center(String s, int size) {
        return center(s, size, ' ');
	}
    private String center(String s, int size, char pad) {
        if(s == null || size <= s.length())
            return s;
        StringBuilder sb = new StringBuilder(size);
        for(int i = 0; i < (size - s.length()) / 2; i++)  sb.append(pad);
        sb.append(s);
        while(sb.length() < size) sb.append(pad);
        return sb.toString();
    }
	public String formatDouble(double d) {
		String decimals = Double.toString(d).split("\\.")[1];
		if(decimals.equals("0")) { decimals = ""; } else { decimals = "." + decimals; }
		return formatInt((int) d) + decimals;
	}
	public String formatInt(int integer) { return String.format("%,d", integer); }
	public Integer parseInt(String input) {
		try {
			return Integer.parseInt(input);
		} catch (NumberFormatException e) {
			return null;
		}
	}
	public Double parseDouble(String input) {
		try {
			return Double.parseDouble(input);
		} catch (NumberFormatException e) {
			return null;
		}
	}
	public void giveItem(Player player, ItemStack is) {
		if(is == null || is.getType().equals(Material.AIR)) return;
		final int f = player.getInventory().first(is.getType());
		if(player.getInventory().firstEmpty() < 0 && f != -1
				|| player.getInventory().firstEmpty() < 0 && f != -1 && !is.hasItemMeta() && !(player.getInventory().getItem(player.getInventory().first(new ItemStack(is.getType(), player.getInventory().getItem(f).getAmount(), is.getData().getData()))).getAmount() <= is.getMaxStackSize())) {
			player.getWorld().dropItem(player.getLocation(), is);
		} else if(player.getInventory().firstEmpty() == -1) {
			player.getWorld().dropItem(player.getLocation(), is);
		} else {
			player.getInventory().addItem(is);
			player.updateInventory();
		}
	}
	public void giveSpawner(Player player, String directory, FileConfiguration config, int amount) {
		String pi = config != null ? config.getString(directory).toLowerCase() : directory.toLowerCase(), type = null;
		final ItemStack spawner = getSpawner(pi);
		if(spawner != null)
			giveItem(player, spawner);
		else if(player != null)
			player.sendMessage("[RandomPackage] The plugin EpicSpawners is required to use this feature!");
	}
    public Object getEpicSpawnerData(String entitytype) {
        Object data = null;
        final Plugin spawner = RandomPackage.spawner;
        if(spawner != null && spawner.getName().equals("EpicSpawners")) {
            for(com.songoda.epicspawners.api.spawner.SpawnerData spawnerData : com.songoda.epicspawners.EpicSpawnersPlugin.getInstance().getSpawnerManager().getAllSpawnerData()) {
                final String input = entitytype.toUpperCase().replace("_", "").replace(" ", ""), compare = spawnerData.getIdentifyingName().toUpperCase().replace("_", "").replace(" ", "");
                if(input.equals(compare))
                    data = spawnerData;
            }
        }
        return data;
    }
    public ItemStack getSpawner(String input) {
		String pi = input.toLowerCase(), type = null;
		if(pi.equals("mysterymobspawner")) {
			return GivedpItem.getGivedpItem().valueOf("mysterymobspawner").clone();
		}
		for(EntityType entitytype : EntityType.values()) {
			if(pi.equalsIgnoreCase(entitytype.name().replace("_", "") + "spawner")
					|| pi.endsWith(entitytype.name().toLowerCase().replace("_", "-")))
				type = entitytype.name().toLowerCase().replace("_", "");
		}
		if(type == null) {
			if(pi.contains("pig") && pi.contains("zombie")) type = "pigzombie";
		}
		if(type == null) return null;

		if(pluginmanager.isPluginEnabled("EpicSpawners")) {
			com.songoda.epicspawners.api.spawner.SpawnerData data = (com.songoda.epicspawners.api.spawner.SpawnerData) getEpicSpawnerData(type);
			return data.toItemStack();
		} else {
			Bukkit.getConsoleSender().sendMessage("[RandomPackage] The plugin EpicSpawners is required to use this feature!");
		}
		return null;
	}
	public int indexOf(Set<? extends Object> set, Object value) {
		int i = 0;
		for(Object o : set) {
			if(value.equals(o)) return i;
			i++;
		}
		return -1;
	}
	public String toMaterial(String input, boolean realitem) {
		if(input.contains(":")) input = input.split(":")[0];
		if(input.contains(" ")) input = input.replace(" ", "");
		if(input.contains("_")) input = input.replace("_", " ");
		String e = "";
		if(input.contains(" ")) {
			for(int i = 0; i < input.split(" ").length; i++) {
				e = e + input.split(" ")[i].substring(0, 1).toUpperCase() + input.split(" ")[i].substring(1).toLowerCase() + (i != input.split(" ").length - 1 ? (realitem ? "_" : " ") : "");
			}
		} else e = input.substring(0, 1).toUpperCase() + input.substring(1).toLowerCase();
		return e;
	}
    public String toString(Location loc) {
        return loc.getWorld().getName() + ";" + loc.getX() + ";" + loc.getY() + ";" + loc.getZ() + ";" + loc.getYaw() + ";" + loc.getPitch();
    }
    public Location toLocation(String string) {
        return new Location(Bukkit.getWorld(string.split(";")[0]), Double.parseDouble(string.split(";")[1]), Double.parseDouble(string.split(";")[2]), Double.parseDouble(string.split(";")[3]), Float.parseFloat(string.split(";")[4]), Float.parseFloat(string.split(";")[5]));
    }

	public String toRPDataString(ItemStack rpDataItemStack) {
		String s = rpDataItemStack.getType().name() + ":" + rpDataItemStack.getData().getData() + ":" + rpDataItemStack.getAmount() + (rpDataItemStack.hasItemMeta() && rpDataItemStack.getItemMeta().hasDisplayName() ? (";name{" + rpDataItemStack.getItemMeta().getDisplayName().replace("�", "&") + "}") : "");
		String l = "";
		if(rpDataItemStack.hasItemMeta()) {
			if(rpDataItemStack.getItemMeta().hasEnchants()) {
				for(Enchantment e : rpDataItemStack.getEnchantments().keySet())
					l = l + e.getName().toLowerCase() + rpDataItemStack.getEnchantments().get(e) + "\\n";
			}
			if(rpDataItemStack.getItemMeta().hasLore()) {
				for(int i = 0; i < rpDataItemStack.getItemMeta().getLore().size(); i++)
					l = l + rpDataItemStack.getItemMeta().getLore().get(i).replace("�", "&") + (i == rpDataItemStack.getItemMeta().getLore().size() - 1 ? "" : "\\n");
			}
			s = s + ";lore{" + l + "}";
		}
		return s;
	}
	public ItemStack toRPDataItemStack(String s) {
		ItemStack i = new ItemStack(Material.getMaterial(s.split(":")[0].toUpperCase()), s.split(":")[2].contains(";") ? Integer.parseInt(s.split(":")[2].split(";")[0]) : Integer.parseInt(s.split(":")[2]), Byte.parseByte(s.split(":")[1]));
		if(!s.contains("name{") && !s.contains("lore{")) return i;
		ItemMeta ii = i.getItemMeta();
		ArrayList<String> lore = new ArrayList<>();
		HashMap<Enchantment, Integer> enchants = new HashMap<>();
		if(s.contains("name{")) ii.setDisplayName(ChatColor.translateAlternateColorCodes('&', s.split("name\\{")[1].split("}")[0]));
		if(s.contains("lore{")) {
			String l = s.split("lore\\{")[1].split("}")[0];
			if(l.contains("\\n")) {
				for(String ss : l.split("\\\\n")) {
					Enchantment e = getEnchantment(ss);
					if(e != null) {
						enchants.put(e, getRemainingInt(ss));
					} else {
						lore.add(ChatColor.translateAlternateColorCodes('&', ss));
					}
				}
			} else {
				Enchantment e = getEnchantment(l);
				if(e != null) {
					enchants.put(e, getRemainingInt(l));
				} else {
					lore.add(ChatColor.translateAlternateColorCodes('&', l));
				}
			}
			ii.setLore(lore);
		}
		i.setItemMeta(ii);
		for(Enchantment e : enchants.keySet()) if(e != null) i.addUnsafeEnchantment(e, enchants.get(e));
		return i;
	}
	// From http://www.baeldung.com/java-round-decimal-number
	public double round(double input, int decimals) {
		if(decimals < 0) throw new IllegalArgumentException();
	    BigDecimal bd = new BigDecimal(Double.toString(input));
	    bd = bd.setScale(decimals, RoundingMode.HALF_UP);
	    return bd.doubleValue();
	}
	public String roundDoubleString(double input, int decimals) {
		final double d = round(input, decimals);
		return Double.toString(d);
	}

	public void playParticle(FileConfiguration config, String path, Location location, int count) {
		if(config != null && config.get(path) != null) {
			final String target = config.getString(path);
			final UParticle up = UParticle.matchParticle(target.toUpperCase());
			if(up != null) up.play(location, count);
		}
	}
	public void playSound(FileConfiguration config, String path, Player player, Location location, boolean globalsound) {
		return;
		/*
		if(config.get(path) != null) {
			String s = config.getString(path).split(":")[0].toUpperCase();
			int v = Integer.parseInt(config.getString(path).split(":")[1]), p = Integer.parseInt(config.getString(path).split(":")[2]);
			if(player != null) {
				if(!globalsound) player.playSound(location, Sound.getHome(s), v, p);
				else             player.getWorld().playSound(location, Sound.getHome(s), v, p);
			} else {
				location.getWorld().playSound(location, Sound.getHome(s), v, p);
			}
		}*/
	}
	public void playSound(String sound, int pitch, int volume, Object loc, int playTimes, boolean globalsound) {
		return;
		/*Sound s = Sound.getHome(sound.toUpperCase());
		if(s == null) {
			Bukkit.broadcastMessage("[RandomPackage] Invalid sound name! \"" + sound.toUpperCase() + "\"");
			return;
		}
		for(int i = 1; i <= playTimes; i++) {
			if(globalsound) {
				if(loc instanceof Location) ((Location) loc).getWorld().playSound((Location) loc, s, volume, pitch);
				else if(loc instanceof Entity) ((Entity) loc).getWorld().playSound(((Player) loc).getLocation(), s, volume, pitch);
			} else if(loc instanceof Player)
				((Player) loc).playSound(((Player) loc).getLocation(), s, pitch, volume);
		}*/
	}
	
	public ItemStack convertStringToPotion(String string, boolean hidePotionEffects) {
		String pathh = string.toLowerCase();
		if(pathh.startsWith("potion:")) {
			pathh = pathh.replace("potion:", "");
			if(pathh.startsWith("0"))       return new ItemStack(Material.POTION, 1);
			PotionType type = null;
			boolean splash = pathh.startsWith("splash"), extended = pathh.endsWith("extended"), upgraded = false; int level = 0;
			pathh = pathh.replace("splash", "").replace("extended", "");
			if(v.contains("1.8")) {
				level = getRemainingInt(pathh);
				if(level != 1 && level != 2) level = 1;
			}
			if(getRemainingInt(pathh) >= 2) upgraded = true;
			pathh = pathh.replaceAll("\\p{Nd}", "");
			if(pathh.equals("fireresistance"))                  type = PotionType.FIRE_RESISTANCE;
			else if(pathh.equals("instantdamage"))              type = PotionType.INSTANT_DAMAGE;
			else if(pathh.equals("instanthealth"))              type = PotionType.INSTANT_HEAL;
			else if(pathh.equals("invisibility"))               type = PotionType.INVISIBILITY;
			else if(pathh.equals("leaping"))                    type = PotionType.JUMP;
			else if(pathh.equals("nightvision"))                type = PotionType.NIGHT_VISION;
			else if(pathh.equals("poison"))                     type = PotionType.POISON;
			else if(pathh.equals("regeneration"))               type = PotionType.REGEN;
			else if(pathh.equals("slowness"))                   type = PotionType.SLOWNESS;
			else if(pathh.equals("swiftness"))                  type = PotionType.SPEED;
			else if(pathh.equals("strength"))                   type = PotionType.STRENGTH;
			else if(pathh.equals("water") || pathh.equals("0")) type = PotionType.WATER;
			else if(pathh.equals("waterbreathing"))             type = PotionType.WATER_BREATHING;
			else if(pathh.equals("weakness"))                   type = PotionType.WEAKNESS;
			else {
				Bukkit.broadcastMessage(ChatColor.YELLOW + "" + ChatColor.BOLD + "[RandomPackage]" + ChatColor.RESET + " Invalid potion name: " + pathh);
				return new ItemStack(Material.AIR);
			}
			item = new ItemStack(!v.contains("1.8") && splash ? Material.valueOf("SPLASH_POTION") : Material.POTION, 1);
			if(v.contains("1.8")) {
				Potion potion = !type.equals(PotionType.WATER) ? new Potion(type, level, splash, extended) : new Potion(type);
				item = potion.toItemStack(1);
			}
			PotionMeta meta = (PotionMeta) item.getItemMeta();
			if(!v.contains("1.8")) meta.setBasePotionData(new PotionData(type, extended, upgraded));
			if(hidePotionEffects) meta.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
			item.setItemMeta(meta);
		} else {
			return null;
		}
		return item;
	}
	
	public Enchantment getEnchantment(String string) {
		if(string != null) {
			for(Enchantment enchant : Enchantment.values())
				if(enchant != null && enchant.getName() != null && string.toLowerCase().replace("_", "").startsWith(enchant.getName().toLowerCase().replace("_", ""))) return enchant;
			string = string.toLowerCase().replace("_", "");
			if(string.startsWith("po")) { return Enchantment.ARROW_DAMAGE; // Power
			} else if(string.startsWith("fl")) { return Enchantment.ARROW_FIRE; // Flame
			} else if(string.startsWith("i")) { return Enchantment.ARROW_INFINITE; // Infinity
			} else if(string.startsWith("pu")) { return Enchantment.ARROW_KNOCKBACK; // Punch
			} else if(string.startsWith("bi") && !(v.contains("1.8")) && !(v.contains("1.9")) && !(v.contains("1.10"))) { return Enchantment.getByName("BINDING_CURSE"); // Blinding Curse
			} else if(string.startsWith("sh")) { return Enchantment.DAMAGE_ALL; // Sharpness
			} else if(string.startsWith("ba")) { return Enchantment.DAMAGE_ARTHROPODS; // Bane of Arthropods
			} else if(string.startsWith("sm")) { return Enchantment.DAMAGE_UNDEAD; // Smite
			} else if(string.startsWith("de")) { return Enchantment.DEPTH_STRIDER; // Depth Strider
			} else if(string.startsWith("e")) { return Enchantment.DIG_SPEED; // Efficiency
			} else if(string.startsWith("u")) { return Enchantment.DURABILITY; // Unbreaking
			} else if(string.startsWith("firea")) { return Enchantment.FIRE_ASPECT; // Fire Aspect
			} else if(string.startsWith("fr") && !(v.contains("1.8"))) { return Enchantment.getByName("FROST_WALKER"); // Frost Walker
			} else if(string.startsWith("k")) { return Enchantment.KNOCKBACK; // Knockback
			} else if(string.startsWith("fo")) { return Enchantment.LOOT_BONUS_BLOCKS; // Fortune
			} else if(string.startsWith("lo")) { return Enchantment.LOOT_BONUS_MOBS; // Looting
			} else if(string.startsWith("luc")) { return Enchantment.LUCK; // Luck
			} else if(string.startsWith("lur")) { return Enchantment.LURE; // Lure
			} else if(string.startsWith("m") && !(v.contains("1.8"))) { return Enchantment.getByName("MENDING"); // Mending
			} else if(string.startsWith("r")) { return Enchantment.OXYGEN; // Respiration
			} else if(string.startsWith("prot")) { return Enchantment.PROTECTION_ENVIRONMENTAL; // Protection
			} else if(string.startsWith("bl") || string.startsWith("bp")) { return Enchantment.PROTECTION_EXPLOSIONS; // Blast Protection
			} else if(string.startsWith("ff") || string.startsWith("fe")) { return Enchantment.PROTECTION_FALL; // Feather Falling
			} else if(string.startsWith("fp") || string.startsWith("firep")) { return Enchantment.PROTECTION_FIRE; // Fire Protection
			} else if(string.startsWith("pp") || string.startsWith("proj")) { return Enchantment.PROTECTION_PROJECTILE; // Projectile Protection
			} else if(string.startsWith("si")) { return Enchantment.SILK_TOUCH; // Silk Touch
			} else if(string.startsWith("th")) { return Enchantment.THORNS; // Thorns
			} else if(string.startsWith("v") && !(v.contains("1.8")) && !(v.contains("1.9")) && !(v.contains("1.10"))) { return Enchantment.getByName("VANISHING_CURSE"); // Vanishing Curse
			} else if(string.startsWith("aa") || string.startsWith("aq")) { return Enchantment.WATER_WORKER; // Aqua Affinity
			} else { return null; }
		}
		return null;
	}
	/*
	 * 
	 */
	public LivingEntity getHitEntity(ProjectileHitEvent event) {
		final List<Entity> n = event.getEntity().getNearbyEntities(0.0, 0.0, 0.0);
        return n.size() > 0 && n.get(0) instanceof LivingEntity ? (LivingEntity) n.get(0) : null;
    }
	public LivingEntity getEntity(String type, Location l, boolean spawn) {
        final boolean baby = type.contains(":") && type.toLowerCase().endsWith(":true");
		type = type.toUpperCase().split(":")[0];
		LivingEntity mob = null;
		final World w = l.getWorld();
		if(type.equals("BAT"))              mob = w.spawn(l, Bat.class);
		else if(type.equals("BLAZE"))       mob = w.spawn(l, Blaze.class);
		else if(type.equals("CAVE_SPIDER")) mob = w.spawn(l, CaveSpider.class);
		else if(type.equals("CHICKEN"))     mob = w.spawn(l, Chicken.class);
		else if(type.equals("COW"))         mob = w.spawn(l, Cow.class);
		else if(type.equals("CREEPER"))     mob = w.spawn(l, Creeper.class);
		else if(type.equals("ENDER_DRAGON"))mob = w.spawn(l, EnderDragon.class);
		else if(type.equals("ENDERMAN"))    mob = w.spawn(l, Enderman.class);
		else if(type.equals("ENDERMITE"))   mob = w.spawn(l, Endermite.class);
		else if(type.equals("GHAST"))       mob = w.spawn(l, Ghast.class);
		else if(type.equals("GIANT"))       mob = w.spawn(l, Giant.class);
		else if(type.equals("GUARDIAN"))    mob = w.spawn(l, Guardian.class);
		else if(type.equals("HORSE"))       mob = w.spawn(l, Horse.class);
		else if(type.equals("IRON_GOLEM"))  mob = w.spawn(l, IronGolem.class);
		else if(type.equals("LLAMA") && !v.contains("1.8") && !v.contains("1.9") && !v.contains("1.10"))
											mob = w.spawn(l, org.bukkit.entity.Llama.class);
		else if(type.equals("MAGMA_CUBE"))  mob = w.spawn(l, MagmaCube.class);
		else if(type.equals("MUSHROOM_COW"))mob = w.spawn(l, MushroomCow.class);
		else if(type.equals("OCELOT"))      mob = w.spawn(l, Ocelot.class);
		else if(type.equals("PARROT") && !v.contains("1.8") && !v.contains("1.9") && !v.contains("1.10") && !v.contains("1.11"))
											mob = w.spawn(l, org.bukkit.entity.Parrot.class);
		else if(type.equals("PIG"))         mob = w.spawn(l, Pig.class);
		else if(type.equals("PIG_ZOMBIE")) {
			mob = w.spawn(l, PigZombie.class);
			((PigZombie) mob).setBaby(baby);
		} else if(type.equals("RABBIT"))    mob = w.spawn(l, Rabbit.class);
		else if(type.equals("SHEEP"))       mob = w.spawn(l, Sheep.class);
		else if(type.equals("SHULKER") && !v.contains("1.8"))
											mob = w.spawn(l, org.bukkit.entity.Shulker.class);
		else if(type.equals("SILVERFISH"))  mob = w.spawn(l, Silverfish.class);
		else if(type.equals("SKELETON"))    mob = w.spawn(l, Skeleton.class);
		else if(type.equals("SLIME"))       mob = w.spawn(l, Slime.class);
		else if(type.equals("SNOWMAN"))     mob = w.spawn(l, Snowman.class);
		else if(type.equals("SQUID"))       mob = w.spawn(l, Squid.class);
		else if(type.equals("SPIDER"))      mob = w.spawn(l, Spider.class);
		else if(type.equals("STRAY") && !v.contains("1.8") && !v.contains("1.9"))
											mob = w.spawn(l, org.bukkit.entity.Stray.class);
		else if(type.equals("VEX") && !v.contains("1.8") && !v.contains("1.9") && !v.contains("1.10"))
											mob = w.spawn(l, org.bukkit.entity.Vex.class);
		else if(type.equals("VILLAGER"))    mob = w.spawn(l, Villager.class);
		else if(type.equals("VINDICATOR") && !v.contains("1.8") && !v.contains("1.9") && !v.contains("1.10"))
											mob = w.spawn(l, org.bukkit.entity.Vindicator.class);
		else if(type.equals("WITCH"))       mob = w.spawn(l, Witch.class);
		else if(type.equals("WITHER"))      mob = w.spawn(l, Wither.class);
		else if(type.equals("WITHER_SKELETON")) {
			if(v.contains("1.8") || v.contains("1.9") || v.contains("1.10")) {
				mob = w.spawn(l, Skeleton.class);
				((Skeleton) mob).setSkeletonType(SkeletonType.WITHER);
			} else {
				mob = w.spawn(l, org.bukkit.entity.WitherSkeleton.class);
			}
		} else if(type.equals("ZOMBIE"))    mob = w.spawn(l, Zombie.class);
		else if(type.equals("ZOMBIE_HORSE") && !v.contains("1.8"))
											mob = w.spawn(l, org.bukkit.entity.ZombieHorse.class);
		else if(type.equals("ZOMBIE_VILLAGER") && !v.contains("1.8"))
											mob = w.spawn(l, org.bukkit.entity.ZombieVillager.class);
		else return null;
		if(!spawn) {
		    mob.remove();
        } else if(mob instanceof Ageable) {
			final Ageable a = (Ageable) mob;
			a.setBaby();
			a.setAgeLock(true);
		}
		return mob;
	}
	// 1.8 sounds -  http://docs.codelanx.com/Bukkit/1.8/org/bukkit/Sound.html
	// 1.9+ sounds - https://hub.spigotmc.org/javadocs/spigot/org/bukkit/Sound.html
	/*
	 * BLAZE_SHOOT
	 */
	public Sound getSound(String string) {
		final boolean v = Bukkit.getVersion().contains("1.8");
		string = string.toUpperCase();
		String s = "";
		if(string.equals("AMBIENT_CAVE"))         return Sound.AMBIENT_CAVE;
		else if(string.contains("ANVIL_BREAK"))   return Sound.valueOf(v ? "ANVIL_BREAK" : "BLOCK_ANVIL_BREAK");
		else if(string.contains("ANVIL_DESTROY")) return Sound.valueOf(v ? "ANVIL_BREAK" : "BLOCK_ANVIL_DESTROY");
		else if(string.contains("ANVIL_USE"))     return Sound.valueOf(v ? "ANVIL_USE" : "BLOCK_ANVIL_USE");
		
		else if(string.contains("BAT_DEATH"))   s = "BAT_DEATH";
		else if(string.contains("BAT_HURT"))    s = "BAT_HURT";
		else if(string.contains("BAT_IDLE") || string.contains("BAT_AMBIENT")) return Sound.valueOf(v ? "BAT_IDLE" : "BAT_AMBIENT");
		else if(string.contains("BAT_LOOP"))    s = "BAT_LOOP";
		else if(string.contains("BAT_TAKEOFF")) s = "BAT_TAKEOFF";
		
		else if(string.contains("BLAZE_DEATH")) s = "BLAZE_DEATH";
		
		else if(string.contains("CHEST_CLOSE"))  return Sound.valueOf(v ? "CHEST_CLOSE" : "CHEST_OPEN");
		else if(string.contains("CHEST_OPEN"))   return Sound.valueOf(v ? "CHEST_OPEN" : "BLOCK_CHEST_OPEN");
		
		else if(string.contains("CAT_HISS"))     s = "CAT_HISS";
		else if(string.contains("CAT_PURR"))     s = "CAT_PURR";
		else if(string.contains("CAT_PURREOW"))  s = "CAT_PURREOW";
		
		else if(string.contains("COW_HURT"))     return Sound.valueOf(v ? "COW_HURT" : "ENTITY_COW_HURT");
		
		else if(string.contains("CHICKEN_EGG"))  return Sound.valueOf(v ? "CHICKEN_EGG_POP" : "ENTITY_CHICKEN_EGG");
		else if(string.contains("CHICKEN_HURT")) s = "CHICKEN_HURT";
		
		else if(string.contains("CREEPER_DEATH")) s = "CREEPER_DEATH";
		
		else if(string.contains("ENDERDRAGON_DEATH"))return Sound.valueOf(v ? "ENDERDRAGON_DEATH" : "ENTITY_ENDERDRAGON_DEATH");
		else if(string.contains("ENDERDRAGON_GROWL"))return Sound.valueOf(v ? "ENDERDRAGON_GROWL" : "ENTITY_ENDERDRAGON_GROWL");
		else if(string.contains("ENDERDRAGON_H"))    return Sound.valueOf(v ? "ENDERDRAGON_HIT" : "ENTITY_ENDERDRAGON_HURT");
		else if(string.contains("ENDERDRAGON"))      return Sound.valueOf(v ? "ENDERDRAGON_WINGS" : "ENTITY_ENDERDRAGON_FLAP");
		
		else if(string.startsWith("ENDERM") && string.endsWith("N_DEATH"))   return Sound.valueOf(v ? "ENDERMAN_DEATH" : "ENTITY_ENDERMEN_DEATH");
		else if(string.startsWith("ENDERM") && string.endsWith("N_SCREAM"))  return Sound.valueOf(v ? "ENDERMAN_SCREAM" : "ENTITY_ENDERMEN_SCREAM");
		else if(string.startsWith("ENDERM") && string.endsWith("N_STARE"))   return Sound.valueOf(v ? "ENDERMAN_STARE" : "ENTITY_ENDERMEN_STARE");
		else if(string.startsWith("ENDERM") && string.endsWith("N_TELEPORT"))return Sound.valueOf(v ? "ENDERMAN_TELEPORT" : "ENTITY_ENDERMEN_TELEPORT");
		
		else if(string.equals("EXPLODE") || string.contains("GENERIC_EXPLODE")) return Sound.valueOf(v ? "EXPLODE" : "ENTITY_GENERIC_EXPLODE");
		
		else if(string.contains("GHAST_DEATH"))                                   return Sound.valueOf(v ? "GHAST_DEATH" : "ENTITY_GHAST_DEATH");
		else if(string.equals("GHAST_FIREBALL") || string.contains("GHAST_SHOOT"))return Sound.valueOf(v ? "GHAST_FIREBALL" : "ENTITY_GHAST_SHOOT");
		
		else if(string.contains("HORSE_ANGRY"))  s = "HORSE_ANGRY";
		else if(string.contains("HORSE_ARMOR"))  s = "HORSE_ARMOR";
		else if(string.contains("HORSE_BREATHE"))s = "HORSE_BREATHE";
		else if(string.contains("HORSE_DEATH"))  s = "HORSE_DEATH";
		else if(string.contains("HORSE_GALLOP")) s = "HORSE_GALLOP";
		else if(string.contains("HORSE_JUMP"))   s = "HORSE_JUMP";
		else if(string.contains("HORSE_LAND"))   s = "HORSE_LAND";
		else if(string.contains("HORSE_SADDLE")) s = "HORSE_SADDLE";
		
		else if(string.contains("ITEM_BREAK"))  s = "ITEM_BREAK";
		else if(string.contains("ITEM_PICKUP")) s = "ITEM_PICKUP";
		
		else if(string.contains("LEVEL_UP") || string.contains("LEVELUP")) return Sound.valueOf(v ? "LEVEL_UP" : "ENTITY_PLAYER_LEVELUP");
		
		else if(string.contains("MAGMACUBE_JUMP") || string.contains("MAGMA_CUBE_JUMP")) return Sound.valueOf(v ? "MAGMACUBE_JUMP" : "ENTITY_MAGMA_CUBE_JUMP");
		
		else if(string.contains("SNOW_STEP"))     return Sound.valueOf(v ? "STEP_SNOW" : "BLOCK_SNOW_STEP");
		
		else if(string.contains("VILLAGER_DEATH")) s = "VILLAGER_DEATH";
		else if(string.contains("VILLAGER_NO"))    s = "VILLAGER_NO";
		
		else if(string.contains("WITHER_DEATH")) s = "WITHER_DEATH";
		else if(string.contains("WITHER_SHOOT")) s = "WITHER_SHOOT";
		
		else if(string.contains("WOLF_DEATH")) s = "WOLF_DEATH";
		else if(string.contains("WOLF_GROWL")) s = "WOLF_GROWL";
		else if(string.contains("WOLF_HURT"))  s = "WOLF_HURT";
		else if(string.contains("WOLF_HOWL"))  s = "WOLF_HOWL";
		else if(string.contains("WOLF_PANT"))  s = "WOLF_PANT";
		else if(string.contains("WOLF_SHAKE")) s = "WOLF_SHAKE";
		else if(string.contains("WOLF_WHINE")) s = "WOLF_WHINE";
		
		else {
			Sound S = Sound.valueOf(string);
			if(S != null) return S;
			Bukkit.broadcastMessage("[UVersion] \"" + string + "\" is an invalid Sound!");
			return null;
		}
		return Sound.valueOf((!v ?
				s.startsWith("BAT") || s.startsWith("BLAZE") || s.startsWith("CAT") || s.startsWith("CHICKEN") || s.startsWith("CREEPER") || s.startsWith("HORSE") || s.startsWith("ITEM") || s.startsWith("VILLAGER") || s.startsWith("WITHER") || s.startsWith("WOLF") ? "ENTITY_"
				: ""
			: "") + s);
	}
}
