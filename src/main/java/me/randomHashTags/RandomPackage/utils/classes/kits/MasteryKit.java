package me.randomHashTags.RandomPackage.utils.classes.kits;

import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class MasteryKit {
    private static final List<MasteryKit> kits = new ArrayList<>();
    private final String path;
    private final int slot;
    public int cooldown;
    private final ItemStack display;
    private final HashMap<Object, Integer> requiredKits;
    private final boolean losesKitsUponUnlocking;
    public MasteryKit(String path, int slot, ItemStack display, HashMap<Object, Integer> requiredKits, boolean losesKitsUponUnlocking) {
        this.path = path;
        this.slot = slot;
        this.display = display;
        this.requiredKits = requiredKits;
        this.losesKitsUponUnlocking = losesKitsUponUnlocking;
        kits.add(this);
    }
    public String getPath() { return path; }
    public int getSlot() { return slot; }
    public ItemStack getDisplay() { return display; }
    public HashMap<Object, Integer> getRequiredKits() { return requiredKits; }
    public boolean losesKitsUponUnlocking() { return losesKitsUponUnlocking; }


    public static MasteryKit valueOf(String path) {
        for(MasteryKit m : kits)
            if(m.getPath().equals(path))
                return m;
        return null;
    }
    public static MasteryKit valueOf(int slot) {
        for(MasteryKit m : kits)
            if(m.getSlot() == slot)
                return m;
        return null;
    }
}
