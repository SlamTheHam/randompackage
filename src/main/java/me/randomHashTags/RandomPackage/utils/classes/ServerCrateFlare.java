package me.randomHashTags.RandomPackage.utils.classes;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import me.randomHashTags.RandomPackage.utils.universal.UVersion;

@SuppressWarnings({"deprecation"})
public class ServerCrateFlare extends UVersion {
	public static final List<ServerCrateFlare> flares = new ArrayList<>();
	public static final HashMap<Location, ServerCrateFlare> locs = new HashMap<>();
	private final ItemStack is;
	public final List<String> requestMessage, nearbySpawnMessage;
	public int spawnRadius, spawnInDelay, nearbyRadius;
	public ServerCrate serverCrate = null;
	public ServerCrateFlare(ItemStack is, List<String> requestMessage, int spawnRadius, int spawnInDelay, int nearbyRadius, List<String> nearbySpawnMessage) {
		this.is = is;
		this.requestMessage = requestMessage;
		this.spawnRadius = spawnRadius;
		this.spawnInDelay = spawnInDelay;
		this.nearbyRadius = nearbyRadius;
		this.nearbySpawnMessage = nearbySpawnMessage;
		flares.add(this);
	}
	
	public ItemStack getItemStack() { return is.clone(); }
	public void spawn(Player player, Location requestLocation) {
		final ServerCrateFlare f = this;
		if(player != null) {
			final String X = formatInt(requestLocation.getBlockX()), Y = formatInt(requestLocation.getBlockY()), Z = formatInt(requestLocation.getBlockZ());
			for(String s : requestMessage)
				player.sendMessage(ChatColor.translateAlternateColorCodes('&', s.replace("{X}", X).replace("{Y}", Y).replace("{Z}", Z)));
			for(Entity e : player.getNearbyEntities(nearbyRadius, nearbyRadius, nearbyRadius))
				if(e instanceof Player)
					for(String s : nearbySpawnMessage) e.sendMessage(ChatColor.translateAlternateColorCodes('&', s));
		}
		final int x = (random.nextInt(2) == 0 ? 1 : -1)*random.nextInt(spawnRadius), z = (random.nextInt(2) == 0 ? 1 : -1)*random.nextInt(spawnRadius);
		Location loc = new Location(requestLocation.getWorld(), requestLocation.getBlockX()+x, 0, requestLocation.getBlockZ()+z);
		loc = new Location(loc.getWorld(), loc.getBlockX(), loc.getWorld().getHighestBlockYAt(loc), loc.getZ());
		final Location l = loc;
		scheduler.scheduleSyncDelayedTask(randompackage, () -> {
			locs.put(l, f);
			l.getWorld().getBlockAt(l).setType(Material.CHEST);
		}, 20*spawnInDelay);
	}
	public void destroy(Location clickedLocation, boolean dropServerCrate) {
		clickedLocation.getWorld().getBlockAt(clickedLocation).setType(Material.AIR);
		locs.remove(clickedLocation);
		if(dropServerCrate) clickedLocation.getWorld().dropItemNaturally(clickedLocation, serverCrate.getPhyiscalItem().clone());
	}
	public static ServerCrateFlare valueOf(ServerCrate crate) { return crate.getFlare(); }
	public static ServerCrateFlare valueOf(ItemStack is) {
		for(ServerCrateFlare f : flares) if(f.getItemStack().getItemMeta().equals(is.getItemMeta())) return f;
		return null;
	}
	public static ServerCrateFlare valueOf(Location loc) {
		for(Location l : locs.keySet()) if(l.equals(loc)) return locs.get(l);
		return null;
	}
}
