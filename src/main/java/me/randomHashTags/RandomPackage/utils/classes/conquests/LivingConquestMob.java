package me.randomHashTags.RandomPackage.utils.classes.conquests;

import me.randomHashTags.RandomPackage.RandomPackageAPI;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.inventory.EntityEquipment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.UUID;

public class LivingConquestMob {
    private static final RandomPackageAPI api = RandomPackageAPI.getAPI();
    public static final List<LivingConquestMob> living = new ArrayList<>();
    private final boolean isLegacy = api.isLegacy;

    public final LivingEntity entity;
    public final ConquestMob type;
    public LivingConquestMob(LivingEntity entity, ConquestMob type) {
        this.entity = entity;
        this.type = type;
        entity.setCustomName(type.name);
        final ItemStack air = new ItemStack(Material.AIR);
        final EntityEquipment e = entity.getEquipment();
        e.setHelmet(air);
        e.setChestplate(air);
        e.setLeggings(air);
        e.setBoots(air);

        if(isLegacy) {
            e.setItemInHand(air);
        } else {
            e.setItemInMainHand(air);
            e.setItemInOffHand(air);
        }
        for(String a : type.equipment) {
            final String A = a;
            a = a.toLowerCase();
            if(a.startsWith("helmet=")) {
                e.setHelmet(api.d(null, A.substring(7), 0));
            } else if(a.startsWith("chestplate=")) {
                e.setChestplate(api.d(null, A.substring(11), 0));
            } else if(a.startsWith("leggings=")) {
                e.setLeggings(api.d(null, A.substring(9), 0));
            } else if(a.startsWith("boots=")) {
                e.setBoots(api.d(null, A.substring(6), 0));
            } else if(a.startsWith("weapon=")) {
                e.setItemInHand(api.d(null, A.substring(7), 0));
            }
        }
        for(String a : type.attributes) {
            a = a.toLowerCase();
            if(a.startsWith("health=")) {
                entity.setMaxHealth(Double.parseDouble(a.split("=")[1]));
                entity.setHealth(entity.getMaxHealth());
            } else if(a.startsWith("pe=")) {
                final String[] b = a.split("=")[1].split(":");
                entity.addPotionEffect(new PotionEffect(api.getPotionEffectType(b[0]), Integer.parseInt(b[2]), Integer.parseInt(b[1]), false, false));
            }
        }
        living.add(this);
    }
    public void kill(EntityDeathEvent event) {
        event.setDroppedExp(0);
        event.getDrops().clear();
        final Random r = new Random();
        final Entity e = event.getEntity();
        final Location l = e.getLocation();
        final World w = e.getWorld();
        for(String s : type.drops) {
            final int chance = s.contains(";chance=") ? api.getRemainingInt(s.split(";chance=")[1].split(";")[0]) : 100;
            if(chance == 100 || chance <= r.nextInt(100)) {
                s = s.split(";chance")[0];
                final ItemStack i = api.d(null, s, 0);
                if(i != null) w.dropItem(l, i);
            }
        }
        living.remove(this);
    }

    public static LivingConquestMob valueOf(UUID uuid) {
        if(uuid != null)
            for(LivingConquestMob c : living)
                if(c.entity.getUniqueId().equals(uuid))
                    return c;
        return null;
    }
}
