package me.randomHashTags.RandomPackage.utils.classes.factionadditions;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.inventory.ItemStack;

public class FactionUpgrade {
	public static final List<FactionUpgrade> upgrades = new ArrayList<>();
	private final String path;
	private final FactionUpgradeType type;
	private final int slot, maxtier;
	private final ItemStack gui;
	private final List<String> desc, requirements, perks;
	public FactionUpgrade(String path, int slot, int maxtier, List<String> desc, FactionUpgradeType type, ItemStack gui, List<String> perks, List<String> requirements) {
		this.path = path;
		this.slot = slot;
		this.maxtier = maxtier;
		this.desc = desc;
		this.type = type;
		this.gui = gui;
		this.requirements = requirements;
		this.perks = perks;
		upgrades.add(this);
	}
	public String getPath() { return path; }
	public int getSlot() { return slot; }
	public int getMaxTier() { return maxtier; }
	public List<String> getDesc() { return desc; }
	public FactionUpgradeType getType() { return type; }
	public ItemStack getDisplayItem() { return gui; }
	public List<String> getRequirements() { return requirements; }
	public List<String> getPerks() { return perks; }
	
	public static FactionUpgrade valueOf(String path) {
		for(FactionUpgrade u : upgrades) if(u.getPath().equals(path)) return u;
		return null;
	}
	public static FactionUpgrade valueOf(int slot) {
		for(FactionUpgrade u : upgrades) if(u.getSlot() == slot) return u;
		return null;
	}
}
