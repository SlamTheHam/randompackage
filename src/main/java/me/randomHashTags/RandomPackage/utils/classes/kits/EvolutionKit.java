package me.randomHashTags.RandomPackage.utils.classes.kits;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.inventory.ItemStack;

public class EvolutionKit {
	public static final List<EvolutionKit> kits = new ArrayList<>();
	public final String path, fallenhero;
	public final int slot, maxLevel, upgradeChance, cooldown;
	private final ItemStack gui;
	public List<KitItem> items;
	public EvolutionKit(String path, int slot, int maxLevel, int upgradechance, int cooldown, ItemStack gui, String fallenhero) {
		this.path = path;
		this.slot = slot;
		this.maxLevel = maxLevel;
		this.upgradeChance = upgradechance;
		this.cooldown = cooldown;
		this.gui = gui;
		this.fallenhero = fallenhero;
		kits.add(this);
	}
	public ItemStack getDisplayedItem() { return gui.clone(); }
	public String getFallenHero() { return fallenhero; }
	public static EvolutionKit valueOf(int slot) {
		for(EvolutionKit k : kits) if(k.slot == slot) return k;
		return null;
	}
	public static EvolutionKit valueOf(String name) {
		for(EvolutionKit k : kits) if(k.path.equals(name)) return k;
		return null;
	}
}
