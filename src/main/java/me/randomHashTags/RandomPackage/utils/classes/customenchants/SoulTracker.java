package me.randomHashTags.RandomPackage.utils.classes.customenchants;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.inventory.ItemStack;

public class SoulTracker {
	public static final List<SoulTracker> trackers = new ArrayList<>();
	private final int number;
	private final RarityGem convertsTo;
	private final ItemStack is;
	private final String appliedlore;
	private final List<String> applymsg, appliesto, splitmsg;
	private final double splitMultiplier;
	public SoulTracker(int number, RarityGem convertsTo, ItemStack is, String appliedlore, List<String> applymsg, List<String> appliesto, double splitMultiplier, List<String> splitmsg) {
		this.number = number;
		this.convertsTo = convertsTo;
		this.is = is;
		this.appliedlore = ChatColor.translateAlternateColorCodes('&', appliedlore);
		this.applymsg = applymsg;
		this.appliesto = appliesto;
		this.splitMultiplier = splitMultiplier;
		this.splitmsg = splitmsg;
		trackers.add(this);
	}
	public int getNumberInConfig() { return number; }
	public RarityGem getConvertsTo() { return convertsTo; }
	public ItemStack getItemStack() { return is.clone(); }
	public String getAppliedLore() { return appliedlore; }
	public List<String> getApplyMessage() { return applymsg; }
	public List<String> getAppliesTo() { return appliesto; }
	public double getSplitMultiplier() { return splitMultiplier; }
	public List<String> getSplitMessage() { return splitmsg; }
	
	public static SoulTracker valueOf(int numberInConfig) {
		for(SoulTracker s : trackers) if(s.getNumberInConfig() == numberInConfig) return s;
		return null;
	}
	public static SoulTracker valueOf(RarityGem gem) {
		for(SoulTracker st : trackers)
			if(st.getConvertsTo().equals(gem))
				return st;
		return null;
	}
	public static SoulTracker valueOf(ItemStack is) {
		if(is != null && is.hasItemMeta() && is.getItemMeta().hasDisplayName() && is.getItemMeta().hasLore()) for(SoulTracker s : trackers) if(s.getItemStack().getItemMeta().equals(is.getItemMeta())) return s;
		return null;
	}
	public static SoulTracker valueOf(String appliedlore) {
		for(SoulTracker st : trackers)
			if(appliedlore.startsWith(st.getAppliedLore().replace("{SOULS}", "")))
				return st;
		return null;
	}
}
