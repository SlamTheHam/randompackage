package me.randomHashTags.RandomPackage.utils.classes.conquests;

import me.randomHashTags.RandomPackage.utils.universal.UVersion;
import org.bukkit.Location;
import org.bukkit.entity.LivingEntity;

import java.util.ArrayList;
import java.util.List;

public class ConquestMob {
    private static final UVersion uv = UVersion.getInstance();
    public static final List<ConquestMob> mobs = new ArrayList<>();

    public final String path, type, name;
    public final List<String> attributes, equipment, drops;

    public ConquestMob(String path, String type, String name, List<String> attributes, List<String> equipment, List<String> drops) {
        this.path = path;
        this.type = type;
        this.name = name;
        this.attributes = attributes;
        this.equipment = equipment;
        this.drops = drops;
        mobs.add(this);
    }

    public LivingConquestMob spawn(Location location) {
        final LivingEntity le = uv.getEntity(type, location, true);
        final LivingConquestMob l = new LivingConquestMob(le, this);
        return l;
    }

    public static ConquestMob valueOf(String path) {
        for(ConquestMob c : mobs)
            if(c.path.equals(path))
                return c;
        return null;
    }
}
