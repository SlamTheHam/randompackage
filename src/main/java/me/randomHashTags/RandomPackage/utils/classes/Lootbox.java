package me.randomHashTags.RandomPackage.utils.classes;

import me.randomHashTags.RandomPackage.RandomPackageAPI;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.List;

public class Lootbox {
    private static final RandomPackageAPI api = RandomPackageAPI.getAPI();
    public static final List<Lootbox> lootboxes = new ArrayList<>();
    public final String path, regularSlots, bonusSlots, regularRewardFormat, bonusRewardFormat;
    private ItemStack itemstack;
    public final int invSize, regularRewardSize, maxUsages;
    public final List<String> regularRewards, bonusRewards;
    public final long cooldown;
    public final boolean allowsDuplicates;
    public Lootbox(String path, int invSize, String regularSlots, String bonusSlots, ItemStack itemstack, int regularRewardSize, String regularRewardFormat, List<String> regularRewards, String bonusRewardFormat, List<String> bonusRewards, int maxUsages, long cooldown, boolean allowsDuplicates) {
        this.path = path;
        this.invSize = invSize;
        this.regularSlots = regularSlots;
        this.bonusSlots = bonusSlots;
        this.itemstack = itemstack;
        this.regularRewardSize = regularRewardSize;
        this.regularRewardFormat = regularRewardFormat;
        this.regularRewards = regularRewards;
        this.bonusRewardFormat = bonusRewardFormat;
        this.bonusRewards = bonusRewards;
        this.maxUsages = maxUsages;
        this.cooldown = cooldown;
        this.allowsDuplicates = allowsDuplicates;
        this.itemstack = getLootbox();
        lootboxes.add(this);
    }
    public ItemStack getItemStack() { return itemstack.clone(); }
    private ItemStack getLootbox() {
        final ItemMeta itemMeta = itemstack.getItemMeta();
        final List<String> lore = new ArrayList<>();
        if(itemMeta.hasLore()) {
            lore.clear();
            for(String l : itemMeta.getLore()) {
                if(l.contains("_REWARDS}")) {
                    final List<String> rewards = l.contains("{B_REWARDS") ? bonusRewards : regularRewards;
                    final String format = l.contains("B_REWARDS") ? bonusRewardFormat : regularRewardFormat;
                    for(String r : rewards) {
                        final ItemStack reward = api.d(null, r, 0);
                        if(reward != null)
                            lore.add(format.replace("{AMOUNT}", Integer.toString(reward.getAmount())).replace("{NAME}", reward.getItemMeta().hasDisplayName() ? reward.getItemMeta().getDisplayName() : api.toMaterial(reward.getType().name(), false)));
                    }
                } else lore.add(l);
            }
            itemMeta.setLore(lore); lore.clear();
            itemstack.setItemMeta(itemMeta);
        }
        return itemstack;
    }

    public static Lootbox valueOf(ItemStack itemstack) {
        if(itemstack != null && itemstack.hasItemMeta())
            for(Lootbox lb : lootboxes)
                if(lb.getItemStack().getItemMeta().equals(itemstack.getItemMeta()))
                    return lb;
        return null;
    }
}
