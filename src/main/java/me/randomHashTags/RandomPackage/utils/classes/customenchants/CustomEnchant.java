package me.randomHashTags.RandomPackage.utils.classes.customenchants;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.inventory.ItemStack;

@SuppressWarnings({"deprecation"})
public class CustomEnchant {
	public static List<EnchantRarity> format = new ArrayList<>();
	public static ArrayList<CustomEnchant> enchants = new ArrayList<>();
	public static HashMap<EnchantRarity, List<CustomEnchant>> enchantRarities = new HashMap<>(), enabledRarityEnchants = new HashMap<>();
	private static HashMap<String, List<CustomEnchant>> enchantAppliesTo = new HashMap<>();
	private final String path, name, tinkerer, alchemist, enchantprocvalue;
	private final EnchantRarity rarity;
	private final int maxlevel;
	private final List<String> attributes, lore, appliesto;
	private final boolean enabled;
	private String requires;
	
	public CustomEnchant(String path, boolean enabled, String name, EnchantRarity rarity, int maxlevel, String enchantprocvalue, List<String> appliesto, List<String> attributes, List<String> lore, String tinkerer, String alchemist) {
		this.path = path;
		this.enabled = enabled;
		this.name = name;
		this.rarity = rarity;
		this.maxlevel = maxlevel;
		this.enchantprocvalue = enchantprocvalue;
		this.appliesto = appliesto;
		this.attributes = attributes;
		this.lore = lore;
		this.tinkerer = tinkerer;
		this.alchemist = alchemist;
		if(!enchantRarities.keySet().contains(rarity)) enchantRarities.put(rarity, new ArrayList<>());
		if(!enabledRarityEnchants.keySet().contains(rarity)) enabledRarityEnchants.put(rarity, new ArrayList<>());
		List<CustomEnchant> a = enchantRarities.get(rarity);
		a.add(this);
		enchantRarities.put(rarity, a);
		if(enabled) enabledRarityEnchants.get(rarity).add(this);
		for(String s : appliesto) {
			final String S = s.toUpperCase();
			if(!enchantAppliesTo.keySet().contains(S)) enchantAppliesTo.put(S, new ArrayList<>());
			List<CustomEnchant> b = enchantAppliesTo.get(S);
			b.add(this);
			enchantAppliesTo.put(S, b);
		}
		if(!enchants.contains(this)) enchants.add(this);
	}
	public CustomEnchant(String path, boolean enabled, String name, EnchantRarity rarity, int maxlevel, String requires, String enchantprocvalue, List<String> appliesto, List<String> attributes, List<String> lore, String tinkerer, String alchemist) {
		this.path = path;
		this.enabled = enabled;
		this.name = name;
		this.rarity = rarity;
		this.maxlevel = maxlevel;
		this.requires = requires;
		this.enchantprocvalue = enchantprocvalue;
		this.appliesto = appliesto;
		this.attributes = attributes;
		this.lore = lore;
		this.tinkerer = tinkerer;
		this.alchemist = alchemist;
		if(!enchantRarities.keySet().contains(rarity)) enchantRarities.put(rarity, new ArrayList<>());
		if(!enabledRarityEnchants.keySet().contains(rarity)) enabledRarityEnchants.put(rarity, new ArrayList<>());
		List<CustomEnchant> a = enchantRarities.get(rarity);
		a.add(this);
		enchantRarities.put(rarity, a);
		if(enabled) enabledRarityEnchants.get(rarity).add(this);
		for(String s : appliesto) {
			final String S = s.toUpperCase();
			if(!enchantAppliesTo.keySet().contains(S)) enchantAppliesTo.put(S, new ArrayList<>());
			List<CustomEnchant> b = enchantAppliesTo.get(S);
			b.add(this);
			enchantAppliesTo.put(S, b);
		}
		if(!enchants.contains(this)) enchants.add(this);
	}
	public String getPath() { return path; }
	public boolean isEnabled() { return enabled; }
	public String getName() { return name; }
	public EnchantRarity getRarity() { return rarity; }
	public int getMaxLevel() { return maxlevel; }
	public String getRequires() { return requires; }
	public String getEnchantProcValue() { return enchantprocvalue; }
	public List<String> getAppliesTo() { return appliesto; }
	public List<String> getAttributes() { return attributes; }
	public List<String> getLore() { return lore; }
	public int getAlchemistUpgradeCost(int currentlevel) {
		return currentlevel + 1 <= maxlevel && alchemist != null ? Integer.parseInt(alchemist.split(":")[currentlevel - 1]) : -1;
	}
	public int getTinkererValue(int currentlevel) {
		return currentlevel <= maxlevel && tinkerer != null ? Integer.parseInt(tinkerer.split(":")[currentlevel - 1]) : -1;
	}
	public static CustomEnchant valueOf(String string) {
		if(string != null)
			for(CustomEnchant ce : enchants)
				if(ChatColor.stripColor(string).startsWith(ChatColor.stripColor(ce.getName())))
					return ce;
		return null;
	}
	public static CustomEnchant valueOf(ItemStack is) {
		if(is != null && is.hasItemMeta() && is.getItemMeta().hasDisplayName() && is.getItemMeta().hasLore())
			for(CustomEnchant e : enchants) if(is.getItemMeta().getDisplayName().startsWith(e.getRarity().getNameColors() + e.getName()) && is.getType().equals(e.getRarity().getRevealedItem().getType()) && is.getData().getData() == e.getRarity().getRevealedItem().getData().getData())
				return e;
		return null;
	}
	public static List<CustomEnchant> getEnabledEnchants(EnchantRarity rarity) { return enabledRarityEnchants.get(rarity); }
	public static List<CustomEnchant> getEnchants(EnchantRarity rarity) { return enchantRarities.get(rarity); }
	public static List<CustomEnchant> getEnchants(String appliesto) { return enchantAppliesTo.keySet().contains(appliesto.toUpperCase()) ? enchantAppliesTo.get(appliesto.toUpperCase()) : new ArrayList<>(); }
}
