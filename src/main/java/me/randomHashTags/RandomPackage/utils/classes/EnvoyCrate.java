package me.randomHashTags.RandomPackage.utils.classes;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

import me.randomHashTags.RandomPackage.api.Envoy;
import org.bukkit.Bukkit;
import org.bukkit.Chunk;
import org.bukkit.Effect;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Firework;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.Plugin;

import me.randomHashTags.RandomPackage.RandomPackage;
import me.randomHashTags.RandomPackage.RandomPackageAPI;
import me.randomHashTags.RandomPackage.utils.universal.UVersion;
import me.randomHashTags.RandomPackage.utils.supported.FactionsAPI;

@SuppressWarnings({"deprecation"})
public class EnvoyCrate {
	private static final UVersion uv = UVersion.getInstance();
	private static final RandomPackageAPI api = RandomPackageAPI.getAPI();
	private static final Plugin randompackage = RandomPackage.getPlugin;

	public static final List<EnvoyCrate> crates = new ArrayList<>();
	public static final HashMap<Integer, HashMap<Location, EnvoyCrate>> activeCrates = new HashMap<>();
	private final String path, rewardSize, block;
	private final int chance;
	private final List<String> rewards, cannotLandAbove;
	private final boolean dropsFromSky, canRepeatRewards;
	private final ItemStack physicalItem, fallingblock;
	private final Firework fw;
	private static final FileConfiguration config = YamlConfiguration.loadConfiguration(new File(randompackage.getDataFolder() + File.separator + "Features" + File.separator, "envoy.yml"));
	private Location loc = null;
	private static int envoys = 0;
	public EnvoyCrate(String path, String block, ItemStack physicalItem, int chance, String rewardSize, List<String> rewards, boolean dropsFromSky, ItemStack fallingblock, Firework fw, boolean canRepeatRewards, List<String> cannotLandAbove) {
		this.path = path;
		this.block = block;
		this.physicalItem = physicalItem;
		this.chance = chance;
		this.rewardSize = rewardSize;
		this.rewards = rewards;
		this.dropsFromSky = dropsFromSky;
		this.fallingblock = fallingblock;
		this.fw = fw;
		this.canRepeatRewards = canRepeatRewards;
		this.cannotLandAbove = cannotLandAbove;
		crates.add(this);
	}
	public String getPath() { return path; }
	public String getBlock() { return block; }
	public ItemStack getPhysicalItem() { return physicalItem.clone(); }
	public int getChance() { return chance; }
	public String getRewardSize() { return rewardSize; }
	public List<String> getRewards() { return rewards; }
	public boolean dropsFromSky() { return dropsFromSky; }
	public ItemStack getFallingBlock() { return fallingblock.clone(); }
	public Firework getFirework() { return fw; }
	public boolean canRepeatRewards() { return canRepeatRewards; }
	public List<String> cannotLandAbove() { return cannotLandAbove; }
	public Location getLocation() { return loc; }
	public static void destroy(int envoy, Location l, boolean dropsRewards) {
		final EnvoyCrate c = valueOf(l);
		l.getWorld().getBlockAt(l).setType(Material.AIR);
		if(dropsRewards) {
			for(String s : c.getRandomRewards())
				l.getWorld().dropItemNaturally(l, api.d(null, s.split(";chance=")[0], 0));
		}
		activeCrates.get(envoy).remove(l);
	}
	public int getRandomRewardSize() {
		final int min = rewardSize.contains("-") ? Integer.parseInt(rewardSize.split("-")[0]) : Integer.parseInt(rewardSize), max = rewardSize.contains("-") ? Integer.parseInt(rewardSize.split("-")[1]) : -1;
		return min+(max == -1 ? 0 : new Random().nextInt(max-min+1));
	}
	public List<String> getRandomRewards() {
		List<String> rewards = new ArrayList<>(this.rewards), actualrewards = new ArrayList<>();
		final Random random = new Random();
		for(int i = 1; i <= getRandomRewardSize(); i++) {
			if(rewards.size() != 0) {
				final String reward = rewards.get(random.nextInt(rewards.size()));
				if(random.nextInt(100) <= uv.getRemainingInt(reward.split(";chance=")[1])) {
					actualrewards.add(reward.split(";chance=")[0]);
					if(!canRepeatRewards) rewards.remove(reward);
				} else {
					i -= 1;
				}
			}
		}
		return actualrewards;
	}
	public void shootFirework() {
		if(fw != null) {
			final World w = loc.getWorld();
			uv.spawnFirework(fw, new Location(w, loc.getX(), loc.getY()+1, loc.getZ()));
			w.playEffect(loc, Effect.STEP_SOUND, loc.getBlock().getType());
		}
	}
	public static void spawnEnvoy(String type, int amount) {
		type = type.toUpperCase();
		final Random random = new Random();
		final int despawn = config.getInt("settings.availability");
		final int envoy = envoys;
		activeCrates.put(envoy, new HashMap<>());
		if(type.equals("WARZONE")) {
			final List<Chunk> c = FactionsAPI.getFactionsAPI().getWarZoneChunks();
			if(!c.isEmpty()) {
				for(int i = 1; i <= amount; i++) {
					final List<Location> cl = uv.getChunkLocations(c.get(random.nextInt(c.size())));
					final EnvoyCrate crate = getRandomCrate(true);
					final Location loc = getRandomLocation(random, cl);
					final World w = loc.getWorld();
					if(EnvoyCrate.valueOf(new Location(loc.getWorld(), loc.getBlockX(), loc.getBlockY()-1, loc.getBlockZ())) == null && crate.canLand(loc)) {
						crate.loc = loc;
						activeCrates.get(envoy).put(loc, crate);
						crate.shootFirework();
						w.getBlockAt(loc).setType(Material.getMaterial(crate.getBlock().split(":")[0].toUpperCase()));
						w.getBlockAt(loc).getState().setRawData(Byte.parseByte(crate.getBlock().split(":")[1]));
					} else {
						i -= 1;
					}
				}
			}
		} else if(type.equals("PRESET")) {
			final List<Location> preset = new ArrayList<>(Envoy.getEnvoy().preset);
			for(int i = 1; i <= amount; i++) {
				final Location r = preset.get(random.nextInt(preset.size()));
				final World w = r.getWorld();
				final EnvoyCrate crate = getRandomCrate(true);
				if(EnvoyCrate.valueOf(new Location(w, r.getBlockX(), r.getBlockY()-1, r.getBlockZ())) == null && crate.canLand(r)) {
					crate.loc = r;
					activeCrates.get(envoy).put(r, crate);
					crate.shootFirework();
					w.getBlockAt(r).setType(Material.getMaterial(crate.getBlock().split(":")[0].toUpperCase()));
					w.getBlockAt(r).getState().setRawData(Byte.parseByte(crate.getBlock().split(":")[1]));
				} else {
					i -= 1;
				}
				preset.remove(r);
			}
		}
		Bukkit.getScheduler().scheduleSyncDelayedTask(randompackage, () -> stopEnvoy(envoy), 20*despawn);
		envoys += 1;
	}
	public static void stopAllEnvoys() {
		for(int i : activeCrates.keySet())
			stopEnvoy(i);
	}
	public static void stopEnvoy(int envoy) {
		for(int i = 0; i < activeCrates.get(envoy).keySet().size(); i++) {
			final Location loc = (Location) activeCrates.get(envoy).keySet().toArray()[i];
			if(activeCrates.get(envoy).keySet().contains(loc)) {
				destroy(envoy, loc, false);
				i -= 1;
			}
		}
	}
	private boolean canLand(Location spawnLocation) {
		final Block b = spawnLocation.getWorld().getBlockAt(new Location(spawnLocation.getWorld(), spawnLocation.getBlockX(), spawnLocation.getBlockY()-1, spawnLocation.getBlockZ()));
		for(String s : cannotLandAbove)
			if(s.split(":")[0].equalsIgnoreCase(b.getType().name()) && Byte.parseByte(s.split(":")[1]) == b.getData())
				return false;
		return true;
	}
	private static Location getRandomLocation(Random random, List<Location> chunklocs) {
		final Location rl = chunklocs.get(random.nextInt(chunklocs.size()));
		final World w = rl.getWorld();
		final int x = rl.getBlockX(), z = rl.getBlockZ();
		return new Location(w, x, w.getHighestBlockYAt(x, z), z);
	}

	public static EnvoyCrate valueOf(String path) {
		for(EnvoyCrate c : crates) if(c.getPath().equals(path)) return c;
		return null;
	}
	public static EnvoyCrate valueOf(ItemStack is) {
		if(is != null && is.hasItemMeta())
			for(EnvoyCrate c : crates)
				if(is.getItemMeta().equals(c.getPhysicalItem().getItemMeta())) return c;
		return null;
	}
	public static EnvoyCrate getRandomCrate(boolean useChances) {
		final Random random = new Random();
		if(useChances) {
			for(EnvoyCrate c : crates)
				if(random.nextInt(100) <= c.getChance())
					return c;
		} else {
			return crates.get(random.nextInt(crates.size()));
		}
		return crates.get(crates.size()-1);
	}
	public static EnvoyCrate valueOf(Location loc) {
		final HashMap<Integer, HashMap<Location, EnvoyCrate>> ec = activeCrates;
		for(int i : ec.keySet())
			for(Location l : ec.get(i).keySet())
				if(l.getWorld() == loc.getWorld() && l.getBlockX() == loc.getBlockX() && l.getBlockY() == loc.getBlockY() && l.getBlockZ() == loc.getBlockZ())
					return ec.get(i).get(loc);
		return null;
	}
	public static int getEnvoy(Location l) {
		for(int i : activeCrates.keySet())
			if(activeCrates.get(i).keySet().contains(l)) return i;
		return -1;
	}
}
