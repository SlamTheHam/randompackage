package me.randomHashTags.RandomPackage.utils.classes.custombosses;

import me.randomHashTags.RandomPackage.RandomPackageAPI;
import me.randomHashTags.RandomPackage.api.events.customboss.CustomBossDamageByEntityEvent;
import me.randomHashTags.RandomPackage.api.events.customboss.CustomBossDeathEvent;
import me.randomHashTags.RandomPackage.api.events.customboss.CustomBossSpawnEvent;
import me.randomHashTags.RandomPackage.utils.universal.UVersion;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.*;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.projectiles.ProjectileSource;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Scoreboard;

import java.util.*;

public class LivingCustomBoss extends UVersion {
    public static final List<LivingCustomBoss> living = new ArrayList<>();

    public final LivingEntity summoner, entity;
    public final CustomBoss type;
    public final List<LivingCustomMinion> minions = new ArrayList<>();
    public HashMap<UUID, Double> damagers = new HashMap<>();
    public LivingCustomBoss(LivingEntity summoner, LivingEntity entity, CustomBoss type) {
        this.summoner = summoner;
        this.entity = entity;
        this.type = type;
        final HashMap<Integer, List<String>> messages = type.messages;
        final int messageRadius = type.messageRadius;
        for(String s : messages.get(-4)) {
            Bukkit.broadcastMessage(ChatColor.translateAlternateColorCodes('&', s));
        }
        for(String s : messages.get(-5)) {
            s = ChatColor.translateAlternateColorCodes('&', s.replace("{SUMMONER}", summoner.getName()));
            summoner.sendMessage(s);
            for(Entity en : summoner.getNearbyEntities(messageRadius, messageRadius, messageRadius)) if(en instanceof Player) en.sendMessage(s);
        }
        for(PotionEffect t : entity.getActivePotionEffects()) entity.removePotionEffect(t.getType());
        entity.setCustomName(type.name);
        entity.setCustomNameVisible(true);
        for(String s : type.attributes) {
            final String d = s.toLowerCase();
            if(d.startsWith("maxhealth=")) {
                entity.setMaxHealth(Double.parseDouble(d.split("=")[1]));
                entity.setHealth(entity.getMaxHealth());
            } else if(d.startsWith("addpotioneffect")) {
                final String p = d.split("\\{")[1].split("}")[0];
                final PotionEffectType t = getPotionEffectType(p.split(":")[0].toUpperCase());
                if(t != null) {
                    entity.addPotionEffect(new PotionEffect(t, Integer.parseInt(p.split(":")[1]), Integer.parseInt(p.split(":")[2])));
                }
            } else if(d.startsWith("size=")) {
                final int r = Integer.parseInt(d.split("=")[1]);
                if(entity instanceof Slime) ((Slime) entity).setSize(r);
            }
        }
        if(entity instanceof Creature) ((Creature) entity).setTarget(summoner);
        updateScoreboards(summoner, 0);
        final CustomBossSpawnEvent e = new CustomBossSpawnEvent(summoner, entity.getLocation(), this);
        pluginmanager.callEvent(e);
        living.add(this);
    }
    public void damage(LivingEntity customboss, Entity damager, double damage) {
        final CustomBossDamageByEntityEvent e = new CustomBossDamageByEntityEvent(customboss, damager, damage);
        pluginmanager.callEvent(e);
        if(!e.isCancelled()) {
            final double d = e.getDamage();
            final HashMap<Integer, List<String>> messages = type.messages;
            UUID i = null;
            if(damager instanceof Player) {
                i = damager.getUniqueId();
            } else if(damager instanceof Projectile) {
                final ProjectileSource ps = ((Projectile) damager).getShooter();
                if(ps instanceof Player) i = ((Player) ps).getUniqueId();
            }
            if(i != null) {
                final double prev = damagers.keySet().contains(i) ? damagers.get(i) : 0.00;
                damagers.put(i, prev+d);
            }
            updateScoreboards(customboss, d);
            final int maxMinions = type.minion.max;
            for(CustomBossAttack atk : type.attacks) {
                if(random.nextInt(100) <= atk.chance) {
                    final int radius = atk.radius;
                    for(String attack : atk.attacks) {
                        final String att = attack.toLowerCase();
                        if(att.startsWith("delay=")) {
                            final int r = Integer.parseInt(att.split("delay=")[1].split("\\{")[0]);
                            final ArrayList<Location> locations = new ArrayList<>();
                            for(Entity entity : customboss.getNearbyEntities(radius, radius, radius)) if(entity instanceof Player) locations.add(entity.getLocation());

                            Bukkit.getScheduler().scheduleSyncDelayedTask(randompackage, () -> {
                                String ss = att.replace("delay=" + r, ""), sss = null;
                                int x1, y1, z1, x2, y2, z2, a;
                                List<String> message;
                                if(ss.startsWith("{surround")) {
                                    sss = ss.replace("{surround:", "").replace("}", "");
                                    x1 = Integer.parseInt(sss.split(":")[0]);
                                    y1 = Integer.parseInt(sss.split(":")[1]);
                                    z1 = Integer.parseInt(sss.split(":")[2]);
                                    x2 = Integer.parseInt(sss.split(":")[3]);
                                    y2 = Integer.parseInt(sss.split(":")[4]);
                                    z2 = Integer.parseInt(sss.split(":")[5]);
                                    for(Location l : locations) {
                                        Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "minecraft:fill " + (l.getX() + x1) + " " + (l.getY() + y1) + " " + (l.getZ() + z1) + " " + (l.getX() + x2) + " " + (l.getY() + y2) + " " + (l.getZ() + z2) + " " + sss.split(":")[6] + " 0 replace " + sss.split(":")[7]);
                                    }
                                } else if (ss.startsWith("{/")) {
                                    for(Entity entity : customboss.getNearbyEntities(radius, radius, radius)) {
                                        if(entity instanceof Player) {
                                            Bukkit.dispatchCommand(entity, ss.replace("{", "").replace("}", "").replace("/", "").replace("~player", entity.getName()));
                                        }
                                    }
                                } else {
                                    if(ss.startsWith("{message")) {
                                        for(String v  : messages.get(Integer.parseInt(ss.split("message")[1].split("}")[0]))) {
                                            for (Entity entity : customboss.getNearbyEntities(radius, radius, radius)) {
                                                entity.sendMessage(ChatColor.translateAlternateColorCodes('&', v));
                                            }
                                        }
                                    } else if (ss.startsWith("{summonminions")) {
                                        spawnMinions(customboss, maxMinions);
                                    } else if (ss.startsWith("{minionheal")) {
                                        a = 0;
                                        final int healby = Integer.parseInt(ss.split(":")[1]), ra = Integer.parseInt(ss.split(":")[2]);
                                        message = messages.get(Integer.parseInt(ss.split(":")[3].replace("message", "").replace("}", "")));
                                        for(LivingCustomMinion minion : minions) {
                                            final double hp = customboss.getHealth(), maxhp = customboss.getMaxHealth();
                                            customboss.setHealth(hp+healby > maxhp ? maxhp : hp+healby);
                                            a += healby;
                                        }
                                        if(a != 0) {
                                            for(Entity entity : customboss.getNearbyEntities(ra, ra, ra)) {
                                                for(String g : message) {
                                                    if(g.contains("{HP}")) g = g.replace("{HP}", Integer.toString(a));
                                                    entity.sendMessage(ChatColor.translateAlternateColorCodes('&', g));
                                                }
                                            }
                                        }
                                    }
                                }
                            }, r);
                        }
                    }
                }
            }
        }
    }
    public void kill(LivingEntity l, EntityDamageEvent damagecause) {
        final CustomBossDeathEvent ev = new CustomBossDeathEvent(this);
        pluginmanager.callEvent(ev);
        if(!ev.isCancelled()) {
            final HashMap<Integer, List<String>> messages = type.messages;
            final int messageRadius = type.messageRadius;
            for(Entity e : l.getNearbyEntities(messageRadius, messageRadius, messageRadius)) {
                if(e instanceof Player)
                    ((Player) e).setScoreboard(Bukkit.getScoreboardManager().getNewScoreboard());
            }
            double totaldamage = 0.00;
            for(UUID d : damagers.keySet()) totaldamage += damagers.get(d);
            final HashMap<Integer, UUID> top = new HashMap<>();
            final HashMap<Integer, Double> topd = new HashMap<>();
            final HashMap<Integer, List<ItemStack>> rewards = new HashMap<>();
            final HashMap<Integer, List<String>> rewardNames = new HashMap<>();
            final RandomPackageAPI api = RandomPackageAPI.getAPI();
            for(String s : type.rewards) {
                final int target = getRemainingInt(s.split(";")[0]), amount = getRemainingInt(s.split(";")[s.split(";").length-1]);
                if(!rewards.keySet().contains(target)) {
                    rewards.put(target, new ArrayList<>());
                    rewardNames.put(target, new ArrayList<>());
                }
                final ItemStack reward = api.d(null, s.split("\\{")[1].split("}")[0], 0);
                reward.setAmount(amount);
                rewards.get(target).add(reward);
                rewardNames.get(target).add(ChatColor.translateAlternateColorCodes('&', s.split(";")[3]));
            }
            for(int i = 1; i <= 10; i++) {
                top.put(i, null);
                topd.put(i, 0.00);
            }
            for(UUID damager : damagers.keySet()) {
                final double dmg = damagers.get(damager);
                for(int i = 1; i <= 10; i++) {
                    if(!top.values().contains(damager)) {
                        if(top.get(i) == null) {
                            top.put(i, damager);
                            topd.put(i, damagers.get(damager));
                        } else if(dmg > topd.get(i)) {
                            for(int z = i; z <= 10; z++) {
                                final UUID target = top.get(z);
                                top.put(z + 1, target);
                                topd.put(z + 1, target != null ? damagers.get(target) : 0.00);
                            }
                            top.put(i, damager);
                        }
                    }
                }
            }
            for(int i : rewards.keySet()) {
                if(top.keySet().contains(i) && top.get(i) != null) {
                    final OfflinePlayer p = Bukkit.getOfflinePlayer(top.get(i));
                    if(p.isOnline())
                        for(ItemStack re : rewards.get(i))
                            api.giveItem(p.getPlayer(), re);
                }
            }
            living.remove(this);
            final List<String> topp = new ArrayList<>();
            for(int i = 1; i <= 10; i++) {
                final UUID t = top.get(i);
                if(t != null && !topp.contains(t))
                    topp.add(Bukkit.getOfflinePlayer(t).getName());
            }
            for(String s : messages.get(-2)) {
                if(s.contains("{TOP_PLAYERS}")) s = s.replace("{TOP_PLAYERS}", topp.toString().substring(1, topp.toString().length()-1));
                for(int i = 1; i <= 10; i++) {
                    if(s != null && s.contains("{TOP" + i + "}"))
                        s = topp.size() > i-1 ? s.replace("{TOP" + i + "}", topp.get(i-1)) : null;
                    if(s != null && s.contains("{TOP" + i + "%}"))
                        s = topd.keySet().size() > i-1 ? s.replace("{TOP" + i + "%}", roundDoubleString((topd.get(i)/totaldamage)*100, 2)) : null;
                    if(s != null && s.contains("{REWARDS}"))
                        s = s.replace("{REWARDS}", rewardNames.get(i).toString().substring(1, rewardNames.get(i).toString().length()-1));
                }
                if(s != null) {
                    Bukkit.broadcastMessage(center(ChatColor.translateAlternateColorCodes('&', s), 60));
                }
            }
            for(String s : messages.get(-3)) {
                s = ChatColor.translateAlternateColorCodes('&', s);
                for(Entity e : l.getNearbyEntities(messageRadius, messageRadius, messageRadius))
                    if(e instanceof Player)
                        e.sendMessage(s);
            }
        }
    }
    public void spawnMinions(LivingEntity boss, int amount) {
        if(boss == null || boss.isDead() || minions.size() + amount > type.maxMinions) return;
        final int messageRadius = type.messageRadius;
        LivingEntity target = entity instanceof Creature ? ((Creature) entity).getTarget() : null;
        if(target == null)
            for(Entity e : boss.getNearbyEntities(messageRadius, messageRadius, messageRadius))
                if(target == null && e instanceof Player)
                    target = (LivingEntity) e;
        final CustomMinion t = type.minion;
        if(target != null) {
            final LivingCustomMinion l = new LivingCustomMinion(getEntity(t.type, entity.getLocation(), true), target, t, this);
            minions.add(l);
        }
    }
    private void updateScoreboards(LivingEntity boss, double dmg) {
        final String scoreboardTitle = type.scoreboardTitle, g = formatDouble(round(boss.getHealth()-dmg, 2)), m = formatInt(minions.size());
        final DisplaySlot scoreboardSlot = type.scoreboardSlot;
        final int messageRadius = type.messageRadius;
        final List<String> scores = type.scores;
        for(Entity e : boss.getNearbyEntities(messageRadius, messageRadius, messageRadius)) {
            if(e instanceof Player) {
                final UUID u = e.getUniqueId();
                final Scoreboard sb = Bukkit.getScoreboardManager().getNewScoreboard();
                final Objective obj = sb.registerNewObjective("dummy", "dummy");
                obj.setDisplayName(scoreboardTitle);
                obj.setDisplaySlot(scoreboardSlot);
                int h = 15;
                for(String s : scores) {
                    if(s.contains("{HEALTH}")) s = s.replace("{HEALTH}", g);
                    if(s.contains("{MINIONS}")) s = s.replace("{MINIONS}", m);
                    if(s.contains("{DAMAGE_DEALT}")) s = s.replace("{DAMAGE_DEALT}", formatDouble(round(damagers.getOrDefault(u, 0.00), 0)));
                    if(s.contains("{DAMAGE_DEALT%}")) s = s.replace("{DAMAGE_DEALT%}", roundDoubleString(getDamagePercentDone(u), 1));
                    obj.getScore(ChatColor.translateAlternateColorCodes('&', s)).setScore(h);
                    h -= 1;
                }
                ((Player) e).setScoreboard(sb);
            }
        }
    }
    public double getDamagePercentDone(UUID damager) {
        double total = 0.00, dmg = 0.00;
        if(damagers.keySet().contains(damager)) {
            for(UUID u : damagers.keySet()) {
                final double d = damagers.get(u);
                total += d;
                if(u.equals(damager)) dmg = d;
            }
        }
        return total != 0.00 ? (dmg/total)*100 : 0.00;
    }
    public static LivingCustomBoss valueOf(UUID uuid) {
        if(uuid != null)
            for(LivingCustomBoss l : living)
                if(l.entity.getUniqueId().equals(uuid))
                    return l;
        return null;
    }
}