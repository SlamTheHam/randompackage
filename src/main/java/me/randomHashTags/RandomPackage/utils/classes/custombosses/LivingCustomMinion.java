package me.randomHashTags.RandomPackage.utils.classes.custombosses;

import me.randomHashTags.RandomPackage.api.events.customboss.CustomMinionDeathEvent;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Creature;
import org.bukkit.entity.LivingEntity;
import org.bukkit.event.entity.EntityDamageEvent;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class LivingCustomMinion {
    public static final List<LivingCustomMinion> living = new ArrayList<>();

    public final LivingEntity entity;
    public final CustomMinion type;
    public final LivingCustomBoss parent;
    public LivingEntity target;
    public LivingCustomMinion(LivingEntity entity, LivingEntity target, CustomMinion type, LivingCustomBoss parent) {
        entity.setCustomName(ChatColor.translateAlternateColorCodes('&', type.path));
        entity.setCustomNameVisible(true);
        if(entity instanceof Creature && target != null) {
            ((Creature) entity).setTarget(target);
            this.target = target;
        }
        this.entity = entity;
        this.type = type;
        this.parent = parent;
        living.add(this);
    }
    public void kill(LivingEntity le, EntityDamageEvent damagecause) {
        le.setHealth(0.00);
        living.remove(this);
        final CustomMinionDeathEvent e = new CustomMinionDeathEvent(le, damagecause);
        Bukkit.getPluginManager().callEvent(e);
    }

    public static LivingCustomMinion valueOf(UUID uuid) {
        if(uuid != null)
            for(LivingCustomMinion m : living)
                if(m.entity.getUniqueId().equals(uuid))
                    return m;
        return null;
    }
}
