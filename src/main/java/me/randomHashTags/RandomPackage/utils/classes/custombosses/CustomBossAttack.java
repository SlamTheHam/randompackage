package me.randomHashTags.RandomPackage.utils.classes.custombosses;

import java.util.List;

public class CustomBossAttack {
	public final int chance, radius;
	public final List<String> attacks;
	public CustomBossAttack(int chance, int radius, List<String> attacks) {
		this.chance = chance;
		this.radius = radius;
		this.attacks = attacks;
	}
}
