package me.randomHashTags.RandomPackage.utils.classes;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class ArmorSet {
	public static final List<ArmorSet> sets = new ArrayList<>();
	private final String path;
	private final List<String> helmetLore, chestplateLore, leggingsLore, bootsLore, weaponLore, attributes, activateMessage;
	public ArmorSet(String path, List<String> helmetLore, List<String> chestplateLore, List<String> leggingsLore, List<String> bootsLore, List<String> weaponLore, List<String> attributes, List<String> activateMessage) {
		this.path = path;
		this.helmetLore = helmetLore;
		this.chestplateLore = chestplateLore;
		this.leggingsLore = leggingsLore;
		this.bootsLore = bootsLore;
		this.weaponLore = weaponLore;
		this.attributes = attributes;
		this.activateMessage = activateMessage;
		sets.add(this);
	}
	public String getPath() { return path; }
	public List<String> getHelmetLore() { return helmetLore; }
	public List<String> getChestplateLore() { return chestplateLore; }
	public List<String> getLeggingsLore() { return leggingsLore; }
	public List<String> getBootsLore() { return bootsLore; }
	public List<String> getWeaponLore() { return weaponLore; }
	public List<String> getAttributes() { return attributes; }
	public List<String> getActivateMessage() { return activateMessage; }
	
	public static ArmorSet valueOf(Player player) {
		final ItemStack h = player.getInventory().getHelmet(), c = player.getInventory().getChestplate(), l = player.getInventory().getLeggings(), b = player.getInventory().getBoots();
		for(ArmorSet set : sets) {
			if(h != null && h.hasItemMeta() && h.getItemMeta().hasLore() && h.getItemMeta().getLore().containsAll(set.helmetLore)
					&& c != null && c.hasItemMeta() && c.getItemMeta().hasLore() && c.getItemMeta().getLore().containsAll(set.chestplateLore)
					&& l != null && l.hasItemMeta() && l.getItemMeta().hasLore() && l.getItemMeta().getLore().containsAll(set.leggingsLore)
					&& b != null && b.hasItemMeta() && b.getItemMeta().hasLore() && b.getItemMeta().getLore().containsAll(set.bootsLore)) {
				return set;
			}
		}
		return null;
	}
	public static ArmorSet valueOf(String path) {
		for(ArmorSet set : sets) if(set.getPath().equals(path)) return set;
		return null;
	}
}
