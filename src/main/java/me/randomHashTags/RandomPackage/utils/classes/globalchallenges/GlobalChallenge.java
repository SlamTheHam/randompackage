package me.randomHashTags.RandomPackage.utils.classes.globalchallenges;

import java.util.*;

import me.randomHashTags.RandomPackage.api.GlobalChallenges;
import org.bukkit.Bukkit;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.PluginManager;

import me.randomHashTags.RandomPackage.RPPlayerData;
import me.randomHashTags.RandomPackage.RandomPackage;
import me.randomHashTags.RandomPackage.api.events.GlobalChallengeEndEvent;
import me.randomHashTags.RandomPackage.utils.universal.UVersion;

public class GlobalChallenge extends UVersion {
	private static final GlobalChallenges globalchallenges = GlobalChallenges.getChallenges();
	public static final List<GlobalChallenge> active = new ArrayList<>(), challenges = new ArrayList<>();
	private final PluginManager pm = Bukkit.getPluginManager();
	public final String path, name, tracks;
	private final ItemStack display;
	private int task = -1;
	public List<UUID> participants;
	public long started;

	public GlobalChallenge(String path, String name, String tracks, ItemStack display, List<UUID> participants) {
		this.path = path;
		this.name = name;
		this.tracks = tracks;
		this.display = display;
		this.participants = participants;
		challenges.add(this);
	}
	public ItemStack getDisplayItem() { return display.clone(); }
	public boolean isActive() { return active.contains(this); }
	public long getRemainingTime() {
		final String t = tracks.split(";")[1].toLowerCase();
		final double days = t.contains("d") ? Double.parseDouble(t.split("d")[0])*60*60*24*1000 : 0, hrs = t.contains("h") ? Double.parseDouble(t.split("h")[0])*60*60*1000 : 0, min = t.contains("m") ? Double.parseDouble(t.split("m")[1])*60*1000 : 0, sec = t.contains("s") ? Double.parseDouble(t.split("s")[1])*1000 : 0;
		final long ttl = Double.valueOf(started+days+hrs+min+sec).longValue();
		return ttl-System.currentTimeMillis();
	}
	public GlobalChallenge begin(long started) { this.started = started; begin(started, participants == null ? new ArrayList<>() : participants); return this; }
	public GlobalChallenge begin(long started, List<UUID> challengeParticipants) {
		if(!active.contains(this)) {
			this.started = started;
			this.participants = challengeParticipants;
			active.add(this);
			long remainingTime = getRemainingTime();
			if(remainingTime < 0) remainingTime = 0;
			task = Bukkit.getScheduler().scheduleSyncDelayedTask(RandomPackage.getPlugin, () -> end(true, 3), remainingTime);
		}
		return this;
	}
	public void end(boolean giveRewards, int recordPlacements) {
		final GlobalChallengeEndEvent e = new GlobalChallengeEndEvent(this, giveRewards);
		pm.callEvent(e);
		final HashMap<UUID, Double> participants = globalchallenges.getParticipants(this);
		final Map<UUID, Double> placements = globalchallenges.getPlacing(participants);
		if(task != -1) Bukkit.getScheduler().cancelTask(task);
		active.remove(this);
		if(giveRewards) {
			int i = 1;
			for(UUID p : placements.keySet()) {
				final RPPlayerData pdata = RPPlayerData.get(p);
				if(i <= recordPlacements) {
					final GlobalChallengePrize prize = GlobalChallengePrize.valueOf(i);
					pdata.addGlobalChallengePrize(prize);
					i += 1;
				}
				pdata.resetGlobalChallengeValue(this);
			}
		}
	}
	public static GlobalChallenge valueOf(String pathORname) {
		for(GlobalChallenge g : challenges) if(g.name.equals(pathORname) || g.path.equals(pathORname)) return g;
		return null;
	}
	public static GlobalChallenge valueOf(ItemStack display) {
		if(display != null && display.hasItemMeta() && display.getItemMeta().hasDisplayName())
			for(GlobalChallenge g : active)
				if(g.getDisplayItem().getItemMeta().getDisplayName().equals(display.getItemMeta().getDisplayName()))
					return g;
		return null;
	}
}
