package me.randomHashTags.RandomPackage.utils.classes.coinflips;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.OfflinePlayer;
import org.bukkit.inventory.ItemStack;

public class CoinFlipMatch {
	public static final List<CoinFlipMatch> matches = new ArrayList<>();
	private final int configSlot;
	private final long startedtime, expirationtime;
	private final OfflinePlayer creator;
	private final double wager, tax;
	private final ItemStack chosen;
	private final String display;
	private int slot;
	public CoinFlipMatch(int configSlot, long startedtime, OfflinePlayer creator, double wager, double tax, ItemStack chosen, String display, long expirationtime) {
		this.configSlot = configSlot;
		this.startedtime = startedtime;
		this.creator = creator;
		this.wager = wager;
		this.tax = tax;
		this.chosen = chosen;
		this.display = display;
		this.expirationtime = expirationtime;
		this.slot = matches.size();
		matches.add(this);
	}
	public int getConfigSlot() { return configSlot; }
	public long getStartedTime() { return startedtime; }
	public OfflinePlayer getCreator() { return creator; }
	public double getWager() { return wager; }
	public double getTax() { return tax; }
	public ItemStack getChosen() { return chosen; }
	public String getDisplay() { return display; }
	public long getExpirationTime() { return expirationtime; }
	public void delete() { matches.remove(this); }
	
	public static CoinFlipMatch valueOf(int slot) {
		return matches.size() > slot ? matches.get(slot) : null;
	}
	public static CoinFlipMatch valueOf(OfflinePlayer player) {
		for(CoinFlipMatch c : matches) if(c.getCreator() != null && c.getCreator().equals(player)) return c;
		return null;
	}
	public int getSlot() { return slot; }
	public void setSlot(int sl) { this.slot = sl; }
}
