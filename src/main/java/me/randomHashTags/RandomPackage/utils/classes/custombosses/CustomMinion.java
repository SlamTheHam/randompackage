package me.randomHashTags.RandomPackage.utils.classes.custombosses;

import java.util.ArrayList;
import java.util.List;

public class CustomMinion {
	public static final List<CustomMinion> minions = new ArrayList<>();

	public final String type, path;
	public final int max;
	public final List<String> attributes;
	public CustomMinion(int max, String type, String path, List<String> attributes) {
		this.max = max;
		this.type = type;
		this.path = path;
		this.attributes = attributes;
		minions.add(this);
	}
	public static CustomMinion valueOf(String path) {
	    for(CustomMinion m : minions)
	        if(m.path.equals(path))
	            return m;
	    return null;
    }
}
