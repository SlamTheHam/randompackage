package me.randomHashTags.RandomPackage.utils.classes.duels;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import me.randomHashTags.RandomPackage.RPPlayerData;
import me.randomHashTags.RandomPackage.RandomPackage;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerTeleportEvent;
import org.bukkit.inventory.ItemStack;

import me.randomHashTags.RandomPackage.api.events.duels.DuelEndEvent;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.plugin.PluginManager;

public class ActiveDuel {
	private static final YamlConfiguration config = YamlConfiguration.loadConfiguration(new File(RandomPackage.getPlugin.getDataFolder() + File.separator + "Features", "duels.yml"));
	private static final PluginManager pm = Bukkit.getPluginManager();
	public static final List<ActiveDuel> activeDuels = new ArrayList<>();
	public final Player requester, target;
	private final Location loc1, loc2;
	private final PlayerInventory inv1, inv2;
	private final PlayerTeleportEvent.TeleportCause cause = PlayerTeleportEvent.TeleportCause.PLUGIN;
	public final DuelSettings settings;
	public final List<ItemStack> stakes;
	public final DuelType type;
	public ActiveDuel(Player requester, Player target, DuelSettings settings, List<ItemStack> stakes, DuelType type) {
		this.requester = requester;
		this.inv1 = requester.getInventory();
		this.loc1 = requester.getLocation();
		this.target = target;
		this.inv2 = target.getInventory();
		this.loc2 = target.getLocation();
		this.settings = settings;
		this.stakes = stakes;
		this.type = type;
		activeDuels.add(this);

		final DuelArena arena = settings.arena;
		requester.teleport(arena.location1, cause);
		target.teleport(arena.location2, cause);

		final DuelKit kit = settings.kit;
		if(kit != null) {
			final ItemStack[] c = kit.contents;
			requester.getInventory().setContents(c);
			target.getInventory().setContents(c);
		} else {

		}
		requester.updateInventory();
		target.updateInventory();
	}
	public void end(Player winner) {
		final Player loser = requester.equals(winner) ? target : requester;
		final String l = loser.getName(), w = winner.getName();
		final DuelArena arena = settings.arena;
		pm.callEvent(new DuelEndEvent(settings, arena));
		for(String s : config.getStringList("messages.defeated broadcast")) {
			s = s.replace("{WINNER}", w).replace("{OPPONENT}", l);
			Bukkit.broadcastMessage(ChatColor.translateAlternateColorCodes('&', s));
		}
		final RPPlayerData pdata1 = RPPlayerData.get(winner.getUniqueId()), pdata2 = RPPlayerData.get(loser.getUniqueId());
		final int winnerELO = pdata1.duelELO, loserELO = pdata2.duelELO, lostELO = 0, wonELO = 0;
		final String wELO = Integer.toString(winnerELO), lELO = Integer.toString(loserELO), T;
		if(type.equals(DuelType.RANKED)) {
			pdata1.duelELO += wonELO;
			pdata2.duelELO -= lostELO;
			T = "ranked";
		} else {
			T = "unranked";
		}
		for(String s : config.getStringList("messages.defeated " + T)) {
			s = s.replace("{OPPONENT}", w).replace("{LOST_ELO}", Integer.toString(lostELO)).replace("{BEFORE}", lELO).replace("{AFTER}", Integer.toString(pdata1.duelELO));
			loser.sendMessage(ChatColor.translateAlternateColorCodes('&', s));
		}
		for(String s : config.getStringList("messages.won " + T)) {
			s = s.replace("{OPPONENT}", l).replace("{WON_ELO}", Integer.toString(wonELO)).replace("{BEFORE}", wELO).replace("{AFTER}", Integer.toString(pdata2.duelELO));
			winner.sendMessage(ChatColor.translateAlternateColorCodes('&', s));
		}

		arena.queue = arena.queue-2;
		activeDuels.remove(this);

		requester.getInventory().setContents(inv1.getContents());
		target.getInventory().setContents(inv2.getContents());
		requester.updateInventory();
		target.updateInventory();

		requester.teleport(loc1, cause);
		target.teleport(loc2, cause);
	}
	public static ActiveDuel valueOf(Player player) {
		for(ActiveDuel a : activeDuels)
			if(a.requester.equals(player) || a.target.equals(player))
				return a;
		return null;
	}
}
