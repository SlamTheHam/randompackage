package me.randomHashTags.RandomPackage.utils.classes.globalchallenges;

import me.randomHashTags.RandomPackage.RandomPackageAPI;
import org.bukkit.inventory.ItemStack;

import java.util.*;

public class GlobalChallengePrize {
    public static final List<GlobalChallengePrize> prizes = new ArrayList<>();
    private static final RandomPackageAPI api = RandomPackageAPI.getAPI();
    private final ItemStack display;
    private final int placement, amount;
    private final List<String> rewards;
    public GlobalChallengePrize(ItemStack display, int amount, int placement, List<String> rewards) {
        this.display = display;
        this.amount = amount;
        this.placement = placement;
        this.rewards = rewards;
        prizes.add(this);
    }
    public ItemStack getDisplay() { return display; }
    public int getAmount() { return amount; }
    public int getPlacement() { return placement; }
    public List<String> getRewards() { return rewards; }
    public LinkedHashMap<String, ItemStack> getRandomRewards() {
        final Random random = new Random();
        final LinkedHashMap<String, ItemStack> rewards = new LinkedHashMap<>();
        final List<String> availableRewards = new ArrayList<>(this.rewards);
        int amount = 0;
        for(int i = 0; i < availableRewards.size(); i++) {
            final String s = availableRewards.get(i);
            if(!s.toLowerCase().startsWith("chance=")) {
                rewards.put(s, api.d(null, s, 0));
                availableRewards.remove(s);
                amount += 1;
                i -= 1;
            }
        }
        for(int i = amount; i < this.amount; i++) {
            if(availableRewards.isEmpty()) return rewards;
            final String randomReward = availableRewards.get(random.nextInt(availableRewards.size()));
            final int chance = api.getRemainingInt(randomReward.split(";")[0]);
            if(randomReward.toLowerCase().startsWith("chance=") && random.nextInt(100) <= chance) {
                final String target = randomReward.split("chance=" + chance + ";")[1];
                if(target.contains("||")) {
                    final String[] t = target.split("\\|\\|");
                    final String ta = t[random.nextInt(t.length)];
                    rewards.put(ta, api.d(null, ta, 0));
                } else {
                    rewards.put(target, api.d(null, target, 0));
                }
                availableRewards.remove(randomReward);
            } else {
                i -= 1;
            }
        }
        return rewards;
    }
    public static GlobalChallengePrize valueOf(int placement) {
        for(GlobalChallengePrize p : prizes)
            if(p.getPlacement() == placement)
                return p;
        return null;
    }
    public static GlobalChallengePrize valueOf(ItemStack display) {
        if(display != null && display.hasItemMeta())
            for(GlobalChallengePrize p : prizes) {
                final ItemStack d = p.getDisplay();
                if(d.getType().equals(display.getType()) && d.getData().getData() == display.getData().getData() && d.getItemMeta().equals(display.getItemMeta()))
                    return p;
            }

        return null;
    }
}
