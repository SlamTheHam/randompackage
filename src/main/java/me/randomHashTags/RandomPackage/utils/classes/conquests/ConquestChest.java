package me.randomHashTags.RandomPackage.utils.classes.conquests;

import org.bukkit.Chunk;
import org.bukkit.Location;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ConquestChest {
    public static final List<ConquestChest> types = new ArrayList<>();

    public final String path, spawnRegion, rewardSize;
    public final HashMap<ConquestMob, String> spawnedBosses;
    public final int spawnInterval, healthMsgRadius, maxHealth, spawnedHealth, dmgPerHit, firstAnnounced, announceIntervalAfterSpawned, despawnDelay;
    public final double dmgDelay;
    public final ItemStack placedBlock;
    public final List<String> rewards, hitAttributes, spawnMsg, willSpawnMsg, stillAliveMsg, healthMsg, unlockedMsg;

    public ConquestChest(String path, int spawnInterval, String spawnRegion, int maxHealth, int spawnedHealth, int dmgPerHit, double dmgDelay, HashMap<ConquestMob, String> spawnedBosses, ItemStack placedBlock, String rewardSize, List<String> rewards, List<String> hitAttributes, List<String> spawnMsg, List<String> willSpawnMsg, int firstAnnounced, int announceIntervalAfterSpawned, List<String> stillAliveMsg, int healthMsgRadius, List<String> healthMsg, List<String> unlockedMsg, int despawnDelay) {
        this.path = path;
        this.spawnInterval = spawnInterval;
        this.spawnRegion = spawnRegion;
        this.maxHealth = maxHealth;
        this.spawnedHealth = spawnedHealth;
        this.dmgPerHit = dmgPerHit;
        this.dmgDelay = dmgDelay;
        this.spawnedBosses = spawnedBosses;
        this.placedBlock = placedBlock;
        this.rewardSize = rewardSize;
        this.rewards = rewards;
        this.hitAttributes = hitAttributes;
        this.spawnMsg = spawnMsg;
        this.willSpawnMsg = willSpawnMsg;
        this.firstAnnounced = firstAnnounced;
        this.announceIntervalAfterSpawned = announceIntervalAfterSpawned;
        this.stillAliveMsg = stillAliveMsg;
        this.healthMsgRadius = healthMsgRadius;
        this.healthMsg = healthMsg;
        this.unlockedMsg = unlockedMsg;
        this.despawnDelay = despawnDelay;
        types.add(this);
    }

    public LivingConquestChest spawn(Location l) {
        final Chunk c = l.getChunk();
        if(!c.isLoaded()) c.load();
        return new LivingConquestChest(l, this, System.currentTimeMillis(), true, true);
    }
    public static ConquestChest valueOf(String path) {
        for(ConquestChest cc : types)
            if(cc.path.equals(path))
                return cc;
        return null;
    }
}
