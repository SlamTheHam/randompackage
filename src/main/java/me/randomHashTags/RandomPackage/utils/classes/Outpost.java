package me.randomHashTags.RandomPackage.utils.classes;

import org.bukkit.Location;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.List;

public class Outpost {
    public static final List<Outpost> outposts = new ArrayList<>();

    public String controllingFaction, attackingFaction;
    public long captureTime;
    public double controllingPercent;

    public final String path;
    public final int slot;
    public final Location teleportLocation, center;
    public final double captureRadius;
    private final ItemStack display;
    public final List<String> limits, rewards;

    public Outpost(String path, int slot, Location teleportLocation, Location center, double captureRadius, ItemStack display, List<String> limits, List<String> rewards) {
        this.path = path;
        this.slot = slot;
        this.teleportLocation = teleportLocation;
        this.center = center;
        this.captureRadius = captureRadius;
        this.display = display;
        this.limits = limits;
        this.rewards = rewards;
        outposts.add(this);
    }
    public ItemStack getDisplay() { return display.clone(); }

    public static Outpost valueOf(String path) {
        for(Outpost o : outposts)
            if(o.path.equals(path))
                return o;
        return null;
    }
    public static Outpost valueOf(int slot) {
        for(Outpost o : outposts)
            if(o.slot == slot)
                return o;
        return null;
    }
}
