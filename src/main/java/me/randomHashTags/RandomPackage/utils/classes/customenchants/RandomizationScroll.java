package me.randomHashTags.RandomPackage.utils.classes.customenchants;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.inventory.ItemStack;

public class RandomizationScroll {
	public static List<RandomizationScroll> scrolls = new ArrayList<RandomizationScroll>();
	private final int number;
	private final ItemStack is;
	private final List<EnchantRarity> appliesto;
	public RandomizationScroll(int number, ItemStack is, List<EnchantRarity> appliesto) {
		this.number = number;
		this.is = is;
		this.appliesto = appliesto;
		scrolls.add(this);
	}
	public int getNumberInConfig() { return number; }
	public ItemStack getItemStack() { return is.clone(); }
	public List<EnchantRarity> getAppliesToRarities() { return appliesto; }
	
	public static RandomizationScroll valueOf(int number) {
		for(RandomizationScroll r : scrolls) if(r.getNumberInConfig() == number) return r;
		return null;
	}
	public static RandomizationScroll valueOf(ItemStack is) {
		if(is != null && is.hasItemMeta() && is.getItemMeta().hasDisplayName() && is.getItemMeta().hasLore()) for(RandomizationScroll r : scrolls) if(r.getItemStack().getItemMeta().equals(is.getItemMeta())) return r;
		return null;
	}
}
