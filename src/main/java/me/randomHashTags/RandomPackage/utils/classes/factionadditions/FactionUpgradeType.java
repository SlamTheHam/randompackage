package me.randomHashTags.RandomPackage.utils.classes.factionadditions;

import java.util.ArrayList;
import java.util.List;

public class FactionUpgradeType {
	private static final List<FactionUpgradeType> types = new ArrayList<>();
	public final String path, perkAchievedPrefix, perkUnachievedPrefix, requirementsPrefix;
	public final List<String> unlock, upgrade, maxed, format;
	public final boolean itemAmountEqualsTier;
	public FactionUpgradeType(String path, String perkAchievedPrefix, String perkUnachievedPrefix, String requirementsPrefix, List<String> unlock, List<String> upgrade, List<String> maxed, List<String> format, boolean itemAmountEqualsTier) {
		this.path = path;
		this.perkAchievedPrefix = perkAchievedPrefix;
		this.perkUnachievedPrefix = perkUnachievedPrefix;
		this.requirementsPrefix = requirementsPrefix;
		this.unlock = unlock;
		this.upgrade = upgrade;
		this.maxed = maxed;
		this.format = format;
		this.itemAmountEqualsTier = itemAmountEqualsTier;
		types.add(this);
	}
	public static FactionUpgradeType valueOf(String path) {
		for(FactionUpgradeType t : types) if(t.path.equals(path)) return t;
		return null;
	}
}
