package me.randomHashTags.RandomPackage.utils.classes.customenchants;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.inventory.ItemStack;

public class EnchantmentOrb {
	public static List<EnchantmentOrb> enchantmentorbs = new ArrayList<>();
	public static HashMap<Integer, List<EnchantmentOrb>> numbers = new HashMap<>();
	private final ItemStack is;
	private final String appliedlore;
	private final List<String> appliesto;
	private final int number, maxenchants, percentlore, increment;
	public EnchantmentOrb(int number, ItemStack is, String appliedlore, List<String> appliesto, int maxenchants, int increment) {
		this.number = number;
		this.is = is;
		int q = 0;
		if(is != null && is.hasItemMeta() && is.getItemMeta().hasDisplayName() && is.getItemMeta().hasLore())
			for(int i = 0; i < is.getItemMeta().getLore().size(); i++)
				if(is.getItemMeta().getLore().get(i).contains("{PERCENT}")) 
					q = i;
		percentlore = q;
		this.appliesto = appliesto;
		this.maxenchants = maxenchants;
		this.appliedlore = ChatColor.translateAlternateColorCodes('&', appliedlore.replace("{SLOTS}", Integer.toString(maxenchants).replace("{ADD_SLOTS}", Integer.toString(increment))));
		this.increment = increment;
		enchantmentorbs.add(this);
		if(!numbers.keySet().contains(number)) numbers.put(number, new ArrayList<>());
		numbers.get(number).add(this);
	}
	public int getNumberInConfig() { return number; }
	public ItemStack getItemStack() { return is.clone(); }
	public String getApplyLore() { return appliedlore; }
	public List<String> getAppliesTo() { return appliesto; }
	public int getMaxAllowableEnchants() { return maxenchants; }
	public int getPercentLoreSlot() { return percentlore; }
	public int getIncrement() { return increment; }
	@SuppressWarnings("deprecation")
	public static EnchantmentOrb valueOf(ItemStack is) {
		if(is != null && is.hasItemMeta() && is.getItemMeta().hasDisplayName() && is.getItemMeta().hasLore()) {
			for(EnchantmentOrb orb : enchantmentorbs) {
				final ItemStack its = orb.getItemStack();
				final List<String> itsL = new ArrayList<>(its.getItemMeta().getLore());
				itsL.remove(orb.getPercentLoreSlot());
				if(is.getType().equals(its.getType()) && is.getData().getData() == its.getData().getData() && is.getItemMeta().getDisplayName().equals(its.getItemMeta().getDisplayName())
					&& is.getItemMeta().getLore().containsAll(itsL))
					return orb;
			}
		}
		return null;
	}
	public static EnchantmentOrb valueOf(String appliedlore) {
		if(appliedlore != null) {
			for(EnchantmentOrb orb : enchantmentorbs)
				if(orb.getApplyLore().equals(appliedlore))
					return orb;
		}
		return null;
	}
	public static EnchantmentOrb valueOf(int number) {
		for(EnchantmentOrb orb : enchantmentorbs)
			if(orb.getNumberInConfig() == number) return orb;
		return null;
	}
	public static boolean hasOrb(ItemStack is) {
		if(is != null && is.hasItemMeta() && is.getItemMeta().hasDisplayName() && is.getItemMeta().hasLore())
			for(EnchantmentOrb orb : enchantmentorbs)
				if(is.getItemMeta().getLore().contains(orb.getApplyLore()))
					return true;
		return false;
	}
	public static EnchantmentOrb getOrb(ItemStack is) {
		if(is != null && is.hasItemMeta() && is.getItemMeta().hasLore()) {
			for(EnchantmentOrb e : enchantmentorbs)
				if(is.getItemMeta().getLore().contains(e.getApplyLore()))
					return e;
		}
		return null;
	}
}
