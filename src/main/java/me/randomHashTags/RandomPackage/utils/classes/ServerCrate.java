package me.randomHashTags.RandomPackage.utils.classes;

import java.util.*;

import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import me.randomHashTags.RandomPackage.RandomPackageAPI;

public class ServerCrate {
	public static final List<ServerCrate> crates = new ArrayList<>();
	private static final RandomPackageAPI api = RandomPackageAPI.getAPI();

	private final Random random = new Random();
	private final String path, displayrarity, bossReward;
	private final int redeemableItems;
	private final Inventory inv;
	private final LinkedHashMap<String, Integer> revealChances;
	private final ItemStack physicalItem, display, opengui, selected, revealSlotRarity, background, background2;
	private final HashMap<String, List<String>> rewards;
	private final List<Integer> selectableslots;
	private final ServerCrateFlare flare;
	public ServerCrate(String path, int redeemableItems, List<Integer> selectableslots, String displayrarity, Inventory inv, LinkedHashMap<String, Integer> revealChances, ItemStack physicalItem, String bossReward, ItemStack display, ItemStack opengui, ItemStack selected, ItemStack revealSlotRarity, HashMap<String, List<String>> rewards, ItemStack background, ItemStack background2, ServerCrateFlare flare) {
		this.path = path;
		this.redeemableItems = redeemableItems;
		this.selectableslots = selectableslots;
		this.displayrarity = displayrarity;
		this.inv = inv;
		this.revealChances = revealChances;
		this.physicalItem = physicalItem;
		this.bossReward = bossReward;
		this.display = display;
		this.opengui = opengui;
		this.selected = selected;
		this.revealSlotRarity = revealSlotRarity;
		this.rewards = rewards;
		this.background = background;
		this.background2 = background2;
		this.flare = flare;
		crates.add(this);
	}
	public String getPath() { return path; }
	public int getRedeemableItems() { return redeemableItems; }
	public String getDisplayRarity() { return displayrarity; }
	public List<Integer> getSelectableSlots() { return selectableslots; }
	public Inventory getInventory() { return inv; }
	public LinkedHashMap<String, Integer> getRevealChances() { return revealChances; }
	public ItemStack getPhyiscalItem() { return physicalItem; }
	public String getBossReward() { return bossReward; }
	public ItemStack getDisplay() { return display.clone(); }
	public ItemStack getOpenGui() { return opengui.clone(); }
	public ItemStack getSelected() { return selected.clone(); }
	public ItemStack getRevealSlotRarity() { return revealSlotRarity.clone(); }
	public HashMap<String, List<String>> getRewards() { return rewards; }
	public ItemStack getBackground() { return background.clone(); }
	public ItemStack getBackground2() { return background2.clone(); }
	public ServerCrateFlare getFlare() { return flare; }
	
	public ServerCrate getRandomRarity(boolean useChances) {
		String rarity = null;
		if(!useChances) {
			rarity = (String) rewards.keySet().toArray()[random.nextInt(rewards.keySet().size())];
		} else {
			for(String s : rewards.keySet()) if(random.nextInt(100) <= revealChances.get(s)) rarity = s;
			if(rarity == null) rarity = (String) revealChances.keySet().toArray()[revealChances.keySet().size()-1];
		}
		return valueOf(rarity);
	}
	public ItemStack getRandomReward(String rarity) {
		final String reward = rewards.get(rarity).get(random.nextInt(rewards.get(rarity).size()));
		return api.d(null, reward, 0);
	}
	
	public static ServerCrate valueOf(String path) {
		for(ServerCrate crate : crates) if(crate.getPath().equals(path)) return crate;
		return null;
	}
	public static ServerCrate valueOf(ItemStack serverCrateItem) {
		for(ServerCrate crate : crates)
			if(crate.getPhyiscalItem().getItemMeta().equals(serverCrateItem.getItemMeta()))
				return crate;
		return null;
	}
	
}
