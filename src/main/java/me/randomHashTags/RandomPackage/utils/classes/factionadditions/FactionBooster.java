package me.randomHashTags.RandomPackage.utils.classes.factionadditions;

import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.List;

public class FactionBooster {
    public static final List<FactionBooster> boosters = new ArrayList<>();
    public final String path;
    public final ItemStack item;
    public FactionBooster(String path, ItemStack item, long expiration) {
        this.path = path;
        this.item = item;
        boosters.add(this);
    }
}
