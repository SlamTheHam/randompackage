package me.randomHashTags.RandomPackage.utils.classes.custombosses;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.bukkit.Location;
import org.bukkit.entity.LivingEntity;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scoreboard.DisplaySlot;

import me.randomHashTags.RandomPackage.utils.universal.UVersion;

@SuppressWarnings({"deprecation"})
public class CustomBoss extends UVersion {
	public static final List<CustomBoss> bosses = new ArrayList<>();

	public final String path, type, name, scoreboardTitle;
	public final DisplaySlot scoreboardSlot;
	private final ItemStack spawnitem;
	public final List<String> attributes, rewards, scores;
	public final HashMap<Integer, List<String>> messages;
	public final int messageRadius, maxMinions;
	public final List<CustomBossAttack> attacks;
	public final CustomMinion minion;
	public CustomBoss(String path, ItemStack spawnitem, String type, String name, String scoreboardTitle, DisplaySlot scoreboardSlot, List<String> scores, List<String> attributes, List<String> rewards, List<CustomBossAttack> attacks, int messageRadius, HashMap<Integer, List<String>> messages, CustomMinion minion, int maxMinions) {
		this.path = path;
		this.spawnitem = spawnitem;
		this.type = type;
		this.name = name;
		this.scoreboardTitle = scoreboardTitle;
		this.scoreboardSlot = scoreboardSlot;
		this.scores = scores;
		this.attributes = attributes;
		this.rewards = rewards;
		this.attacks = attacks;
		this.messageRadius = messageRadius;
		this.messages = messages;
		this.minion = minion;
		this.maxMinions = maxMinions;
		bosses.add(this);
	}
	public ItemStack getSpawnItem() { return spawnitem.clone(); }

	public LivingCustomBoss spawn(LivingEntity summoner, Location location) {
		return new LivingCustomBoss(summoner, getEntity(type, location, true), this);
	}
	
	public static CustomBoss valueOf(String path) {
		for(CustomBoss b : bosses) if(b.path.equals(path)) return b;
		return null;
	}
	public static CustomBoss valueOf(ItemStack spawnitem) {
		if(spawnitem != null && spawnitem.hasItemMeta())
			for(CustomBoss c : bosses)
				if(c.getSpawnItem().getItemMeta().equals(spawnitem.getItemMeta()))
					return c;
		return null;
	}
}
