package me.randomHashTags.RandomPackage.utils.classes.factionadditions;

import java.util.ArrayList;
import java.util.List;

public class ActiveFactionBooster {
    public static final List<ActiveFactionBooster> active = new ArrayList<>();
    public final String faction;
    public final FactionBooster type;
    public long startedTime, expirationTime;
    public ActiveFactionBooster(long startedTime, long expirationTime, String faction, FactionBooster type) {
        this.startedTime = startedTime;
        this.expirationTime = expirationTime;
        this.faction = faction;
        this.type = type;
        active.add(this);
    }
}
