package me.randomHashTags.RandomPackage.api;

import me.randomHashTags.RandomPackage.RandomPackageAPI;
import me.randomHashTags.RandomPackage.utils.classes.StackedEntity;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.World;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.*;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

public class MobStacker extends RandomPackageAPI implements Listener {
    public boolean isEnabled = false;
    private static MobStacker instance;
    public static final MobStacker getMobStacker() {
        if(instance == null) instance = new MobStacker();
        return instance;
    }

    private int task = -1, max = -1;
    private double radius = 5.00;
    private String defaultName;
    public List<EntityType> stackable = new ArrayList<>();
    public HashMap<EntityType, String> customNames = new HashMap<>();
    private HashMap<UUID, LivingEntity> lastDamager = new HashMap<>();
    private boolean stacksViaNatural = false, stacksViaSpawner = false, stacksViaEgg = false;


    public void enable() {
        if(isEnabled) return;
        pluginmanager.registerEvents(this, randompackage);
        isEnabled = true;
        final FileConfiguration config = randompackage.getConfig();
        defaultName = ChatColor.translateAlternateColorCodes('&', config.getString("mob stacker.default"));
        for(String s : config.getStringList("mob stacker.stackable")) {
            stackable.add(EntityType.valueOf(s.toUpperCase()));
        }
        for(EntityType t : EntityType.values()) {
            final String s = config.getString("mob stacker." + t.name());
            customNames.put(t, s != null ? s : defaultName.replace("{TYPE}", t.name().replace("_", " ")));
        }
        max = config.getInt("mob stacker.max");
        radius = config.getDouble("mob stacker.stack radius");
        task = Bukkit.getScheduler().scheduleSyncRepeatingTask(randompackage, () -> stackEntities(), 0, config.getInt("mob stacker.checks"));
        loadBackup();
    }
    public void disable() {
        if(!isEnabled) return;
        isEnabled = false;
        Bukkit.getScheduler().cancelTask(task);
        stackable.clear();
        customNames.clear();
        lastDamager.clear();
        backup();
        HandlerList.unregisterAll(this);
    }

    public void backup() {
        final List<StackedEntity> se = StackedEntity.stackedEntities;
        otherdata.set("stacked mobs", null);
        for(StackedEntity e : se) {
            if(e.creationTime != 0) {
                final UUID u = e.uuid;
                otherdata.set("stacked mobs." + u + ".creation", e.creationTime);
                otherdata.set("stacked mobs." + u + ".size", e.size);
            }
        }
        saveOtherData();
        se.clear();
    }
    public void loadBackup() {
        final long started = System.currentTimeMillis();
        int loaded = 0;
        final ConfigurationSection o = otherdata.getConfigurationSection("stacked mobs");
        if(o != null) {
            for(String s : o.getKeys(false)) {
                final Entity e = getEntity(UUID.fromString(s));
                if(e != null && !e.isDead() && e instanceof LivingEntity) {
                    new StackedEntity(otherdata.getLong("stacked mobs." + s + ".creation"), (LivingEntity) e, customNames.get(e.getType()), otherdata.getInt("stacked mobs." + s + ".size"));
                    loaded += 1;
                }
            }
        }
        Bukkit.getConsoleSender().sendMessage(ChatColor.translateAlternateColorCodes('&', "&6[RandomPackage] &aLoaded " + loaded + " stacked mobs &e(took " + (System.currentTimeMillis()-started) + "ms)"));
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    private void creatureSpawnEvent(CreatureSpawnEvent event) {
        if(!event.isCancelled()) {
            final String sr = event.getSpawnReason().name();
            if(sr.contains("NATURAL") || sr.contains("CHUNK_GEN")) {
            } else if(sr.contains("EGG")) {
            } else if(sr.contains("SPAWNER")) {
            }
        }
    }
    public void stackEntities() {
        for(World w : Bukkit.getWorlds()) {
            for(EntityType t : EntityType.values()) {
                stackEntities(w, t);
            }
        }
    }
    public void stackEntities(World w, EntityType type) {
        if(!stackable.contains(type)) return;
        final List<Entity> entities = new ArrayList<>();
        for(Entity e : w.getEntities())
            if(e.getType().equals(type))
                entities.add(e);
        final String name = customNames.get(type);
        for(int i = 0; i < entities.size(); i++) {
            final Entity e = entities.get(i);
            final UUID u = e.getUniqueId();
            StackedEntity s = StackedEntity.valueOf(u);
            final EntityType t = e.getType();
            final List<Entity> nearby = e.getNearbyEntities(radius, radius, radius);
            for(int o = 0; o < nearby.size(); o++) {
                final Entity E = nearby.get(o);
                final StackedEntity se = StackedEntity.valueOf(E.getUniqueId());
                if(!e.isDead() && !E.isDead() && E.getType().equals(t) && (se != null || E.getCustomName() == null)) {
                    if(se != null && (max == -1 || se.size+1 <= max)) {
                        se.merge((LivingEntity) e);
                    } else if(s != null && (max == -1 || s.size+1 <= max)) {
                        s.merge((LivingEntity) E);
                    } else {
                        s = new StackedEntity(System.currentTimeMillis(), (LivingEntity) e, name, 1);
                        s.merge((LivingEntity) E);
                    }
                }
            }
        }
    }
    @EventHandler
    private void entityDeathEvent(EntityDeathEvent event) {
        final UUID u = event.getEntity().getUniqueId();
        final StackedEntity s = StackedEntity.valueOf(u);
        if(s != null)
            s.kill(null, 1);
    }
    @EventHandler
    private void entityDamageEvent(EntityDamageEvent event) {
        final String cause = event.getCause().name();
        final Entity e = event.getEntity();
        final UUID u = e.getUniqueId();
        final StackedEntity s = StackedEntity.valueOf(u);
        if(s != null && (cause.contains("LAVA") || cause.contains("FIRE"))) {
            final LivingEntity le = (LivingEntity) e;
            if(le.getHealth() - event.getFinalDamage() <= 0.000) {
                le.setHealth(le.getMaxHealth());
                le.setFireTicks(0);
                s.kill(null, 1);
            }
            if(s.size == 1) StackedEntity.stackedEntities.remove(s);
        }
    }
    @EventHandler(priority = EventPriority.HIGHEST)
    private void entityDamageByEntityEvent(EntityDamageByEntityEvent event) {
        final Entity entity = event.getEntity(), dam = event.getDamager();
        final LivingEntity e = entity instanceof LivingEntity ? (LivingEntity) entity : null, damager = e != null && dam instanceof LivingEntity ? (LivingEntity) dam : null;
        if(!event.isCancelled() && e != null && !event.getEntityType().equals(EntityType.PLAYER)) {
            final StackedEntity s = StackedEntity.valueOf(e.getUniqueId());
            if(s != null && e.getHealth()-event.getFinalDamage() <= 0.000) {
                event.setDamage(0);
                e.setHealth(e.getMaxHealth());
                s.kill(damager, 1);
            }
            if(s != null && s.size == 1) StackedEntity.stackedEntities.remove(s);
        }
    }
}
