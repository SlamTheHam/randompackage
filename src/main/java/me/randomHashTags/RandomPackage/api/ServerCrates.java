package me.randomHashTags.RandomPackage.api;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.UUID;

import me.randomHashTags.RandomPackage.utils.universal.UMaterial;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.OfflinePlayer;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import me.randomHashTags.RandomPackage.RandomPackageAPI;
import me.randomHashTags.RandomPackage.utils.classes.ServerCrate;
import me.randomHashTags.RandomPackage.utils.classes.ServerCrateFlare;

public class ServerCrates extends RandomPackageAPI implements Listener {
	
	public boolean isEnabled = false;
	private static ServerCrates instance;
	public static final ServerCrates getServerCrates() {
	    if(instance == null) instance = new ServerCrates();
	    return instance;
	}

	public FileConfiguration config;
	
	public final List<ItemStack> crates = new ArrayList<>();
	private final HashMap<UUID, ServerCrate> revealingLoot = new HashMap<>();
	private final HashMap<UUID, HashMap<Integer, ServerCrate>> selectedSlots = new HashMap<>();
	private final HashMap<UUID, List<ItemStack>> revealingloot = new HashMap<>();
	private final HashMap<UUID, List<Integer>> tasks = new HashMap<>(), revealedslots = new HashMap<>();
	private final List<UUID> canRevealRarities = new ArrayList<>();


	public void enable() {
	    final long started = System.currentTimeMillis();
		if(isEnabled) return;
		save("Features", "server crates.yml");
		isEnabled = true;
		pluginmanager.registerEvents(this, randompackage);
		config = YamlConfiguration.loadConfiguration(new File(randompackage.getDataFolder() + File.separator + "Features" + File.separator, "server crates.yml"));
		final List<ItemStack> flares = new ArrayList<>();
		final int defaultSize = config.getInt("crates.default-settings.size"), defaultRevealItems = config.getInt("crates.default-settings.redeemable-items"), defaultSpawnRadius = config.getInt("default-flare-settings.spawn-radius"), defaultSpawnInDelay = config.getInt("default-flare-settings.spawn-in-delay"), defaultNearbyRadius = config.getInt("default-flare-settings.nearby-radius");
		final List<String> defaultFormat = config.getStringList("crates.default-settings.format");
		final ItemStack defaultBackground = d(config, "crates.default-settings.background", 0), defaultBackground2 = d(config, "crates.default-settings.background-2", 0);
		int loaded = 0 ;
		for(String s : config.getConfigurationSection("crates").getKeys(false)) {
			if(!s.equals("default-settings")) {
				final ItemStack opengui = d(config, "crates." + s + ".open-gui", 0),
						background = config.get("crates." + s + ".background") != null ? d(config, "crates." + s + ".background", 0) : defaultBackground, background2 = config.get("crates." + s + ".background-2") != null ? d(config, "crates." + s + ".background2", 0) : defaultBackground2;
				Inventory inv = Bukkit.createInventory(null, config.get("crates." + s + ".size") != null ? config.getInt("crates." + s + ".size") : defaultSize, ChatColor.translateAlternateColorCodes('&', config.getString("crates." + s + ".title")));
				final int revealitems = config.get("crates." + s + ".redeemable-items") != null ? config.getInt("crates." + s + ".redeemable-items") : defaultRevealItems;
				final List<String> format = config.get("crates." + s + ".format") != null ? config.getStringList("crates." + s + ".format") : defaultFormat;
				final LinkedHashMap<String, Integer> revealChances = new LinkedHashMap<>();
				for(String k : config.getConfigurationSection("crates." + s + ".reveal-chances").getKeys(false)) revealChances.put(k, config.getInt("crates." + s + ".reveal-chances." + k));
				final HashMap<String, List<String>> rewards = new HashMap<>();
				for(String k : config.getConfigurationSection("crates." + s + ".rewards").getKeys(false)) rewards.put(k, config.getStringList("crates." + s + ".rewards." + k));

				final List<Integer> pickableslots = new ArrayList<>();
				int a = 0;
				for(int f = 0; f < format.size(); f++) {
					for(int g = 0; g < format.get(f).length(); g++) {
						final int slot = (f*9)+g;
						final String slotString = format.get(f).substring(g, g+1);
						if(slotString.equals("-")) {
							item = background.clone();
						} else if(slotString.equals("+")) {
							a += 1;
							item = opengui.clone();
							pickableslots.add(slot);
						} else item = null;
						if(item != null) {
							itemMeta = item.getItemMeta(); lore.clear();
							if(item.hasItemMeta()) {
								if(itemMeta.hasDisplayName()) {
									if(itemMeta.getDisplayName().contains("{SLOT}")) itemMeta.setDisplayName(itemMeta.getDisplayName().replace("{SLOT}", Integer.toString(a)));
								}
								if(itemMeta.hasLore()) lore.addAll(itemMeta.getLore());
								itemMeta.setLore(lore); lore.clear();
								item.setItemMeta(itemMeta);
							}
						}
						inv.setItem(slot, item);
					}
				}
				final int spawnRadius = config.get("crates." + s + ".flare.spawn-radius") != null ? config.getInt("crates." + s + ".flare.spawn-radius") : defaultSpawnRadius, spawnInDelay = config.get("crates." + s + ".flare.spawn-in-delay") != null ? config.getInt("crates." + s + ".flare.spawn-in-delay") : defaultSpawnInDelay, nearbyRadius = config.get("crates." + s + ".flare.nearby-radius") != null ? config.getInt("crates." + s + ".flare.nearby-radius") : defaultNearbyRadius;
				final ServerCrateFlare scf = new ServerCrateFlare(d(config, "crates." + s + ".flare", 0), config.getStringList("crates." + s + ".flare.request-msg"), spawnRadius, spawnInDelay, nearbyRadius, config.getStringList("crates." + s + ".flare.nearby-spawn-msg"));
				final ServerCrate c = new ServerCrate(s, revealitems, pickableslots, ChatColor.translateAlternateColorCodes('&', config.getString("crates." + s + ".display.rarity")), inv, revealChances, d(config, "crates." + s, 0), ChatColor.translateAlternateColorCodes('&', config.getString("crates." + s + ".boss-reward")), d(config, "crates." + s + ".display", 0), opengui, d(config, "crates." + s + ".selected", 0), d(config, "crates." + s + ".reveal-slot-rarity", 0), rewards, background, background2, scf);
				scf.serverCrate = c;
				flares.add(scf.getItemStack().clone());
				crates.add(c.getPhyiscalItem().clone());
				loaded += 1;
			}
		}
		addGivedpCategory(crates, UMaterial.CHEST, "Server Crates", "Givedp: Server Crates");
		addGivedpCategory(flares, UMaterial.TORCH, "Server Crate Flares", "Givedp: Server Crate Flares");
		Bukkit.getConsoleSender().sendMessage(ChatColor.translateAlternateColorCodes('&', "&6[RandomPackage] &aLoaded " + loaded + " server crates and flares &e(took " + (System.currentTimeMillis()-started) + "ms)"));
	}
	public void disable() {
		if(!isEnabled) return;
		isEnabled = false;
		crates.clear();
		ServerCrate.crates.clear();
		for(UUID uuid : revealingLoot.keySet()) {
			final OfflinePlayer o = Bukkit.getOfflinePlayer(uuid);
			if(o != null && o.isOnline()) o.getPlayer().closeInventory();
		}
		selectedSlots.clear();
		revealingloot.clear();
		for(UUID u : tasks.keySet()) stopTasks(u);
		tasks.clear();
		revealedslots.clear();
		final HashMap<Location, ServerCrateFlare> a = new HashMap<>(ServerCrateFlare.locs);
		for(int i = 0; i < a.keySet().size(); i++) {
			final ServerCrateFlare f = (ServerCrateFlare) a.values().toArray()[i];
			f.destroy((Location) a.keySet().toArray()[i], true);
		}
		ServerCrateFlare.flares.clear();
		HandlerList.unregisterAll(this);
	}

	@EventHandler
	private void playerInteractEvent(PlayerInteractEvent event) {
		final ServerCrateFlare fl = event.getClickedBlock() != null ? ServerCrateFlare.valueOf(event.getClickedBlock().getLocation()) : null;
		if(fl != null) {
			fl.destroy(event.getClickedBlock().getLocation(), true);
		} else if(event.getItem() != null && event.getItem().hasItemMeta() && event.getAction().name().contains("RIGHT")) {
			final ServerCrate c = ServerCrate.valueOf(event.getItem());
			if(c != null) {
				final Inventory i = c.getInventory();
				final Player player = event.getPlayer();
				event.setCancelled(true);
				player.openInventory(Bukkit.createInventory(player, i.getSize(), i.getTitle()));
				player.getOpenInventory().getTopInventory().setContents(c.getInventory().getContents());
				player.updateInventory();
				removeItem(player, event.getItem(), 1);
				revealingLoot.put(player.getUniqueId(), c);
				selectedSlots.put(player.getUniqueId(), new HashMap<>());
			} else if(event.getClickedBlock() != null) {
				final ServerCrateFlare f = ServerCrateFlare.valueOf(event.getItem());
				if(f != null) {
					event.setCancelled(true);
					event.getPlayer().updateInventory();
					f.spawn(event.getPlayer(), event.getClickedBlock().getLocation());
					removeItem(event.getPlayer(), event.getItem(), 1);
				}
			}
		}
	}
	@EventHandler
	private void inventoryClickEvent(InventoryClickEvent event) {
		if(event.getCurrentItem() != null && event.getWhoClicked().getOpenInventory().getTopInventory().getHolder() == event.getWhoClicked()) {
			final Player player = (Player) event.getWhoClicked();
			final UUID uuid = player.getUniqueId();
			if(revealingLoot.keySet().contains(uuid))  {
				final int r = event.getRawSlot();
				final ServerCrate c = revealingLoot.get(uuid);
				event.setCancelled(true);
				player.updateInventory();
				if(!event.getClick().isLeftClick() && !event.getClick().isRightClick() || event.getCurrentItem().getType().equals(Material.AIR) || r >= event.getWhoClicked().getOpenInventory().getTopInventory().getSize()) return;
				if(c != null && c.getSelectableSlots().contains(r)) {
					if(selectedSlots.get(uuid).keySet().size() != c.getRedeemableItems()) {
						if(!selectedSlots.get(uuid).keySet().contains(r)) {
							selectedSlots.get(uuid).put(r, null);
							item = c.getSelected().clone(); itemMeta = item.getItemMeta(); lore.clear();
							if(item.hasItemMeta()) {
								if(itemMeta.hasDisplayName()) {
									if(itemMeta.getDisplayName().contains("{SLOT}")) itemMeta.setDisplayName(itemMeta.getDisplayName().replace("{SLOT}", Integer.toString(getRemainingInt(event.getCurrentItem().getItemMeta().getDisplayName()))));
								}
								if(itemMeta.hasLore()) lore.addAll(itemMeta.getLore());
								itemMeta.setLore(lore); lore.clear();
								item.setItemMeta(itemMeta);
							}
							player.getOpenInventory().getTopInventory().setItem(r, item);
						} else {
							item = c.getOpenGui().clone(); itemMeta = item.getItemMeta();
							if(itemMeta.hasDisplayName() && itemMeta.getDisplayName().contains("{SLOT}")) itemMeta.setDisplayName(itemMeta.getDisplayName().replace("{SLOT}", Integer.toString(getRemainingInt(event.getCurrentItem().getItemMeta().getDisplayName()))));
							item.setItemMeta(itemMeta);
							selectedSlots.get(uuid).remove(r);
							player.getOpenInventory().getTopInventory().setItem(r, item);
						}
						player.updateInventory();
						if(!tasks.keySet().contains(uuid) && selectedSlots.get(uuid).keySet().size() == c.getRedeemableItems()) {
							revealBackgroundLoot(player);
						}
					} else if(canRevealRarities.contains(uuid) && selectedSlots.get(uuid).keySet().contains(r) && selectedSlots.get(uuid).get(r) == null) {
						final ServerCrate sc = c.getRandomRarity(true);
						selectedSlots.get(uuid).put(r, sc);
						item = sc.getDisplay().clone(); itemMeta = item.getItemMeta(); lore.clear();
						if(item.hasItemMeta()) {
							if(itemMeta.hasLore()) {
								for(String s : itemMeta.getLore()) {
									if(s.contains("{SC_RARITY}")) s = s.replace("{SC_RARITY}", c.getDisplayRarity());
									if(s.contains("{LOOT_RARITY}")) s = s.replace("{LOOT_RARITY}", sc.getDisplayRarity());
									lore.add(s);
								}
								itemMeta.setLore(lore); lore.clear();
							}
							item.setItemMeta(itemMeta);
						}
						player.getOpenInventory().getTopInventory().setItem(event.getRawSlot(), item);
						player.updateInventory();
					} else if(selectedSlots.get(uuid).keySet().contains(r) && selectedSlots.get(uuid).get(r) != null) {
						if(!revealedslots.keySet().contains(uuid) || !revealedslots.get(uuid).contains(r)) {
							final ItemStack reward = revealingLoot.get(uuid).getRandomReward(selectedSlots.get(uuid).get(r).getPath());
							player.getOpenInventory().getTopInventory().setItem(r, reward);
							player.updateInventory();
							if(!revealingloot.keySet().contains(uuid)) revealingloot.put(uuid, new ArrayList<>());
							revealingloot.get(uuid).add(reward);
							if(!revealedslots.keySet().contains(uuid)) revealedslots.put(uuid, new ArrayList<>());
							revealedslots.get(uuid).add(r);
						}
					}
				}
			}
		}
	}
	
	private void stopTasks(UUID uuid) { if(tasks.keySet().contains(uuid)) { for(int i : tasks.get(uuid)) Bukkit.getScheduler().cancelTask(i); tasks.remove(uuid); } }
	private void revealBackgroundLoot(Player player) {
		final UUID uuid = player.getUniqueId();
		final ServerCrate c = revealingLoot.get(uuid);
		tasks.put(player.getUniqueId(), new ArrayList<>());
		List<Integer> background = new ArrayList<>();
		for(int i = 0; i < player.getOpenInventory().getTopInventory().getSize(); i++) {
			item = player.getOpenInventory().getTopInventory().getItem(i);
			if(item != null && !item.equals(c.getBackground()) && !selectedSlots.get(uuid).keySet().contains(i)) background.add(i);
		}
		final int bgSize = background.size();
		final List<Integer> bg = new ArrayList<>(background);
		for(int i = 1; i <= bgSize; i++) {
			final int I = i;
			int k = Bukkit.getScheduler().scheduleSyncDelayedTask(randompackage, new Runnable() {
				public void run() {
					final int randomSlot = (int) background.toArray()[random.nextInt(background.size())];
					ItemStack reward = c.getRandomReward(c.getRandomRarity(true).getPath());
					player.getOpenInventory().getTopInventory().setItem(randomSlot, reward);
					player.updateInventory();
					background.remove((Object) randomSlot);
					if(I == bgSize) {
						for(int n = 1; n <= bgSize; n++) {
							int K = Bukkit.getScheduler().scheduleSyncDelayedTask(randompackage, new Runnable() {
								public void run() {
									final int randomSlot = (int) bg.toArray()[random.nextInt(bg.size())];
									player.getOpenInventory().getTopInventory().setItem(randomSlot, c.getBackground2());
									player.updateInventory();
									bg.remove((Object) randomSlot);
									if(bg.isEmpty()) {
										for(int P : selectedSlots.get(uuid).keySet()) {
											item = c.getRevealSlotRarity().clone(); itemMeta = item.getItemMeta(); lore.clear();
											if(item.hasItemMeta()) {
												if(itemMeta.hasDisplayName())
													if(itemMeta.getDisplayName().contains("{SLOT}")) itemMeta.setDisplayName(itemMeta.getDisplayName().replace("{SLOT}", Integer.toString(getRemainingInt(player.getOpenInventory().getTopInventory().getItem(P).getItemMeta().getDisplayName()))));
												if(itemMeta.hasLore()) lore.addAll(itemMeta.getLore());
											}
											itemMeta.setLore(lore); lore.clear();
											item.setItemMeta(itemMeta);
											player.getOpenInventory().getTopInventory().setItem(P, item);
										}
										player.updateInventory();
										canRevealRarities.add(uuid);
									}
								}
							}, n*5);
							tasks.get(player.getUniqueId()).add(K);
						}
					}
				}
			}, i*5);
			tasks.get(player.getUniqueId()).add(k);
		}
	}
	
	@EventHandler
	private void inventoryCloseEvent(InventoryCloseEvent event) {
		final Player player = (Player) event.getPlayer();
		final UUID uuid = player.getUniqueId();
		if(revealingLoot.keySet().contains(uuid)) {
			canRevealRarities.remove(uuid);
			stopTasks(uuid);
			final ServerCrate c = revealingLoot.get(uuid);
			revealingLoot.remove(uuid);
			if(!revealingloot.keySet().contains(uuid)) revealingloot.put(uuid, new ArrayList<>());
			final HashMap<String, List<String>> cr = c.getRewards();
			final int ri = c.getRedeemableItems(), s = revealingloot.get(uuid).size();
			for(int i = 1; i <= ri-s; i++) {
				revealingloot.get(uuid).add(c.getRandomReward((String) cr.keySet().toArray()[random.nextInt(cr.keySet().size())]));
			}
			selectedSlots.remove(uuid);
			if(player.isOnline()) {
				for(ItemStack is : revealingloot.get(uuid)) {
					giveItem(player, is);
				}
			}
			revealingloot.remove(uuid);
			revealedslots.remove(uuid);
		}
	}
}
