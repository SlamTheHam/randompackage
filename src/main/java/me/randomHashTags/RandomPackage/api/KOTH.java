package me.randomHashTags.RandomPackage.api;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.player.PlayerTeleportEvent;
import org.bukkit.event.player.PlayerTeleportEvent.TeleportCause;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Scoreboard;

import me.randomHashTags.RandomPackage.RandomPackageAPI;

public class KOTH extends RandomPackageAPI implements Listener, CommandExecutor {
	
	public boolean isEnabled = false;
	private static KOTH instance;
	public static final KOTH getKOTH() {
	    if(instance == null) instance = new KOTH();
	    return instance;
	}

	public YamlConfiguration config;
	private Inventory lootbagInv;
	
	private Player editor = null;
	private ItemStack[] editorinv = null;
	
	private ArrayList<ItemStack> lootbagrewards = new ArrayList<>();
	
	private int scorestart = 0, startCapCountdown = -1, scoreboard;
	
	private ItemStack placedcenter = new ItemStack(Material.GLOWSTONE, 1);
	public ItemStack item_lootbag, item_editorCenter, item_editorConfirm;
	
	public DisplaySlot kothdisplayslot;
	public String kothtitle, kothname;
	public Location teleportLocation = null, center = null;
	private List<String> cappingscoreboard, capturedscoreboard, captured, limitedcommands;
	
	public String status = "Not Active";
	public int captureTime = 0, captureRadius = 0;
	
	public long kothCapTimeLeft = -1, started = -1;
	
	private String rewardformat = null;
	private long cappingStartedTime = -1;
	public Player currentPlayerCapturing = null, previouscapturer = null;

	public void enable() {
		final long started = System.currentTimeMillis();
		if(isEnabled) return;
		save("Features", "koth.yml");
		pluginmanager.registerEvents(this, randompackage);
		isEnabled = true;
		config = YamlConfiguration.loadConfiguration(new File(randompackage.getDataFolder() + File.separator + "Features" + File.separator, "koth.yml"));
		lootbagInv = Bukkit.createInventory(null, config.getInt("Lootbag.size"), ChatColor.translateAlternateColorCodes('&', config.getString("Lootbag.title")));

		item_lootbag = d(config, "Lootbag", 0);
		item_editorCenter = d(config, "editor.center", 0);
		item_editorConfirm = d(config, "editor.confirm", 0);

		kothdisplayslot = DisplaySlot.valueOf(config.getString("KOTH.scoreboards.display slot").toUpperCase());
		kothtitle = ChatColor.translateAlternateColorCodes('&', config.getString("KOTH.scoreboards.title"));
		kothname = ChatColor.translateAlternateColorCodes('&', config.getString("KOTH.name"));
		editor = null; editorinv = null;

		placedcenter = new ItemStack(Material.getMaterial(config.getString("editor.center.placed block").toUpperCase()), 1);
		teleportLocation = otherdata.getString("koth.tp") != null && !otherdata.getString("koth.tp").equals("") ? toLocation(otherdata.getString("koth.tp")) : null;
		captureTime = config.getInt("KOTH.time to cap");
		startCapCountdown = config.getInt("KOTH.start cap countdown");
		captureRadius = config.getInt("KOTH.capture radius");
		captured = config.getStringList("messages.captured");
		rewardformat = ChatColor.translateAlternateColorCodes('&', config.getString("messages.reward format"));
		capturedscoreboard = config.getStringList("KOTH.scoreboards.captured");
		cappingscoreboard = config.getStringList("KOTH.scoreboards.capping");
		scorestart = config.getInt("KOTH.scoreboards.score start");

		final String center = otherdata.getString("koth.center");
		if(center != null && !center.equals("")) this.center = toLocation(center);
		limitedcommands = config.getStringList("limited commands");
		lore.clear();
		for(String string : config.getStringList("Lootbag.rewards")) {
			item = new ItemStack(Material.PAPER, 1);
			itemMeta.setDisplayName("item");
			for(String l : string.split(" "))
				if(!l.equals("||") && !l.equals("&&") && !l.equals("->"))
					lore.add(l);
			itemMeta.setLore(lore); lore.clear();
			item.setItemMeta(itemMeta);
			lootbagrewards.add(item);
		}
		if(center != null && !center.equals("")) startKOTH();
		Bukkit.getConsoleSender().sendMessage(ChatColor.translateAlternateColorCodes('&', "&6[RandomPackage] &aLoaded King of the Hill &e(took " + (System.currentTimeMillis()-started) + "ms)"));
	}
	public void disable() {
		if(!isEnabled) return;
		isEnabled = false;
		otherdata.set("koth.center", center != null ? toString(center) : "");
		otherdata.set("koth.tp", teleportLocation != null ? toString(teleportLocation) : "");
		saveOtherData();
		lootbagrewards.clear();
		if(editor != null) finishEdit();
		if(center != null && !status.equals("STOPPED"))
			for(Player player : center.getWorld().getPlayers())
				player.setScoreboard(Bukkit.getScoreboardManager().getNewScoreboard());
		stopKOTH();
		HandlerList.unregisterAll(this);
	}
	
	public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {
		final Player player = sender instanceof Player ? (Player) sender : null;
		if(args.length == 0)
			viewStatus(sender);
		else {
			final String a = args[0];
			if(a.equals("stop") && hasPermission(sender, "RandomPackage.koth.stop", true))        stopKOTH();
			else if(a.equals("start") && hasPermission(sender, "RandomPackage.koth.start", true)) startKOTH();
			else if(player != null) {
				if(a.equals("teleport") || a.equals("tp") || a.equals("warp")) teleportToKOTH(player);
				else if(a.equals("edit")) enterEditMode(player);
				else if(a.equals("loot")) previewLootbag(player);
			}
		}
		return true;
	}
	
	public ArrayList<ItemStack> getRandomLootbagContents() {
		final ArrayList<ItemStack> L = new ArrayList<>();
		for(ItemStack is : lootbagrewards) {
			item = new ItemStack(Material.AIR);
			boolean did = false;
			if(is.hasItemMeta() && is.getItemMeta().hasLore())
				for(String string : is.getItemMeta().getLore()) {
					int chance = 100;
					if(string.contains(";chance=")) {
						item = new ItemStack(Material.AIR);
						chance = Integer.parseInt(string.split("chance\\=")[1]);
						string = string.replace(";chance=" + chance, "");
					} else item = null;
					
					if(item != null) item = checkStringForRPItem(null, string.split(";chance=")[0]);
					if(item != null && !did) {
						item = item.clone();
						int amount = 1;
						if(string.contains("amount")) {
							String amt = string.split("\\;")[1].split("\\;")[0].replace("amount=", "");
							if(amt.contains("-")) amount = Integer.parseInt(amt.split("\\-")[0]) + random.nextInt(Integer.parseInt(amt.split("\\-")[1]));
							else amount = Integer.parseInt(amt);
						}
						if(random.nextInt(100) <= chance) {
							itemMeta = item.getItemMeta(); 
							if(itemMeta.hasLore()) {
								lore.clear();
								lore.addAll(itemMeta.getLore());
								itemMeta.setLore(lore); lore.clear();
								item.setItemMeta(itemMeta);
							}
							item.setAmount(amount);
							L.add(item);
							did = true;
						}
					}
				}
		}
		return L;
	}
	public void previewLootbag(Player player) {
		if(hasPermission(player, "RandomPackage.koth.loot", true)) {
			player.openInventory(Bukkit.createInventory(player, lootbagInv.getSize(), lootbagInv.getTitle()));
			final Inventory top = player.getOpenInventory().getTopInventory();
			final ArrayList<ItemStack> lootbag = getRandomLootbagContents();
			final String n = player.getName();
			for(ItemStack is : lootbag) {
			    final int f = top.firstEmpty();
				if(is != null && !(f < 0)) {
				    item = is;
				    itemMeta = item.getItemMeta();
				    if(itemMeta.hasLore()) {
				        lore.clear();
				        for(String s : itemMeta.getLore()) {
				            lore.add(s.replace("{PLAYER}", n).replace("{UNLOCKED_BY}", n));
                        }
				        itemMeta.setLore(lore); lore.clear();
				        item.setItemMeta(itemMeta);
                    }
					top.setItem(f, item);
				}
			}
			player.updateInventory();
		}
	}
	public void viewStatus(CommandSender sender) {
		if(hasPermission(sender, "RandomPackage.koth", true)) {
			String status = null, faction = currentPlayerCapturing != null ? fapi.getFaction(currentPlayerCapturing) : "", flag = null, name = currentPlayerCapturing != null ? currentPlayerCapturing.getName() : "";
			if(!faction.equals("")) faction = faction + " ";

			if(this.status.equals("CAPTURED") || this.status.equals("STOPPED")) {
				flag = config.getString("messages.flag.captured");
			} else if(currentPlayerCapturing == null) {
				flag = "&3&lUNCONTESTED";
			} else {
				flag = config.getString("messages.flag.capturing");
			}
			status = center == null || this.status.equals("CAPTURED") || this.status.equals("STOPPED") ? config.getString("messages.status.closed") : config.getString("messages.status.open");

			String runtime = "", timeleft = "";
			int sec = (int) (System.currentTimeMillis() - started) / 1000, sec1 = (int) kothCapTimeLeft;
			if(kothCapTimeLeft == -1) sec1 = captureTime;
			int min = sec / 60, min1 = sec1 / 60;
			sec -= min * 60;
			sec1 -= min1 * 60;
			int hrs = min / 60, hrs1 = min1 / 60;
			min -= hrs * 60;
			min1 -= hrs1 * 60;
			if(hrs != 0)  runtime = runtime + hrs + "h ";
			if(min != 0)  runtime = runtime + min + "m ";
			if(sec != 0)  runtime = runtime + sec + "s";

			if(hrs1 != 0) timeleft = timeleft + hrs1 + "h ";
			if(min1 != 0) timeleft = timeleft + min1 + "m ";
			if(sec1 != 0) timeleft = timeleft + sec1 + "s";

			for(String string : config.getStringList("messages.command")) {
				String i = string;
				if(i.contains("{STATUS}"))
					i = i.replace("{STATUS}", center != null ? status : "&c&lNOT SETUP");
				if(i.contains("{PLAYERS}"))
					i = center != null ? i.replace("{PLAYERS}", formatInt(center.getWorld().getNearbyEntities(center, captureRadius, captureRadius, captureRadius).size())) : null;
				if(i != null) {
					if(this.status.equals("CAPTURED")) {
						if(i.contains("{CAPTURED_BY}")) i = i.replace("{CAPTURED_BY}", name);
						if(i.contains("{NEXT_KOTH_TIME}")) i = i.replace("{NEXT_KOTH_TIME}", "&f&lUnspecified");
						if(i.contains("{RUNTIME}") || i.contains("{JOIN_EVENT}") || i.contains("{TIME_LEFT}"))     i = null;

					} else if(this.status.equals("STOPPED")) {
						if(i.contains("{NEXT_KOTH_TIME}")) i = i.replace("{NEXT_KOTH_TIME}", "");
						if(i.contains("{RUNTIME}") || i.contains("JOIN_EVENT") || i.contains("{FLAG}") || i.contains("{FACTION}") || i.contains("{TIME_LEFT}")
								|| i.contains("{CAPTURED_BY}") || i.contains("{PLAYERS}"))
							i = null;

					} else if(this.status.equals("ACTIVE")) {
						if(i.contains("{RUNTIME}"))   i = i.replace("{RUNTIME}", runtime);
						if(i.contains("{JOIN_EVENT}"))i = i.replace("{JOIN_EVENT}", config.getString("messages.join event"));
						if(i.contains("{PLAYER}"))    i = i.replace("{PLAYER}", name);
						if(i.contains("{FACTION}"))   i = i.replace("{FACTION}", faction);
						if(i.contains("{TIME_LEFT}")) i = i.replace("{TIME_LEFT}", timeleft);
						if(i.contains("{FLAG}"))      i = i.replace("{FLAG}", flag);
						if(i.contains("{NEXT_KOTH_TIME}") || i.contains("{CAPTURED_BY}"))  i = null;
					} else {
						if(i.contains("{RUNTIME}") || i .contains("{JOIN_EVENT}") || i.contains("{PLAYER}") || i.contains("FACTION")
								|| i.contains("{TIME_LEFT}") || i.contains("{FLAG}") || i.contains("{NEXT_KOTH_TIME}") || i.contains("{CAPTURED_BY}"))
							i = null;
					}
					if(i != null) sender.sendMessage(ChatColor.translateAlternateColorCodes('&', i));
				}
			}
		}
	}
	
	@EventHandler
	private void playerTeleportEvent(PlayerTeleportEvent event) {
		final World w = center != null ? center.getWorld() : null;
		final Player player = event.getPlayer();
		if(w != null) {
			if(event.getTo().getWorld().equals(w) && event.getCause().equals(TeleportCause.COMMAND) && !player.hasPermission("RandomPackage.koth.teleport bypass") && (status.equals("STOPPED") || status.equals("CAPTURED"))) {
				event.setCancelled(true);
				sendStringListMessage(player, null, config.getStringList("messages." + (status.equals("STOPPED") ? "no event running" : "already capped")), -1, null);
			} else if(event.getFrom().getWorld().equals(w)) {
				player.setScoreboard(Bukkit.getScoreboardManager().getNewScoreboard());
			}
		}
	}
	
	public void startKOTH() {
		started = System.currentTimeMillis();
		status = "ACTIVE";
		if(center != null) {
			scoreboard = Bukkit.getScheduler().scheduleSyncRepeatingTask(randompackage, () -> {
				final boolean c = status.equals("CAPTURED");
				setScoreboard(c);
				if(c) Bukkit.getScheduler().cancelTask(scoreboard);
			}, 0, 20);
		}
	}
	public void stopKOTH() {
		status = "STOPPED";
		Bukkit.getScheduler().cancelTask(scoreboard);
		if(center != null)
			for(Player player : center.getWorld().getPlayers())
				player.setScoreboard(Bukkit.getScoreboardManager().getNewScoreboard());
	}
	//
	public void broadcastStartCapping() {
		long sec = TimeUnit.MILLISECONDS.toSeconds((cappingStartedTime + (captureTime *1000)) - System.currentTimeMillis());
		long min = sec / 60;
		sec -= min * 60;
		
		String MIN = "" + min, SEC = "" + sec;
		if(min <= 9) MIN = "0" + min;
		if(sec <= 9) SEC = "0" + sec;
		for(Player player : center.getWorld().getPlayers()) {
			for(String string : config.getStringList("messages.start capping")) {
				if(string.contains("{PLAYER}")) string = string.replace("{PLAYER}", currentPlayerCapturing.getName());
				if(string.contains("{TIME}")) string = string.replace("{TIME}", MIN + "m " + SEC + "s");
				player.sendMessage(ChatColor.translateAlternateColorCodes('&', string));
			}
		}
	}
	public void broadcastCapping() {
		long sec = TimeUnit.MILLISECONDS.toSeconds((cappingStartedTime + (captureTime * 1000)) - System.currentTimeMillis());
		long min = sec / 60;
		sec -= min * 60;
		for(Player player : center.getWorld().getPlayers()) {
			for(String string : config.getStringList("messages.capping")) {
				if(string.contains("{PLAYER}")) string = string.replace("{PLAYER}", currentPlayerCapturing.getName());
				if(string.contains("{MIN}")) string = string.replace("{MIN}", min + "");
				if(string.contains("{SEC}")) string = string.replace("{SEC}", sec + "");
				player.sendMessage(ChatColor.translateAlternateColorCodes('&', string));
			}
		}
	}
	public void broadcastNoLongerCapping() {
		for(Player player : center.getWorld().getPlayers()) {
			for(String string : config.getStringList("messages.no longer capping")) {
				if(string.contains("{PLAYER}")) string = string.replace("{PLAYER}", previouscapturer.getName());
				player.sendMessage(ChatColor.translateAlternateColorCodes('&', string));
			}
		}
	}
	//
	private void setScoreboard(boolean captured) {
		long sec = TimeUnit.MILLISECONDS.toSeconds((cappingStartedTime + (captureTime * 1000)) - System.currentTimeMillis()), min = 0;
		if(cappingStartedTime == -1) sec = captureTime;
		if(status.equals("STOPPED")) {
			sec = captureTime;
			cappingStartedTime = System.currentTimeMillis();
			currentPlayerCapturing = null;
			previouscapturer = null;
		} else {
			kothCapTimeLeft = sec;
			previouscapturer = currentPlayerCapturing;
		}
		min = sec / 60;
		sec -= min * 60;
		
		String MIN = "" + min, SEC = "" + sec;
		if(min <= 9) MIN = "0" + min;
		if(sec <= 9) SEC = "0" + sec;
		
		List<String> liststring = captured ? capturedscoreboard : cappingscoreboard;
		if(center == null) return;
		String closestPlayerToKOTH = null;
		int distance = captureRadius;
		//
		if(!status.equals("STOPPED")) {
			if(kothCapTimeLeft <= 0 && currentPlayerCapturing != null && !status.equals("CAPTURED")) {
				status = "CAPTURED";
				for(Player player : center.getWorld().getPlayers()) {
					for(String string : this.captured) {
						if(string.contains("{PLAYER}")) string = string.replace("{PLAYER}", currentPlayerCapturing.getName());
						player.sendMessage(ChatColor.translateAlternateColorCodes('&', string));
					}
				}
				item = item_lootbag.clone(); itemMeta = item.getItemMeta(); lore.clear();
				for(String string : itemMeta.getLore()) {
					if(string.contains("{PLAYER}")) string = string.replace("{PLAYER}", currentPlayerCapturing.getName());
					lore.add(string);
				}
				itemMeta.setLore(lore); lore.clear();
				item.setItemMeta(itemMeta);
				giveItem(currentPlayerCapturing, item.clone());
				return;
			} else {
				if(!status.equals("CAPTURED") && kothCapTimeLeft <= startCapCountdown && currentPlayerCapturing != null) broadcastCapping();
			}
			if(currentPlayerCapturing != null && center.getWorld().getNearbyEntities(center, captureRadius, captureRadius, captureRadius).contains(currentPlayerCapturing) && (int) currentPlayerCapturing.getLocation().distance(center) <= captureRadius) {
				closestPlayerToKOTH = currentPlayerCapturing.getName();
			} else
				for(Entity entity : center.getWorld().getNearbyEntities(center, captureRadius, captureRadius, captureRadius)) {
					if(entity instanceof Player && (int) entity.getLocation().distance(center) <= distance) {
						currentPlayerCapturing = (Player) entity;
						cappingStartedTime = System.currentTimeMillis();
						closestPlayerToKOTH = entity.getName();
						distance = (int) entity.getLocation().distance(center);
						if(previouscapturer != null) broadcastNoLongerCapping();
						broadcastStartCapping();
					}
				}
			if(closestPlayerToKOTH == null) {
				closestPlayerToKOTH = "N/A";
				cappingStartedTime = -1;
				kothCapTimeLeft = -1;
				if(currentPlayerCapturing != null) {
					for(Player player : center.getWorld().getPlayers()) {
						for(String string : config.getStringList("messages.no longer capping")) {
							if(previouscapturer != null && string.contains("{PLAYER}")) string = string.replace("{PLAYER}", previouscapturer.getName());
							player.sendMessage(ChatColor.translateAlternateColorCodes('&', string));
						}
					}
				}
				currentPlayerCapturing = null;
			} else {
				
			}
		} else
			closestPlayerToKOTH = "N/A";
		//
		for(Player player : center.getWorld().getPlayers()) {
			final Scoreboard scoreboard = Bukkit.getScoreboardManager().getNewScoreboard();
			final Objective obj = scoreboard.registerNewObjective("config", "dummy");
			obj.setDisplayName(kothtitle);
			obj.setDisplaySlot(kothdisplayslot);
			final String dis = Double.toString(player.getLocation().distance(center)).split("\\.")[0];
			for(int i = 0; i < liststring.size(); i++) {
				String score = ChatColor.translateAlternateColorCodes('&', liststring.get(i));
				if(score.contains("{DISTANCE}")) score = score.replace("{DISTANCE}", dis);
				if(score.contains("{TIME_LEFT}")) score = score.replace("{TIME_LEFT}", MIN + ":" + SEC);
				if(score.contains("{PLAYER}")) score = score.replace("{PLAYER}", closestPlayerToKOTH);
				obj.getScore(score).setScore(scorestart - i);
			}
			player.setScoreboard(scoreboard);
		}
	}
	/*
	 * 
	 * 
	 * 
	 */
	@EventHandler
	private void playerQuitEvent(PlayerQuitEvent event) {
		final Player player = event.getPlayer();
		if(center != null && player.getWorld().equals(center.getWorld())) player.setScoreboard(Bukkit.getScoreboardManager().getNewScoreboard());
	}
	@EventHandler
	private void playerCommandPreprocessEvent(PlayerCommandPreprocessEvent event) {
		if(center != null && center.getWorld().getPlayers().contains(event.getPlayer()) && !status.equals("STOPPED")) {
			boolean did = false;
			for(String string : limitedcommands) if(event.getMessage().toLowerCase().startsWith(string.toLowerCase())) did = true;
			if(!did) {
				sendStringListMessage(event.getPlayer(), null, config.getStringList("messages.blocked command"), -1, null);
				if(!event.getPlayer().isOp()) event.setCancelled(true);
				else event.getPlayer().sendMessage(ChatColor.translateAlternateColorCodes('&', "&e&l(!)&r &eSince you're OP, the command has been executed."));
				return;
			}
		}
	}
	@EventHandler
	private void blockPlaceEvent(BlockPlaceEvent event) {
		if(event.getPlayer() == editor && event.getItemInHand() != null && event.getItemInHand().hasItemMeta() && event.getItemInHand().getItemMeta().hasDisplayName() && event.getItemInHand().getItemMeta().hasLore() && event.getItemInHand().equals(item_editorCenter)) {
			event.setCancelled(true);
			event.getPlayer().updateInventory();
			Bukkit.getScheduler().scheduleSyncDelayedTask(randompackage, () -> event.getBlockPlaced().setType(placedcenter.getType()), 1);
			sendStringListMessage(event.getPlayer(), null, config.getStringList("messages.set center"), 0, event.getBlockPlaced().getLocation());
			center = event.getBlockPlaced().getLocation();
		}
	}
	@EventHandler
	private void blockBreakEvent(BlockBreakEvent event) {
		final Location l = event.getBlock().getLocation();
		if(l.equals(center)) {
			final Player player = event.getPlayer();
			if(editor.equals(player)) {
				center = null;
				sendStringListMessage(player, null, config.getStringList("messages.removed center"), 0, l);
			} else event.setCancelled(true);
		}
	}
	@EventHandler
	private void inventoryClickEvent(InventoryClickEvent event) {
		if(event.getWhoClicked() == editor || event.getWhoClicked().getOpenInventory().getTopInventory().getTitle().equals(lootbagInv.getTitle())) {
			event.setCancelled(true);
			((Player) event.getWhoClicked()).updateInventory();
			event.getWhoClicked().closeInventory();
		}
	}
	@EventHandler
	private void playerInteractEvent(PlayerInteractEvent event) {
		if(event.getItem() != null && !event.getItem().getType().equals(Material.AIR) && event.getItem().hasItemMeta() && event.getItem().getItemMeta().hasDisplayName() && event.getItem().getItemMeta().hasLore()) {
			if(event.getPlayer() == editor && event.getAction().name().contains("RIGHT_CLICK") && event.getItem().getItemMeta().equals(item_editorConfirm.getItemMeta())) {
				event.setCancelled(true);
				finishEdit();
			} else if(event.getPlayer().getGameMode().equals(GameMode.SURVIVAL) && event.getAction().equals(Action.LEFT_CLICK_BLOCK) && editor == event.getPlayer() && event.getClickedBlock() != null && center == event.getClickedBlock().getLocation()) {
				sendStringListMessage(event.getPlayer(), null, config.getStringList("messages.removed center"), 0, event.getClickedBlock().getLocation());
				event.getClickedBlock().setType(Material.AIR);
				center = null;
			} else if(event.getItem().getItemMeta().hasDisplayName() && event.getItem().getItemMeta().getDisplayName().equals(item_lootbag.getItemMeta().getDisplayName())) {
				event.setCancelled(true);
				event.getPlayer().updateInventory();
				removeItem(event.getPlayer(), event.getItem(), 1);
				final ArrayList<ItemStack> lootbag = getRandomLootbagContents();
				final ArrayList<String> loot = new ArrayList<>();
				int z = -1;
				for(ItemStack is : lootbag) {
					z++;
					giveItem(event.getPlayer(), is);
					if(is.hasItemMeta() && is.getItemMeta().hasDisplayName())
						loot.add(is.getItemMeta().getDisplayName());
					else if(is.getType().equals(Material.ENCHANTED_BOOK)) {
						loot.add(ChatColor.YELLOW + "Enchanted Book");
						item = lootbag.get(z); itemMeta = item.getItemMeta();
						itemMeta.setDisplayName(loot.get(loot.size() - 1));
						item.setItemMeta(itemMeta);
					}
				}
				for(String string : config.getStringList("messages.open loot bag")) {
					if(string.contains("{PLAYER}")) string = string.replace("{PLAYER}", event.getPlayer().getName());
					if(string.equals("{REWARDS}")) {
						final ArrayList<String> a = new ArrayList<>();
						for(int i = 0; i < lootbag.size(); i++)
							a.add(rewardformat.replace("{NAME}", "" + lootbag.get(i).getItemMeta().getDisplayName()).replace("{AMOUNT}", "" + lootbag.get(i).getAmount()));
						for(String s : a)
							for(Player p : event.getPlayer().getWorld().getPlayers())
								p.sendMessage(s);
					} else  {
						string = ChatColor.translateAlternateColorCodes('&', string);
						for(Player p : event.getPlayer().getWorld().getPlayers()) p.sendMessage(string);
					}
				}
			}
		}
	}
	public void finishEdit() {
		editor.getInventory().setContents(editorinv);
		editor.updateInventory();
		sendStringListMessage(editor, null, config.getStringList("messages.edit was finished"), 0, null);
		hidecenter();
		editor = null; editorinv = null;
	}
	
	public void teleportToKOTH(Player player) {
		if(hasPermission(player, "RandomPackage.koth.teleport", true) && teleportLocation != null) {
			sendStringListMessage(player, null, config.getStringList("messages." + (status.equals("CAPTURED") ? "already capped" : "teleport")), 0, null);
			if(!status.equals("CAPTURED")) player.teleport(teleportLocation);
		}
	}
	public void enterEditMode(Player player) {
		if(hasPermission(player, "RandomPackage.koth.edit", true) && editor == null) {
			editor = player;
			editorinv = player.getInventory().getContents();
			player.getInventory().clear();
			giveItem(player, item_editorCenter.clone());
			giveItem(player, item_editorConfirm.clone());
			sendStringListMessage(player, null, config.getStringList("messages.enter edit mode"), 0, null);
			revealcenter();
		}
	}
	private void revealcenter() {
		if(center != null) center.getWorld().getBlockAt(center).setType(placedcenter.getType());
	}
	private void hidecenter() {
		if(center != null) center.getWorld().getBlockAt(center).setType(Material.AIR);
	}

	private void sendStringListMessage(Player sender, Player target, List<String> message, int number, Location location) {
		for(String string : message) {
			if(string.contains("{AMOUNT}")) string = string.replace("{AMOUNT}", formatInt(number));
			if(string.contains("{SENDER}")) string = string.replace("{SENDER}", sender.getName());
			if(string.contains("{TARGET}")) string = string.replace("{TARGET}", target.getName());
			if(string.contains("{LOCATION}")) string = string.replace("{LOCATION}", location.getBlockX() + "x " + location.getBlockY() + "y " + location.getBlockZ() + "z");
			if(string.contains("{KOTH}")) string = string.replace("{KOTH}", "" + kothname);
			sender.sendMessage(ChatColor.translateAlternateColorCodes('&', string));
		}
	}
}