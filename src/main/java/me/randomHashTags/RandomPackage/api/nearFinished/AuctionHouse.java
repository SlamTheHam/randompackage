package me.randomHashTags.RandomPackage.api.nearFinished;

import me.randomHashTags.RandomPackage.RPPlayerData;
import me.randomHashTags.RandomPackage.RandomPackageAPI;
import me.randomHashTags.RandomPackage.api.events.auctionhouse.PlayerAuctionBiddableItemEvent;
import me.randomHashTags.RandomPackage.api.events.auctionhouse.PlayerAuctionItemEvent;
import me.randomHashTags.RandomPackage.utils.classes.AuctionedBiddableItem;
import me.randomHashTags.RandomPackage.utils.classes.AuctionedItem;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.io.File;
import java.util.*;

public class AuctionHouse extends RandomPackageAPI implements Listener, CommandExecutor {

    private static AuctionHouse instance;
    public static final AuctionHouse getAuctionHouse() {
        if(instance == null) instance = new AuctionHouse();
        return instance;
    }

    public boolean isEnabled = false, autoupdates = false;
    public YamlConfiguration config;

    public Inventory auctionhouse, playerListings, cancelledExpiredListings, purchaseItem, confirmAuction, playerBids, playerBiddingWatchlist,
            biddableauctionhouse, auctionpackage;
    private ItemStack item_playerauctions, item_playercollectionbin, item_auctionbids, item_previouspage, item_refreshauctions, item_nextpage,
            item_returntoauctionhouse, item_returntobids, item_viewwatchlist,
            item_refreshedpage,
            item_cancelauction, item_cannotafford,
            purchaseConfirm, purchaseCancel;
    private double listingfee = 0.00;
    private int confirmAccept_priceslot = -1;
    private String itemslots;
    private List<Integer> confirmauctionItem = new ArrayList<>(), confirmauctionAccept = new ArrayList<>(), confirmauctionDecline = new ArrayList<>(), purchaseItemSlots = new ArrayList<>(), availableItemSlots = new ArrayList<>();

    private HashMap<Player, Integer> onPage = new HashMap<>();



    public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {
        final Player player = sender instanceof Player ? (Player) sender : null;
        final int l = args.length;
        if(args.length == 0 && player != null) {
            viewAuctionHouse(player);
        } else if(l >= 1) {
            final String a = args[0];
            if(a.equals("help")) {
                viewHelp(sender);
            } else if(player != null && a.equals("listed"))
                viewListings(player);
            else if(player != null && a.equals("expired"))
                viewCollectionBin(player);
            else if(player != null && a.equals("stfu"))
                toggleOutbidAlerts(player);
            else if(l >= 2 && player != null && a.equals("sell"))
                confirmAuction(player, player.getItemInHand(), Double.parseDouble(args[1]));
            else if(player != null && a.equals("startbid")) {
                if(args.length <= 2)
                    sendStringListMessage(player, config.getStringList("messages.startbid-usage"));
                else {

                }
            }
        }
        return true;
    }

    public void enable() {
        final long started = System.currentTimeMillis();
        if(isEnabled) return;
        save("Features", "auction house.yml");
        pluginmanager.registerEvents(this, randompackage);
        isEnabled = true;
        config = YamlConfiguration.loadConfiguration(new File(randompackage.getDataFolder() + File.separator + "Features" + File.separator, "auction house.yml"));
        itemslots = config.getString("auction-house.item-slots");
        for(int i = Integer.parseInt(itemslots.split("-")[0]); i <= Integer.parseInt(itemslots.split("-")[1]); i++)
            availableItemSlots.add(i);
        autoupdates = config.getBoolean("auction-house.auto-updates");
        listingfee = config.getDouble("auction-house.listing-fee-percent");

        item_playerauctions = d(config, "player-auctions", 0);
        item_playercollectionbin = d(config, "player-collection-bin", 0);
        item_auctionbids = d(config, "auction-bids", 0);

        item_returntoauctionhouse = d(config, "return-to-ah", 0);
        item_returntobids = d(config, "return-to-bids", 0);
        item_viewwatchlist = d(config, "view-watchlist", 0);
        item_refreshauctions = d(config, "refresh-auctions", 0);

        item_refreshedpage = d(config, "auction-house.refreshed-page", 0);
        item_cancelauction = d(config, "auction-house.cancel", 0);
        item_cannotafford = d(config, "auction-house.cannot-afford", 0);

        item_nextpage = d(config, "auction-house.next-page", 0);
        item_previouspage = d(config, "auction-house.previous-page", 0);

        purchaseConfirm = d(config, "purchase-item.confirm", 0);
        purchaseCancel = d(config, "purchase-item.cancel", 0);

        auctionhouse = Bukkit.createInventory(null, config.getInt("auction-house.size"), ChatColor.translateAlternateColorCodes('&', config.getString("auction-house.title")));
        biddableauctionhouse = Bukkit.createInventory(null, config.getInt("active-bids.size"), ChatColor.translateAlternateColorCodes('&', config.getString("active-bids.title")));
        auctionpackage = Bukkit.createInventory(null, config.getInt("auction-package.size"), ChatColor.translateAlternateColorCodes('&', config.getString("auction-package.title")));
        playerListings = Bukkit.createInventory(null, config.getInt("listings.size"), ChatColor.translateAlternateColorCodes('&', config.getString("listings.title")));
        cancelledExpiredListings = Bukkit.createInventory(null, config.getInt("cancelled-expired-listings.size"), ChatColor.translateAlternateColorCodes('&', config.getString("cancelled-expired-listings.self-title")));
        purchaseItem = Bukkit.createInventory(null, config.getInt("purchase-item.size"), ChatColor.translateAlternateColorCodes('&', config.getString("purchase-item.title")));
        confirmAuction = Bukkit.createInventory(null, config.getInt("confirm-auction.size"), ChatColor.translateAlternateColorCodes('&', config.getString("confirm-auction.title")));
        playerBids = Bukkit.createInventory(null, config.getInt("active-bids.size"), ChatColor.translateAlternateColorCodes('&', config.getString("active-bids.title")));
        playerBiddingWatchlist = Bukkit.createInventory(null, config.getInt("bidding-watchlist.size"), ChatColor.translateAlternateColorCodes('&', config.getString("bidding-watchlist.title")));

        final ItemStack duration = d(config, "auction-package.duration", 0);
        for(int q = 1; q <= 8; q++) {
            final Inventory e = q == 1 ? auctionhouse : q == 2 ? playerListings : q == 3 ? cancelledExpiredListings : q == 4 ? purchaseItem : q == 5 ? confirmAuction : q == 6 ? playerBids : q == 7 ? playerBiddingWatchlist : q == 8 ? biddableauctionhouse : null;
            final String y = q == 1 ? "auction-house." : q == 2 ? "listings." : q == 3 ? "cancelled-expired-listings." : q == 4 ? "purchase-item." : q == 5 ? "confirm-auction." : q == 6 ? "active-bids." : q == 7 ? "bidding-watchlist." : q == 8 ? "active-bids." : "";
            for(int i = 0; i < e.getSize(); i++) {
                if(config.get(y + i) != null) {
                    final String d = config.getString(y + i + ".item").toUpperCase();
                    if(d.equals("{PLAYER_AUCTIONS}"))                                               e.setItem(i, item_playerauctions.clone());
                    else if(d.equals("{AUCTION_BIDS}"))                                             e.setItem(i, item_auctionbids.clone());
                    else if(d.equals("{PLAYER_COLLECTION_BIN}"))                                    e.setItem(i, item_playercollectionbin.clone());
                    else if(d.equals("{RETURN_TO_AH}"))                                             e.setItem(i, item_returntoauctionhouse.clone());
                    else if(d.equals("{RETURN_TO_BIDS}"))                                           e.setItem(i, item_returntobids.clone());
                    else if(d.equals("{REFRESH_AUCTIONS}"))                                         e.setItem(i, item_refreshauctions.clone());
                    else if(d.equals("{VIEW_WATCHLIST}"))                                           e.setItem(i, item_viewwatchlist.clone());
                    else if(d.equals("{PREVIOUS_PAGE}") && config.get(y + "previous-page") != null) e.setItem(i, d(config, y + "previous-page", 0));
                    else if(d.equals("{NEXT_PAGE}") && config.get(y + "next-page") != null)         e.setItem(i, d(config, y + "next-page", 0));
                    else if(d.equals("{CONFIRM}") && config.get(y + "confirm") != null)             e.setItem(i, d(config, y + "confirm", 0));
                    else if(d.equals("{CANCEL}") && config.get(y + "cancel") != null)               e.setItem(i, d(config, y + "cancel", 0));
                    else if(d.equals("{ITEM}")) {
                        e.setItem(i, new ItemStack(Material.AIR));
                        if(q == 4) purchaseItemSlots.add(i);
                        else if(q == 5) confirmauctionItem.add(i);
                    } else if(d.equals("{ACCEPT}") && config.get(y + "accept") != null) {
                        ItemStack a = d(config, y + "accept", 0);
                        e.setItem(i, a);
                        if(confirmAccept_priceslot == -1) for(int z = 0; z < a.getItemMeta().getLore().size(); z++) if(a.getItemMeta().getLore().get(z).contains("{PRICE}")) confirmAccept_priceslot = z;
                        if(q == 5) confirmauctionAccept.add(i);
                    } else if(d.equals("{DECLINE}") && config.get(y + "decline") != null) {
                        e.setItem(i, d(config, y + "decline", 0));
                        if(q == 5) confirmauctionDecline.add(i);
                    } else if(d.startsWith("{DURATION:") && q == 8) {
                        item = duration.clone(); itemMeta = item.getItemMeta(); lore.clear();
                        if(itemMeta.hasDisplayName() && itemMeta.getDisplayName().contains("{MINUTE}")) itemMeta.setDisplayName(itemMeta.getDisplayName().replace("{MINUTE}", d.split("DURATION:")[1].split("}")[0]));
                        item.setItemMeta(itemMeta);
                        e.setItem(i, item);
                    } else {
                        e.setItem(i, d(config, y + i, 0));
                    }
                }
            }
        }
        Bukkit.getConsoleSender().sendMessage(ChatColor.translateAlternateColorCodes('&', "&6[RandomPackage] &aLoaded Auction House &e(took " + (System.currentTimeMillis()-started) + "ms)"));
    }
    public void disable() {
        if(!isEnabled) return;
        isEnabled = false;
        final HashMap<AuctionedItem, Integer> b = AuctionedItem.tasks;
        for(AuctionedItem a : b.keySet())
            scheduler.cancelTask(b.get(a));
        auctionhouse.clear();
        AuctionedItem.getAuctionedItems.clear();
        AuctionedItem.getCollectionBins.clear();
        AuctionedItem.auctionhouse.clear();
        HandlerList.unregisterAll(this);
    }


    @EventHandler
    private void inventoryClickEvent(InventoryClickEvent event) {
        if(!event.isCancelled() && event.getWhoClicked().getOpenInventory().getTopInventory().getHolder() == event.getWhoClicked() && event.getCurrentItem() != null) {
            final Inventory top = event.getWhoClicked().getOpenInventory().getTopInventory();
            final String title = top.getTitle();
            if(title.equals(auctionhouse.getTitle()) || title.equals(playerListings.getTitle())
                    || title.equals(cancelledExpiredListings.getTitle())
                    || title.equals(confirmAuction.getTitle())
                    || title.equals(biddableauctionhouse.getTitle())
                    || title.equals(purchaseItem.getTitle())) {
                final Player player = (Player) event.getWhoClicked();
                event.setCancelled(true);
                player.updateInventory();

                final UUID uuid = player.getUniqueId();
                final ItemStack c = event.getCurrentItem();
                final Material t = c.getType();
                final byte d = c.getData().getData();
                final int a = c.getAmount();
                final int r = event.getRawSlot();

                if(r >= top.getSize()) {
                    sendStringListMessage(player, config.getStringList("messages.start-listing"));
                    player.closeInventory();
                    return;
                } else if(t.equals(Material.AIR)) return;

                if(t.equals(item_auctionbids.getType()) && d == item_auctionbids.getData().getData() && a == item_auctionbids.getAmount() && c.getItemMeta().getDisplayName().equals(item_auctionbids.getItemMeta().getDisplayName())
                        || event.getCurrentItem().equals(item_returntobids))
                    viewBiddableAuctionHouse(player);
                else if(c.equals(item_returntoauctionhouse))
                    viewAuctionHouse(player);
                else if(c.equals(item_refreshauctions)) {
                    viewPage(player, onPage.get(player));
                    visualizeItem(player, title, r, c, item_refreshedpage, config.getInt("auction-house.refreshed-page.visible-for"));
                    return;
                } else if(c.equals(item_nextpage)) {
                    viewPage(player, onPage.get(player)+1);
                    return;
                } else if(c.equals(item_previouspage)) {
                    viewPage(player, onPage.get(player)-1);
                    return;
                } else if(isViewAuctions(player, c))
                    viewListings(player);
                else if(isViewCollectionBin(player, c))
                    viewCollectionBin(player);

                if(c.equals(item_returntobids) || c.equals(item_playerauctions) || c.equals(item_returntoauctionhouse)) return;

                if(title.equals(confirmAuction.getTitle())) {
                    if(confirmauctionAccept.contains(r)) {
                        auctionItem(player, top.getItem(confirmauctionItem.get(0)), getRemainingDouble(top.getItem(confirmauctionAccept.get(0)).getItemMeta().getLore().get(confirmAccept_priceslot)));
                        top.setItem(confirmauctionItem.get(0), new ItemStack(Material.AIR));
                    } else if(confirmauctionDecline.contains(r)) {

                    } else { return; }
                    player.closeInventory();
                } else if(title.equals(cancelledExpiredListings.getTitle()) || title.equals(playerListings.getTitle())) {
                    final boolean collectionbin = title.equals(cancelledExpiredListings.getTitle());
                    item = c.clone(); itemMeta = item.getItemMeta(); lore.clear();
                    if(itemMeta.hasLore()) {
                        for(int i = 0; i < itemMeta.getLore().size() - (collectionbin ? config.getStringList("cancelled-expired-listings.lore").size() : title.equals(playerListings.getTitle()) ? config.getStringList("listings.lore").size() : 0); i++)
                            lore.add(itemMeta.getLore().get(i));
                    }
                    itemMeta.setLore(lore); lore.clear();
                    item.setItemMeta(itemMeta);
                    final AuctionedItem ai = collectionbin ? AuctionedItem.getCollectionBins.get(uuid).get(r) : AuctionedItem.getAuctionedItems.get(uuid).get(r);
                    if(ai != null) {
                        final ItemStack air = new ItemStack(Material.AIR);
                        top.setItem(r, air);
                        if(collectionbin) {
                            giveItem(player, item);
                            ai.cancel(false);
                        } else if(title.equals(playerListings.getTitle())) {
                            cancelAuction(player, ai);
                        }
                        int slot = r;
                        final int last = (int) availableItemSlots.toArray()[availableItemSlots.size()-1];
                        for(int i = r; i <= last; i++) {
                            final ItemStack n = slot+1 <= last ? top.getItem(slot+1) : air;
                            top.setItem(slot, n);
                            slot += 1;
                        }
                    }
                } else if(title.equals(auctionhouse.getTitle())) {
                    if(t.equals(Material.AIR)) return;
                    final AuctionedItem ai = AuctionedItem.valueOf(r);
                    if(ai != null) {
                        final boolean A = player.getUniqueId().equals(ai.seller);
                        if(A || eco.getBalance(player) < ai.price) {
                            visualizeItem(player, auctionhouse.getTitle(), r, c, A ? item_cancelauction : item_cannotafford, config.getInt("auction-house." + (A ? "cancel" : "cannot-afford") + ".visible-for"));
                            return;
                        } else {
                            tryToPurchase(player, ai);
                        }
                    }
                } else if(title.equals(biddableauctionhouse.getTitle())) {

                } else if(title.equals(purchaseItem.getTitle())) {
                    if(c.equals(purchaseCancel)) {
                        player.closeInventory();
                        viewAuctionHouse(player);
                    } else if(c.equals(purchaseConfirm)) {
                        purchaseItem(player, AuctionedItem.valueOf(top.getItem((int) purchaseItemSlots.toArray()[0])), item);
                        player.closeInventory();
                    }
                } else return;
                player.updateInventory();
            }
        }
    }
    public void purchaseItem(Player player, AuctionedItem ai, ItemStack ahitem) {
        final String it = ai.getItemStack().getType().name(), price = formatDouble(ai.price);
        giveItem(player, ai.getItemStack());
        final OfflinePlayer seller = Bukkit.getOfflinePlayer(ai.seller);
        eco.withdrawPlayer(player, ai.price);
        eco.depositPlayer(seller, ai.price);
        if(seller != null && seller.isOnline()) {
            for(String s : config.getStringList("messages.sold-auction")) {
                s = s.replace("{PURCHASER}", player.getName()).replace("{ITEM}", it).replace("{PRICE}", price);
                seller.getPlayer().sendMessage(ChatColor.translateAlternateColorCodes('&', s));
            }
        }
        for(String s : config.getStringList("messages.purchased-auction")) {
            if(s.contains("{SELLER}")) s = s.replace("{SELLER}", seller.getName());
            if(s.contains("{ITEM}")) s = s.replace("{ITEM}", it);
            if(s.contains("{PRICE}")) s = s.replace("{PRICE}", price);
            player.sendMessage(ChatColor.translateAlternateColorCodes('&', s));
        }
        auctionhouse.remove(ahitem);
        ai.remove();
    }
    @EventHandler
    private void inventoryCloseEvent(InventoryCloseEvent event) {
        final Inventory inv = event.getInventory();
        final Player player = (Player) event.getPlayer();
        onPage.remove(player);
        if(inv.getHolder() == player && inv.getTitle().equals(confirmAuction.getTitle()) && inv.getItem(confirmauctionItem.get(0)) != null && !inv.getItem(confirmauctionItem.get(0)).getType().equals(Material.AIR)) {
            giveItem(player, inv.getItem(confirmauctionItem.get(0)));
            sendStringListMessage(player, config.getStringList("messages.cancelled-auction"));
        }
    }
    private boolean isViewAuctions(Player player, ItemStack is) {
        final UUID u = player.getUniqueId();
        final ItemStack I = item_playerauctions.clone(); itemMeta = I.getItemMeta(); lore.clear();
        final HashMap<UUID, List<AuctionedItem>> a = AuctionedItem.getAuctionedItems;
        final String size = Integer.toString(a.keySet().contains(u) ? a.get(u).size() : 0);
        for(String s : itemMeta.getLore())
            lore.add(s.replace("{ITEMS}", size));
        itemMeta.setLore(lore); lore.clear();
        I.setItemMeta(itemMeta);
        return I.equals(is);
    }
    private boolean isViewCollectionBin(Player player, ItemStack is) {
        final UUID u = player.getUniqueId();
        final ItemStack I = item_playercollectionbin.clone(); itemMeta = I.getItemMeta(); lore.clear();
        final HashMap<UUID, List<AuctionedItem>> a = AuctionedItem.getCollectionBins;
        final String size = Integer.toString(a.keySet().contains(u) ? a.get(u).size() : 0);
        for(String s : itemMeta.getLore())
            lore.add(s.replace("{ITEMS}", size));
        itemMeta.setLore(lore); lore.clear();
        I.setItemMeta(itemMeta);
        return I.equals(is);
    }

    public void viewAuctionHouse(Player player) {
        if(hasPermission(player, "RandomPackage.ah.view", true)) {
            player.closeInventory();
            final HashMap<UUID, List<AuctionedItem>> auctions = AuctionedItem.getAuctionedItems, collectionbins = AuctionedItem.getCollectionBins;
            final UUID u = player.getUniqueId();
            final int playerAuctions = auctions.keySet().contains(u) ? auctions.get(u).size() : 0, collectionbin = collectionbins.keySet().contains(u) ? collectionbins.get(u).size() : 0;
            if(!autoupdates) {
                player.openInventory(Bukkit.createInventory(player, auctionhouse.getSize(), auctionhouse.getTitle()));
                final Inventory top = player.getOpenInventory().getTopInventory();
                top.setContents(auctionhouse.getContents());
                onPage.put(player, 0);
                viewPage(player, 0);

                for(int i = 0; i < top.getSize(); i++) {
                    item = top.getItem(i);
                    if(item != null && !item.getType().equals(Material.AIR)) {
                        item = item.clone(); itemMeta = item.getItemMeta(); lore.clear();
                        if(itemMeta.hasLore()) {
                            for(String s : itemMeta.getLore()) {
                                if(s.contains("{ITEMS}"))
                                    s = s.replace("{ITEMS}", "" + (item.equals(item_playerauctions) ? playerAuctions : item.equals(item_playercollectionbin) ? collectionbin : "0"));
                                lore.add(s);
                            }
                            itemMeta.setLore(lore); lore.clear();
                            item.setItemMeta(itemMeta);
                        }
                        top.setItem(i, item);
                    }
                }
                player.updateInventory();
            } else {

            }
        }
    }
    public void viewHelp(CommandSender sender) {
        if(hasPermission(sender, "RandomPackage.ah.help", true))
            sendStringListMessage(sender, config.getStringList("messages.help"));
    }
    private ItemStack formatAuctionedItem(AuctionedItem ai) {
        final OfflinePlayer off = Bukkit.getOfflinePlayer(ai.seller);
        item = ai.getItemStack(); itemMeta = item.getItemMeta(); lore.clear();
        if(itemMeta.hasLore()) lore.addAll(itemMeta.getLore());
        for(String s : config.getStringList("auction-house.added-lore")) {
            if(s.contains("{PRICE}")) s = s.replace("{PRICE}", formatDouble(ai.price));
            if(s.contains("{SELLER}")) s = s.replace("{SELLER}", off != null ? off.getName() : "UNKNOWN");
            lore.add(ChatColor.translateAlternateColorCodes('&', s));
        }
        itemMeta.setLore(lore); lore.clear();
        item.setItemMeta(itemMeta);
        return item;
    }
    public void viewPage(Player player, int page) {
        if(page >= 0 && onPage.containsKey(player)) {
            final TreeMap<Long, AuctionedItem> auctions = AuctionedItem.auctionhouse;
            int slot = page*availableItemSlots.size(), auctionSize = 0;
            for(long l : auctions.keySet()) auctionSize += 1;
            if(auctionSize > slot) {
                onPage.put(player, page);
                final Inventory top = player.getOpenInventory().getTopInventory();
                final ItemStack air = new ItemStack(Material.AIR);
                final Integer[] s = availableItemSlots.toArray(new Integer[availableItemSlots.size()]);
                for(int i = s[0]; i <= s[s.length-1]; i++) {
                    top.setItem(i, air);
                    if(auctionSize > slot) {
                        final long f = (long) auctions.keySet().toArray()[slot];
                        top.setItem(i, formatAuctionedItem(auctions.get(f)));
                        slot += 1;
                    }
                }
                player.updateInventory();
            }
        }
    }
    public void tryToPurchase(Player purchaser, AuctionedItem ai) {
        if(ai != null) {
            purchaser.openInventory(Bukkit.createInventory(purchaser, purchaseItem.getSize(), purchaseItem.getTitle()));
            purchaser.getOpenInventory().getTopInventory().setContents(purchaseItem.getContents());
            purchaser.getOpenInventory().getTopInventory().setItem(purchaser.getOpenInventory().getTopInventory().firstEmpty(), ai.getItemStack());
            purchaser.updateInventory();
        } else {
            purchaser.closeInventory();
            viewAuctionHouse(purchaser);
        }
    }
    public void viewBiddableAuctionHouse(Player player) {
        player.closeInventory();
        if(!autoupdates) {

        }
        player.openInventory(Bukkit.createInventory(player, biddableauctionhouse.getSize(), biddableauctionhouse.getTitle()));
        player.getOpenInventory().getTopInventory().setContents(biddableauctionhouse.getContents());
        final TreeMap<Long, List<AuctionedBiddableItem>> q = AuctionedBiddableItem.auctionedbids;
        for(long L : q.keySet()) {
            for(AuctionedBiddableItem i : q.get(L)) {
                final Player P = i.player, bidder = i.bidder;
                final List<ItemStack> items = i.items;
                final double startingBid = i.startingprice, bidIncrement = i.bidincrement, currentBid = i.currentbid, buynowprice = i.buynow;
                int sec = (int) ((System.currentTimeMillis() - i.auctionedtime) / 1000), min = sec = 60;
                sec -= min * 60;
                item = items.get(0).clone(); itemMeta = item.getItemMeta(); lore.clear();
                if(itemMeta.hasLore()) lore.addAll(itemMeta.getLore());
                if(items.size() > 1)
                    for(String s : config.getStringList("active-bids.bundle-lore")) {
                        if(s.contains("{ITEM_SIZE}")) s = s.replace("{ITEM_SIZE}", Integer.toString(items.size()));
                        if(s.contains("{ITEM}"))
                            for(int b = 0; b < items.size(); b++)
                                lore.add(s.replace("{ITEM}", toMaterial(item.getType().name(), false)));
                        if(!s.contains("{ITEM}")) lore.add(ChatColor.translateAlternateColorCodes('&', s));
                    }
                for(String s : config.getStringList("active-bids.added-lore")) {
                    if(s.contains("{STARTING_BID}")) s = s.replace("{STARTING_BID}", formatDouble(startingBid));
                    if(s.contains("{BID_INCREMENT}")) s = s.replace("{BID_INCREMENT}", formatDouble(bidIncrement));
                    if(s.contains("{CURRENT_BID}")) s = s.replace("{CURRENT_BID}", formatDouble(currentBid));
                    if(buynowprice != -1 && s.contains("{BUY_NOW_PRICE}")) s = s.replace("{BUY_NOW_PRICE}", formatDouble(buynowprice));
                    if(buynowprice != -1 && s.contains("{BUY_NOW_STRING}")) s = s.replace("{BUY_NOW_STRING}", "");
                    if(s.contains("{HIGHEST_BIDDER}")) s = s.replace("{HIGHEST_BIDDER}", bidder.getName());
                    if(s.contains("{SELLER}")) s = s.replace("{SELLER}", P.getName());
                    if(s.contains("{TIME}")) s = s.replace("{TIME}", min > 0 ? min + "m" + (sec > 0 ? " " + sec + "s" : "") : sec + "s");
                    if(!s.contains("{BUY_NOW_PRICE") || !s.contains("{BUY_NOW_STRING}")) lore.add(ChatColor.translateAlternateColorCodes('&', s));
                }
                itemMeta.setLore(lore); lore.clear();
                item.setItemMeta(itemMeta);
                player.getOpenInventory().getTopInventory().setItem(player.getOpenInventory().getTopInventory().firstEmpty(), item);
            }
        }
        player.updateInventory();
    }
    public void viewListings(Player player) {
        if(hasPermission(player, "RandomPackage.ah.listed", true)) {
            player.closeInventory();
            player.openInventory(Bukkit.createInventory(player, playerListings.getSize(), playerListings.getTitle()));
            player.getOpenInventory().getTopInventory().setContents(playerListings.getContents());
            final HashMap<UUID, List<AuctionedItem>> auctioned = AuctionedItem.getAuctionedItems;
            final UUID u = player.getUniqueId();
            if(auctioned.keySet().contains(u)) {
                final List<AuctionedItem> au = auctioned.get(u);
                for(int i = 0; i < au.size(); i++) {
                    final AuctionedItem is = au.get(i);
                    item = is.getItemStack().clone(); itemMeta = item.getItemMeta(); lore.clear();
                    final long t = is.listingExpiration - System.currentTimeMillis();
                    if(t > 0) {
                        int seconds = (int) t / 1000;
                        int minutes = seconds / 60;
                        seconds -= minutes * 60;
                        int hours = minutes / 60;
                        minutes -= hours * 60;
                        final String time = t > 0 ? hours + "h " + minutes + "m " + seconds + "s" : "null";
                        if(itemMeta.hasLore()) lore.addAll(itemMeta.getLore());
                        for(String s : config.getStringList("listings.lore")) {
                            if(s.contains("{PRICE}")) s = s.replace("{PRICE}", formatDouble(is.price));
                            if(s.contains("{TIME}")) s = s.replace("{TIME}", time);
                            lore.add(ChatColor.translateAlternateColorCodes('&', s));
                        }
                        itemMeta.setLore(lore); lore.clear();
                        item.setItemMeta(itemMeta);
                        player.getOpenInventory().getTopInventory().setItem(player.getOpenInventory().getTopInventory().firstEmpty(), item);
                    } else {
                        is.cancel(true);
                        i -= 1;
                    }
                }
            }
            player.updateInventory();
        }
    }
    public void viewCollectionBin(Player viewer) {
        if(hasPermission(viewer, "RandomPackage.ah.expired", true)) {
            viewer.closeInventory();
            viewer.openInventory(Bukkit.createInventory(viewer, cancelledExpiredListings.getSize(), cancelledExpiredListings.getTitle()));
            final Inventory top = viewer.getOpenInventory().getTopInventory();
            final UUID u = viewer.getUniqueId();
            final HashMap<UUID, List<AuctionedItem>> collection = AuctionedItem.getCollectionBins;
            if(collection.keySet().contains(u)) {
                for(AuctionedItem is : collection.get(u)) {
                    item = is.getItemStack().clone(); itemMeta = item.getItemMeta(); lore.clear();
                    if(itemMeta.hasLore()) lore.addAll(itemMeta.getLore());
                    final long t = is.collectionBinExpiration - System.currentTimeMillis();
                    if(t > 0) {
                        int seconds = (int) t / 1000;
                        int minutes = seconds / 60;
                        seconds -= minutes * 60;
                        int hours = minutes / 60;
                        minutes -= hours * 60;
                        final String time = t > 0 ? hours + "h " + minutes + "m " + seconds + "s" : "null";
                        for(String s : config.getStringList("cancelled-expired-listings.lore")) {
                            if(s.contains("{TIME}")) s = s.replace("{TIME}", time);
                            lore.add(ChatColor.translateAlternateColorCodes('&', s));
                        }
                        itemMeta.setLore(lore); lore.clear();
                        item.setItemMeta(itemMeta);
                        top.setItem(top.firstEmpty(), item);
                    } else {
                        is.cancel(false);
                    }
                }
            }
            for(int i = 0; i < cancelledExpiredListings.getSize(); i++)
                if(cancelledExpiredListings.getItem(i) != null && !cancelledExpiredListings.getItem(i).getType().equals(Material.AIR))
                    top.setItem(i, cancelledExpiredListings.getItem(i));
        }
    }


    public void toggleOutbidAlerts(Player player) {
        if(hasPermission(player, "RandomPackage.ah.stfu", true)) {
            final RPPlayerData pdata = RPPlayerData.get(player.getUniqueId());
            pdata.receivesAHOutbidNotications = !pdata.receivesAHOutbidNotications;
            if(player.isOnline()) sendStringListMessage(player, config.getStringList("messages.toggle-bid-notifications-" + (pdata.receivesAHOutbidNotications ? "on" : "off")));
        }
    }
    public void confirmAuction(Player seller, ItemStack is, double price) {
        if(hasPermission(seller, "RandomPackage.ah.sell", true)) {
            if(is == null || is.getType().equals(Material.AIR)) {
                sendStringListMessage(seller, config.getStringList("messages.equip-item-to-sell"));
            } else {
                seller.closeInventory();
                seller.openInventory(Bukkit.createInventory(seller, confirmAuction.getSize(), confirmAuction.getTitle()));
                seller.getOpenInventory().getTopInventory().setContents(confirmAuction.getContents());
                final String q = toMaterial(is.getType().name(), false);
                final double listingfeepercent = Double.parseDouble(roundDoubleString((listingfee / 100) * price, 2));
                for(int i : confirmauctionItem) seller.getOpenInventory().getTopInventory().setItem(i, is);
                for(int i = 0; i < seller.getOpenInventory().getTopInventory().getSize(); i++) {
                    if(seller.getOpenInventory().getTopInventory().getItem(i) != null && !seller.getOpenInventory().getTopInventory().getItem(i).getType().equals(Material.AIR)) {
                        item = seller.getOpenInventory().getTopInventory().getItem(i).clone(); itemMeta = item.getItemMeta(); lore.clear();
                        if(itemMeta.hasLore()) {
                            for(String s : itemMeta.getLore()) {
                                if(s.contains("{ITEM}")) s = s.replace("{ITEM}", q);
                                if(s.contains("{PRICE}")) s = s.replace("{PRICE}", formatDouble(price));
                                if(s.contains("{LISTING_FEE$}")) s = s.replace("{LISTING_FEE$}", formatDouble(listingfeepercent));
                                if(s.contains("{LISTING_FEE%}")) s = s.replace("{LISTING_FEE%}", formatDouble(listingfee));
                                lore.add(s);
                            }
                            itemMeta.setLore(lore); lore.clear();
                            item.setItemMeta(itemMeta);
                        }
                        seller.getOpenInventory().getTopInventory().setItem(i, item);
                    }
                }
                seller.setItemInHand(new ItemStack(Material.AIR));
                seller.updateInventory();
            }
        }
    }
    public void confirmBiddableAuction(Player seller, double startingprice, double bidincrement, double buynow) {
        ItemStack y = seller.getItemInHand();
        seller.closeInventory();
        seller.openInventory(Bukkit.createInventory(seller, auctionpackage.getSize(), auctionpackage.getTitle()));
        seller.getOpenInventory().getTopInventory().setContents(auctionpackage.getContents());
    }
    public void auctionItem(Player player, ItemStack is, double price) {
        final long time = System.currentTimeMillis();
        final PlayerAuctionItemEvent event = new PlayerAuctionItemEvent(player, time, is, price);
        pluginmanager.callEvent(event);
        if(!event.isCancelled()) {
            final Player play = event.player;
            final double pr = event.price, tax = (listingfee / 100) * pr;
            final ItemStack i = event.item;
            new AuctionedItem(play.getUniqueId(), event.item, pr, time, false);
            final String p = formatDouble(pr), lf = roundDoubleString(tax, 2);
            eco.withdrawPlayer(player, tax);
            for(String s : config.getStringList("messages.listed")) {
                if(s.contains("{ITEM}")) s = s.replace("{ITEM}", toMaterial(i.getType().name(), false));
                if(s.contains("{PRICE}")) s = s.replace("{PRICE}", p);
                if(s.contains("{LISTING_FEE$}")) s = s.replace("{LISTING_FEE$}", lf);
                play.sendMessage(ChatColor.translateAlternateColorCodes('&', s));
            }
        }
    }
    public void auctionBiddableItem(Player player, List<ItemStack> items, double startingbid, double bidincrement, double buynow, int duration) {
        final long time = System.currentTimeMillis();
        final PlayerAuctionBiddableItemEvent event = new PlayerAuctionBiddableItemEvent(player, time, items, startingbid, bidincrement, buynow);
        pluginmanager.callEvent(event);
        if(!event.isCancelled()) {
            new AuctionedBiddableItem(time, player, items, startingbid, bidincrement, buynow, duration);
            for(String s : config.getStringList("messages.start-bid")) {
                if(s.contains("{ITEMS_AMOUNT}")) s = s.replace("{ITEMS_AMOUNT}", Integer.toString(items.size()));
                if(s.contains("{STARTING_BID}")) s = s.replace("{STARTING_BID}", formatDouble(startingbid));
                player.sendMessage(ChatColor.translateAlternateColorCodes('&', s));
            }
        }
    }
    public void cancelAuction(Player seller, AuctionedItem is) {
        if(is != null) {
            is.cancel(true);
            sendStringListMessage(seller, config.getStringList("messages.cancelled-listing"));
        }
    }
    public void updateAuctions() {

    }
    private void visualizeItem(Player player, String title, int rawslot, ItemStack prev, ItemStack set, int seconds) {
        player.getOpenInventory().getTopInventory().setItem(rawslot, set);
        player.updateInventory();
        final Inventory i = player.getOpenInventory().getTopInventory();
        scheduler.scheduleSyncDelayedTask(randompackage, () -> {
            if(player.getOpenInventory().getTopInventory().equals(i)) {
                player.getOpenInventory().getTopInventory().setItem(rawslot, prev);
                player.updateInventory();
            }
        }, 20 * seconds);
    }
}
