package me.randomHashTags.RandomPackage.api.nearFinished;

import me.randomHashTags.RandomPackage.api.CustomEnchants;
import me.randomHashTags.RandomPackage.api.events.ArmorSetEquipEvent;
import me.randomHashTags.RandomPackage.api.events.ArmorSetUnequipEvent;
import me.randomHashTags.RandomPackage.api.events.customboss.CustomBossDamageByEntityEvent;
import me.randomHashTags.RandomPackage.api.events.customenchant.*;
import me.randomHashTags.RandomPackage.api.events.masks.MaskEquipEvent;
import me.randomHashTags.RandomPackage.api.events.masks.MaskUnequipEvent;
import me.randomHashTags.RandomPackage.api.events.mobstacker.MobStackDepleteEvent;
import me.randomHashTags.RandomPackage.api.events.player.PlayerArmorEvent;
import me.randomHashTags.RandomPackage.utils.universal.UMaterial;
import me.randomHashTags.RandomPackage.utils.classes.Mask;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.*;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.*;
import org.bukkit.event.inventory.InventoryAction;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerItemDamageEvent;
import org.bukkit.inventory.ItemStack;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;

public class Masks extends CustomEnchants implements Listener {

    private static Masks instance;
    public static final Masks getMasks() {
        if(instance == null) instance = new Masks();
        return instance;
    }

    public boolean isEnabled = false;
    public YamlConfiguration config;
    private HashMap<Player, ItemStack> equippedMasks = new HashMap<>();

    public void enable() {
        final long started = System.currentTimeMillis();
        if(isEnabled) return;
        save("Features", "masks.yml");
        pluginmanager.registerEvents(this, randompackage);
        isEnabled = true;
        config = YamlConfiguration.loadConfiguration(new File(randompackage.getDataFolder() + File.separator + "Features" + File.separator, "masks.yml"));

        final ArrayList<ItemStack> ms = new ArrayList<>();
        final ArrayList<String> addedlore = new ArrayList<>();
        for(String s : config.getStringList("added lore"))
            addedlore.add(ChatColor.translateAlternateColorCodes('&', s));
        int loaded = 0;
        for(String s : config.getConfigurationSection("masks").getKeys(false)) {
            final ItemStack i = d(config, "masks." + s, 0);
            if(i != null) {
                itemMeta = i.getItemMeta(); lore.clear();
                if(itemMeta.hasLore()) lore.addAll(itemMeta.getLore());
                lore.addAll(addedlore);
                itemMeta.setLore(lore); lore.clear();
                i.setItemMeta(itemMeta);
            }
            final String t = config.getString("masks." + s + ".texture"), o = config.getString("masks." + s + ".owner");
            final Mask m = new Mask(s, t != null ? t : o, i, config.getString("masks." + s + ".added lore"), config.getStringList("masks." + s + ".attributes"));
            ms.add(m.getItemStack());
            loaded += 1;
        }
        addGivedpCategory(ms, UMaterial.PLAYER_HEAD_ITEM, "Masks", "Givedp: Masks");
        Bukkit.getConsoleSender().sendMessage(ChatColor.translateAlternateColorCodes('&', "&6[RandomPackage] &aLoaded " + loaded + " masks &e(took " + (System.currentTimeMillis()-started) + "ms)"));
    }
    public void disable() {
        if(!isEnabled) return;
        isEnabled = false;
        for(Player p : equippedMasks.keySet()) {
            p.getInventory().setHelmet(equippedMasks.get(p));
            p.updateInventory();
        }
        equippedMasks.clear();
        Mask.masks.clear();
        HandlerList.unregisterAll(this);
    }

    @EventHandler
    private void inventoryClickEvent(InventoryClickEvent event) {
        if(!event.isCancelled()) {
            final int r = event.getRawSlot();
            final String click = event.getClick().name();
            if(r < 0 || !click.equals("RIGHT") && !event.getAction().equals(InventoryAction.SWAP_WITH_CURSOR)) return;
            final ItemStack current = event.getCurrentItem();
            final String c = current.getType().name();
            if(!c.endsWith("HELMET")) return;
            final ItemStack mask = event.getCursor();
            final Mask m = Mask.valueOf(mask), onitem = Mask.getOnItem(current);
            final Player player = (Player) event.getWhoClicked();
            if(m != null && onitem == null) {
                event.setCancelled(true);
                player.updateInventory();
                apply(m, current);
                item = m.getItemStack();
                final int a = item.getAmount()-mask.getAmount();
                if(a <= 0) item = new ItemStack(Material.AIR);
                else       item.setAmount(a);
                event.setCursor(item);
            } else if(click.equals("RIGHT") && onitem != null) {
                item = current; itemMeta = item.getItemMeta(); lore.clear();
                lore.addAll(itemMeta.getLore());
                lore.remove(onitem.addedLore);
                itemMeta.setLore(lore); lore.clear();
                item.setItemMeta(itemMeta);
                event.setCancelled(true);
                event.setCurrentItem(item);
                event.setCursor(onitem.getItemStack());
                player.updateInventory();
            }
        }
    }

    @EventHandler(priority = EventPriority.LOW)
    private void entityDamageEvent(EntityDamageEvent event) {
        if(!event.isCancelled()) {
            final Player victim = event.getEntity() instanceof Player ? (Player) event.getEntity() : null;
            if(victim != null && equippedMasks.keySet().contains(victim)) {
                final ItemStack i = equippedMasks.get(victim).clone();
                final Mask m = Mask.valueOf(victim.getInventory().getHelmet());
                if(m != null) {
                    victim.getInventory().setHelmet(i);
                    victim.updateInventory();
                    equippedMasks.remove(victim);
                }
            }
        }
    }
    @EventHandler(priority = EventPriority.HIGH)
    private void pvAnyEvent(PvAnyEvent event) {
        if(!event.isCancelled()) {
            tryToProcMask(event.getDamager(), event);
        }
    }
    @EventHandler(priority = EventPriority.HIGH)
    private void isDamagedEvent(isDamagedEvent event) {
        if(!event.isCancelled())
            tryToProcMask(event.getVictim(), event);
    }
    @EventHandler
    private void entityDeathEvent(EntityDeathEvent event) {
        final LivingEntity e = event.getEntity();
        if(!(e instanceof Player)) tryToProcMask(event.getEntity().getKiller(), event);
    }
    @EventHandler
    private void playerDeathEvent(PlayerDeathEvent event) {
        tryToProcMask(event.getEntity().getKiller(), event);
    }
    @EventHandler(priority = EventPriority.HIGH)
    private void blockBreakEvent(BlockBreakEvent event) {
        if(!event.isCancelled()) tryToProcMask(event.getPlayer(), event);
    }
    @EventHandler(priority = EventPriority.HIGH)
    private void blockPlaceEvent(BlockPlaceEvent event) {
        if(!event.isCancelled()) {
            final Player player = event.getPlayer();
            final Mask m = Mask.valueOf(event.getItemInHand());
            if(m != null) {
                event.setCancelled(true);
                player.updateInventory();
            } else {
                tryToProcMask(player, event);
            }
        }
    }
    @EventHandler(priority = EventPriority.HIGH)
    private void foodLevelChangeEvent(FoodLevelChangeEvent event) {
        if(!event.isCancelled())
            tryToProcMask((Player) event.getEntity(), event);
    }
    @EventHandler(priority = EventPriority.HIGH)
    private void playerInteractEvent(PlayerInteractEvent event) {
        if(!event.isCancelled())
            tryToProcMask(event.getPlayer(), event);
    }
    @EventHandler(priority = EventPriority.HIGH)
    private void entityShootBowEvent(EntityShootBowEvent event) {
        if(!event.isCancelled() && event.getEntity() instanceof Player)
            tryToProcMask((Player) event.getEntity(), event);
    }

    public void tryToProcMask(Player player, Event event) {
        if(player != null && event != null) {
            final ItemStack hel = player.getInventory().getHelmet();
            final Mask m = Mask.valueOf(hel), mm = m == null ? Mask.getOnItem(hel) : null;
            if(m != null) procMaskAttributes(player, event, m);
            else if(mm != null) procMaskAttributes(player, event, mm);
        }
    }
    public void procMaskAttributes(Player player, Event event, Mask mask) {
        for(String attr : mask.getAttributes()) {
            final String A = attr.split(";")[0].toLowerCase();
            if(event instanceof PlayerArmorEvent && (A.equals("armorequip") && ((PlayerArmorEvent) event).getReason().name().contains("_EQUIP") || A.equals("armorunequip") && ((PlayerArmorEvent) event).getReason().name().contains("_UNEQUIP") || A.equals("armorpiecebreak") && ((PlayerArmorEvent) event).getReason().equals(PlayerArmorEvent.ArmorEventReason.BREAK))
                    || event instanceof PvAnyEvent && (A.equals("pva") || A.equals("pvp") && ((PvAnyEvent) event).getVictim() instanceof Player || A.equals("pve") && !(((PvAnyEvent) event).getVictim() instanceof Player))

                    || event instanceof isDamagedEvent && (A.equals("isdamaged") || A.equals("hitbyarrow") && ((isDamagedEvent) event).getDamager() instanceof Arrow || A.startsWith("damagedby(") && ((isDamagedEvent) event).getCause() != null && A.toUpperCase().contains(((isDamagedEvent) event).getCause().name()))

                    || event instanceof CustomEnchantEntityDamageByEntityEvent && A.startsWith("ceentityisdamaged")
                    || event instanceof CustomBossDamageByEntityEvent && A.startsWith("custombossisdamaged")

                    || event instanceof ArmorSetEquipEvent && A.equals("armorsetequip")
                    || event instanceof ArmorSetUnequipEvent && A.equals("armorsetunequip")

                    || event instanceof BlockPlaceEvent && A.equals("blockplace")
                    || event instanceof BlockBreakEvent && A.equals("blockbreak")

                    || event instanceof FoodLevelChangeEvent && (A.equals("foodlevelgained") && ((FoodLevelChangeEvent) event).getFoodLevel() > ((Player) ((FoodLevelChangeEvent) event).getEntity()).getFoodLevel() || A.equals("foodlevellost") && ((FoodLevelChangeEvent) event).getFoodLevel() < ((Player) ((FoodLevelChangeEvent) event).getEntity()).getFoodLevel())

                    || event instanceof PlayerItemDamageEvent && A.equals("isdurabilitydamaged")

                    || event instanceof PlayerInteractEvent && A.equals("playerinteract")
                    || event instanceof ProjectileHitEvent && (A.equals("arrowhit") && ((ProjectileHitEvent) event).getEntity() instanceof Arrow && (((ProjectileHitEvent) event).getEntity()).getShooter() instanceof Player && shotbows.keySet().contains(((ProjectileHitEvent) event).getEntity().getUniqueId()) || A.equals("arrowland") && ((ProjectileHitEvent) event).getEntity() instanceof Arrow && getHitEntity((ProjectileHitEvent) event) == null)
                    || event instanceof EntityShootBowEvent && A.equals("shootbow")

                    || event instanceof PlayerDeathEvent && (A.equals("playerdeath") || A.equals("killedplayer"))
                    || event instanceof EntityDeathEvent && A.equals("killedentity") && !(((EntityDeathEvent) event).getEntity() instanceof Player)

                    || event instanceof CustomEnchantProcEvent && A.equals("enchantproc")
                    || event instanceof CEAApplyPotionEffectEvent && A.equals("ceapplypotioneffect")

                    || event instanceof MobStackDepleteEvent && A.equals("mobstackdeplete")

                    || event instanceof MaskEquipEvent && A.equals("maskequip")
                    || event instanceof MaskUnequipEvent && A.equals("maskunequip")

                    || mcmmoIsEnabled && event instanceof com.gmail.nossr50.events.experience.McMMOPlayerXpGainEvent && (A.equals("mcmmoxpgained") || A.equals("mcmmoxpgained:" + ((com.gmail.nossr50.events.experience.McMMOPlayerXpGainEvent) event).getSkill().name().toLowerCase()))
            ) {
                executeAttributes(player, event, attr);
            }
        }
    }

    private void executeAttributes(Player player, Event event, String attribute) {
        for(String a : attribute.substring(attribute.split(";")[0].length()).split(";")) {
            if(event != null && a.toLowerCase().startsWith("cancel")) {
                ((Cancellable) event).setCancelled(true);
                if(event instanceof EntityDamageByEntityEvent && ((EntityDamageByEntityEvent) event).getDamager() instanceof Arrow) {
                    ((EntityDamageByEntityEvent) event).getDamager().remove();
                }
                return;
            } else {
                w(null, event, null, getRecipients(event, a.contains("[") ? a.split("\\[")[1].split("]")[0] : a, null), a, attribute, -1, player);
            }
        }
    }
    @EventHandler(priority = EventPriority.HIGH)
    private void playerArmorEvent(PlayerArmorEvent event) {
        if(!event.isCancelled()) {
            final String r = event.getReason().name();
            final Player player = event.getPlayer();
            final boolean contains = equippedMasks.keySet().contains(player);
            final ItemStack i = event.getItem().clone(), o = contains ? equippedMasks.get(player) : new ItemStack(Material.AIR);
            final PlayerArmorEvent.ArmorEventReason reason = event.getReason();
            if(!contains && r.contains("_EQUIP")) {
                final Mask m = Mask.getOnItem(i);
                if(m != null) {
                    final MaskEquipEvent e = new MaskEquipEvent(player, m, o, reason);
                    pluginmanager.callEvent(e);
                    if(!e.isCancelled()) {
                        equippedMasks.put(player, i);
                        procMaskAttributes(player, e, m);
                        scheduler.scheduleSyncDelayedTask(randompackage, () -> {
                            player.getInventory().setHelmet(m.getItemStack().clone());
                            player.updateInventory();
                        }, 0);
                    }
                } else return;
            } else if(contains && r.contains("_UNEQUIP")) {
                final Mask m = Mask.valueOf(i);
                if(m != null) {
                    final MaskUnequipEvent e = new MaskUnequipEvent(player, m, o, reason);
                    pluginmanager.callEvent(e);
                    if(!e.isCancelled()) {
                        final ItemStack h = e.helmet;
                        event.setCurrentItem(h);
                        equippedMasks.remove(player);
                        procMaskAttributes(player, e, e.mask);
                        procPlayerItem(event, player, h);
                    }
                } else return;
            } else return;
            player.updateInventory();
        }
    }

    public void apply(Mask m, ItemStack is) {
        if(m != null && is != null) {
            itemMeta = is.getItemMeta(); lore.clear();
            if(itemMeta.hasLore()) lore.addAll(itemMeta.getLore());
            lore.add(m.addedLore);
            itemMeta.setLore(lore); lore.clear();
            is.setItemMeta(itemMeta);
        }
    }
}
