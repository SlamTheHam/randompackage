package me.randomHashTags.RandomPackage.api.nearFinished;

import me.randomHashTags.RandomPackage.RandomPackageAPI;
import me.randomHashTags.RandomPackage.utils.classes.CollectionChest;
import me.randomHashTags.RandomPackage.utils.universal.UMaterial;
import org.bukkit.*;
import org.bukkit.block.Block;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

public class CollectionFilter extends RandomPackageAPI implements Listener, CommandExecutor {

    public boolean isEnabled = false;
    private static CollectionFilter instance;
    public static final CollectionFilter getCollectionFilter() {
        if(instance == null) instance = new CollectionFilter();
        return instance;
    }

    public YamlConfiguration config;
    public ItemStack item_collectionchest;
    public String defaultType, allType, itemType, filtertypeString;
    public int filtertypeSlot = -1;

    private Inventory collectionchestgui;
    private HashMap<Integer, UMaterial> picksup = new HashMap<>();
    private Material allMaterial;
    private HashMap<UUID, Location> editingfilter = new HashMap<>();

    public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {
        final Player player = sender instanceof Player ? (Player) sender : null;
        if(player != null && hasPermission(sender, "RandomPackage.collectionfilter", true)) {
            if(args.length == 0 || args.length == 1 || args.length == 2 && !args[1].equals("all")) {
                final String a = args.length >= 1 ? args.length == 1 ? args[0] : args[0] + "_" + args[1] : "";
                final ItemStack q = getItemInHand(player);
                if(q.getType().equals(item_collectionchest.getType()) && q.getData().getData() == item_collectionchest.getData().getData()
                        && q.hasItemMeta() && q.getItemMeta().getDisplayName().equals(item_collectionchest.getItemMeta().getDisplayName())) {
                    if(a.equals("default"))  setFilter(player, q, defaultType);
                    else if(a.equals("all")) setFilter(player, q, ChatColor.translateAlternateColorCodes('&', config.getString("collection chests.chest.filter types.all")));
                    else {
                        if(args.length == 0)
                            editFilter(player, null);
                        else {
                            Material f = Material.getMaterial(a.toUpperCase());
                            if(f != null) {
                                setFilter(player, q, itemType.replace("{ITEM}", toMaterial(a, false)));
                            } else {
                                sendStringListMessage(player, config.getStringList("messages.invalid filter type"));
                                editFilter(player, null);
                            }
                        }
                    }
                } else {
                    if(args.length == 1) sendStringListMessage(player, config.getStringList("messages.invalid filter type"));
                    sendStringListMessage(sender, config.getStringList("messages.need to be holding cc"));
                }
            } else if(args.length == 2 && args[1].equals("all") || args.length == 3 && args[2].equals("all")) {

            }
        }
        return true;
    }

    public void enable() {
        final long started = System.currentTimeMillis();
        if(isEnabled) return;
        save("Features", "collection filter.yml");
        pluginmanager.registerEvents(this, randompackage);
        isEnabled = true;
        config = YamlConfiguration.loadConfiguration(new File(randompackage.getDataFolder() + File.separator + "Features" + File.separator, "collection filter.yml"));

        item_collectionchest = d(config, "collection chests.chest", 0);
        allType = ChatColor.translateAlternateColorCodes('&', config.getString("collection chests.chest.filter types.all"));
        defaultType = ChatColor.translateAlternateColorCodes('&', config.getString("collection chests.chest.filter types.default"));
        itemType = ChatColor.translateAlternateColorCodes('&', config.getString("collection chests.chest.filter types.item"));

        collectionchestgui = Bukkit.createInventory(null, config.getInt("gui.size"), ChatColor.translateAlternateColorCodes('&', config.getString("gui.title")));
        final ItemStack background = d(config, "gui.background", 0);
        for(int i = 0; i < collectionchestgui.getSize(); i++) {
            if(config.get("gui." + i) != null) {
                final ItemStack itemstack = d(config, "gui." + i, 0);
                collectionchestgui.setItem(i, itemstack);
                final String[] j = config.getString("gui." + i + ".picks up").toUpperCase().split(":");
                final UMaterial um = UMaterial.matchUMaterial(j[0], j.length > 1 ? Byte.parseByte(j[1]) : 0);
                if(um == null && j[0].equalsIgnoreCase("all")) allMaterial = itemstack.getType();
                picksup.put(i, um);
            } else collectionchestgui.setItem(i, background.clone());
        }
        for(int i = 0; i < item_collectionchest.getItemMeta().getLore().size(); i++) if(item_collectionchest.getItemMeta().getLore().get(i).contains("{FILTER_TYPE}")) {
            filtertypeSlot = i;
            filtertypeString = item_collectionchest.getItemMeta().getLore().get(i);
        }
        Bukkit.getConsoleSender().sendMessage(ChatColor.translateAlternateColorCodes('&', "&6[RandomPackage] &aLoaded CollectionFilter &e(took " + (System.currentTimeMillis()-started) + "ms)"));
        final long cc = System.currentTimeMillis();
        scheduler.runTaskAsynchronously(randompackage, () -> {
            final long J = System.currentTimeMillis();
            final ConfigurationSection cf = otherdata.getConfigurationSection("collection chests");
            int loaded = 0;
            if(cf != null) {
                for(String s : cf.getKeys(false)) {
                    final String info = otherdata.getString("collection chests." + s + ".info");
                    final CollectionChest c = new CollectionChest(info.split(":")[0], toLocation(info.split(":")[1]), UMaterial.matchUMaterial(info.split(":")[2], (byte) 0));
                    c.uuid = UUID.fromString(s);
                    CollectionChest.storage.put(c, new HashMap<>());
                    final HashMap<ItemStack, Integer> storage = CollectionChest.storage.get(c);
                    final List<String> st = otherdata.getStringList("collection chests." + s + ".storage");
                    if(st != null && !st.isEmpty())
                        for(String i : st)
                            storage.put(toRPDataItemStack(i.split("\\|\\|")[0]), Integer.parseInt(i.split("\\|\\|")[1]));
                    loaded += 1;
                }
            }
            final long u = System.currentTimeMillis();
            Bukkit.getConsoleSender().sendMessage(ChatColor.translateAlternateColorCodes('&', "&6[RandomPackage] &aLoaded " + loaded + " collection chests &e(took " + (u-J) + "ms) [async " + (u-cc) + "ms]"));
        });
    }
    public void disable() {
        if(!isEnabled) return;
        isEnabled = false;
        picksup.clear();
        for(UUID u : editingfilter.keySet()) {
            final OfflinePlayer o = Bukkit.getOfflinePlayer(u);
            if(o.isOnline()) o.getPlayer().closeInventory();
        }
        editingfilter.clear();
        otherdata.set("collection chests", null);
        for(CollectionChest c : CollectionChest.chests) {
            final UUID u = c.uuid;
            otherdata.set("collection chests." + u + ".info", c.placer + ":" + toString(c.location) + ":" + c.filter);
            final List<String> st = new ArrayList<>();
            final HashMap<ItemStack, Integer> stor = CollectionChest.storage.get(c);
            for(ItemStack is : stor.keySet()) st.add(toRPDataString(is) + "||" + stor.get(is));
            otherdata.set("collection chests." + u + ".storage", st);
        }
        saveOtherData();
        CollectionChest.chests.clear();
        CollectionChest.storage.clear();
        HandlerList.unregisterAll(this);
    }

    @EventHandler
    private void entityDeathEvent(EntityDeathEvent event) {
        if(config.getStringList("enabled worlds").contains(event.getEntity().getWorld().getName())) {
            for(CollectionChest cc : CollectionChest.chests) {
                final HashMap<ItemStack, Integer> storage = CollectionChest.storage.get(cc);
                if(cc.location.getChunk().equals(event.getEntity().getLocation().getChunk())) {
                    final UMaterial um = cc.filter;
                    Material t = null;
                    byte d = 0;
                    if(um != null) {
                        t = um.getMaterial();
                        d = um.getData();
                    }

                    for(int i = 0; i < event.getDrops().size(); i++) {
                        final ItemStack loot = event.getDrops().get(i);
                        final Material mat = loot.getType();
                        final byte dat = loot.getData().getData();
                        if(um == null || um != null && t.equals(mat) && d == dat) {
                            final ItemStack prev = new ItemStack(mat, 1, dat);
                            storage.put(prev, storage.keySet().contains(prev) ? storage.get(prev) + loot.getAmount() : loot.getAmount());
                            event.getDrops().remove(loot);
                            i--;
                        }
                    }
                }
            }
        }
    }

    @EventHandler
    private void playerInteractEvent(PlayerInteractEvent event) {
        if(!event.isCancelled() && event.getClickedBlock() != null && !event.getClickedBlock().getType().equals(Material.AIR) && event.getClickedBlock().getType().name().contains("CHEST")) {
            final CollectionChest cc = CollectionChest.valueOf(event.getClickedBlock());
            if(cc != null) {
                if(event.getAction().equals(Action.RIGHT_CLICK_BLOCK)) {
                    event.setCancelled(true);
                    if(!event.getPlayer().isSneaking()) {
                        event.getPlayer().openInventory(Bukkit.createInventory(event.getPlayer(), config.getInt("collection chests.chest.size"), ChatColor.translateAlternateColorCodes('&', config.getString("collection chests.chest.title"))));
                        final HashMap<ItemStack, Integer> storage = CollectionChest.storage.get(cc);
                        for(ItemStack i : storage.keySet()) {
                            event.getPlayer().getOpenInventory().getTopInventory().addItem(new ItemStack(i.getType(), storage.get(i), i.getData().getData()));
                        }
                        event.getPlayer().updateInventory();
                    } else
                        editFilter(event.getPlayer(), event.getClickedBlock());
                }
            }
        }
    }
    @EventHandler
    private void blockPlaceEvent(BlockPlaceEvent event) {
        if(!event.isCancelled() && event.getItemInHand().getType().equals(item_collectionchest.getType()) && event.getItemInHand().getData().getData() == item_collectionchest.getData().getData()
                && event.getItemInHand().hasItemMeta() && event.getItemInHand().getItemMeta().hasDisplayName() && event.getItemInHand().getItemMeta().getDisplayName().equals(item_collectionchest.getItemMeta().getDisplayName())) {
            item = event.getItemInHand().clone(); item.setAmount(1);
            final UMaterial u = getFiltered(item);
            final CollectionChest cc = new CollectionChest(event.getPlayer().getUniqueId().toString(), event.getBlockPlaced().getLocation(), u);
            sendStringListMessage(event.getPlayer(), config.getStringList("messages.placed"));
            for(String s : config.getStringList("messages.set")) {
                if(s.contains("{ITEM}")) s = s.replace("{ITEM}", cc.filter == null ? defaultType : toMaterial(cc.filter.getMaterial().name(), false));
                event.getPlayer().sendMessage(ChatColor.translateAlternateColorCodes('&', s));
            }
        }
    }
    @EventHandler
    private void blockBreakEvent(BlockBreakEvent event) {
        if(!event.isCancelled() && event.getBlock().getType().equals(item_collectionchest.getType())) {
            final CollectionChest cc = CollectionChest.valueOf(event.getBlock());
            if(cc != null) {
                event.setCancelled(true);
                event.getBlock().setType(Material.AIR);
                if(config.getBoolean("collection chests.chest.keeps meta")) {
                    final UMaterial u = cc.filter;
                    final ItemStack is = item_collectionchest.clone();
                    ItemMeta itemMeta = is.getItemMeta();
                    List<String> lore = itemMeta.getLore();
                    lore.set(filtertypeSlot, filtertypeString.replace("{FILTER_TYPE}", u == null ? allType : itemType.replace("{ITEM}", toMaterial(u.name(), false))));
                    itemMeta.setLore(lore);
                    is.setItemMeta(itemMeta);
                    Bukkit.getWorld(cc.location.getWorld().getName()).dropItemNaturally(cc.location, is);
                }
                cc.destroy();
            }
        }
    }

    @EventHandler
    private void inventoryClickEvent(InventoryClickEvent event) {
        if(!event.isCancelled() && event.getWhoClicked().getOpenInventory().getTopInventory().getHolder() != null && event.getWhoClicked().getOpenInventory().getTopInventory().getHolder().equals(event.getWhoClicked())
                && event.getWhoClicked().getOpenInventory().getTopInventory().getTitle().equals(collectionchestgui.getTitle())) {
            final Player player = (Player) event.getWhoClicked();
            event.setCancelled(true);
            player.updateInventory();

            final int r = event.getRawSlot();
            final ItemStack c = event.getCurrentItem();
            if(c == null || c.getType().equals(Material.AIR) || r >= player.getOpenInventory().getTopInventory().getSize()) return;
            final CollectionChest cc = editingfilter.keySet().contains(player.getUniqueId()) ? CollectionChest.valueOf(player.getWorld().getBlockAt(editingfilter.get(player.getUniqueId()))) : null;
            final UMaterial filter = cc.filter;
            final Material mat = filter != null ? filter.getMaterial() : null;
            final byte data = filter != null ? filter.getData() : -1;
            if(filter != null && mat.equals(c.getType()) && data == c.getData().getData())
                sendStringListMessage(player, config.getStringList("messages.item already being filtered"));
            else {
                if(editingfilter.keySet().contains(player.getUniqueId())) {
                    cc.setFilter(picksup.get(r));
                    editingfilter.remove(player.getUniqueId());
                } else
                    setFilter(player, getItemInHand(player), r);
                player.closeInventory();
            }
        }
    }
    public void editFilter(Player player, Block clickedblock) {
        if(clickedblock != null) editingfilter.put(player.getUniqueId(), clickedblock.getLocation());
        player.openInventory(Bukkit.createInventory(player, collectionchestgui.getSize(), collectionchestgui.getTitle()));
        player.getOpenInventory().getTopInventory().setContents(collectionchestgui.getContents());
        player.updateInventory();
        final String selected = ChatColor.translateAlternateColorCodes('&', config.getString("gui.selected.prefix")), notselected = ChatColor.translateAlternateColorCodes('&', config.getString("gui.not selected.prefix"));
        final boolean selectedEnchanted = config.getBoolean("gui.selected.enchanted"), notselectedEnchanted = config.getBoolean("gui.not selected.enchanted");
        final CollectionChest cc = CollectionChest.valueOf(clickedblock);
        final UMaterial filter = cc.filter;
        final Material mat = filter != null ? filter.getMaterial() : null;
        final byte data = filter != null ? filter.getData() : -1;
        for(int i = 0; i < player.getOpenInventory().getTopInventory().getSize(); i++) {
            if(player.getOpenInventory().getTopInventory().getItem(i) != null && !player.getOpenInventory().getTopInventory().getItem(i).getType().equals(Material.AIR)) {
                final UMaterial u = picksup.get(i);
                final String a = toMaterial(u != null ? u.name() : allMaterial.name(), false);
                item = player.getOpenInventory().getTopInventory().getItem(i); itemMeta = item.getItemMeta();
                final String q = itemMeta.hasDisplayName() ? itemMeta.getDisplayName() : itemMeta.hasEnchants() ? ChatColor.AQUA + a : a;
                final ItemStack target = player.getOpenInventory().getTopInventory().getItem(i);
                boolean sel = filter == null && target.getType().equals(allMaterial) || filter != null && mat.equals(target.getType()) && data == player.getOpenInventory().getTopInventory().getItem(i).getData().getData();
                itemMeta.setDisplayName(sel ? selected : notselected);
                itemMeta.setDisplayName(itemMeta.getDisplayName() + q);
                lore.clear();
                if(itemMeta.hasLore()) lore.addAll(itemMeta.getLore());
                for(String r : config.getStringList("gui." + (sel ? "selected" : "not selected") + ".added lore")) lore.add(ChatColor.translateAlternateColorCodes('&', r));
                if(sel && selectedEnchanted || !sel && notselectedEnchanted) itemMeta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
                itemMeta.setLore(lore); lore.clear();
                item.setItemMeta(itemMeta);
                if(sel && selectedEnchanted || !sel && notselectedEnchanted) item.addUnsafeEnchantment(Enchantment.ARROW_DAMAGE, 1);
            }
        }
        player.updateInventory();
    }

    public ItemStack getCollectionChest(String filter) {
        filter = filter.toLowerCase();
        filter = filter.equals("all") ? allType : filter.equals("default") ? defaultType : toMaterial(filter, false);
        item = item_collectionchest.clone(); itemMeta = item.getItemMeta(); lore.clear();
        if(itemMeta.hasLore()) {
            for(String s : itemMeta.getLore()) {
                if(s.contains("{FILTER_TYPE}")) s = s.replace("{FILTER_TYPE}", filter);
                lore.add(s);
            }
        }
        itemMeta.setLore(lore); lore.clone();
        item.setItemMeta(itemMeta);
        return item;
    }
    public void setFilter(Player player, ItemStack is, int rawslot) {
        itemMeta = is.getItemMeta(); lore.clear();
        for(int i = 0; i < item_collectionchest.getItemMeta().getLore().size(); i++) {
            if(i == filtertypeSlot) {
                lore.add(ChatColor.translateAlternateColorCodes('&', item_collectionchest.getItemMeta().getLore().get(i).replace("{FILTER_TYPE}", toMaterial(picksup.get(rawslot).name(), false))));
            } else {
                lore.add(item_collectionchest.getItemMeta().getLore().get(i));
            }
        }
        itemMeta.setLore(lore);
        is.setItemMeta(itemMeta);
        lore.clear();
        for(String string : config.getStringList("messages.updated cc")) {
            if(string.contains("{AMOUNT}")) string = string.replace("{AMOUNT}", Integer.toString(is.getAmount()));
            if(string.contains("{ITEM}")) string = string.replace("{ITEM}", toMaterial(picksup.get(rawslot).name(), false));
            player.sendMessage(ChatColor.translateAlternateColorCodes('&', string));
        }
        if(player != null) player.updateInventory();
    }
    public void setFilter(Player player, ItemStack is, String filter) {
        itemMeta = is.getItemMeta(); lore.clear(); lore.addAll(itemMeta.getLore());
        lore.set(filtertypeSlot, item_collectionchest.getItemMeta().getLore().get(filtertypeSlot).replace("{FILTER_TYPE}", filter));
        itemMeta.setLore(lore);
        is.setItemMeta(itemMeta);
        lore.clear();
        for(String string : config.getStringList("messages.set")) {
            if(string.contains("{ITEM}")) string = string.replace("{ITEM}", filter);
            player.sendMessage(ChatColor.translateAlternateColorCodes('&', string));
        }
    }

    public UMaterial getFiltered(ItemStack is) {
        final String u = item_collectionchest.clone().getItemMeta().getLore().get(filtertypeSlot).replace("{FILTER_TYPE}", itemType);
        for(UMaterial s : picksup.values()) {
            if(s == null) return null;
            if(ChatColor.stripColor(is.getItemMeta().getLore().get(filtertypeSlot).toUpperCase()).equals(toMaterial(ChatColor.stripColor(u.replace("{ITEM}", s.name().replace("_", " "))), false))) {
                return s;
            }
        }
        return null;
    }
    public ItemStack getFilteredItem(ItemStack is) {
        final String u = item_collectionchest.clone().getItemMeta().getLore().get(filtertypeSlot).replace("{FILTER_TYPE}", itemType);
        for(UMaterial s : picksup.values()) {
            if(s == null) return null;
            if(ChatColor.stripColor(is.getItemMeta().getLore().get(filtertypeSlot).toUpperCase()).equals(ChatColor.stripColor(u.replace("{ITEM}", s.name().replace("_", " "))))) {
                return s.getItemStack();
            }
        }
        return null;
    }

}
