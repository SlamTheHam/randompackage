package me.randomHashTags.RandomPackage.api.nearFinished;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import me.randomHashTags.RandomPackage.api.events.customboss.CustomBossDamageByEntityEvent;
import me.randomHashTags.RandomPackage.utils.universal.UMaterial;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockGrowEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;

import me.randomHashTags.RandomPackage.RPPlayerData;
import me.randomHashTags.RandomPackage.RandomPackageAPI;
import me.randomHashTags.RandomPackage.utils.classes.factionadditions.FactionUpgrade;
import me.randomHashTags.RandomPackage.utils.classes.factionadditions.FactionUpgradeType;
import me.randomHashTags.RandomPackage.utils.GivedpItem;
import me.randomHashTags.RandomPackage.utils.supported.VaultAPI;

public class FactionAdditions extends RandomPackageAPI implements Listener {
	public boolean isEnabled = false;
	private static FactionAdditions instance;
	public static final FactionAdditions getFactionAdditions() {
	    if(instance == null) instance = new FactionAdditions();
	    return instance;
	}

	public YamlConfiguration config;
	private Inventory factionUpgrades;
	private ItemStack background, locked;
	public ItemStack heroicFactionCrystal, factionCrystal, factionMCMMOBooster, factionXPBooster;
	private List<String> aliases;
	private final List<String> attributes = new ArrayList<>(Arrays.asList(
			"allowfactionflight", "allowsmeltable", "increasemaxfactionwarps", "increasemaxfactionsize", "increasefactionpower", "reducemcmmocooldown", "setmobspawnrate", "setmobxpmultiplier", "setteleportdelaymultiplier",
			"sethungermultiplier", "setcropgrowthmultiplier", "setenemydamagemultiplier", "decreaseraritygemcost", "lmsmultiplier", "setoutpostcapmultiplier", "setconquestmobdamagemultiplier", "setbossdamagemultiplier",
			"reducecombattagtimer", "reduceenderpearlcooldown", "setdungeonportalappearancetime", "reducedungeonlootredeemable", "increasevkitlevelingchance", "setdungeonlootbagbonus", "setarmorsetdamagemultiplier")
			);
	public static final HashMap<String, HashMap<Location, Double>> cropGrowthRate = new HashMap<>();
	
	@EventHandler(priority = EventPriority.HIGHEST)
	private void playerCommandPreprocessEvent(PlayerCommandPreprocessEvent event) {
		final Player player = event.getPlayer();
		final String a = event.getMessage().toLowerCase().substring(1);
		for(String s : aliases) {
			if(a.startsWith(s)) {
				event.setCancelled(true);
				if(a.contains("reset")) {
					if(hasPermission(player, "RandomPackage.fupgrade", true)) {
						RPPlayerData.setFactionUpgrades(fapi.getFaction(player), new HashMap<>());
					}
				} else if(hasPermission(player, "RandomPackage.fupgrade.reset", true)) {
					viewFactionUpgrades(player);
				}
				return;
			}
		}
	}

	public void enable() {
		if(isEnabled) return;
		save("Features", "faction additions.yml");
		pluginmanager.registerEvents(this, randompackage);
		isEnabled = true;
		aliases = randompackage.getConfig().getStringList("faction-additions.aliases");
		config = YamlConfiguration.loadConfiguration(new File(randompackage.getDataFolder() + File.separator + "Features" + File.separator, "faction additions.yml"));
		heroicFactionCrystal = d(config, "items.heroic-faction-crystal", 0);
		factionCrystal = d(config, "items.faction-crystal", 0);
		factionMCMMOBooster = d(config, "items.faction-mcmmo-booster", 0);
		factionXPBooster = d(config, "items.faction-xp-booster", 0);
		factionUpgrades = Bukkit.createInventory(null, config.getInt("f-upgrades.size"), ChatColor.translateAlternateColorCodes('&', config.getString("f-upgrades.title")));
		background = d(config, "f-upgrades.background", 0);
		locked = d(config, "f-upgrades.locked", 0);
		addGivedpCategory(Arrays.asList(factionCrystal, heroicFactionCrystal, factionMCMMOBooster, factionXPBooster), UMaterial.DIAMOND_SWORD, "Faction Items", "Givedp: Faction Items");
		FactionUpgradeType defaultType = null;
		final int defaultMaxTier = config.getInt("upgrades.default-settings.max-tier");
		final boolean itemAmountEqualsTier = config.getBoolean("upgrades.default-settings.item-amount-equals-tier");
		for(String s : config.getConfigurationSection("upgrades").getKeys(false)) {
			if(s.equals("types")) {
				for(String S : config.getConfigurationSection("upgrades.types").getKeys(false)) {
					final boolean a = config.get("upgrades." + S + ".item-amount-equals-tier") != null ? config.getBoolean("upgrades." + S + ".item-amount-equals-tier") : itemAmountEqualsTier;
					final FactionUpgradeType u = new FactionUpgradeType(S, config.getString("upgrades.types." + S + ".perk-achieved-prefix"), config.getString("upgrades.types." + S + ".perk-unachieved-prefix"), config.getString("upgrades.types." + S + ".requirements-prefix"), config.getStringList("upgrades.types." + S + ".unlock"), config.getStringList("upgrades.types." + S + ".upgrade"), config.getStringList("upgrades.types." + S + ".maxed"), config.getStringList("upgrades.types." + S + ".format"), a);
					if(S.equals("DEFAULT")) defaultType = u;
				}
			} else if(!s.equals("default-settings") && !s.equals("types")) {
				final FactionUpgradeType type = config.get("upgrades." + s + ".type") == null ? defaultType : FactionUpgradeType.valueOf(config.getString("upgrades." + s + ".type"));
				final List<String> desc = config.getStringList("upgrades." + s + ".desc"), perks = config.getStringList("upgrades." + s + ".perks"), requirements = config.getStringList("upgrades." + s + ".requirements");
				final int slot = config.getInt("upgrades." + s + ".slot"), maxtier = config.get("upgrades." + s + ".max-tier") != null ? config.getInt("upgrades." + s + ".max-tier") : defaultMaxTier;
				final ItemStack i = d(config, "upgrades." + s, 0); itemMeta = i.getItemMeta(); itemMeta.setLore(type.format); lore.clear();
				for(String f : itemMeta.getLore()) {
					if(f.equals("{DESC}"))
						for(String d : desc) lore.add(ChatColor.translateAlternateColorCodes('&', d));
					else
						lore.add(ChatColor.translateAlternateColorCodes('&', f));
				}
				itemMeta.setLore(lore); lore.clear();
				i.setItemMeta(itemMeta);
				final FactionUpgrade u = new FactionUpgrade(s, slot, maxtier, desc, type, i, perks, requirements);
				factionUpgrades.setItem(slot, u.getDisplayItem().clone());
			}
		}
	}
	public void disable() {
		if(!isEnabled) return;
		isEnabled = false;
		aliases.clear();
		cropGrowthRate.clear();
		HandlerList.unregisterAll(this);
	}

	@EventHandler
	private void inventoryClickEvent(InventoryClickEvent event) {
		if(!event.isCancelled() && event.getWhoClicked().getOpenInventory().getTopInventory().getHolder() == event.getWhoClicked()) {
			final Player player = (Player) event.getWhoClicked();
			final String t = player.getOpenInventory().getTopInventory().getTitle();
			if(t.equals(factionUpgrades.getTitle())) {
				event.setCancelled(true);
				player.updateInventory();
				final String c = event.getClick().name();
				if(event.getRawSlot() < 0 || event.getRawSlot() >= event.getWhoClicked().getOpenInventory().getTopInventory().getSize() || !c.contains("LEFT") && !c.contains("RIGHT") || event.getCurrentItem() == null) return;
				final FactionUpgrade f = FactionUpgrade.valueOf(event.getRawSlot());
				if(f != null) tryToUpgrade(player, f);
			}
		}
	}

	private ItemStack getUpgrade(HashMap<FactionUpgrade, Integer> upgrades, int slot, String W, String L) {
		final FactionUpgrade f = FactionUpgrade.valueOf(slot);
		if(f != null) {
			final int tier = upgrades != null && upgrades.keySet().contains(f) ? upgrades.get(f) : 0;
			item = f.getDisplayItem().clone();
			itemMeta = item.getItemMeta(); lore.clear();
			final FactionUpgradeType type = f.getType();
			final String perkAchived = type.perkAchievedPrefix, perkUnachived = type.perkUnachievedPrefix, requirementsPrefix = type.requirementsPrefix;
			if(item.hasItemMeta() && itemMeta.hasLore()) {
				for(String s : itemMeta.getLore()) {
					if(s.equals("{TIER}")) {
						lore.add(tier == 0 ? L : W.replace("{MAX_TIER}", Integer.toString(f.getMaxTier())).replace("{TIER}", Integer.toString(tier)));
					} else {
						boolean did = false;
						int a = 0;
						if(s.equals("{PERKS}")) {
							did = true;
							for(String p : f.getPerks()) {
								a += 1;
								final int targetTier = getRemainingInt(p.replace("#", Integer.toString(a)).split(";")[0]);
								lore.add(ChatColor.translateAlternateColorCodes('&', (tier >= targetTier ? perkAchived : perkUnachived) + p.replace("#", Integer.toString(a)).split(";")[2]));
								if(a == 1 && f.getPerks().size() == 1 && p.split(";")[0].contains("#")) {
									for(int g = a + 1; g <= f.getMaxTier(); g++) {
										lore.add(ChatColor.translateAlternateColorCodes('&', (tier >= g ? perkAchived : perkUnachived) + p.replace("#", Integer.toString(g)).split(";")[2]));
									}
								}
							}
						} else if(s.equals("{REQUIREMENTS}")) {
							did = true;
							for(String r : f.getRequirements()) {
								final int targetTier = getRemainingInt(r.split(";")[0]);
								final String R = r.contains("}") ? r.split("};")[1] : r.split(";")[2];
								if(tier+1 == targetTier) {
									lore.add(ChatColor.translateAlternateColorCodes('&', requirementsPrefix + R));
								}
							}
						}
						if(!did) lore.add(s);
					}
				}
				for(String s : tier == 0 ? type.unlock : type.upgrade) lore.add(ChatColor.translateAlternateColorCodes('&', s));
				itemMeta.setLore(lore); lore.clear();
				itemMeta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES, ItemFlag.HIDE_ENCHANTS, ItemFlag.HIDE_POTION_EFFECTS);
				item.setItemMeta(itemMeta);
				if(tier == 0) {
					final String n = itemMeta.getDisplayName();
					final List<String> l = itemMeta.getLore();
					ItemStack F = locked.clone(); itemMeta = F.getItemMeta();
					itemMeta.setDisplayName(n);
					itemMeta.setLore(l);
					F.setItemMeta(itemMeta);
					item = F;
				}
				if(type.itemAmountEqualsTier) item.setAmount(tier == 0 ? 1 : tier);
			}
			return item;
		} else {
			return null;
		}
	}
	public void viewFactionUpgrades(Player player) {
		player.closeInventory();
		final String f = fapi.getFaction(player);
		final HashMap<FactionUpgrade, Integer> u = f != null ? RPPlayerData.getFactionUpgrades(f) : new HashMap<>();
		player.openInventory(Bukkit.createInventory(player, factionUpgrades.getSize(), factionUpgrades.getTitle()));
		final Inventory top = player.getOpenInventory().getTopInventory();
		top.setContents(factionUpgrades.getContents());
		final String W = ChatColor.translateAlternateColorCodes('&', config.getString("f-upgrades.tier")), L = ChatColor.translateAlternateColorCodes('&', config.getString("f-upgrades.locked.tier"));
		for(int i = 0; i < top.getSize(); i++) {
			item = top.getItem(i);
			if(item != null) {
				final ItemStack upgrade = getUpgrade(u, i, W, L);
				if(upgrade != null) {
					top.setItem(i, upgrade);
				}
			} else top.setItem(i, background.clone());
		}
		player.updateInventory();
	}
	public void tryToUpgrade(Player player, FactionUpgrade fu) {
	    final String f = fapi.getFaction(player);
		final HashMap<FactionUpgrade, Integer> upgrades = f != null ? RPPlayerData.getFactionUpgrades(f) : new HashMap<>();
		final int ti = upgrades.keySet().contains(fu) ? upgrades.get(fu) : 0;
		if(ti >= fu.getMaxTier()) return;
		double requiredCash = 0.00, requiredSpawnerValue = 0.00;
		ItemStack requiredItem = null;

		for(String req : fu.getRequirements()) {
			final int tier = getRemainingInt(req.split(";")[0]);
			if(tier == ti+1) {
				String target = req.toLowerCase().split("tier" + (ti+1) + ";")[1].split("}")[0];
				if(target.startsWith("cash{")) {
					final double amount = getRemainingDouble(target.split("\\{")[1]);
					requiredCash = amount;
					if(VaultAPI.economy.getBalance(player) < amount) {
						final String a = formatDouble(amount);
						for(String s : config.getStringList("messages.dont-have-cash")) {
							if(s.contains("{COST}")) s = s.replace("{COST}", a);
							player.sendMessage(ChatColor.translateAlternateColorCodes('&', s));
						}
						return;
					}
				} else if(target.startsWith("item{")) {
					final ItemStack a = GivedpItem.getGivedpItem().valueOf(target.split("\\{")[1]);
					requiredItem = a;
					a.setAmount(Integer.parseInt(target.split("\\{")[1].split("}")[0].split("amount=")[1]));
					if(!player.getInventory().containsAtLeast(a, a.getAmount())) {
						for(String s : config.getStringList("messages.dont-have-item")) {
							if(s.contains("{ITEM}")) s = s.replace("{ITEM}", req.split("};")[1]);
							player.sendMessage(ChatColor.translateAlternateColorCodes('&', s));
						}
						return;
					}
				} else if(target.startsWith("spawnervalue{")) {
					
				} else if(target.startsWith("factionupgrade{")) {
					final String p = target.split("\\{")[1].split("}")[0];
					final FactionUpgrade fuu = FactionUpgrade.valueOf(p.split(":")[0]);
					final int lvl = Integer.parseInt(p.split(":")[1].split("=")[1]);
					if(!upgrades.keySet().contains(fuu)) {
					    final String di = ChatColor.stripColor(fuu.getDisplayItem().getItemMeta().getDisplayName());
						for(String s : config.getStringList("messages.dont-have-f-upgrade")) {
							if(s.contains("{UPGRADE}")) s = s.replace("{UPGRADE}", di + " " + toRoman(lvl));
							player.sendMessage(ChatColor.translateAlternateColorCodes('&', s));
						}
						return;
					}
				}
			}
		}
		if(requiredCash != 0.00)
			VaultAPI.economy.withdrawPlayer(player, requiredCash);
		if(requiredItem != null)
			removeItem(player, requiredItem, requiredItem.getAmount());
		if(!RPPlayerData.factionUpgrades.keySet().contains(f)) RPPlayerData.factionUpgrades.put(f, new HashMap<>());
        RPPlayerData.factionUpgrades.get(f).put(fu, ti+1);
        final HashMap<String, String> values = getValues(f, fu);
		for(String key : values.keySet()) {
			final double value = Double.parseDouble(values.get(key));
			if(key.equals("increasefactionpower"))
				fapi.increasePowerBoost(f, value);
			else if(key.equals("setteleportdelaymultiplier"))
				fapi.setTeleportDelayMultiplier(f, value);
			else if(key.equals("setcropgrowthmultiplier"))
				fapi.setCropGrowthMultiplier(f, value);
			else if(key.equals("setenemydamagemultiplier"))
				fapi.setEnemyDamageMultiplier(f, value);
			else if(key.equals("setbossdamagemultiplier"))
				fapi.setBossDamageMultiplier(f, value);
		}
		final int slot = fu.getSlot();
		player.getOpenInventory().getTopInventory().setItem(slot, getUpgrade(upgrades, slot, ChatColor.translateAlternateColorCodes('&', config.getString("f-upgrades.tier")), ChatColor.translateAlternateColorCodes('&', config.getString("f-upgrades.locked.tier"))));
		player.updateInventory();
	}



	public HashMap<String, String> getValues(String factionName, FactionUpgrade upgrade) {
		final HashMap<FactionUpgrade, Integer> upgrades = RPPlayerData.getFactionUpgrades(factionName);
		final int tier = upgrades.keySet().contains(upgrade) ? upgrades.get(upgrade) : 0;
		final HashMap<String, String> values = new HashMap<>();
		for(String s : upgrade.getPerks()) {
			final int targetTier = getRemainingInt(s.split(";")[0]);
			if(targetTier == tier) {
				for(String at : attributes)
					if(s.toLowerCase().contains(at))
						values.put(at, s.toLowerCase().split(at + "\\{")[1].split("};")[0]);
			}
		}
		return values;
	}

	@EventHandler(priority = EventPriority.LOWEST)
	private void customBossDamageByEntityEvent(CustomBossDamageByEntityEvent event) {
		if(!event.isCancelled() && event.getDamager() instanceof Player) {
			final Player damager = (Player) event.getDamager();
			final String fn = fapi.getFaction(damager);
			event.setDamage(event.getDamage()*fapi.getBossDamageMultiplier(fn));
		}
	}
	@EventHandler(priority = EventPriority.LOWEST)
	private void entityDamageByEntityEvent(EntityDamageByEntityEvent event) {
		if(!event.isCancelled() && event.getDamager() instanceof Player && event.getEntity() instanceof Player) {
			final Player damager = (Player) event.getDamager(), victim = (Player) event.getEntity();
			if(fapi.relationIsEnemyOrNull(damager, victim)) {
				final String f = fapi.getFaction(damager);
				event.setDamage(event.getDamage()*fapi.getEnemyDamageMultiplier(f));
			}
		}
	}
	@EventHandler(priority = EventPriority.HIGHEST)
	private void blockGrowEvent(BlockGrowEvent event) {
		if(!event.isCancelled()) {
			final Location l = event.getBlock().getLocation();
			final String f = fapi.getFactionAt(l);
			final double cgm = f != null ? fapi.getCropGrowthMultiplier(f) : 1.00;
			if(cgm != 1.00) {
				final Material m = l.getBlock().getType();
				if(!cropGrowthRate.keySet().contains(f)) {
					cropGrowthRate.put(f, new HashMap<>());
					cropGrowthRate.get(f).put(l, 0.00);
				} else if(!cropGrowthRate.get(f).keySet().contains(l)) {
					cropGrowthRate.get(f).put(l, 0.00);
				}
				cropGrowthRate.get(f).put(l, cropGrowthRate.get(f).get(l) + fapi.getCropGrowthMultiplier(f));
				byte d = (byte) (Math.floor(cropGrowthRate.get(f).get(l))), max = (byte) (m.name().equals("CROPS") || m.name().equals("CARROT") || m.name().equals("POTATO") ? 7 : 7);
				if(d > max) {
					d = max;
				}
				event.getNewState().setRawData(d);
			}
		}
	}
	@EventHandler(priority = EventPriority.HIGHEST)
	private void blockBreakEvent(BlockBreakEvent event) {
		if(!event.isCancelled()) {
			final Location l = event.getBlock().getLocation();
			final String f = fapi.getFactionAt(l);
			if(f != null && cropGrowthRate.keySet().contains(f) && cropGrowthRate.get(f).keySet().contains(l)) {
				cropGrowthRate.get(f).remove(l);
			}
		}
	}
}
