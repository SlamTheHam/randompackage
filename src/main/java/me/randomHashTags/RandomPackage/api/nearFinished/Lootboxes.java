package me.randomHashTags.RandomPackage.api.nearFinished;

import me.randomHashTags.RandomPackage.RandomPackageAPI;
import me.randomHashTags.RandomPackage.utils.classes.Lootbox;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.io.File;
import java.util.List;

public class Lootboxes extends RandomPackageAPI implements CommandExecutor, Listener {

    private static Lootboxes instance;
    public static final Lootboxes getLootboxes() {
        if(instance == null) instance = new Lootboxes();
        return instance;
    }

    public boolean isEnabled = false;
    public YamlConfiguration config;
    private String title;
    private int cooldownStart;
    private ItemStack cooldown, background;


    public boolean onCommand(CommandSender sender, Command command, String commandLabel, String[] args) {
        final Player player = sender instanceof Player ? (Player) sender : null;
        final int l = args.length;
        if(l == 0) {
        } else {
            final String a = args[0];
            if(a.equals("help")) viewHelp(sender);
        }
        return true;
    }

    public void enable() {
        final long started = System.currentTimeMillis();
        if(isEnabled) return;
        save("Features", "lootboxes.yml");
        config = YamlConfiguration.loadConfiguration(new File(randompackage.getDataFolder() + File.separator + "Features" + File.separator, "lootboxes.yml"));
        pluginmanager.registerEvents(this, randompackage);
        isEnabled = true;
        title = ChatColor.translateAlternateColorCodes('&', config.getString("gui.title"));
        cooldown = d(config, "gui.cooldown", 0);
        cooldownStart = config.getInt("gui.countdown.start number");
        background = d(config, "gui.background", 0);

        int loaded = 0;
        final int defaultInvSize = config.getInt("default settings.size"), defaultMaxUsages = config.getInt("default settings.max usages");
        final long defaultCooldown = config.getLong("default settings.cooldown");
        final String defaultRegularSlots = config.getString("default settings.regular slots");
        final boolean defaultAllowsDuplicates = config.getBoolean("default settings.allows duplicates");

        for(String s : config.getConfigurationSection("lootboxes").getKeys(false)) {
            final ItemStack is = d(config, "lootboxes." + s, 0);
            final int rrs = config.getInt("lootboxes." + s + ".regular reward size"), invSize = config.getInt("lootboxes." + s + ".size"), maxUsages = config.getInt("lootboxes." + s + ".max usages");
            final String rs = config.getString("lootboxes." + s + ".regular slots"), bs = config.getString("lootboxes." + s + ".bonus slots"), rrf = ChatColor.translateAlternateColorCodes('&', config.getString("lootboxes." + s + ".regular reward format")), brf = ChatColor.translateAlternateColorCodes('&', "lootboxes." + s + ".bonus reward format");
            final List<String> rr = config.getStringList("lootboxes." + s + ".regular rewards"), br = config.getStringList("lootboxes." + s + ".bonus rewards");
            final long cooldown = config.getLong("lootboxes." + s + ".cooldown");
            final boolean allowsDuplicates = config.get("lootboxes." + s + ".allows duplicates") != null ? config.getBoolean("lootboxes." + s + ".allows duplicates") : defaultAllowsDuplicates;
            new Lootbox(s, invSize != 0 ? invSize : defaultInvSize, rs, bs, is, rrs, rrf, rr, brf, br, maxUsages != 0 ? maxUsages : defaultMaxUsages, cooldown != 0 ? cooldown : defaultCooldown, allowsDuplicates);
            loaded += 1;
        }
        Bukkit.getConsoleSender().sendMessage(ChatColor.translateAlternateColorCodes('&', "&6[RandomPackage] &aLoaded " + loaded + " lootboxes &e(took " + (System.currentTimeMillis()-started) + "ms)"));
    }
    public void disable() {
        if(!isEnabled) return;
        isEnabled = false;
        Lootbox.lootboxes.clear();
        HandlerList.unregisterAll(this);
    }

    @EventHandler
    private void playerInteractEvent(PlayerInteractEvent event) {
        final Lootbox lb = Lootbox.valueOf(event.getItem());
        if(lb != null) {
            open(event.getPlayer(), lb);
        }
    }

    @EventHandler
    private void inventoryCloseEvent(InventoryCloseEvent event) {
    }

    public void viewHelp(CommandSender sender) {
        if(hasPermission(sender, "RandomPackage.lootbox.help", true))
            sendStringListMessage(sender, config.getStringList("messages.help"));
    }
    public void open(Player player, Lootbox lb) {
        player.openInventory(Bukkit.createInventory(player, 9, title));
        final String[] regular = lb.regularSlots.split(";"), bonus = lb.bonusSlots.split(";");
        final List<String> reg = lb.regularRewards, bon = lb.bonusRewards;
        final Inventory top = player.getOpenInventory().getTopInventory();
        for(int i = 0; i < regular.length; i++) {
            top.setItem(Integer.parseInt(regular[i]), d(null, reg.get(i), 0));
        }
        for(int i = 0; i < bonus.length; i++) {
            top.setItem(Integer.parseInt(bonus[i]), d(null, bon.get(i), 0));
        }
        for(int i = 0; i < top.getSize(); i++) {
            final ItemStack is = top.getItem(i);
            if(is == null || is.getType().equals(Material.AIR))
                top.setItem(i, background.clone());
        }
        player.updateInventory();
    }
}
