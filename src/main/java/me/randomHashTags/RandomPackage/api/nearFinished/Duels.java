package me.randomHashTags.RandomPackage.api.nearFinished;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;

import me.randomHashTags.RandomPackage.RPPlayerData;
import me.randomHashTags.RandomPackage.RandomPackageAPI;
import me.randomHashTags.RandomPackage.utils.classes.customenchants.CustomEnchant;
import me.randomHashTags.RandomPackage.utils.classes.customenchants.EnchantRarity;
import me.randomHashTags.RandomPackage.utils.classes.duels.ActiveDuel;
import me.randomHashTags.RandomPackage.utils.classes.duels.DuelArena;
import me.randomHashTags.RandomPackage.utils.classes.duels.DuelSettings;

public class Duels extends RandomPackageAPI implements Listener, CommandExecutor {
	public boolean isEnabled = false;
	private static Duels instance;
	public static final Duels getDuels() {
		if(instance == null) instance = new Duels();
		return instance;
	}
	
	private HashMap<Integer, EnchantRarity> duelEnchantRaritieS = new HashMap<>();
	
	private FileConfiguration config;
	public Inventory duelGodset, duelType, duelSettings, duelArena, duelEnchantRarities, duelEnchants;
	private ItemStack settingsbackground;
	private int confirmslot = -1;
	private String enabled = null, disabled = null;
	
	private HashMap<UUID, Material> types = new HashMap<>();

	private ItemStack[] items = new ItemStack[19];
	private HashMap<Integer, String> kits = new HashMap<>(), settings = new HashMap<>();
	
	private HashMap<String, Integer> maxcustomenchants = new HashMap<>(), duelsettings = new HashMap<>();
	private HashMap<EnchantRarity, Integer> raritycost = new HashMap<>();
	private HashMap<Player, Integer> back = new HashMap<>();
	
	public HashMap<UUID, HashMap<Material, HashMap<CustomEnchant, Integer>>> editedGodsets = new HashMap<>();
	
	public int getMaxEnchants(ItemStack is) {
		if(is != null)
			for(String s : maxcustomenchants.keySet())
				if(is.getType().name().toLowerCase().endsWith(s.toLowerCase()))
					return maxcustomenchants.get(s);
		return -1;
	}

	public void enable() {
	    if(isEnabled) return;
	    save("Features", "duels.yml");
	    pluginmanager.registerEvents(this, randompackage);
        isEnabled = true;
        config = YamlConfiguration.loadConfiguration(new File(randompackage.getDataFolder() + File.separator + "Features" + File.separator, "duels.yml"));
        items[0] = d(config, "items.ranked", 0);
        items[1] = d(config, "items.unranked", 0);
        items[2] = d(config, "duel-settings.confirm", 0);
        items[3] = d(config, "duel-settings.gapples", 0);
        items[4] = d(config, "duel-settings.mcmmo", 0);
        items[5] = d(config, "duel-settings.potions", 0);
        items[6] = d(config, "duel-settings.bows", 0);
        items[7] = d(config, "duel-settings.healing", 0);
        items[8] = d(config, "duel-settings.food-loss", 0);
        items[9] = d(config, "duel-settings.ender-pearls", 0);
        items[10] = d(config, "duel-settings.risk-inventory", 0);
        items[11] = d(config, "duel-settings.bounty", 0);
        items[12] = d(config, "duel-settings.armor", 0);
        items[13] = d(config, "duel-settings.weapons", 0);
        items[14] = d(config, "duel-settings./fix", 0);
        items[15] = d(config, "duel-settings./fly", 0);
        items[16] = d(config, "duel-settings.envoy", 0);
        items[17] = d(config, "duel-settings.death-certificates", 0);
        for(int i = 0; i <= 100; i++) if(config.get("kits." + i) != null) kits.put(i, config.getString("kits." + i + ".kit-name"));
        final String defaultkit = config.getString("duel-settings.kit.default-kit");
        for(int m : kits.keySet()) {
            if(kits.get(m).equals(defaultkit))
                items[18] = d(config, "kits." + m, 0);
        }
        //
        for(String s : config.getConfigurationSection("godset.max-custom-enchants").getKeys(true))
            maxcustomenchants.put(s, config.getInt("godset.max-custom-enchants." + s));
        //
        List<String> u = null;
        String path = null;
        enabled = ChatColor.translateAlternateColorCodes('&', config.getString("duel-settings.enabled"));
        disabled = ChatColor.translateAlternateColorCodes('&', config.getString("duel-settings.disabled"));
        settingsbackground = d(config, "duel-settings.background", 0);
        for(int i = 1; i <= 7; i++) {
            if(i == 1) path = "duel-type.";
            else if(i == 2) path = "duel-settings.";
            else if(i == 3) path = "arenas.";
            else if(i == 4) path = "godset.";
            else if(i == 5) path = "duel-enchantments.";
            else if(i == 6) path = "duel-enchants.";
            else if(i == 7) { // Kits
                path = "kits.";
                u = config.getStringList("kits.select-kit-lore");
            } else return;

            Inventory inv = Bukkit.createInventory(null, config.getInt(path + "size"), ChatColor.translateAlternateColorCodes('&', config.getString(path + "title")));
            if(inv != null) {
                lore.clear();
                for(int z = 0; z < inv.getSize(); z++) {
                    item = null;
                    if(config.get(path + z + ".item") != null || i == 2) {
                        if(i == 1 && config.getString(path + z + ".item").equalsIgnoreCase("{RANKED}"))       item = items[0].clone();
                        else if(i == 1 && config.getString(path + z + ".item").equalsIgnoreCase("{UNRANKED}"))item = items[1].clone();
                        else if(i == 2 && config.get(path + z) == null) item = settingsbackground;
                        else if(i == 2 && config.getString(path + z).equalsIgnoreCase("{CONFIRM}")) {
                            item = items[2].clone();
                            confirmslot = z;
                        } else if(i == 2 && config.getString(path + z).equalsIgnoreCase("{GAPPLES}"))         item = items[3].clone();
                        else if(i == 2 && config.getString(path + z).equalsIgnoreCase("{MCMMO}"))             item = items[4].clone();
                        else if(i == 2 && config.getString(path + z).equalsIgnoreCase("{POTIONS}"))           item = items[5].clone();
                        else if(i == 2 && config.getString(path + z).equalsIgnoreCase("{BOWS}"))              item = items[6].clone();
                        else if(i == 2 && config.getString(path + z).equalsIgnoreCase("{HEALING}"))           item = items[7].clone();
                        else if(i == 2 && config.getString(path + z).equalsIgnoreCase("{FOOD_LOSS}"))         item = items[8].clone();
                        else if(i == 2 && config.getString(path + z).equalsIgnoreCase("{ENDER_PEARLS}"))      item = items[9].clone();
                        else if(i == 2 && config.getString(path + z).equalsIgnoreCase("{RISK_INVENTORY}"))    item = items[10].clone();
                        else if(i == 2 && config.getString(path + z).equalsIgnoreCase("{BOUNTY}"))            item = items[11].clone();
                        else if(i == 2 && config.getString(path + z).equalsIgnoreCase("{ARMOR}"))             item = items[12].clone();
                        else if(i == 2 && config.getString(path + z).equalsIgnoreCase("{WEAPONS}"))           item = items[13].clone();
                        else if(i == 2 && config.getString(path + z).equalsIgnoreCase("{/FIX}"))              item = items[14].clone();
                        else if(i == 2 && config.getString(path + z).equalsIgnoreCase("{/FLY}"))              item = items[15].clone();
                        else if(i == 2 && config.getString(path + z).equalsIgnoreCase("{ENVOY}"))             item = items[16].clone();
                        else if(i == 2 && config.getString(path + z).equalsIgnoreCase("{DEATH_CERTIFICATES}"))item = items[17].clone();
                        else if(i == 2 && config.getString(path + z).equalsIgnoreCase("{KIT}"))               item = items[18].clone();

                        else if(config.get(path + z + ".item") != null) {
                            item = d(config, path + z, 0); itemMeta = item.getItemMeta();
                            if(i == 4) {
                                item = addVanillaEnchants(item);
                                if(config.get(path + z + ".lore") != null)
                                    for(String string : config.getStringList(path + z + ".lore"))
                                        lore.add(ChatColor.translateAlternateColorCodes('&', string));
                            } else if(config.get(path + z + ".lore") == null && u != null)
                                for(String string : u) lore.add(ChatColor.translateAlternateColorCodes('&', string));
                            else for(String string : config.getStringList(path + z + ".lore")) lore.add(ChatColor.translateAlternateColorCodes('&', string));
                            if(i != 4) {
                                itemMeta.setLore(lore); lore.clear();
                                item.setItemMeta(itemMeta);
                            }
                        }
                        if(item != null && i == 2) {
                            itemMeta = item.getItemMeta(); lore.clear();
                            boolean ee = false;
                            if(config.getString(path + z) != null && config.getString(path + z).startsWith("{")) {
                                settings.put(z, config.getString(path + z));
                                if(z != confirmslot) {
                                    final String Q = config.getString(path + z).toLowerCase().split("\\{")[1].split("}")[0].replace("_", "-");
                                    duelsettings.put(Q, z);
                                    if(config.get(path + Q + ".default-value") != null && config.getString(path + Q + ".default-value").equalsIgnoreCase("enabled")) {
                                        ee = true;
                                        lore.add(enabled);
                                    } else lore.add(disabled);
                                }
                            }
                            itemMeta.addItemFlags(ItemFlag.HIDE_ENCHANTS, ItemFlag.HIDE_ATTRIBUTES);
                            if(itemMeta.hasLore()) lore.addAll(itemMeta.getLore());
                            if(item != items[2] && item != settingsbackground)
                                for(String s : config.getStringList("duel-settings.toggle-lore")) lore.add(ChatColor.translateAlternateColorCodes('&', s));
                            itemMeta.setLore(lore); lore.clear();
                            item.setItemMeta(itemMeta);
                            if(ee) item.addUnsafeEnchantment(Enchantment.ARROW_DAMAGE, 1);
                        }
                        inv.setItem(z, item);
                        if(i == 5 && config.get(path + z + ".rarity") != null) {
                            final EnchantRarity e = EnchantRarity.valueOf(config.getString(path + z + ".rarity"));
                            if(e != null) duelEnchantRaritieS.put(z, e);
                        }
                        if(i == 5 && config.get(path + z + ".costs") != null) {
                            final EnchantRarity e = EnchantRarity.valueOf(config.getString(path + z + ".rarity"));
                            if(e != null) raritycost.put(e, config.getInt(path + z + ".costs"));
                        }
                    }
                }
                if(i == 1)      duelType = inv;
                else if(i == 2) duelSettings = inv;
                else if(i == 3) duelArena = inv;
                else if(i == 4) duelGodset = inv;
                else if(i == 5) duelEnchantRarities = inv;
                else if(i == 6) duelEnchants = inv;
            }
        }
        for(String s : config.getConfigurationSection("arenas").getKeys(false)) {
            if(!s.equals("title") && !s.equals("size") && !s.startsWith("countdown") && !s.startsWith("in-joinQueue") && !s.startsWith("open")) {
                //final DuelArena a = new DuelArena(s, d(config, "arenas." + s, 0), toLocation(config.getString("arenas." + s + ".spawn-locations").split("\\|")[0]), toLocation(config.getString("arenas." + s + ".spawn-locations").split("\\|")[1]));
                //duelArena.setItem(a.getSlot(), a.getItemStack());
            }
        }
        for(int i = 0; i < 54; i++) {
            if(config.get("arenas." + i) != null) {

            }
        }
    }
    public void disable() {
	    if(!isEnabled) return;
	    isEnabled = false;
	    duelEnchantRaritieS.clear();
	    editedGodsets.clear();
	    types.clear();
	    kits.clear();
	    settings.clear();
	    maxcustomenchants.clear();
	    duelsettings.clear();
	    raritycost.clear();
	    back.clear();
        HandlerList.unregisterAll(this);
    }

	public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {
		final Player player = sender instanceof Player ? (Player) sender : null;
		if(args.length == 0)
			if(player != null) viewDuelType(player);
			else               sender.sendMessage("Only players can execute this command!");
		else if(args.length == 1) {
			if(args[0].equals("godset") && player != null && hasPermission(sender, "RandomPackage.duel.godset", true))
				viewGodset(player);
			else if(args[0].equals("help") && hasPermission(sender, "RandomPackage.duel.help", true))
				for(String s : config.getStringList("messages.help"))
					sender.sendMessage(ChatColor.translateAlternateColorCodes('&', s));
			else if(args[0].equals("toggle") && player != null && hasPermission(sender, "RandomPackage.duel.toggle", true))
				toggleInvites(player);
			else if(args[0].equals("collect") && player != null && hasPermission(sender, "RandomPackage.duel.stakecollect", true))
				collectStakeBin(player);
			else if(Bukkit.getPlayer(args[0]) != null) {
				Player target = Bukkit.getPlayer(args[0]);
				if(target == null) {
					for(String s : config.getStringList("messages.target-doesnt-exist")) player.sendMessage(ChatColor.translateAlternateColorCodes('&', s.replace("{TARGET}", args[0])));
				} else if(player == target) {
					sendStringListMessage(player, config.getStringList("messages.cannot-duel-self"));
				} else
					createDuel(player, target);
			}
		}
		
		return true;
	}
	@EventHandler
	private void playerQuitEvent(PlayerQuitEvent event) {
		
	}
	public void toggleInvites(Player player) {
		final RPPlayerData pdata = RPPlayerData.get(player.getUniqueId());
		pdata.receivesDuelInvites = !pdata.receivesDuelInvites;
		if(pdata.getOfflinePlayer().isOnline())
			sendStringListMessage(pdata.getOfflinePlayer().getPlayer(), config.getStringList("messages.toggle-notifications-" + (pdata.receivesDuelInvites ? "on" : "off")));
	}
	public void collectStakeBin(Player player) {
		final List<ItemStack> bin = RPPlayerData.get(player.getUniqueId()).duelStakeCollectionBin;
		if(bin.size() == 0)
			sendStringListMessage(player, config.getStringList("messages.stake-collection-bin-empty"));
		else
			for(ItemStack i : bin) giveItem(player, i);
	}
	public DuelSettings createDuel(Player requester, Player receiver) {
		if(RPPlayerData.get(requester.getUniqueId()).receivesDuelInvites) {
			requester.openInventory(Bukkit.createInventory(requester, duelSettings.getSize(), duelSettings.getTitle()));
			requester.getOpenInventory().getTopInventory().setContents(duelSettings.getContents());
			item = requester.getOpenInventory().getTopInventory().getItem(confirmslot).clone(); itemMeta = item.getItemMeta(); lore.clear(); lore.addAll(getConfirmSettings(requester));
			itemMeta.setLore(lore); lore.clear();
			item.setItemMeta(itemMeta);
			requester.getOpenInventory().getTopInventory().setItem(confirmslot, item);
			requester.updateInventory();
		} else
			sendStringListMessage(requester, config.getStringList("messages.duels-toggled-off"));
		return null;
	}
	public void sendRequest(DuelSettings settings) {
		
	}
	
	public void selectArena(DuelSettings settings) {
		if(settings.creator.isOnline()) {
			final Player creator = settings.creator;
			if(settings.target.isOnline()) {
				creator.openInventory(Bukkit.createInventory(creator, duelArena.getSize(), duelArena.getTitle()));
				creator.getOpenInventory().getTopInventory().setContents(duelArena.getContents());
				for(int i = 0; i < creator.getOpenInventory().getTopInventory().getSize(); i++) {
					item = creator.getOpenInventory().getTopInventory().getItem(i);
					if(item != null) {
						final DuelArena a = DuelArena.valueOf(i);
						if(a != null) {
							final int size = a.queue;
							itemMeta = item.getItemMeta(); lore.clear();
							if(itemMeta.hasLore()) lore.addAll(itemMeta.getLore());
							if(size == 0) for(String s : config.getStringList("arenas.open.lore")) lore.add(ChatColor.translateAlternateColorCodes('&', s));
							else          for(String s : config.getStringList("arenas.in-joinQueue.lore")) lore.add(ChatColor.translateAlternateColorCodes('&', s.replace("{QUEUE}", Integer.toString(size+1))));
							itemMeta.setLore(lore); lore.clear();
							item.setItemMeta(itemMeta);
						}
					}
				}
				creator.updateInventory();
			}
		}
	}
	private ItemStack getDuelGodsetItem(UUID uuid, ItemStack is) {
		if(is != null) {
			final String q = ChatColor.translateAlternateColorCodes('&', config.getString("godset.empty-placeholder"));
			item = is.clone(); itemMeta = item.getItemMeta(); lore.clear();
			int max = getMaxEnchants(is);
			for(String string : config.getStringList("godset.lore")) {
				if(string.equals("{EMPTY}")) {
					for(CustomEnchant e : editedGodsets.get(uuid).get(is.getType()).keySet()) {
						lore.add(e.getRarity().getApplyColors() + e.getName() + " " + toRoman(editedGodsets.get(uuid).get(is.getType()).get(e)));
						max -= raritycost.get(e.getRarity());
					}
					for(int p = 0; p < max; p++) lore.add(q);
				} else {
					if(string.contains("{ENCHANTS_LEFT}")) string = string.replace("{ENCHANTS_LEFT}", Integer.toString(max));
					lore.add(ChatColor.translateAlternateColorCodes('&', string));
				}
			}
			itemMeta.setLore(lore); lore.clear();
			item.setItemMeta(itemMeta);
			return item;
		} else return null;
	}
	public List<String> getConfirmSettings(Player player) {
		List<String> l = new ArrayList<>();
		if(player.getOpenInventory().getTopInventory().getTitle().equals(duelSettings.getTitle())) {
			item = items[2].clone(); itemMeta = item.getItemMeta();
			if(itemMeta.hasLore()) {
				final String on = ChatColor.translateAlternateColorCodes('&', config.getString("duel-settings.confirm.enabled")), off = ChatColor.translateAlternateColorCodes('&', config.getString("duel-settings.confirm.disabled"));
				for(String s : itemMeta.getLore()) {
					for(int m : settings.keySet()) {
						if(s.contains(settings.get(m)))
							s = s.replace(settings.get(m), player.getOpenInventory().getTopInventory().getItem(m).getItemMeta().getLore().contains(enabled) ? on : off);
					}
					l.add(s);
				}
			}
		}
		return l;
	}
	public DuelSettings getConfirmSettings(Player player, Player target) {
		item = items[2].clone(); itemMeta = item.getItemMeta();
		List<String> allowedCommands = new ArrayList<>();
		boolean gapples = false, mcmmo = false, potions = false, bows = false, healing = false, foodloss = false, enderpearls = false, risksInventory = false, deathCertificates = false;
		if(itemMeta.hasLore()) {
			for(String s : itemMeta.getLore()) {
				for(int m : settings.keySet()) {
					if(s.contains(settings.get(m))) {
						final boolean d = player.getOpenInventory().getTopInventory().getItem(m).getItemMeta().getLore().contains(enabled) ? true : false;
						if(s.contains("GAPPLES")) gapples = d;
						else if(s.contains("MCMMO")) mcmmo = d;
						else if(s.contains("POTIONS")) potions = d;
						else if(s.contains("BOWS")) bows = d;
						else if(s.contains("HEALING")) healing = d;
						else if(s.contains("FOOD_LOSS")) foodloss = d;
						else if(s.contains("ENDER_PEARLS")) enderpearls = d;
						else if(s.contains("RISKS_INVENTORY")) risksInventory = d;
						else if(s.contains("DEATH_CERTIFICATES")) deathCertificates = d;
						else if(s.startsWith("/") && d) allowedCommands.add(s);
					}
				}
			}
		}
		return null;//new DuelSettings(player, target, gapples, mcmmo, potions, bows, healing, foodloss, enderpearls, risksInventory, allowedCommands, deathCertificates, null);
	}
	public void viewGodset(Player player) {
		Bukkit.getScheduler().scheduleSyncDelayedTask(randompackage, new Runnable() {
			public void run() {
				back.put(player, 0);
				if(!editedGodsets.keySet().contains(player.getUniqueId())) editedGodsets.put(player.getUniqueId(), new HashMap<>());
				player.openInventory(Bukkit.createInventory(player, duelGodset.getSize(), duelGodset.getTitle()));
				player.getOpenInventory().getTopInventory().setContents(duelGodset.getContents());
				for(int i = 0; i < player.getOpenInventory().getTopInventory().getSize(); i++) {
					ItemStack is = player.getOpenInventory().getTopInventory().getItem(i);
					if(is != null && !is.getType().equals(Material.AIR)) {
						if(!editedGodsets.get(player.getUniqueId()).containsKey(is.getType())) editedGodsets.get(player.getUniqueId()).put(is.getType(), new HashMap<>());
						player.getOpenInventory().getTopInventory().setItem(i, getDuelGodsetItem(player.getUniqueId(), is));
					}
				}
				player.updateInventory();
			}
		}, 0);
	}
	public void viewDuelType(Player player) {
		player.openInventory(Bukkit.createInventory(player, duelType.getSize(), duelType.getTitle()));
		player.getOpenInventory().getTopInventory().setContents(duelType.getContents());
		for(int i = 0; i < player.getOpenInventory().getTopInventory().getSize(); i++) {
			ItemStack is = player.getOpenInventory().getTopInventory().getItem(i);
			if(is != null && !is.getType().equals(Material.AIR)) {
				item = is.clone(); itemMeta = item.getItemMeta(); lore.clear();
				if(itemMeta.hasDisplayName()) {
					//if(itemMeta.getDisplayName().contains("{QUEUE}")) itemMeta.setDisplayName(itemMeta.getDisplayName().replace("{QUEUE}", Integer.toString(getQueue(true).split(";").length)));
					item.setItemMeta(itemMeta);
				}
				if(itemMeta.hasLore()) {
					for(String string : itemMeta.getLore()) {
						if(string.contains("{ELO}")) string = string.replace("{ELO}", "" + RPPlayerData.get(player.getUniqueId()).duelELO);
						lore.add(string);
					}
				}
				itemMeta.setLore(lore); lore.clear();
				item.setItemMeta(itemMeta);
				player.getOpenInventory().getTopInventory().setItem(i, item);
			}
		}
		player.updateInventory();
	}
	public void viewApplyEnchantRarities(Player player) {
		Bukkit.getScheduler().scheduleSyncDelayedTask(randompackage, new Runnable() {
			public void run() {
				back.put(player, 1);
				player.openInventory(Bukkit.createInventory(player, duelEnchantRarities.getSize(), duelEnchantRarities.getTitle()));
				player.getOpenInventory().getTopInventory().setContents(duelEnchantRarities.getContents());
				player.updateInventory();
			}
		}, 0);
	}
	public void viewApplyableEnchants(Player player, Material m, int rawslot) {
		Bukkit.getScheduler().scheduleSyncDelayedTask(randompackage, new Runnable() {
			public void run() {
				back.put(player, 2);
				final List<CustomEnchant> e = CustomEnchant.getEnchants(duelEnchantRaritieS.get(rawslot));
				List<CustomEnchant> enchants = new ArrayList<CustomEnchant>();
				List<ItemStack> u = new ArrayList<ItemStack>();
				for(CustomEnchant enchant : e) {
					for(String s : enchant.getAppliesTo()) {
						if(m.name().endsWith(s.toUpperCase())) {
							enchants.add(enchant);
							final ItemStack is = customenchants.getRevealedItem(enchant, enchant.getMaxLevel(), -1, -1, false, false).clone(); itemMeta = is.getItemMeta(); lore.clear();
							itemMeta.setDisplayName(ChatColor.translateAlternateColorCodes('&', config.getString("duel-enchantments." + rawslot + ".name-prefix") + ChatColor.stripColor(itemMeta.getDisplayName())));
							if(itemMeta.hasLore()) {
								final String b = ChatColor.translateAlternateColorCodes('&', config.getString("duel-enchantments." + rawslot + ".enchant-lore-prefix"));
								for(String q : itemMeta.getLore()) lore.add(b + ChatColor.stripColor(q));
							}
							for(String y : config.getStringList("duel-enchantments." + rawslot + ".added-lore")) lore.add(ChatColor.translateAlternateColorCodes('&', y));
							itemMeta.setLore(lore); lore.clear();
							is.setItemMeta(itemMeta);
							u.add(is);
						}
					}
				}
				player.openInventory(Bukkit.createInventory(player, ((enchants.size() + 9) / 9) * 9, duelEnchants.getTitle()));
				player.getOpenInventory().getTopInventory().setContents(duelEnchants.getContents());
				for(ItemStack is : u) player.getOpenInventory().getTopInventory().addItem(is);
				player.updateInventory();
			}
		}, 0);
	}
	public void clearEnchants(Player player, int rawslot) {
		Material m = player.getOpenInventory().getTopInventory().getItem(rawslot).getType();
		HashMap<CustomEnchant, Integer> a = editedGodsets.get(player.getUniqueId()).get(m);
		if(a.keySet().size() == 0) {
			if(player.isOnline())
				sendStringListMessage(player, config.getStringList("messages.clear-enchants-has-none"));
		} else {
			for(int i = 0; i < a.keySet().size(); i++) {
				a.keySet().remove(a.keySet().toArray()[i]);
				i--;
			}
			player.getOpenInventory().getTopInventory().setItem(rawslot, getDuelGodsetItem(player.getUniqueId(), new ItemStack(m)));
			if(player.isOnline())
				for(String s : config.getStringList("messages.clear-enchants"))
					player.sendMessage(ChatColor.translateAlternateColorCodes('&', s.replace("{ITEM}", toMaterial(m.name(), false))));
			player.updateInventory();
		}
	}
	private void editGodsetEnchant(OfflinePlayer player, Material m, CustomEnchant enchant, boolean add) {
		final UUID uuid = player.getUniqueId();
		int slotsleft = getMaxEnchants(new ItemStack(m)), r = raritycost.get(enchant.getRarity());
		for(CustomEnchant e : editedGodsets.get(uuid).get(m).keySet()) slotsleft -= raritycost.get(e.getRarity());
		if(add && slotsleft - r < 0) {
			if(player.isOnline())
				for(String s : config.getStringList("messages.not-enough-enchant-slots"))
					player.getPlayer().sendMessage(ChatColor.translateAlternateColorCodes('&', s.replace("{SLOTS_LEFT}", Integer.toString(slotsleft))));
			return;
		}
		if(!editedGodsets.keySet().contains(uuid)) editedGodsets.put(uuid, new HashMap<>());
		if(!editedGodsets.get(uuid).keySet().contains(m)) editedGodsets.get(uuid).put(m, new HashMap<>());
		if(add) editedGodsets.get(uuid).get(m).put(enchant, enchant.getMaxLevel());
		else    editedGodsets.get(uuid).get(m).remove(enchant);
		if(player.isOnline())
			for(String s : config.getStringList("messages." + enchant.getRarity().getName() + (add ? ".add" : ".remove")))
				player.getPlayer().sendMessage(ChatColor.translateAlternateColorCodes('&', s.replace("{SLOTS_LEFT}", Integer.toString(add ? slotsleft - r : slotsleft + r)).replace("{ENCHANT}", enchant.getName()).replace("{ITEM}", toMaterial(m.name(), false).replace("{SLOTS_LEFT}", Integer.toString(0)))));
	}
	@EventHandler
	private void inventoryClickEvent(InventoryClickEvent event) {
		if(!event.isCancelled() && event.getCurrentItem() != null && !event.getCurrentItem().getType().equals(Material.AIR) && event.getWhoClicked().getOpenInventory().getTopInventory().getHolder() == event.getWhoClicked()) {
			final Player player = (Player) event.getWhoClicked();
			final String title = player.getOpenInventory().getTopInventory().getTitle();
			if(title.equals(duelType.getTitle()) || title.equals(duelSettings.getTitle()) || title.equals(duelGodset.getTitle()) || title.equals(duelEnchantRarities.getTitle()) || title.equals(duelEnchants.getTitle()) || title.equals(duelArena.getTitle())) {
				event.setCancelled(true);
				player.updateInventory();
				if(event.getRawSlot() >= event.getWhoClicked().getOpenInventory().getTopInventory().getSize()) return;
				
				if(title.equals(duelGodset.getTitle())) {
					if(event.getClick().equals(ClickType.LEFT)) {
						back.put(player, 1);
						types.put(player.getUniqueId(), event.getCurrentItem().getType());
						viewApplyEnchantRarities(player);
					} else if(event.getClick().equals(ClickType.MIDDLE)) {
						clearEnchants(player, event.getRawSlot());
					}
				} else if(title.equals(duelSettings.getTitle())) {
					if(event.getRawSlot() == confirmslot) {
						selectArena(getConfirmSettings(player, player));
					} else if(event.getCurrentItem() != settingsbackground) {
						item = event.getCurrentItem().clone(); itemMeta = item.getItemMeta(); lore.clear(); if(itemMeta.hasLore()) lore.addAll(itemMeta.getLore());
						final String replace = lore.contains(enabled) ? enabled : lore.contains(disabled) ? disabled : null, replaced = replace == enabled ? disabled : enabled;
						if(replace == null) return;
						for(int i = 0; i < lore.size(); i++)
							if(lore.get(i).equals(replace))
								lore.set(i, replaced);
						itemMeta.setLore(lore); lore.clear();
						item.setItemMeta(itemMeta);
						if(replaced == enabled) item.addUnsafeEnchantment(Enchantment.ARROW_DAMAGE, 1);
						else                    item.removeEnchantment(Enchantment.ARROW_DAMAGE);
						player.getOpenInventory().getTopInventory().setItem(event.getRawSlot(), item);
						lore.clear();
						item = items[2].clone(); itemMeta = item.getItemMeta(); lore.addAll(getConfirmSettings(player));
						itemMeta.setLore(lore); lore.clear();
						item.setItemMeta(itemMeta);
						player.getOpenInventory().getTopInventory().setItem(confirmslot, item);
						player.updateInventory();
					}
				} else if(title.equals(duelType.getTitle())) {
					
				} else if(title.equals(duelEnchantRarities.getTitle()) && duelEnchantRaritieS.keySet().contains(event.getRawSlot())) {
					final Material type = types.get(player.getUniqueId());
					if(event.getClick().name().endsWith("LEFT")) {
						viewApplyableEnchants(player, type, event.getRawSlot());
					} else if(event.getClick().equals(ClickType.MIDDLE)) {
						player.getOpenInventory().getTopInventory().setItem(event.getRawSlot(), getDuelGodsetItem(player.getUniqueId(), event.getCurrentItem()));
					}
				} else if(title.equals(duelEnchants.getTitle())) {
					Material m = types.get(player.getUniqueId());
					CustomEnchant e = CustomEnchant.valueOf(event.getCurrentItem().getItemMeta().getDisplayName());
					editGodsetEnchant(player, m, e, !editedGodsets.get(player.getUniqueId()).get(m).containsKey(e));
				} else if(title.equals(duelArena.getTitle())) {
					DuelArena a = DuelArena.valueOf(event.getRawSlot());
					DuelSettings s = DuelSettings.valueOf(player);
					if(s.creator.isOnline() && s.target.isOnline()) {
						//new ActiveDuel(s.creator, s.target, s, a, new ArrayList<>());
					}
				}
			}
		}
	}
	@EventHandler
	private void inventoryCloseEvent(InventoryCloseEvent event) {
		final Player player = (Player) event.getPlayer();
		final String title = event.getInventory().getTitle();
		if(title != null && event.getInventory().getHolder() == player) {
			if(title.equals(duelGodset.getTitle()) && back.keySet().contains(player) && back.get(player) == 0) {
				RPPlayerData pdata = RPPlayerData.get(player.getUniqueId());
				if(pdata.duelGodset != editedGodsets.get(player.getUniqueId())) {
					sendStringListMessage(player, config.getStringList("messages.save-godset"));
					pdata.duelGodset = editedGodsets.get(player.getUniqueId());
				}
			} else if(title.equals(duelEnchants.getTitle()) && back.keySet().contains(player) && back.get(player) == 2) {
				viewApplyEnchantRarities(player);
			} else if(title.equals(duelEnchantRarities.getTitle()) && back.keySet().contains(player) && back.get(player) == 1) {
				viewGodset(player);
			} else if(title.equals(duelSettings.getTitle()) && DuelSettings.valueOf(player) == null) {
				sendStringListMessage(player, config.getStringList("messages.closed-settings"));
			} else if(title.equals(duelArena.getTitle())) {
				DuelSettings.valueOf(player).delete();
				sendStringListMessage(player, config.getStringList("messages.closed-arena"));
			}
		}
	}
	
}
