package me.randomHashTags.RandomPackage.api;

import me.randomHashTags.RandomPackage.RPPlayerData;
import me.randomHashTags.RandomPackage.RandomPackageAPI;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerPickupItemEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;

import java.io.File;
import java.util.List;

public class ItemFilter extends RandomPackageAPI implements Listener, CommandExecutor {

    public boolean isEnabled = false;
    private static ItemFilter instance;
    public static ItemFilter getItemFilter() {
        if(instance == null) instance = new ItemFilter();
        return instance;
    }

    private YamlConfiguration config;
    private Inventory[] filterinventories;
    private int LOADED = 0;

    public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {
        if(!(sender instanceof Player)) return true;
        final Player player = (Player) sender;
        final RPPlayerData pdata = RPPlayerData.get(player.getUniqueId());
        if(args.length == 0 && hasPermission(player, "filter.cmd-perm", true)) {
            sendStringListMessage(sender, randompackage.getConfig().getStringList("filter.command"));
        } else if(args[0].equals("edit")) {
            view(player);
        } else if(args[0].equals("toggle") && hasPermission(player, "filter.toggle-perm", true)) {
            pdata.activeFilter = !pdata.activeFilter;
            sendStringListMessage(player, randompackage.getConfig().getStringList("filter." + (pdata.activeFilter ? "enable" : "disable")));
        }
        return true;
    }

    public void enable() {
        final long started = System.currentTimeMillis();
        if(isEnabled) return;
        save("Features", "item filter.yml");
        pluginmanager.registerEvents(this, randompackage);
        isEnabled = true;
        config = YamlConfiguration.loadConfiguration(new File(randompackage.getDataFolder() + File.separator + "Features" + File.separator, "item filter.yml"));
        filterinventories = new Inventory[55];
        filterinventories[0] = setupFilterInv("menu");
        Bukkit.getConsoleSender().sendMessage(ChatColor.translateAlternateColorCodes('&', "&6[RandomPackage] &aLoaded " + LOADED + " filter categories &e(took " + (System.currentTimeMillis()-started) + "ms)"));
    }
    public void disable() {
        if(!isEnabled) return;
        LOADED = 0;
        isEnabled = false;
        HandlerList.unregisterAll(this);
    }

    @EventHandler
    private void inventoryClickEvent(InventoryClickEvent event) {
        if(!event.isCancelled() && event.getCurrentItem() != null && !event.getCurrentItem().getType().equals(Material.AIR)) {
            final ItemStack c = event.getCurrentItem();
            final Player player = (Player) event.getWhoClicked();
            String t = player.getOpenInventory().getTopInventory().getTitle(), d = null;
            if(t.equals(filterinventories[0].getTitle())) {
                d = "filtermenu";
            } else {
                for(int i = 1; i <= filterinventories[0].getSize(); i++) if(filterinventories[i] != null && t.equals(filterinventories[i].getTitle())) d = "filterinventories[" + i + "]";
                if(d == null) return;
            }
            final int r = event.getRawSlot();
            event.setCancelled(true);
            player.updateInventory();
            if(r < 0 || r >= player.getOpenInventory().getTopInventory().getSize() || c == null || c.getType().equals(Material.AIR)) return;

            if(d.equals("filtermenu") && filterinventories[event.getRawSlot() + 1] != null) {
                openFilterInventory(player, event.getRawSlot());
            } else if(d.startsWith("filterinventories[")) {
                final RPPlayerData pdata = RPPlayerData.get(player.getUniqueId());
                final List<ItemStack> filtereditems = pdata.filteredItems;
                final ItemStack C = new ItemStack(c.getType(), 1, c.getData().getData());
                if(!filtereditems.contains(C)) pdata.filteredItems.add(C);
                else pdata.filteredItems.remove(C);
                player.getOpenInventory().getTopInventory().setItem(r, setFilteredItem(pdata, c));
            }
        }
    }
    @EventHandler
    private void playerPickupItemEvent(PlayerPickupItemEvent event) {
        final RPPlayerData pdata = RPPlayerData.get(event.getPlayer().getUniqueId());
        final ItemStack i = event.getItem().getItemStack();
        if(pdata.activeFilter && !pdata.filteredItems.contains(new ItemStack(i.getType(), 1, i.getData().getData()))) event.setCancelled(true);
    }
    private Inventory setupFilterInv(String path) {
        final Inventory inv = Bukkit.createInventory(null, config.getInt(path + ".size"), ChatColor.translateAlternateColorCodes('&', config.getString(path + ".title")));
        for(String s : config.getConfigurationSection(path).getKeys(false)) {
            if(!s.equals("title") && !s.equals("size")) {
                final String o = config.getString(path + "." + s + ".opens");
                final int slot = config.getInt(path + "." + s + ".slot");
                inv.setItem(slot, d(config, path + "." + s, 0));
                if(o != null) filterinventories[slot+1] = setupFilterInv(o);
            }
        }
        if(!path.equals("menu")) LOADED += 1;
        return inv;
    }
    public void view(Player player) {
        if(hasPermission(player, "filter.edit-perm", true)) {
            player.openInventory(Bukkit.createInventory(player, filterinventories[0].getSize(), filterinventories[0].getTitle()));
            player.getOpenInventory().getTopInventory().setContents(filterinventories[0].getContents());
            player.updateInventory();
        }
    }
    public void openFilterInventory(Player player, int rawslot) {
        if(filterinventories[rawslot + 1] != null) {
            player.openInventory(Bukkit.createInventory(player, filterinventories[rawslot + 1].getSize(), filterinventories[rawslot + 1].getTitle()));
            final Inventory top = player.getOpenInventory().getTopInventory();
            top.setContents(filterinventories[rawslot + 1].getContents());
            final RPPlayerData pdata = RPPlayerData.get(player.getUniqueId());
            for(int i = 0; i < top.getSize(); i++) {
                if(top.getItem(i) != null && !top.getItem(i).getType().equals(Material.AIR)) {
                    top.setItem(i, setFilteredItem(pdata, top.getItem(i)));
                }
            }
            player.updateInventory();
        }
    }
    private ItemStack setFilteredItem(RPPlayerData pdata, ItemStack is) {
        final List<ItemStack> filtereditems = pdata.filteredItems;
        item = is; itemMeta = item.getItemMeta(); lore.clear();
        final boolean isFiltered = !filtereditems.isEmpty() && filtereditems.contains(new ItemStack(item.getType(), 1, item.getData().getData()));
        final boolean enchanted = isFiltered && config.getBoolean("filtered.enchanted") || !isFiltered && config.getBoolean("not filtered.enchanted");
        final String displaycolor = ChatColor.translateAlternateColorCodes('&', config.getString((isFiltered ? "" : "not ") + "filtered.color"));
        final List<String> l = config.getStringList((isFiltered ? "" : "not ") + "filtered.lore");
        if(itemMeta.hasDisplayName()) itemMeta.setDisplayName(displaycolor + ChatColor.stripColor(itemMeta.getDisplayName()));
        if(enchanted) itemMeta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
        for(String s : l) lore.add(ChatColor.translateAlternateColorCodes('&', s));
        itemMeta.setLore(lore); lore.clear();
        item.setItemMeta(itemMeta);
        if(enchanted) item.addUnsafeEnchantment(Enchantment.ARROW_DAMAGE, 1);
        return item;
    }
}
