package me.randomHashTags.RandomPackage.api;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import me.randomHashTags.RandomPackage.utils.universal.UMaterial;
import me.randomHashTags.RandomPackage.utils.classes.CustomExplosion;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Entity;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.event.block.*;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.material.Dispenser;

import me.randomHashTags.RandomPackage.RandomPackageAPI;

@SuppressWarnings("deprecation")
public class CustomExplosions extends RandomPackageAPI implements Listener {

	public boolean isEnabled = false;
	private static CustomExplosions instance;
	public static final CustomExplosions getCustomExplosions() {
		if(instance == null) instance = new CustomExplosions();
		return instance;
	}

	public YamlConfiguration config;

	public void enable() {
		if(isEnabled) return;
		save("Features", "custom tnt.yml");
		pluginmanager.registerEvents(this, randompackage);
		isEnabled = true;
		config = YamlConfiguration.loadConfiguration(new File(randompackage.getDataFolder() + File.separator + "Features" + File.separator, "custom tnt.yml"));
		for(String s : config.getConfigurationSection("tnt").getKeys(false))
			new CustomExplosion(s, d(config, "tnt." + s, 0), config.getStringList("tnt." + s + ".attributes"));
		for(String s : config.getConfigurationSection("creepers").getKeys(false))
			new CustomExplosion(s, d(config, "creepers." + s, 0), config.getString("creepers." + s + ".creeper name"), config.getStringList("creepers." + s + ".attributes"));
		final ArrayList<ItemStack> creepers = new ArrayList<>(), tnt = new ArrayList<>();
		for(CustomExplosion ce : CustomExplosion.customtnt) tnt.add(ce.getItemStack());
		for(CustomExplosion ce : CustomExplosion.customcreepers) creepers.add(ce.getItemStack());

		addGivedpCategory(creepers, UMaterial.CREEPER_SPAWN_EGG, "Custom Creepers", "Givedp: Custom Creepers");
		addGivedpCategory(tnt, UMaterial.TNT, "Custom TNT", "Givedp: Custom TNT");

		int loadedPlaced = 0, loadedPrimed = 0, loadedLiving = 0;
		final List<String> placedtnt = otherdata.getStringList("tnt.placed"), primedtnt = otherdata.getStringList("tnt.primed"), livingcreepers = otherdata.getStringList("creepers");
		if(placedtnt != null && !placedtnt.isEmpty()) {
			for(String s : placedtnt) {
				final Location l = toLocation(s.split(":")[0]);
				if(l.getWorld().getBlockAt(l).getType().equals(Material.TNT)) {
					CustomExplosion.placedTNT.put(l, CustomExplosion.getCustomTNT(s.split(":")[1]));
					loadedPlaced += 1;
				}
			}
			if(loadedPlaced != 0) Bukkit.getConsoleSender().sendMessage(ChatColor.translateAlternateColorCodes('&', "&6[RandomPackage]&r &aLoaded " + loadedPlaced + " existing placed tnt"));
		}
		if(primedtnt != null && !primedtnt.isEmpty()) {
			for(String s : primedtnt) {
				final UUID u = UUID.fromString(s.split(":")[0]);
				final Entity e = getEntity(u);
				if(e != null && !e.isDead()) {
					CustomExplosion.livingTNT.put(u, CustomExplosion.getCustomTNT(s.split(":")[1]));
					loadedPrimed += 1;
				}
			}
			if(loadedPrimed != 0) Bukkit.getConsoleSender().sendMessage(ChatColor.translateAlternateColorCodes('&', "&6[RandomPackage]&r &aLoaded " + loadedPrimed + " existing primed tnt"));
		}
		if(livingcreepers != null && !livingcreepers.isEmpty()) {
			for(String s : livingcreepers) {
				final UUID u = UUID.fromString(s.split(":")[0]);
				final Entity e = getEntity(u);
				if(e != null && !e.isDead()) {
					CustomExplosion.livingCreepers.put(u, CustomExplosion.getCustomCreeper(s.split(":")[1]));
					loadedLiving += 1;
				}
			}
			if(loadedLiving != 0) Bukkit.getConsoleSender().sendMessage(ChatColor.translateAlternateColorCodes('&', "&6[RandomPackage]&r &aLoaded " + loadedLiving + " custom living creepers"));
		}
	}
	public void disable() {
		if(!isEnabled) return;
		isEnabled = false;
		final HashMap<Location, CustomExplosion> tnt = CustomExplosion.placedTNT;
		final HashMap<UUID, CustomExplosion> creepers = CustomExplosion.livingCreepers, ltnt = CustomExplosion.livingTNT;
		otherdata.set("tnt", null);
		otherdata.set("creepers", null);
		final List<String> placedtnt = new ArrayList<>(), primedtnt = new ArrayList<>(), cree = new ArrayList<>();
		for(Location l : tnt.keySet()) placedtnt.add(toString(l) + ":" + tnt.get(l).getPath());
		otherdata.set("tnt.placed", placedtnt);
		for(UUID u : ltnt.keySet()) primedtnt.add(u.toString() + ":" + ltnt.get(u).getPath());
		otherdata.set("tnt.primed", primedtnt);
		for(UUID u : creepers.keySet()) cree.add(u.toString() + ":" + creepers.get(u).getPath());
		otherdata.set("creepers", cree);
		saveOtherData();
		CustomExplosion.customtnt.clear();
		CustomExplosion.customcreepers.clear();
		CustomExplosion.placedTNT.clear();
		CustomExplosion.livingCreepers.clear();
		CustomExplosion.livingTNT.clear();
		HandlerList.unregisterAll(this);
	}
	
	@EventHandler(priority = EventPriority.HIGHEST)
	private void blockBreakEvent(BlockBreakEvent event) {
		final Block b = event.getBlock();
		if(!event.isCancelled() && b.getType().equals(Material.TNT)) {
			final Location l = b.getLocation();
			final HashMap<Location, CustomExplosion> p = CustomExplosion.placedTNT;
			final CustomExplosion ce = p.keySet().contains(l) ? p.get(l) : null;
			if(ce != null) {
				if(!event.getPlayer().getGameMode().equals(GameMode.CREATIVE)) {
					event.setCancelled(true);
					b.setType(Material.AIR);
					b.getWorld().dropItemNaturally(l, ce.getItemStack());
				}
				p.remove(l);
			}
		}
	}
	@EventHandler(priority = EventPriority.HIGHEST)
	private void blockPlaceEvent(BlockPlaceEvent event) {
		final ItemStack i = event.getItemInHand();
		if(i != null && i.getType().equals(Material.TNT) && i.hasItemMeta()) {
			final CustomExplosion ce = CustomExplosion.getCustomTNT(i);
			if(ce != null) ce.placeTNT(event.getBlockPlaced().getLocation(), ce);
		}
	}
	@EventHandler
	private void playerInteractEvent(PlayerInteractEvent event) {
		if(event.getItem() != null && event.getAction().equals(Action.RIGHT_CLICK_BLOCK)) {
			final Location l = event.getClickedBlock().getLocation();
			if(event.getClickedBlock().getType().equals(Material.TNT) && !event.getItem().getType().equals(Material.AIR)) {
				final Material t = event.getItem().getType();
				final HashMap<Location, CustomExplosion> p = CustomExplosion.placedTNT;
				final CustomExplosion ce = p.keySet().contains(l) ? p.get(l) : null;
				if(ce != null && (t.equals(UMaterial.FIREWORK_STAR.getMaterial()) || t.equals(Material.FLINT_AND_STEEL))) {
					event.setCancelled(true);
					l.getWorld().getBlockAt(l).setType(Material.AIR);
					ce.ignite(l);
				}
			} else {
				final CustomExplosion ce = CustomExplosion.getCustomCreeper(event.getItem());
				if(ce != null) {
					final Location lo = new Location(l.getWorld(), l.getX(), l.getY()+1.0, l.getZ());
					event.setCancelled(true);
					removeItem(event.getPlayer(), event.getItem(), 1);
					spawnCustomCreeper(lo, ce);
				}
			}
		}

	}
	public void spawnCustomCreeper(Location location, CustomExplosion ce) {
		CustomExplosion.spawnCreeper(location, ce);
	}
	@EventHandler
	private void blockDispenseEvent(BlockDispenseEvent event) {
		if(event.getItem() != null && event.getItem().hasItemMeta()) {
			final CustomExplosion ce = CustomExplosion.getCustomTNT(event.getItem());
			if(ce != null) {
				event.setCancelled(true);
				final Block b = event.getBlock();
				final Location bl = b.getLocation();
				double x = bl.getBlockX(), y = bl.getBlockY(), z = bl.getBlockZ();
				final Dispenser disp = (Dispenser) b.getState().getData();
				final BlockFace bf = disp.getFacing();
				if (bf.equals(BlockFace.DOWN)) y -= 1.0;
				else if (bf.equals(BlockFace.UP)) y += 1.0;
				else if (bf.equals(BlockFace.NORTH)) z -= 0.5;
				else if (bf.equals(BlockFace.SOUTH)) z += 1.5;
				else if (bf.equals(BlockFace.WEST)) x -= 0.5;
				else if (bf.equals(BlockFace.EAST)) x += 1.5;
				else {
					Bukkit.broadcastMessage("[CustomExplosions] Different direction! \"" + disp.getFacing().name() + "\"");
					return;
				}
				if(!bf.equals(BlockFace.EAST) && !bf.equals(BlockFace.WEST)) x += 0.5;
				if(!bf.name().endsWith("TH")) z += 0.5;
				final Location l = new Location(event.getBlock().getWorld(), x, y, z);
				ce.spawnTNT(l);

				org.bukkit.block.Dispenser dis = (org.bukkit.block.Dispenser) b.getState();
				boolean did = false;
				final Inventory i = dis.getInventory();
				for(int d = 0; d < i.getSize(); d++) {
					if(!did && i.getItem(d) != null && i.getItem(d).hasItemMeta() && i.getItem(d).getItemMeta().equals(event.getItem().getItemMeta())) {
						did = true;
						final int e = d;
						Bukkit.getScheduler().scheduleSyncDelayedTask(randompackage, () -> {
							final ItemStack a = i.getItem(e);
							if(a.getAmount() == 1) i.setItem(e, new ItemStack(Material.AIR));
							else                   a.setAmount(a.getAmount() - 1);
							dis.update();
						}, 0);
					}
				}
			}
		}
	}
	@EventHandler(priority = EventPriority.HIGHEST)
	private void entityExplodeEvent(EntityExplodeEvent event) {
		if(!event.isCancelled()) {
			final UUID uuid = event.getEntity().getUniqueId();
			final CustomExplosion creeper = CustomExplosion.getCustomCreeper(uuid), tnt = creeper == null ? CustomExplosion.getCustomTNT(uuid) : null;
			final Location l = event.getEntity().getLocation();
			if(creeper != null) creeper.explode(event, l);
			else if(tnt != null) tnt.explode(event, l);
		}
	}
}
