package me.randomHashTags.RandomPackage.api.events.customenchant;

import org.bukkit.entity.Entity;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

import me.randomHashTags.RandomPackage.utils.classes.customenchants.CustomEnchantEntity;

public class CustomEnchantEntityDamageByEntityEvent extends Event implements Cancellable {
	private static final HandlerList handlers = new HandlerList();
	private final CustomEnchantEntity entity;
	private final Entity damager;
	private final double finaldamage, initialdamage;
	private boolean cancelled;
	public CustomEnchantEntityDamageByEntityEvent(CustomEnchantEntity entity, Entity damager, double finaldamage, double initialdamage) {
		this.entity = entity;
		this.damager = damager;
		this.finaldamage = finaldamage;
		this.initialdamage = initialdamage;
	}
	public CustomEnchantEntity getCustomEnchantEntity() { return entity; }
	public Entity getDamager() { return damager; }
	public double getFinalDamage() { return finaldamage; }
	public double getDamage() { return initialdamage; }
	public boolean isCancelled() { return cancelled; }
	public void setCancelled(boolean cancel) { cancelled = cancel; }
	public HandlerList getHandlers() { return handlers; }
	public static HandlerList getHandlerList() { return handlers; }
}
