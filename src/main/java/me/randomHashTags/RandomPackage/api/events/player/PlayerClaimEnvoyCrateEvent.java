package me.randomHashTags.RandomPackage.api.events.player;

import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

import me.randomHashTags.RandomPackage.utils.classes.EnvoyCrate;

public class PlayerClaimEnvoyCrateEvent extends Event implements Cancellable {
	private static final HandlerList handlers = new HandlerList();
	private final Player player;
	private final Location l;
	private final EnvoyCrate type;
	private boolean cancelled;
	public PlayerClaimEnvoyCrateEvent(Player player, Location l, EnvoyCrate type) {
		this.player = player;
		this.l = l;
		this.type = type;
	}
	public Player getPlayer() { return player; }
	public Location getCrateLocation() { return l; }
	public EnvoyCrate getCrateType() { return type; }
	public boolean isCancelled() { return cancelled; }
	public void setCancelled(boolean cancel) { cancelled = cancel; }
	public HandlerList getHandlers() { return handlers; }
	public static HandlerList getHandlerList() { return handlers; }
}
