package me.randomHashTags.RandomPackage.api.events;

import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

import me.randomHashTags.RandomPackage.utils.classes.globalchallenges.GlobalChallenge;

public class GlobalChallengeEndEvent extends Event {
	private static final HandlerList handlers = new HandlerList();
	private final GlobalChallenge challenge;
	private final boolean givesRewards;
	public GlobalChallengeEndEvent(GlobalChallenge challenge, boolean givesRewards) {
		this.challenge = challenge;
		this.givesRewards = givesRewards;
	}
	public GlobalChallenge getGlobalChallenge() { return challenge; }
	public boolean givesRewards() { return givesRewards; }
	public HandlerList getHandlers() { return handlers; }
	public static HandlerList getHandlerList() { return handlers; }
}
