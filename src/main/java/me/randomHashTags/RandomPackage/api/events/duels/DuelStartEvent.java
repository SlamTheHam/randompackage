package me.randomHashTags.RandomPackage.api.events.duels;

import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

import me.randomHashTags.RandomPackage.utils.classes.duels.DuelArena;
import me.randomHashTags.RandomPackage.utils.classes.duels.DuelSettings;

public class DuelStartEvent extends Event {
	private static final HandlerList handlers = new HandlerList();
	private final DuelSettings settings;
	private final DuelArena arena;
	public DuelStartEvent(DuelSettings settings, DuelArena arena) {
		this.settings = settings;
		this.arena = arena;
	}
	public DuelSettings getSettings() { return settings; }
	public DuelArena getArena() { return arena; }
	public HandlerList getHandlers() { return handlers; }
	public static HandlerList getHandlerList() { return handlers; }
}
