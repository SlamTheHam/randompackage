package me.randomHashTags.RandomPackage.api.events.player;

import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
import org.bukkit.inventory.ItemStack;

import me.randomHashTags.RandomPackage.utils.classes.customenchants.CustomEnchant;

public class PlayerRevealCustomEnchantEvent extends Event implements Cancellable {
	private static final HandlerList handlers = new HandlerList();
	private boolean cancelled;
	private final Player player;
	private final CustomEnchant enchant;
	private final int level;
	private final ItemStack item;
	public PlayerRevealCustomEnchantEvent(Player player, ItemStack item, CustomEnchant enchant, int level) {
		this.player = player;
		this.item = item;
		this.enchant = enchant;
		this.level = level;
	}
	public Player getPlayer() { return player; }
	public ItemStack getItem() { return item; }
	public CustomEnchant getEnchant() { return enchant; }
	public int getLevel() { return level; }
	public boolean isCancelled() { return cancelled; }
	public void setCancelled(boolean cancel) { cancelled = cancel; }
	public HandlerList getHandlers() { return handlers; }
	public static HandlerList getHandlerList() { return handlers; }
}