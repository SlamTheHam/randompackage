package me.randomHashTags.RandomPackage.api.events.player;

import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

import me.randomHashTags.RandomPackage.utils.classes.globalchallenges.GlobalChallenge;

public class PlayerGlobalChallengeParticipateEvent extends Event implements Cancellable {
	private static final HandlerList handlers = new HandlerList();
	private final GlobalChallenge chall;
	private final Event event;
	private final String tracked;
	private final double amount;
	private boolean cancelled;
	public PlayerGlobalChallengeParticipateEvent(GlobalChallenge chall, Event event, String tracked, double amount) {
		this.chall = chall;
		this.event = event;
		this.tracked = tracked;
		this.amount = amount;
	}
	public GlobalChallenge getGlobalChallenge() { return chall; }
	public Event getEvent() { return event; }
	public String getTracked() { return tracked; }
	public double getValue() { return amount; }
	public boolean isCancelled() { return cancelled; }
	public void setCancelled(boolean cancel) { cancelled = cancel; }
	public HandlerList getHandlers() { return handlers; }
	public static HandlerList getHandlerList() { return handlers; }
}