package me.randomHashTags.RandomPackage.api.events.player;

import java.util.HashMap;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
import org.bukkit.event.player.PlayerTeleportEvent.TeleportCause;

import me.randomHashTags.RandomPackage.RandomPackage;

public class PlayerTeleportDelayEvent extends Event implements Cancellable {
	public static final HashMap<UUID, PlayerTeleportDelayEvent> teleporting = new HashMap<>();
	private static final HandlerList handlers = new HandlerList();
	private boolean cancelled;
	private final UUID uuid;
	public final Player player;
	private final double delay;
	public final Location from, to;
	public final int task;
	
	public PlayerTeleportDelayEvent(Player player, double delay, Location from, Location to) {
		cancelled = false;
		this.uuid = player.getUniqueId();
		this.player = player;
		this.delay = delay;
		this.from = from;
		this.to = to;
		final long de = (long) ((((long) delay * 20)) + (20 * Double.parseDouble("0." + Double.toString(delay).split("\\.")[1])));
		final int t = Bukkit.getScheduler().scheduleSyncDelayedTask(RandomPackage.getPlugin, new Runnable() {
			public void run() {
				player.teleport(to, TeleportCause.PLUGIN);
				teleporting.remove(uuid);
			}
		}, de);
		this.task = t;
		teleporting.put(uuid, this);
	}
	
	public double getDelay() { return delay; }
	
	public boolean isCancelled() { return cancelled; }
	public void setCancelled(boolean cancel) { cancelled = cancel; }
	public HandlerList getHandlers() { return handlers; }
	public static HandlerList getHandlerList() { return handlers; }
}
