package me.randomHashTags.RandomPackage.api.events.player;

import org.bukkit.OfflinePlayer;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

import me.randomHashTags.RandomPackage.utils.classes.customenchants.RarityGem;

public class PlayerToggleRarityGemEvent extends Event implements Cancellable {
	private static final HandlerList handlers = new HandlerList();
	public final OfflinePlayer player;
	public final RarityGem gem;
	private final ToggleRarityGemStatus status;
	private boolean cancelled;
	public PlayerToggleRarityGemEvent(OfflinePlayer player, RarityGem gem, ToggleRarityGemStatus status) {
		this.player = player;
		this.gem = gem;
		this.status = status;
	}
	public OfflinePlayer getOfflinePlayer() { return player; }
	public RarityGem getRarityGem() { return gem; }
	public ToggleRarityGemStatus getStatus() { return status; }
	public boolean isCancelled() { return cancelled; }
	public void setCancelled(boolean cancel) { cancelled = cancel; }
	public HandlerList getHandlers() { return handlers; }
	public static HandlerList getHandlerList() { return handlers; }
	public enum ToggleRarityGemStatus { ON, OFF_INTERACT, OFF_DROPPED, OFF_MOVED, OFF_RAN_OUT, }
}
