package me.randomHashTags.RandomPackage.api.events;

import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
import org.bukkit.inventory.ItemStack;

public class EnchanterPurchaseEvent extends Event implements Cancellable {
	private static final HandlerList handlers = new HandlerList();
	private final Player player;
	private final ItemStack purchased;
	private final String currency;
	private final double cost;
	private boolean cancelled;
	public EnchanterPurchaseEvent(Player player, ItemStack purchased, String currency, double cost) {
		this.player = player;
		this.purchased = purchased;
		this.currency = currency;
		this.cost = cost;
	}
	public Player getPlayer() { return player; }
	public ItemStack getPurchased() { return purchased; }
	public String getCurrency() { return currency; }
	public double getCost() { return cost; }
	public boolean isCancelled() { return cancelled; }
	public void setCancelled(boolean cancel) { cancelled = cancel; }
	public HandlerList getHandlers() { return handlers; }
	public static HandlerList getHandlerList() { return handlers; }
}