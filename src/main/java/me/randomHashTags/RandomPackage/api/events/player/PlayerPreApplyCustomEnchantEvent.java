package me.randomHashTags.RandomPackage.api.events.player;

import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
import org.bukkit.inventory.ItemStack;

import me.randomHashTags.RandomPackage.utils.classes.customenchants.CustomEnchant;

public class PlayerPreApplyCustomEnchantEvent extends Event implements Cancellable {
	private static final HandlerList handlers = new HandlerList();
	private final Player player;
	private final CustomEnchant enchant;
	private final int level;
	private boolean cancelled;
	public PlayerPreApplyCustomEnchantEvent(Player player, CustomEnchant enchant, int level, ItemStack applytoItem) {
		this.player = player;
		this.enchant = enchant;
		this.level = level;
	}
	public Player getPlayer() { return player; }
	public CustomEnchant getEnchant() { return enchant; }
	public int getLevel() { return level; }
	public boolean isCancelled() { return cancelled; }
	public void setCancelled(boolean cancel) { cancelled = cancel; }
	public HandlerList getHandlers() { return handlers; }
	public static HandlerList getHandlerList() { return handlers; }
}