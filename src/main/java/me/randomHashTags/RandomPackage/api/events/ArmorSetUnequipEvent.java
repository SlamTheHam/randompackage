package me.randomHashTags.RandomPackage.api.events;

import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

import me.randomHashTags.RandomPackage.utils.classes.ArmorSet;

public class ArmorSetUnequipEvent extends Event {
	private static final HandlerList handlers = new HandlerList();
	private final Player player;
	private final ArmorSet set;
	public ArmorSetUnequipEvent(Player player, ArmorSet set) {
		this.player = player;
		this.set = set;
	}
	public Player getPlayer() { return player; }
	public ArmorSet getArmorSet() { return set; }
	public HandlerList getHandlers() { return handlers; }
	public static HandlerList getHandlerList() { return handlers; }
}
