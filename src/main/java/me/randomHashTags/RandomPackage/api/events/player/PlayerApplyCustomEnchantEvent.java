package me.randomHashTags.RandomPackage.api.events.player;

import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

import me.randomHashTags.RandomPackage.utils.classes.customenchants.CustomEnchant;

public class PlayerApplyCustomEnchantEvent extends Event {
	private static final HandlerList handlers = new HandlerList();
	private final Player player;
	private final CustomEnchant enchant;
	private final int level;
	private final CustomEnchantApplyResult result;
	public PlayerApplyCustomEnchantEvent(Player player, CustomEnchant enchant, int level, CustomEnchantApplyResult result) {
		this.player = player;
		this.enchant = enchant;
		this.level = level;
		this.result = result;
	}
	public Player getPlayer() { return player; }
	public CustomEnchant getEnchant() { return enchant; }
	public int getLevel() { return level; }
	public CustomEnchantApplyResult getResult() { return result; }
	public HandlerList getHandlers() { return handlers; }
	public static HandlerList getHandlerList() { return handlers; }
	public enum CustomEnchantApplyResult { SUCCESS_APPLY, DESTROY, DESTROY_WHITE_SCROLL, FAILED, SUCCESS_UPGRADE, }
}
