package me.randomHashTags.RandomPackage.api.events.customenchant;

import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class PvAnyEvent extends Event implements Cancellable {
    private static final HandlerList handlers = new HandlerList();
    private boolean cancelled;
    private final Player damager;
    private final LivingEntity victim;
    private double damage;
    private Projectile proj;
    public PvAnyEvent(Player damager, LivingEntity victim, double damage) {
        this.damager = damager;
        this.victim = victim;
        this.damage = damage;
    }
    public PvAnyEvent(Player damager, LivingEntity victim, double damage, Projectile proj) {
        this.damager = damager;
        this.victim = victim;
        this.damage = damage;
        this.proj = proj;
    }
    public Player getDamager() { return damager; }
    public LivingEntity getVictim() { return victim; }
    public double getDamage() { return damage; }
    public void setDamage(double damage) { this.damage = damage; }
    public Projectile getProjectile() { return proj; }

    public void setCancelled(boolean cancel) { cancelled = cancel; }
    public boolean isCancelled() { return cancelled; }
    public HandlerList getHandlers() { return handlers; }
    public static HandlerList getHandlerList() { return handlers; }
}
