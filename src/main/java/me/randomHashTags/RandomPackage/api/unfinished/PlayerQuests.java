package me.randomHashTags.RandomPackage.api.unfinished;

import me.randomHashTags.RandomPackage.RandomPackageAPI;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandExecutor;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.io.File;

public class PlayerQuests extends RandomPackageAPI implements Listener, CommandExecutor {

    private static PlayerQuests instance;
    public static PlayerQuests getPlayerQuests() {
        if(instance == null) instance = new PlayerQuests();
        return instance;
    }

    public boolean isEnabled = false;
    private ItemStack background, backToMenu, viewShop;
    public YamlConfiguration config;
    private Inventory menu, shop;

    public void enable() {
        if(isEnabled) return;
        save("Features", "player quests.yml");
        pluginmanager.registerEvents(this, randompackage);
        isEnabled = true;
        config = YamlConfiguration.loadConfiguration(new File(randompackage.getDataFolder() + File.separator + "Features" + File.separator, "player quests.yml"));

        menu = Bukkit.createInventory(null, config.getInt("menu.size"), ChatColor.translateAlternateColorCodes('&', config.getString("menu.title")));
        shop = Bukkit.createInventory(null, config.getInt("shop.size"), ChatColor.translateAlternateColorCodes('&', config.getString("shop.title")));
    }
    public void disable() {
        if(!isEnabled) return;
        isEnabled = false;
        HandlerList.unregisterAll(this);
    }


    @EventHandler
    private void inventoryClickEvent(InventoryClickEvent event) {
        if(!event.isCancelled()) {
            final Player player = (Player) event.getWhoClicked();
            final Inventory top = player.getOpenInventory().getTopInventory();
            final String t = top.getTitle();
            final boolean m = t.equals(menu.getTitle()), s = t.equals(shop.getTitle());
            if(top.getHolder() == player && (m || s)) {
                event.setCancelled(true);
                player.updateInventory();
                final ItemStack current = event.getCurrentItem();
                final int r = event.getRawSlot();
                if(current == null || r <= top.getSize()) return;
                if(m) {
                    if(current.equals(backToMenu)) {
                        viewMenu(player);
                    } else {

                    }
                } else {
                    if(current.equals(viewShop)) {
                        viewShop(player);
                    }
                }
                player.updateInventory();
            }
        }
    }

    public void viewMenu(Player player) {
        player.closeInventory();
        final int s = menu.getSize();
        player.openInventory(Bukkit.createInventory(player, s, menu.getTitle()));
        final Inventory top = player.getOpenInventory().getTopInventory();
        top.setContents(menu.getContents());
        for(int i = 0; i < s; i++) {
            item = top.getItem(i);
            if(item != null) {

            }
        }
        player.updateInventory();
    }
    public void viewShop(Player player) {
        player.closeInventory();
        final int s = shop.getSize();
        player.openInventory(Bukkit.createInventory(player, s, shop.getTitle()));
        final Inventory top = player.getOpenInventory().getTopInventory();
        top.setContents(shop.getContents());
        for(int i = 0; i < s; i++) {
            item = top.getItem(i);
            if(item != null) {

            }
        }
        player.updateInventory();
    }
}
