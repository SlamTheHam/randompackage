package me.randomHashTags.RandomPackage.api.unfinished;

import me.randomHashTags.RandomPackage.RandomPackageAPI;
import me.randomHashTags.RandomPackage.utils.classes.Outpost;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.server.PluginEnableEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.io.File;

public class Outposts extends RandomPackageAPI implements CommandExecutor, Listener {

    public boolean isEnabled = false;
    private static Outposts instance;
    public static final Outposts getOutposts() {
        if(instance == null) instance = new Outposts();
        return instance;
    }

    private YamlConfiguration config;
    private Inventory inv;
    private ItemStack background;

    public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {
        final Player player = sender instanceof Player ? (Player) sender : null;
        if(args.length == 0) {

        } else {
            if(args[0].equals("help") && hasPermission(sender, "RandomPackage.outpost.help", true)) viewHelp(sender);
            else if(player != null && args[0].equals("warp") && hasPermission(player, "RandomPackage.outpost.warp", true)) viewWarps(player);
        }
        return true;
    }


    @EventHandler
    private void pluginEnableEvent(PluginEnableEvent event) {
        if(event.getPlugin().equals(randompackage)) {
            isEnabled = true;
            config = YamlConfiguration.loadConfiguration(new File(randompackage.getDataFolder() + File.separator + "Features" + File.separator, "outposts.yml"));
            inv = Bukkit.createInventory(null, config.getInt("warp.size"), ChatColor.translateAlternateColorCodes('&', config.getString("warp.title")));
            background = d(config, "warp.background", 0);
            for(String s : config.getConfigurationSection("outposts").getKeys(false)) {
                final int slot = config.getInt("outposts." + s + ".slot");
                final ItemStack display = d(config, "outposts." + s + ".display", 0);
                final Outpost o = new Outpost(s, slot, null, null, 0.00, display, config.getStringList("outposts." + s + ".limits"), config.getStringList("outposts." + s + ".rewards"));
                inv.setItem(slot, display);
            }
            for(int i = 0; i < inv.getSize(); i++)
                if(inv.getItem(i) == null)
                    inv.setItem(i, background);
        }
    }


    @EventHandler
    private void inventoryClickEvent(InventoryClickEvent event) {
        final Player player = (Player) event.getWhoClicked();
        if(!event.isCancelled() && player.getOpenInventory().getTopInventory().getHolder() == player) {
            final String t = player.getOpenInventory().getTopInventory().getTitle();
            if(t.equals(inv.getTitle())) {
                event.setCancelled(true);
                player.updateInventory();
                final int r = event.getRawSlot();
                final String c = event.getClick().name();
                if(r < 0 || r >= player.getOpenInventory().getTopInventory().getSize() || !c.contains("LEFT") && !c.contains("RIGHT") || event.getCurrentItem() == null || event.getCurrentItem().equals(background)) return;
            }
        }
    }

    public void viewWarps(Player player) {
        player.closeInventory();
        player.openInventory(Bukkit.createInventory(player, inv.getSize(), inv.getTitle()));
        player.getOpenInventory().getTopInventory().setContents(inv.getContents());
        for(int i = 0; i < player.getOpenInventory().getTopInventory().getSize(); i++) {
            
        }
        player.updateInventory();
    }
    public void viewHelp(CommandSender sender) {
        sendStringListMessage(sender, config.getStringList("messages.help"));
    }
}
