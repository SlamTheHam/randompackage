package me.randomHashTags.RandomPackage.api.unfinished;

import me.randomHashTags.RandomPackage.RandomPackageAPI;
import me.randomHashTags.RandomPackage.utils.universal.UMaterial;
import me.randomHashTags.RandomPackage.utils.classes.Dungeon;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.io.File;
import java.util.Arrays;
import java.util.HashMap;

public class Dungeons extends RandomPackageAPI implements CommandExecutor, Listener {

    public boolean isEnabled = false;
    private static Dungeons instance;
    public static final Dungeons getDungeons() {
        if(instance == null) instance = new Dungeons();
        return instance;
    }

    public YamlConfiguration config;
    private Inventory dungeons, master;
    private ItemStack background;
    private HashMap<Integer, Dungeon> keys = new HashMap<>();

    public ItemStack dimensionweb, enchantedobsidian, fuelcell, holywhitescroll, soulanvil, soulpearl;

    public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {
        final Player player = sender instanceof Player ? (Player) sender : null;
        if(player != null && args.length == 0 && hasPermission(player, "RandomPackage.dungeons", true)) viewDungeons(player);
        return true;
    }

    public void enable() {
        if(isEnabled) return;
        save("Features", "dungeons.yml");
        pluginmanager.registerEvents(this, randompackage);
        isEnabled = true;
        config = YamlConfiguration.loadConfiguration(new File(randompackage.getDataFolder() + File.separator + "Features" + File.separator, "dungeons.yml"));
        int loaded = 0;
        for(String s : config.getConfigurationSection("dungeons").getKeys(false)) {
            loaded += 1;
            final Dungeon d = new Dungeon(s, config.getInt("dungeons." + s + " .slot"), null, d(config, "dungeons." + s + ".key", 0), d(config, "dungeons." + s + ".key locked", 0), d(config, "dungeons." + s + ".lootbag", 0), config.getStringList("dungeons." + s + ".lootbag.rewards"), null);
        }

        dimensionweb = d(config, "items.dimension web", 0);
        enchantedobsidian = d(config, "items.enchanted obsidian", 0);
        fuelcell = d(config, "items.fuel cell", 0);
        holywhitescroll = d(config, "items.holy white scroll", 0);
        soulanvil = d(config, "items.soul anvil", 0);
        soulpearl = d(config, "items.soul pearl", 0);
        addGivedpCategory(Arrays.asList(dimensionweb, enchantedobsidian, fuelcell, holywhitescroll, soulanvil, soulpearl), UMaterial.IRON_BARS, "Dungeon Items", "Givedp: Dungeon Items");

        dungeons = Bukkit.createInventory(null, config.getInt("gui.size"), ChatColor.translateAlternateColorCodes('&', config.getString("gui.title")));
        master = Bukkit.createInventory(null, config.getInt("master.size"), ChatColor.translateAlternateColorCodes('&', config.getString("master.title")));
        background = d(config, "gui.background", 0);
        final ItemStack undisDungeon = d(config, "gui.undiscovered.dungeon", 0), undisKey = d(config, "gui.undiscovered.key", 0);
        for(String s : config.getConfigurationSection("gui").getKeys(false)) {
            if(!s.equals("background") && !s.contains("discovered") && config.get("gui." + s + ".slot") != null) {
                final int slot = config.getInt("gui." + s + ".slot");
                final String i = config.getString("gui." + s + ".item").toUpperCase();
                if(i.startsWith("KEY:")) keys.put(slot, Dungeon.valueOf(config.getString("gui." + s + ".item").split(":")[1]));
                dungeons.setItem(slot, i.equals("{DUNGEON}") ? undisDungeon.clone() : i.equals("{KEY}") || i.startsWith("KEY:") ? undisKey.clone() : d(config, "gui." + s, 0));
            }
        }
        for(int i = 0; i < dungeons.getSize(); i++)
            if(dungeons.getItem(i) == null)
                dungeons.setItem(i, background);

        Bukkit.getConsoleSender().sendMessage(ChatColor.translateAlternateColorCodes('&', "&6[RandomPackage] &aLoaded " + loaded + " dungeons"));
    }
    public void disable() {
        if(!isEnabled) return;
        isEnabled = false;
        keys.clear();
        Dungeon.dungeons.clear();
        HandlerList.unregisterAll(this);
    }

    @EventHandler
    private void inventoryClickEvent(InventoryClickEvent event) {
        if(!event.isCancelled() && event.getWhoClicked().getOpenInventory().getTopInventory().getHolder() == event.getWhoClicked()) {
            final Player player = (Player) event.getWhoClicked();
            final String t = player.getOpenInventory().getTopInventory().getTitle();
            if(t.equals(dungeons.getTitle())) {
                event.setCancelled(true);
                player.updateInventory();
                final int r = event.getRawSlot();
                final String c = event.getClick().name();
                if(r < 0 || r >= player.getOpenInventory().getTopInventory().getSize() || !c.contains("LEFT") && !c.contains("RIGHT") || event.getCurrentItem() == null) return;
            }
        }
    }

    @EventHandler
    private void playerInteractEvent(PlayerInteractEvent event) {
        final ItemStack i = event.getItem();
        if(i != null && i.hasItemMeta()) {
            final ItemMeta im = i.getItemMeta();
            final Material t = i.getType();
            final byte d = i.getData().getData();
            final String o = im.equals(dimensionweb.getItemMeta()) && t.equals(dimensionweb.getType()) && d == dimensionweb.getData().getData() ? "d"
                            : im.equals(enchantedobsidian.getItemMeta()) && t.equals(enchantedobsidian.getType()) && d == enchantedobsidian.getData().getData() ? "e"
                            : im.equals(fuelcell.getItemMeta()) && t.equals(fuelcell.getType()) && d == fuelcell.getData().getData() ? "f"
                            : im.equals(holywhitescroll.getItemMeta()) && t.equals(holywhitescroll.getType()) && d == holywhitescroll.getData().getData() ? "h"
                            : im.equals(soulanvil.getItemMeta()) && t.equals(soulanvil.getType()) && d == soulanvil.getData().getData() ? "sa"
                            : im.equals(soulpearl.getItemMeta()) && t.equals(soulpearl.getType()) && d == soulpearl.getData().getData() ? "sp"
                            : null;
            if(o != null) {
                final Player player = event.getPlayer();
                event.setCancelled(true);
                player.updateInventory();
                if(o.equals("h")) return;
            }
        }
    }

    public void viewDungeons(Player player) {
        player.closeInventory();
        player.openInventory(Bukkit.createInventory(player, dungeons.getSize(), dungeons.getTitle()));
        player.getOpenInventory().getTopInventory().setContents(dungeons.getContents());
        for(int i : keys.keySet()) {
            final Dungeon d = keys.get(i);
            if(player.getInventory().containsAtLeast(d.getKey(), 1)) {
            } else {
                player.getOpenInventory().getTopInventory().setItem(i, d.getKeyLocked());
            }
        }
        player.updateInventory();
    }
    public void viewMaster(Player player) {
        player.closeInventory();
        player.openInventory(Bukkit.createInventory(player, master.getSize(), master.getTitle()));
        player.updateInventory();
    }
}
