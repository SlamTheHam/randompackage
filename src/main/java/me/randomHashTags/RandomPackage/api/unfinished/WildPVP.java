package me.randomHashTags.RandomPackage.api.unfinished;

import me.randomHashTags.RandomPackage.RandomPackageAPI;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;

import java.io.File;

public class WildPVP extends RandomPackageAPI implements Listener, CommandExecutor {
    public boolean isEnabled = false;
    private static WildPVP instance;
    public static final WildPVP getWildPVP() {
        if(instance == null) instance = new WildPVP();
        return instance;
    }


    public YamlConfiguration config;



    public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {
        final Player player = sender instanceof Player ? (Player) sender : null;
        return true;
    }

    public void enable() {
        if(isEnabled) return;
        save("Features", "wild pvp.yml");
        config = YamlConfiguration.loadConfiguration(new File(randompackage.getDataFolder() + File.separator + "Features" + File.separator, "wild pvp.yml"));
        pluginmanager.registerEvents(this, randompackage);
        isEnabled = true;
    }
    public void disable() {
        if(!isEnabled) return;
        isEnabled = false;
        HandlerList.unregisterAll(this);
    }


    public void createMatch(Player player) {

    }
}
