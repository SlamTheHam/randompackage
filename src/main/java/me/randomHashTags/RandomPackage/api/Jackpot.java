package me.randomHashTags.RandomPackage.api;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import me.randomHashTags.RandomPackage.RPPlayerData;
import me.randomHashTags.RandomPackage.RandomPackageAPI;

@SuppressWarnings({"deprecation", "unchecked"})
public class Jackpot extends RandomPackageAPI implements Listener, CommandExecutor {

	public boolean isEnabled = false;
	private static Jackpot instance;
	public static final Jackpot getJackpot() {
		if(instance == null) instance = new Jackpot();
		return instance;
	}

	private Inventory jackpotInv;
	private ItemStack accept, cancel;

	public long lastJackpot;
	private int choosewinnertime;

	private HashMap<UUID, Integer> jackpot = new HashMap<>();

	public YamlConfiguration config;
	private String winnerformat, timeformat;
	private int ticketprice, showperpage, task;

	public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {
		final Player player = sender instanceof Player ? (Player) sender : null;
		if(args.length == 0)
			viewCurrentJackpot(sender);
		else if(args[0].equals("pick") && hasPermission(sender, "RandomPackage.jackpot.pick", true))
			chooseWinner();
		else if(args[0].equals("help"))
			viewHelp(sender);
		else if(player != null) {
			final String a = args[0];
			if(a.equals("stats"))
				viewStats(player);
			else if(args.length >= 2 && (a.equals("buy") || a.equals("purchase"))) {
				final int tickets = getRemainingInt(args[1]);
				if(tickets != -1) openConfirmPurchase(player, tickets);
			} else if(a.equals("notifications"))
				toggleNotifications(player);
		}
		return true;
	}

	public void enable() {
		final long started = System.currentTimeMillis();
		if(isEnabled) return;
		save("Features", "jackpot.yml");
		pluginmanager.registerEvents(this, randompackage);
		isEnabled = true;
		config = YamlConfiguration.loadConfiguration(new File(randompackage.getDataFolder() + File.separator + "Features" + File.separator, "jackpot.yml"));
		jackpotInv = Bukkit.createInventory(null, config.getInt("gui.size"), ChatColor.translateAlternateColorCodes('&', config.getString("gui.title")));
		accept = d(config, "accept", 0);
		cancel = d(config, "cancel", 0);

		for(int i = 0; i < jackpotInv.getSize(); i++)
			if(config.get("gui." + i) != null)
				jackpotInv.setItem(i, config.get("gui." + i + ".item").equals("{CANCEL}") ? cancel : accept);

		choosewinnertime = config.getInt("choose winner every");
		ticketprice = config.getInt("ticket price");
		winnerformat = config.getString("messages.winner format");
		timeformat = config.getString("messages.time format");
		loadBackup();

		Bukkit.getConsoleSender().sendMessage(ChatColor.translateAlternateColorCodes('&', "&6[RandomPackage] &aLoaded Jackpot &e(took " + (System.currentTimeMillis()-started) + "ms)"));
	}
	public void disable() {
		if(!isEnabled) return;
		backup();
		isEnabled = false;
		Bukkit.getScheduler().cancelTask(task);
		HandlerList.unregisterAll(this);
	}
	public void backup() {
		otherdata.set("jackpot.started", lastJackpot);
		final List<String> current = new ArrayList<>();
		for(UUID u : jackpot.keySet()) current.add(u.toString() + ";" + jackpot.get(u));
		otherdata.set("jackpot.current", current);
		saveOtherData();
	}
	public void loadBackup() {
		final List<String> current = otherdata.getStringList("jackpot.current");
		for(String s : current) jackpot.put(UUID.fromString(s.split(";")[0]), Integer.parseInt(s.split(";")[1]));

		final long o = otherdata.getLong("jackpot.started");
		lastJackpot = o != 0 ? o : System.currentTimeMillis();
		task = Bukkit.getScheduler().scheduleSyncRepeatingTask(randompackage, () -> {
			int sec = (int) ((lastJackpot-System.currentTimeMillis())/1000) + choosewinnertime;
			final int seconds = sec;
			if(seconds <= 0) {
				chooseWinner();
			} else {
				int days = sec / (60 * 60 * 24);
				sec -= days * (60 * 60 * 24);
				int hours = sec / (60 * 60);
				sec -= hours * (60 * 60);
				int min = sec / 60;
				sec = sec - (min * 60);
				final String time = timeformat.replace("{HRS}", "" + hours).replace("{MIN}", "" + min).replace("{SEC}", "" + sec), cj = formatInt(getCurrentJackpot());
				if(config.getStringList("messages.countdowns").contains(seconds + "s")) {
					for(String string : config.getStringList("messages.drawing countdown")) {
						Bukkit.broadcastMessage(ChatColor.translateAlternateColorCodes('&', string.replace("{TIME}", time).replace("{JACKPOT}", cj)));
					}
				}
			}
		}, 0, 20);
	}
	public int getCurrentJackpot() {
		int t = 0;
		for(int l : jackpot.values()) t += l;
		return t * ticketprice;
	}
	public void chooseWinner() {
		if(jackpot.size() != 0) {
			final int randomplayer = random.nextInt(jackpot.size());
			final OfflinePlayer winner = Bukkit.getOfflinePlayer((UUID) jackpot.keySet().toArray()[randomplayer]);
			final UUID u = winner.getUniqueId();
			int totalticketssold = 0, ticketssold = jackpot.get(u), payout = 0;
			for(int l : jackpot.values()) totalticketssold = totalticketssold + l;
			payout = totalticketssold * ticketprice;
			for(String string : config.getStringList("messages.broadcast")) {
				if(string.contains("{PLAYER}")) string = string.replace("{PLAYER}", winner.getName());
				if(string.contains("{$}")) string = string.replace("{$}", formatInt(payout));
				if(string.contains("{TICKETS_P}")) string = string.replace("{TICKETS_P}", formatInt(ticketssold));
				if(string.contains("{TICKETS_P%}")) string = string.replace("{TICKETS_P%}", "" + ((ticketssold / totalticketssold) * 100));
				if(string.contains("{TICKETS_TOTAL}")) string = string.replace("{TICKETS_TOTAL}", formatInt(totalticketssold));
				Bukkit.broadcastMessage(ChatColor.translateAlternateColorCodes('&', string));
			}
			eco.depositPlayer(winner, payout);
			final RPPlayerData w = RPPlayerData.get(u);
			w.jackpotWinnings += payout;
			w.jackpotWins += 1;
			jackpot.clear();
		}
		lastJackpot = System.currentTimeMillis();
	}
	@EventHandler
	private void inventoryClickEvent(InventoryClickEvent event) {
		final Player player = (Player) event.getWhoClicked();
		if(!event.isCancelled() && player.getOpenInventory().getTopInventory().getTitle() != null && player.getOpenInventory().getTopInventory().getTitle().equals(jackpotInv.getTitle())) {
			event.setCancelled(true);
			player.updateInventory();
			item = event.getCurrentItem();
			final String c = event.getClick().name();
			if(event.getRawSlot() < 0 || event.getRawSlot() >= player.getOpenInventory().getTopInventory().getSize() || !c.contains("LEFT") && !c.contains("RIGHT") || item == null || !item.hasItemMeta() || !item.getItemMeta().hasDisplayName() || !item.getItemMeta().hasLore()) return;

			if(item.getItemMeta().getDisplayName().equals(accept.getItemMeta().getDisplayName())) {
				int line = 0, space = 0;
				for(int i = 0; i < accept.getItemMeta().getLore().size(); i++) if(accept.getItemMeta().getLore().get(i).contains("{TICKETS}")) line = i;
				for(int i = 0; i < accept.getItemMeta().getLore().get(line).split(" ").length; i++) if(accept.getItemMeta().getLore().get(line).split(" ")[i].equals("{TICKETS}")) space = i;
				final int purchasedtickets = getRemainingInt(item.getItemMeta().getLore().get(line).split(" ")[space]);
				eco.withdrawPlayer(player, ticketprice * purchasedtickets);
				purchaseTickets(RPPlayerData.get(player.getUniqueId()), purchasedtickets);
			} else if(item.getItemMeta().getDisplayName().equals(cancel.getItemMeta().getDisplayName())) {
				sendStringListMessage(player, config.getStringList("messages.cancelled"), 0);
			} else return;
			player.closeInventory();
		}
	}

	public void openConfirmPurchase(Player p, int amount) {
		if(hasPermission(p, "RandomPackage.jackpot.buy", true)) {
			if(eco.getBalance(p.getName()) >= amount * ticketprice) {
				p.openInventory(Bukkit.createInventory(p, jackpotInv.getSize(), jackpotInv.getTitle()));
				p.getOpenInventory().getTopInventory().setContents(jackpotInv.getContents()); lore.clear();
				sendStringListMessage(p, config.getStringList("messages.confirm purchase"), amount);
				for(int i = 0; i < p.getOpenInventory().getTopInventory().getSize(); i++) {
					item = p.getOpenInventory().getTopInventory().getItem(i);
					if(item != null && item.hasItemMeta() && item.getItemMeta().hasDisplayName() && item.getItemMeta().hasLore()) {
						itemMeta = item.getItemMeta();
						if(itemMeta.hasLore()) {
							for(int t = 0; t < itemMeta.getLore().size(); t++) {
								String string = ChatColor.translateAlternateColorCodes('&', itemMeta.getLore().get(t));
								if(string.contains("{TICKETS}")) string = string.replace("{TICKETS}", formatInt(amount));
								if(string.contains("{PRICE}")) string = string.replace("{PRICE}", formatInt(amount * ticketprice));
								lore.add(string);
							}
						}
						itemMeta.setLore(lore); lore.clear();
						item.setItemMeta(itemMeta);
					}
				}
				p.updateInventory();
			} else {
				sendStringListMessage(p, config.getStringList("messages.cannot afford"), amount);
			}
		}
	}
	public void purchaseTickets(RPPlayerData pdata, int amount) {
		final OfflinePlayer pl = pdata.getOfflinePlayer();
		final UUID u = pl.getUniqueId();
		pdata.jackpotTicketsPurchased += amount;
		if(pl.isOnline())
			sendStringListMessage(pl.getPlayer(), config.getStringList("messages.purchased"), amount);
		jackpot.put(u, jackpot.keySet().contains(u) ? jackpot.get(u) + amount : amount);
	}
	public void viewCurrentJackpot(CommandSender sender) {
		if(hasPermission(sender, "RandomPackage.jackpot", true)) {
			final Player p = sender instanceof Player ? (Player) sender : null;
			String time = timeformat;

			int sec = (int) ((lastJackpot - System.currentTimeMillis()) / 1000) + choosewinnertime;
			int days = sec / (60 * 60 * 24);
			sec -= days * (60 * 60 * 24);
			int hours = sec / (60 * 60);
			sec -= hours * (60 * 60);
			int min = sec / 60;
			sec = sec - (min * 60);
			if(time.contains("{HRS}")) time = time.replace("{HRS}", "" + hours);
			if(time.contains("{MIN}")) time = time.replace("{MIN}", "" + min);
			if(time.contains("{SEC}")) time = time.replace("{SEC}", "" + sec);

			int ticketssold = 0, yourtickets = 0;
			for(int l : jackpot.values()) ticketssold = ticketssold + l;
			if(p != null) for(UUID uuid : jackpot.keySet()) if(p.getUniqueId() == uuid) yourtickets = jackpot.get(uuid);
			int percent = 0;
			if(ticketssold != 0) percent = (yourtickets / ticketssold) * 100;
			for(String string : config.getStringList("messages.view")) {
				if(string.contains("{TICKETS_SOLD}")) string = string.replace("{TICKETS_SOLD}", formatInt(ticketssold));
				if(string.contains("{VALUE}")) string = string.replace("{VALUE}", formatInt(ticketssold * ticketprice));
				if(string.contains("{YOUR_TICKETS}")) string = string.replace("{YOUR_TICKETS}", formatInt(yourtickets));
				if(string.contains("{PERCENT}")) string = string.replace("{PERCENT}", "" + percent);
				if(string.contains("{TIME}")) string = string.replace("{TIME}", time);
				sender.sendMessage(ChatColor.translateAlternateColorCodes('&', string));
			}
		}
	}
	public void viewStats(Player player) {
		if(hasPermission(player, "RandomPackage.jackpot.stats", true)) {
			final RPPlayerData pdata = RPPlayerData.get(player.getUniqueId());
			final HashMap<String, String> replacements = new HashMap<>();
			replacements.put("{TOTAL_WINNINGS}", formatDouble(pdata.jackpotWinnings));
			replacements.put("{TOTAL_TICKETS_PURCHASED}", formatInt(pdata.jackpotTicketsPurchased));
			replacements.put("{TOTAL_JACKPOT_WINS}", formatDouble(pdata.jackpotWins));
			sendStringListMessage(player, config.getStringList("messages.stats"), replacements);
		}
	}
	public void viewHelp(CommandSender sender) {
		if(hasPermission(sender, "RandomPackage.jackpot.help", true))
			sendStringListMessage(sender, config.getStringList("messages.help"));
	}
	public void toggleNotifications(Player player) {
		if(hasPermission(player, "RandomPackage.jackpot.notifications", true)) {
			final RPPlayerData pdata = RPPlayerData.get(player.getUniqueId());
			pdata.receivesJackpotNotifications = !pdata.receivesJackpotNotifications;
			sendStringListMessage(player, config.getStringList("messages.notification " + (pdata.receivesJackpotNotifications ? "enable" : "disable")));
		}
	}
	private void sendStringListMessage(Player player, List<String> message, int number) {
		final String amount = formatInt(number), v = formatInt(number * ticketprice);
		for(String string : message) {
			if(string.contains("{TICKETS}")) string = string.replace("{TICKETS}", amount);
			if(string.contains("{$}")) string = string.replace("{$}", v);
			player.sendMessage(ChatColor.translateAlternateColorCodes('&', string));
		}
	}
}
