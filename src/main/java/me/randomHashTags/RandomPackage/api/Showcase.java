package me.randomHashTags.RandomPackage.api;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import me.randomHashTags.RandomPackage.RPPlayerData;
import me.randomHashTags.RandomPackage.RandomPackageAPI;

public class Showcase extends RandomPackageAPI implements CommandExecutor, Listener {
	
	public boolean isEnabled = false;
	private static Showcase instance;
	public static final Showcase getShowcase() {
		if(instance == null) instance = new Showcase();
		return instance;
	}
	
	private ItemStack addItemConfirm, addItemCancel, removeItemConfirm, removeItemCancel, expansion;
	private int addedRows = 0;
	
	private Inventory additems, removeitems;
	private String othertitle, selftitle, TCOLOR;
	private ArrayList<Integer> itemslots = new ArrayList<>();
	
	private ArrayList<Player> inSelf = new ArrayList<>(), inOther = new ArrayList<>();
	private HashMap<Player, Integer> deleteSlot = new HashMap<>();

	public YamlConfiguration config;

	public void enable() {
		final long started = System.currentTimeMillis();
		if(isEnabled) return;
		save("Features", "showcase.yml");
		pluginmanager.registerEvents(this, randompackage);
		isEnabled = true;
		config = YamlConfiguration.loadConfiguration(new File(randompackage.getDataFolder() + File.separator + "Features" + File.separator + "showcase.yml"));
		expansion = d(itemsConfig, "showcase-expansion", 0);
		addedRows = itemsConfig.getInt("showcase-expansion.added-rows");
		additems = Bukkit.createInventory(null, config.getInt("add-item.size"), ChatColor.translateAlternateColorCodes('&', config.getString("add-item.title")));
		removeitems = Bukkit.createInventory(null, config.getInt("remove-item.size"), ChatColor.translateAlternateColorCodes('&', config.getString("remove-item.title")));

		othertitle = ChatColor.translateAlternateColorCodes('&', config.getString("view-other-title"));
		selftitle = ChatColor.translateAlternateColorCodes('&', config.getString("view-self-title"));
		TCOLOR = config.getString("time-color");

		addItemConfirm = d(config, "add-item.confirm", 0);
		addItemCancel = d(config, "add-item.cancel", 0);
		removeItemConfirm = d(config, "remove-item.confirm", 0);
		removeItemCancel = d(config, "remove-item.cancel", 0);

		for(int i = 1; i <= 2; i++) {
			for(int o = 0; o < (i == 1 ? additems.getSize() : removeitems.getSize()); o++) {
				String s = config.getString((i == 1 ? "add-item." : "remove-item.") + o + ".item");
				if(s != null) {
					if(s.equals("{CONFIRM}")) {
						if(i == 1) additems.setItem(o, addItemConfirm.clone());
						else       removeitems.setItem(o, removeItemConfirm.clone());
					} else if(s.equals("{CANCEL}")) {
						(i == 1 ? additems : removeitems).setItem(o, i == 1 ? addItemCancel : removeItemCancel);
					} else if(s.equals("{ITEM}")) itemslots.add(o);
				}
			}
		}
		Bukkit.getConsoleSender().sendMessage(ChatColor.translateAlternateColorCodes('&', "&6[RandomPackage] &aLoaded Showcase &e(took " + (System.currentTimeMillis()-started) + "ms)"));
	}
	public void disable() {
		if(!isEnabled) return;
		isEnabled = false;
		for(int i = 0; i < inSelf.size(); i++) {
		    final Player p = inSelf.get(i);
			p.sendMessage(ChatColor.translateAlternateColorCodes('&', "&e&l(!)&r &eYou've been forced to exit a showcase due to reloading the server."));
			p.closeInventory();
			inSelf.remove(i);
			i--;
		}
		for(int i = 0; i < inOther.size(); i++) {
		    final Player p = inOther.get(i);
			p.sendMessage(ChatColor.translateAlternateColorCodes('&', "&e&l(!)&r &eYou've been forced to exit a showcase due to reloading the server."));
			p.closeInventory();
			inOther.remove(i);
			i--;
		}
		itemslots.clear();
		deleteSlot.clear();
		HandlerList.unregisterAll(this);
	}
	@EventHandler
	private void inventoryClickEvent(InventoryClickEvent event) {
		if(!event.isCancelled() && event.getCurrentItem() != null && !event.getCurrentItem().getType().equals(Material.AIR)) {
			final Player player = (Player) event.getWhoClicked();
			final Inventory top = player.getOpenInventory().getTopInventory();
			final String t = top.getTitle();
			final boolean edit = t.equals(additems.getTitle()) || t.equals(removeitems.getTitle());
			final ItemStack c = event.getCurrentItem();
			
			if(edit) {
				final ItemStack item = top.getItem(itemslots.get(0));
				if(t.equals(additems.getTitle()) && c.equals(addItemConfirm)) {
					add(player.getUniqueId(), item, 1);
				} else if(c.equals(removeItemConfirm)) {
					delete(player.getUniqueId(), 1, top.getItem(deleteSlot.get(player)));
					deleteSlot.remove(player);
				}
			} else if(inSelf.contains(player)) {
				if(event.getRawSlot() >= top.getSize()) {
					confirmAddition(player, c);
				} else {
					confirmDeletion(player, c);
					deleteSlot.put(player, itemslots.get(1));
				}
			}
			else return;
			
			event.setCancelled(true);
			player.updateInventory();
			if(edit && (t.equals(additems.getTitle()) || t.equals(removeitems.getTitle())) && (c.equals(addItemConfirm) || c.equals(addItemCancel) || c.equals(removeItemConfirm) || c.equals(removeItemCancel))) {
				open(player, player, 1);
				inSelf.add(player);
			}
		}
	}
	@EventHandler
	private void inventoryCloseEvent(InventoryCloseEvent event) {
		final Player player = (Player) event.getPlayer();
		inSelf.remove(player);
		inOther.remove(player);
		deleteSlot.remove(player);
	}
	@EventHandler
    private void playerInteractEvent(PlayerInteractEvent event) {
	    final ItemStack i = event.getItem();
	    if(i != null && i.hasItemMeta() && i.getItemMeta().equals(expansion.getItemMeta())) {
	        final Material m = i.getType();
	        final byte d = i.getData().getData();
	        if(expansion.getType().equals(m) && d == expansion.getData().getData()) {
                final Player player = event.getPlayer();
                final RPPlayerData pdata = RPPlayerData.get(player.getUniqueId());
	            event.setCancelled(true);
	            for(int o = 1; o <= 10; o++) {
                    if(pdata.showcaseSizes.keySet().contains(o) && pdata.showcaseSizes.get(o) != 54) {
                        pdata.showcaseSizes.put(o, pdata.showcaseSizes.get(o) + (addedRows * 9));
                        removeItem(player, i, 1);
                        return;
                    }
                }
            }
        }
    }
	
	public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {
		final Player player = sender instanceof Player ? (Player) sender : null;
		final int l = args.length;
		if(l == 0 && player != null) {
            open(player, player, 1);
		} else if(l >= 1 && player != null) {
			if(args[0].startsWith("add")) {
				confirmAddition(player, getItemInHand(player));
			} else if(hasPermission(player, "RandomPackage.showcase.other", true)) {
				OfflinePlayer tplayer = Bukkit.getPlayer(args[0]);
				if(tplayer != null) {
					open(player, tplayer, 1);
				}
			}
		}
		return true;
	}
	public void confirmAddition(Player player, ItemStack item) {
		if(hasPermission(player, "RandomPackage.showcase.add", true))
			confirm(player, item, additems);
	}
	public void confirmDeletion(Player player, ItemStack item) { confirm(player, item, removeitems); }
	private void confirm(Player player, ItemStack item, Inventory type) {
		if(item != null && !item.getType().equals(Material.AIR)) {
			player.openInventory(Bukkit.createInventory(player, type.getSize(), type.getTitle()));
			player.getOpenInventory().getTopInventory().setContents(type.getContents());
			for(int i : itemslots) player.getOpenInventory().getTopInventory().setItem(i, item);
		}
		player.updateInventory();
	}
	private void add(UUID player, ItemStack item, int page) {
		if(item == null || item.getType().equals(Material.AIR)) {
			
		} else {
		    final OfflinePlayer op = Bukkit.getOfflinePlayer(player);
			if(op.isOnline())
				removeItem(op.getPlayer(), item, item.getAmount());
			final String format = toReadableDate(new Date(), "MMMM dd, yyyy");
			itemMeta = item.getItemMeta();
			if(item.getItemMeta().hasLore()) lore.addAll(item.getItemMeta().getLore());
			lore.add(ChatColor.translateAlternateColorCodes('&', TCOLOR + format));
			itemMeta.setLore(lore); lore.clear();
			item.setItemMeta(itemMeta);
			final RPPlayerData pdata = RPPlayerData.get(player);
			if(!pdata.showcasedItems.keySet().contains(page)) pdata.showcasedItems.put(page, new ArrayList<>());
			if(pdata.showcaseSizes.get(page) > pdata.showcasedItems.get(page).size())
			    pdata.showcasedItems.get(page).add(item);
		}
	}
	private void delete(UUID player, int page, ItemStack is) {
		final RPPlayerData pdata = RPPlayerData.get(player);
		pdata.showcasedItems.get(page).remove(is);
	}
	public void open(Player opener, OfflinePlayer target, int page) {
		if(target == null || target == opener) target = opener;
		final RPPlayerData pdata = RPPlayerData.get(target.getUniqueId());
		final int actualsize = pdata.showcasedItems.keySet().size();
		if(!hasPermission(opener, "RandomPackage.showcase" + (target == opener ? "" : ".other"), false)) {
			sendStringListMessage(opener, config.getStringList("messages.no-access"));
		} else {
		    if(target == opener) inSelf.add(opener);
		    else inOther.add(opener);
			final Inventory inv = Bukkit.createInventory(opener, pdata.showcaseSizes.get(page), (opener.equals(target) ? selftitle : othertitle).replace("{PLAYER}", opener.getName()).replace("{CURRENT}", "" + page).replace("{MAX}", "" + actualsize));
			opener.openInventory(inv);
			final Inventory top = opener.getOpenInventory().getTopInventory();
            for(ItemStack is : pdata.showcasedItems.get(page))
                if(top.firstEmpty() != -1)
                    top.setItem(top.firstEmpty(), is);
		}
	}
}
