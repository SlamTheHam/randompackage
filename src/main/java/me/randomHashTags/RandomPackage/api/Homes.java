package me.randomHashTags.RandomPackage.api;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import me.randomHashTags.RandomPackage.utils.universal.UMaterial;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.player.PlayerTeleportEvent.TeleportCause;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import me.randomHashTags.RandomPackage.RPPlayerData;
import me.randomHashTags.RandomPackage.RPPlayerData.Home;
import me.randomHashTags.RandomPackage.RandomPackageAPI;

public class Homes extends RandomPackageAPI implements CommandExecutor, Listener {
	
	public boolean isEnabled = false;
	private static Homes instance;
	public static final Homes getHomes() {
		if(instance == null) instance = new Homes();
		return instance;
	}
	private ArrayList<Player> viewingHomes = new ArrayList<>();
	private HashMap<Player, Home> editingIcons = new HashMap<>();
	private Inventory editicon;
	public YamlConfiguration config;


	public void enable() {
		final long started = System.currentTimeMillis();
		if(isEnabled) return;
		save("Features", "homes.yml");
		pluginmanager.registerEvents(this, randompackage);
		isEnabled = true;
		config = YamlConfiguration.loadConfiguration(new File(randompackage.getDataFolder() + File.separator + "Features" + File.separator, "homes.yml"));
		editicon = Bukkit.createInventory(null, config.getInt("edit icon.size"), ChatColor.translateAlternateColorCodes('&', config.getString("edit icon.title")));
		final List<String> addedlore = config.getStringList("edit icon.added lore"), ad = new ArrayList<>();
		for(String s : addedlore) ad.add(ChatColor.translateAlternateColorCodes('&', s));
		for(String s : config.getConfigurationSection("edit icon").getKeys(false)) {
			if(!s.equals("title") && !s.equals("size") && !s.equals("added lore")) {
				item = d(config, "edit icon." + s, 0); itemMeta = item.getItemMeta(); lore.clear();
				if(itemMeta.hasLore()) lore.addAll(itemMeta.getLore());
				lore.addAll(ad);
				itemMeta.setLore(lore); lore.clear();
				item.setItemMeta(itemMeta);
				editicon.setItem(config.getInt("edit icon." + s + ".slot"), item);
			}
		}
		Bukkit.getConsoleSender().sendMessage(ChatColor.translateAlternateColorCodes('&', "&6[RandomPackage] &aLoaded Homes &e(took " + (System.currentTimeMillis()-started) + "ms)"));
	}
	public void disable() {
		if(!isEnabled) return;
		isEnabled = false;
		for(Player p : viewingHomes) p.closeInventory();
		for(Player player : editingIcons.keySet()) player.closeInventory();
		HandlerList.unregisterAll(this);
	}
	
	public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {
		if(!(sender instanceof Player)) return true;
		final Player player = (Player) sender;
		final RPPlayerData pdata = RPPlayerData.get(player.getUniqueId());
		final String c = cmd.getName();
		if(c.equals("home")) {
			if(args.length == 0) {
				viewHomes(player, pdata);
			} else {
				final Home home = pdata.getHome(getArguments(args));
				if(home != null)
					teleportToHome(player, home);
				else
					viewHomes(player, pdata);
			}
		} else if(c.equals("sethome")) {
			if(hasPermission(player, "RandomPackage.sethome", true) && pdata.homes.size()+1 <= pdata.getMaxHomes()) {
				pdata.addHome(player.getLocation(), args.length == 0 ? randompackage.getConfig().getString("sethome.default-name") : getArguments(args), new ItemStack(Material.GRASS, 1), true, false);
			}
		}
		return true;
	}
	
	private String getArguments(String[] args) {
		String arguments = "";
		for(int i = 0; i < args.length; i++) arguments = arguments + args[i] + (i == args.length - 1 ? "" : " ");
		return arguments;
	}
	public void viewHomes(Player opener, RPPlayerData target) {
		if(hasPermission(opener, "RandomPackage.home", true)) {
			viewingHomes.add(opener);
			final List<Home> homes = target.homes;
			final String name = ChatColor.translateAlternateColorCodes('&', config.getString("menu.name"));
			final List<String> l = config.getStringList("menu.lore");
			opener.openInventory(Bukkit.createInventory(opener, ((homes.size() + 9) / 9) * 9, ChatColor.translateAlternateColorCodes('&', config.getString("menu.title").replace("{SET}", Integer.toString(homes.size())).replace("{MAX}", Integer.toString(target.getMaxHomes())))));
			final Inventory top = opener.getOpenInventory().getTopInventory();
			for(Home h : homes) {
				final Location lo = h.location;
				final String w = lo.getWorld().getName();
				final double x = round(lo.getX(), 1), y = round(lo.getY(), 1), z = round(lo.getZ(), 1);
				item = h.icon.clone(); itemMeta = item.getItemMeta(); lore.clear();
				itemMeta.setDisplayName(name.replace("{HOME}", h.name));
				for(String s : l) lore.add(ChatColor.translateAlternateColorCodes('&', s.replace("{WORLD}", w).replace("{X}", Double.toString(x)).replace("{Y}", Double.toString(y)).replace("{Z}", Double.toString(z))));
				itemMeta.setLore(lore); lore.clear();
				item.setItemMeta(itemMeta);
				top.setItem(top.firstEmpty(), item);
			}
			opener.updateInventory();
		}
	}
	public void teleportToHome(Player player, Home home) {
		if(hasPermission(player, "RandomPackage.home.teleport", true))
			player.teleport(home.location, TeleportCause.PLUGIN);
	}
	public void editIcon(Player player, Home home) {
		player.closeInventory();
		player.openInventory(Bukkit.createInventory(player, editicon.getSize(), editicon.getTitle()));
		player.getOpenInventory().getTopInventory().setContents(editicon.getContents());
		player.updateInventory();
		editingIcons.put(player, home);
	}
	@EventHandler
	private void inventoryCloseEvent(InventoryCloseEvent event) {
		final Player p = (Player) event.getPlayer();
		viewingHomes.remove(p);
		if(editingIcons.keySet().contains(p)) editingIcons.remove(p);
	}
	@EventHandler
	private void inventoryClickEvent(InventoryClickEvent event) {
		if(!event.isCancelled()) {
			final Player player = (Player) event.getWhoClicked();
			if(viewingHomes.contains(player) || editingIcons.keySet().contains(player)) {
				event.setCancelled(true);
				player.updateInventory();
				final int r = event.getRawSlot();
				final String click = event.getClick().name();
				if(r < 0 || r >= player.getOpenInventory().getTopInventory().getSize() || !click.contains("RIGHT") && !click.contains("LEFT") && !click.contains("MIDDLE") || event.getCurrentItem() == null || event.getCurrentItem().getType().equals(Material.AIR)) return;
				if(viewingHomes.contains(player)) {
					final RPPlayerData pdata = RPPlayerData.get(player.getUniqueId());
					final List<Home> homes = pdata.homes;
					player.closeInventory();
					final Home h = homes.get(r);
					if(click.contains("LEFT"))       teleportToHome(player, h);
					else if(click.equals("MIDDLE")) {
						pdata.deleteHome(h);
					} else if(click.contains("RIGHT")) editIcon(player, h);
				} else {
					final ItemStack i = event.getCurrentItem();
					final Home h = editingIcons.get(player);
					final UMaterial um = UMaterial.matchUMaterial(i.getType().name(), i.getData().getData());
					final String n = h.name, umn = um.name();
					h.icon = um.getItemStack();
					player.closeInventory();
					for(String s : randompackage.getConfig().getStringList("home.save-icon"))
						player.sendMessage(ChatColor.translateAlternateColorCodes('&', s.replace("{HOME}", n).replace("{ICON}", umn)));
				}
			}
		}
	}
}
