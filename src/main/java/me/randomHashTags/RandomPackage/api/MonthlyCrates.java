package me.randomHashTags.RandomPackage.api;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import me.randomHashTags.RandomPackage.utils.GivedpItem;
import me.randomHashTags.RandomPackage.utils.universal.UMaterial;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import me.randomHashTags.RandomPackage.RPPlayerData;
import me.randomHashTags.RandomPackage.RandomPackageAPI;
import me.randomHashTags.RandomPackage.utils.classes.MonthlyCrate;

@SuppressWarnings({"deprecation"})
public class MonthlyCrates extends RandomPackageAPI implements Listener, CommandExecutor {
	
	public boolean isEnabled = false;
	private static MonthlyCrates instance;
	public static final MonthlyCrates getMonthlyCrates() {
		if(instance == null) instance = new MonthlyCrates();
		return instance;
	}

	private FileConfiguration config;
	private Inventory gui;
	private ItemStack locked, background, alreadyClaimed;
	public ItemStack mysterycrate, heroicmysterycrate;
	public List<ItemStack> crates = new ArrayList<>();
	
	private final HashMap<Player, List<Integer>> regularRewardsLeft = new HashMap<>(), bonusRewardsLeft = new HashMap<>(), playertimers = new HashMap<>();
	private final HashMap<Integer, List<ItemStack>> rewardAnimation = new HashMap<>(), bonusRewardAnimation = new HashMap<>();


	public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {
		final Player player = sender instanceof Player ? (Player) sender : null;
		if(args.length == 0 && player != null) {
			viewCrates(player);
		} else if(args.length == 2 && args[0].equals("reset") && hasPermission(player, "RandomPackage.monthlycrates.reset", true)) {
			reset(player, Bukkit.getOfflinePlayer(args[1]));
		}
		return true;
	}

	public void enable() {
	    final long started = System.currentTimeMillis();
	    if(isEnabled) return;
	    save("Features", "monthly crates.yml");
	    pluginmanager.registerEvents(this, randompackage);
        isEnabled = true;
        config = YamlConfiguration.loadConfiguration(new File(randompackage.getDataFolder() + File.separator + "Features" + File.separator, "monthly crates.yml"));
        mysterycrate = d(config, "items.mystery-crate", 0);
        heroicmysterycrate = d(config, "items.heroic-mystery-crate", 0);
        alreadyClaimed = d(config, "gui.already claimed", 0);
        locked = d(config, "gui.locked", 0);
        background = d(config, "gui.background", 0);
        gui = Bukkit.createInventory(null, config.getInt("gui.size"), ChatColor.translateAlternateColorCodes('&', config.getString("gui.title")));
        final List<ItemStack> is1 = new ArrayList<>(), is2 = new ArrayList<>();
        for(String s : config.getStringList("animations.reward.cycles thru"))
            is1.add(d(null, s, 0));
        for(String s : config.getStringList("animations.bonus reward.cycles thru"))
            is2.add(d(null, s, 0));
        rewardAnimation.put(config.getInt("animations.reward.duration"), is1);
        bonusRewardAnimation.put(config.getInt("animations.bonus reward.duration"), is2);
        final int defaultsize = config.getInt("crate-settings.default-size");
        final ItemStack defaultbackground = d(config, "crate-settings.default-background", 0), defaultbonus1 = d(config, "crate-settings.default-bonus-1", 0), defaultbonus2 = d(config, "crate-settings.default-bonus-2", 0), defaultredeem = d(config, "crate-settings.default-redeem", 0),
                defaultbonus = d(config, "crate-settings.default-bonus", 0);
        final String defaulttitle = ChatColor.translateAlternateColorCodes('&', config.getString("crate-settings.default-title"));
        final List<String> defaultredeemformat = config.getStringList("crate-settings.default-redeem-format"), defaultredeembonusformat = config.getStringList("crate-settings.default-redeem-bonus-format");
        int loaded = 0;
        for(String s : config.getConfigurationSection("crates").getKeys(false)) {
            final List<String> redeemformat = config.get("crates." + s + ".redeem-format") != null ? config.getStringList("crates." + s + ".redeem-format") : defaultredeemformat, redeembonusformat = config.get("crates." + s + ".redeem-bonus-format") != null ? config.getStringList("crates." + s + ".redeem-bonus-format") : defaultredeembonusformat;
            final String title = config.get("crates." + s + ".title") == null ? defaulttitle.replace("{PATH}", s) : ChatColor.translateAlternateColorCodes('&', config.getString("crates." + s + ".title"));
            final Inventory redeeminv = Bukkit.createInventory(null, config.get("crates." + s + ".size") == null ? defaultsize : config.getInt("crates." + s + ".size"), title),
                    bonusinv = Bukkit.createInventory(null, redeeminv.getSize(), redeeminv.getTitle());
            final ItemStack bonus1 = config.get("crates." + s + ".bonus-1") == null ? defaultbonus1 : d(config, "crates." + s + ".bonus-1", 0),
                    bonus2 = config.get("crates." + s + ".bonus-2") == null ? defaultbonus2 : d(config, "crates." + s + ".bonus-2", 0),
                    bonus = config.get("crates." + s + ".bonus-reward") == null ? defaultbonus : d(config, "crates." + s + ".bonus-reward", 0);
            for(int q = 1; q <= 2; q++) {
                final String g = q == 1 ? "" : "-bonus";
                for(int i = 0; i < config.getStringList("crate-settings.default-redeem" + g + "-format").size(); i++) {
                    for(int o = 0; o < config.getStringList("crate-settings.default-redeem" + g + "-format").get(i).length(); o++) {
                        final String p = config.getStringList("crate-settings.default-redeem" + g + "-format").get(i).substring(o, o+1);
                        final int slot = (i*9)+o;
                        if(p.equals("=")) (q == 1 ? redeeminv : bonusinv).setItem(slot, q == 1 ? defaultbackground : bonus2);
                        else if(p.equals("-")) {
                            (q == 1 ? redeeminv : bonusinv).setItem(slot, q == 1 ? defaultredeem : bonus1);
                        } else if(p.equals("+")) {
                            redeeminv.setItem(slot, defaultbonus);
                        } else if(p.equals("x"))
                            bonusinv.setItem(slot, defaultredeem);
                    }
                }
            }
            final MonthlyCrate mc = new MonthlyCrate(s, d(config, "crates." + s, 0), redeeminv, bonusinv, config.getStringList("crates." + s + ".rewards"), config.getStringList("crates." + s + ".bonus"), redeemformat, redeembonusformat, config.get("crates." + s + ".can-dupe-rewards") == null && !config.getBoolean("crates" + s + ".can-dupe-rewards") ? false : true);
            gui.addItem(mc.getItemStack().clone());
            crates.add(mc.getItemStack().clone());
            loaded += 1;
        }
        addGivedpCategory(crates, UMaterial.ENDER_CHEST, "Monthly Crates", "Givedp: Monthly Crates");
		Bukkit.getConsoleSender().sendMessage(ChatColor.translateAlternateColorCodes('&', "&6[RandomPackage] &aLoaded " + loaded + " monthly crates &e(took " + (System.currentTimeMillis()-started) + "ms)"));
	}
    public void disable() {
	    if(!isEnabled) return;
	    isEnabled = false;
	    MonthlyCrate.monthlycrates.clear();
	    MonthlyCrate.revealing.clear();
		HandlerList.unregisterAll(this);
    }

	public void viewCrates(Player player) {
		if(hasPermission(player, "RandomPackage.monthlycrates", true)) {
			final RPPlayerData pdata = RPPlayerData.get(player.getUniqueId());
			final List<String> owned = pdata.monthlycrates, redeemed = pdata.redeemedMonthlyCrates;
			player.openInventory(Bukkit.createInventory(player, gui.getSize(), gui.getTitle()));
			final Inventory top = player.getOpenInventory().getTopInventory();
			top.setContents(gui.getContents());
			final String playerName = player.getName();
			for(int i = 0; i < top.getSize(); i++) {
				item = top.getItem(i);
				if(item != null) {
					final MonthlyCrate cc = MonthlyCrate.valueOf(i);
					if(cc != null) {
						final String n = cc.path;
						final boolean owns = owned.contains(n), hasPerm = player.hasPermission("RandomPackage.monthlycrates." + n);
						itemMeta = item.getItemMeta(); lore.clear();
						if(!owns && !hasPerm) {
							item = locked.clone();
							check(playerName, item, cc, false);
							top.setItem(i, item);
						} else if(redeemed.contains(n)) {
							item = alreadyClaimed.clone();
							check(playerName, item, cc, false);
							top.setItem(i, item);
						} else {
							check(playerName, item, cc, true);
							top.setItem(i, item);
						}
					}
				} else
					top.setItem(i, background.clone());
			}
			player.updateInventory();
		}
	}
	private void check(String playerName, ItemStack is, MonthlyCrate crate, boolean unlocked) {
	    if(is != null && is.hasItemMeta()) {
	        final ItemStack j = crate.getItemStack();
	        itemMeta = is.getItemMeta();
            if(itemMeta.hasDisplayName()) {
                itemMeta.setDisplayName(itemMeta.getDisplayName().replace("{PATH}", j.getItemMeta().getDisplayName()));
            }
            lore.clear();
            if(itemMeta.hasLore()) {
                for(String s : (unlocked ? j : locked).getItemMeta().getLore()) {
                    if(s.equals("{LORE}")) {
                        for(String p : j.getItemMeta().getLore()) if(!p.contains("{UNLOCKED_BY}")) lore.add(p);
                    } else
                        lore.add(s.replace("{UNLOCKED_BY}", playerName));
                }
            }
            itemMeta.setLore(lore); lore.clear();
            is.setItemMeta(itemMeta);
        }
    }
	public void give(Player player, MonthlyCrate crate, boolean claimed) {
		item = crate.getItemStack(); itemMeta = item.getItemMeta(); lore.clear();
		if(item.hasItemMeta()) {
			if(itemMeta.hasLore()) for(String s : itemMeta.getLore()) lore.add(s.replace("{UNLOCKED_BY}", player.getName()));
			itemMeta.setLore(lore); lore.clear();
			item.setItemMeta(itemMeta);
		}
		giveItem(player, item);
		if(claimed) {
			RPPlayerData.get(player.getUniqueId()).redeemedMonthlyCrates.add(crate.path);
		}
	}
	public void reset(Player sender, OfflinePlayer target) {
		if(target == null || !target.isOnline()) {
			sendStringListMessage(sender, config.getStringList("messages.reset.target-doesnt-exist"));
		} else {
			final RPPlayerData pdata = RPPlayerData.get(target.getUniqueId());
			pdata.redeemedMonthlyCrates.clear();
			for(String s : config.getStringList("messages.reset.success"))
				sender.sendMessage(ChatColor.translateAlternateColorCodes('&', s.replace("{TARGET}", target.getName())));
		}
	}
	@EventHandler
	private void inventoryClickEvent(InventoryClickEvent event) {
		if(!event.isCancelled() && event.getCurrentItem() != null && !event.getCurrentItem().getType().equals(Material.AIR) && event.getWhoClicked().getOpenInventory().getTopInventory().getHolder() == event.getWhoClicked()) {
			final Player player = (Player) event.getWhoClicked();
			final Inventory top = player.getOpenInventory().getTopInventory();
			final int r = event.getRawSlot();
			if(top.getTitle().equals(gui.getTitle())) {
				event.setCancelled(true);
				player.updateInventory();
				final MonthlyCrate mc = MonthlyCrate.valueOf(r);
				if(mc != null) {
					final String n = mc.path;
					final RPPlayerData pdata = RPPlayerData.get(player.getUniqueId());
					final boolean hasPerm = pdata.monthlycrates.contains(n) || player.hasPermission("RandomPackage.monthlycrates." + n);
					if(!hasPerm) {
						sendStringListMessage(player, config.getStringList("messages.no-access"));
					} else if(!pdata.redeemedMonthlyCrates.contains(n)) {
                        give(player, mc, true);
                    }
                    player.closeInventory();
				}
			} else {
				final MonthlyCrate m = MonthlyCrate.valueOf(top.getTitle());
				if(m != null) {
					event.setCancelled(true);
					player.updateInventory();
					if(r >= top.getSize()) return;
					final Object rr = event.getRawSlot();
					final int b = bonusRewardsLeft.keySet().contains(player) ? bonusRewardsLeft.get(player).size() : -1;
					if(regularRewardsLeft.keySet().contains(player) && regularRewardsLeft.get(player).contains(r)) {
						if(!regularRewardsLeft.get(player).isEmpty()) {
							top.setItem(r, m.getRandomReward(player, true, false));
							regularRewardsLeft.get(player).remove(rr);
							if(regularRewardsLeft.get(player).size() == 0 && !bonusRewardsLeft.keySet().contains(player)) {
								bonusRewardsLeft.put(player, new ArrayList<>(m.bonusRewardSlots));
								doAnimation(player, m);
							}
						}
					} else if(bonusRewardsLeft.keySet().contains(player) && regularRewardsLeft.get(player).isEmpty() && bonusRewardsLeft.get(player).contains(r)) {
						if(!bonusRewardsLeft.isEmpty()) {
							top.setItem(event.getRawSlot(), m.getRandomReward(player, true, true));
							bonusRewardsLeft.get(player).remove(rr);
						}
					}
					player.updateInventory();
					if(regularRewardsLeft.keySet().contains(player) && regularRewardsLeft.get(player).isEmpty() && bonusRewardsLeft.keySet().contains(player) && b == 0) player.closeInventory();
				}
			}
		}
	}
	private void doAnimation(Player player, MonthlyCrate m) {
		final Inventory b = m.bonusInv;
		final List<Integer> r = m.rewardSlots, rb = m.bonusRewardSlots;
		final Inventory top = player.getOpenInventory().getTopInventory();
		for(int i = 0; i < top.getSize(); i++)
			if(!r.contains(i) && !rb.contains(i))
				top.setItem(i, b.getItem(i));
		for(int i : rb) {
			item = b.getItem(i).clone(); itemMeta = item.getItemMeta(); lore.clear();
			if(itemMeta.hasLore()) {
				for(String s : itemMeta.getLore()) {
					if(s.contains("{PATH}")) s = s.replace("{PATH}", m.path);
					lore.add(s);
				}
				itemMeta.setLore(lore); lore.clear();
			}
			item.setItemMeta(itemMeta);
			top.setItem(i, item);
		}
		player.updateInventory();
	}
	@EventHandler
	private void playerInteractEvent(PlayerInteractEvent event) {
		final ItemStack i = event.getItem();
		if(i != null && i.hasItemMeta() && i.getItemMeta().hasDisplayName() && i.getItemMeta().hasLore()) {
			final Player player = event.getPlayer();
			final String n = player.getName();
			final MonthlyCrate c = MonthlyCrate.valueOf(n, i);
			if(c != null) {
				event.setCancelled(true);
				player.updateInventory();
				openMonthlyCrate(player, c);
				removeItem(player, i, 1);
			} else if(i.hasItemMeta() && (i.getItemMeta().equals(heroicmysterycrate.getItemMeta()) || i.getItemMeta().equals(mysterycrate.getItemMeta()))) {
				final String p = i.getItemMeta().equals(heroicmysterycrate.getItemMeta()) ? "heroic-mystery-crate" : "mystery-crate", kk = itemsConfig.getString(p + ".can-obtain");
				int r = Integer.parseInt(kk.split(",")[random.nextInt(kk.split(",").length)]);
				item = crates.get(r).clone(); itemMeta = item.getItemMeta(); lore.clear();
				if(itemMeta.hasLore()) {
					for(String string : itemMeta.getLore()) {
						if(string.contains("{UNLOCKED_BY}")) string = string.replace("{UNLOCKED_BY}", n);
						lore.add(string);
					}
					itemMeta.setLore(lore); lore.clear();
					item.setItemMeta(itemMeta);
				}
				event.setCancelled(true);
				removeItem(player, i, 1);
				giveItem(player, item);
				player.updateInventory();
			}
		}
	}
	
	@EventHandler
	private void inventoryCloseEvent(InventoryCloseEvent event) {
		final Inventory i = event.getInventory();
		final Player player = (Player) event.getPlayer();
		if(i.getHolder() == player) {
			final MonthlyCrate mc = MonthlyCrate.valueOf(i.getTitle());
			if(mc != null) {
				exit(player, i, mc);
			}
		}
	}
	private void exit(Player player, Inventory inv, MonthlyCrate m) {
		stopTimers(player);
		final String p = player.getName();
		final ItemStack cmd = GivedpItem.items.get("command-reward").clone();
		final ConsoleCommandSender sender = Bukkit.getConsoleSender();
		if(regularRewardsLeft.keySet().contains(player)) {
			for(int i : m.rewardSlots) {
				final ItemStack is = regularRewardsLeft.get(player).contains(i) ? m.getRandomReward(player, true, false) : inv.getItem(i);
				if(is.hasItemMeta() && is.getItemMeta().getDisplayName().equals(cmd.getItemMeta().getDisplayName()) && is.getItemMeta().hasLore() && is.getItemMeta().getLore().size() == cmd.getItemMeta().getLore().size()) {
					Bukkit.dispatchCommand(sender, is.getItemMeta().getLore().get(0).substring(1).replace("<player>", p));
				} else {
					giveItem(player, is);
				}
			}
		}
		if(bonusRewardsLeft.keySet().contains(player) || !bonusRewardsLeft.keySet().contains(player) && regularRewardsLeft.keySet().contains(player)) {
			for(int i : m.bonusRewardSlots) {
				final ItemStack is = !bonusRewardsLeft.keySet().contains(player) || bonusRewardsLeft.get(player).contains(i) ? m.getRandomReward(player, true, true) : inv.getItem(i);
				if(is.hasItemMeta() && is.getItemMeta().getDisplayName().equals(cmd.getItemMeta().getDisplayName()) && is.getItemMeta().hasLore() && is.getItemMeta().getLore().size() == cmd.getItemMeta().getLore().size()) {
					Bukkit.dispatchCommand(sender, is.getItemMeta().getLore().get(0).substring(1).replace("<player>", p));
				} else {
					giveItem(player, is);
				}
			}
		}
		MonthlyCrate.revealing.remove(player.getUniqueId());
		regularRewardsLeft.remove(player);
		bonusRewardsLeft.remove(player);
	}
	@EventHandler
	private void playerQuitEvent(PlayerQuitEvent event) {
		final Player player = event.getPlayer();
		final Inventory top = player.getOpenInventory().getTopInventory();
		final MonthlyCrate m = MonthlyCrate.valueOf(top.getTitle());
		if(m != null)
			exit(player, top, m);
	}
	private void stopTimers(Player player) {
		if(playertimers.keySet().contains(player)) {
			for(int i : playertimers.get(player)) scheduler.cancelTask(i);
			playertimers.remove(player);
		}
	}
	
	public void openMonthlyCrate(Player player, MonthlyCrate crate) {
		final String p = crate.path;
		final Inventory inv = crate.regularInv;
		final List<Integer> rewardSlots = crate.rewardSlots, bonusRewardSlots = crate.bonusRewardSlots;
		player.openInventory(Bukkit.createInventory(player, inv.getSize(), inv.getTitle()));
		final Inventory top = player.getOpenInventory().getTopInventory();
		top.setContents(inv.getContents());
		regularRewardsLeft.put(player, new ArrayList<>(rewardSlots));
		for(int i = 0; i < top.getSize(); i++) {
			if(rewardSlots.contains(i) || bonusRewardSlots.contains(i)) {
				item = top.getItem(i); itemMeta = item.getItemMeta(); lore.clear();
				if(item.hasItemMeta()) {
					if(itemMeta.hasDisplayName()) itemMeta.setDisplayName(itemMeta.getDisplayName().replace("{PATH}", p));
					if(itemMeta.hasLore())
						for(String s : itemMeta.getLore())
							lore.add(s.replace("{PATH}", p));
				}
				itemMeta.setLore(lore); lore.clear();
				item.setItemMeta(itemMeta);
			}
		}
		player.updateInventory();
	}
}
