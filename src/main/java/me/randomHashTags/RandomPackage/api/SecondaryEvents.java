package me.randomHashTags.RandomPackage.api;

import me.randomHashTags.RandomPackage.RPPlayerData;
import me.randomHashTags.RandomPackage.RandomPackageAPI;
import me.randomHashTags.RandomPackage.utils.supported.VaultAPI;
import me.randomHashTags.RandomPackage.utils.universal.UMaterial;
import net.milkbowl.vault.economy.Economy;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.OfflinePlayer;
import org.bukkit.block.Block;
import org.bukkit.block.Chest;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;

public class SecondaryEvents extends RandomPackageAPI implements CommandExecutor, Listener {

    public boolean isEnabled = false;
    private static SecondaryEvents instance;
    public static final SecondaryEvents getSecondaryEvents() {
        if(instance == null) instance = new SecondaryEvents();
        return instance;
    }

    private ArrayList<String> combineitems = new ArrayList<>();
    private final String confirm = ChatColor.translateAlternateColorCodes('&', randompackage.getConfig().getString("confirm.title"));

    public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {
        final Player player = sender instanceof Player ? (Player) sender : null;
        final String n = cmd.getName();
        if(n.equals("balance")) {
            String q, qq = "", bal = player != null ? formatDouble(eco.getBalance(player)) : "0.00";
            if(player != null && args.length == 0 && hasPermission(sender, "RandomPackage.balance", true)) {
                q = "self";
            } else if(args.length >= 1 && hasPermission(sender, "RandomPackage.balance-other", true) && Bukkit.getOfflinePlayer(args[0]) != null) {
                final OfflinePlayer op = Bukkit.getOfflinePlayer(args[0]);
                if(player != null && op.equals(player)) {
                    q = "self";
                } else {
                    q = "other";
                    qq = eco.getBalance(player) >= eco.getBalance(op) ? "other" : "self";
                    bal = formatDouble(eco.getBalance(op));
                }
            } else return true;
            for(String s : randompackage.getConfig().getStringList("balance.view-" + q)) {
                if(s.contains("{INT}")) s = s.replace("{INT}", bal.contains(".") ? bal.split("\\.")[0] : bal);
                if(s.contains("{DECIMALS}")) s = s.replace("{DECIMALS}", bal.contains(".") ? "." + (bal.split("\\.")[1].length() > 2 ? bal.split("\\.")[1].substring(0, 2) : bal.split("\\.")[1]) : "");
                if(s.equals("{RICHER}") && player != null) s = randompackage.getConfig().getString("balance.richer-than-" + qq);
                if(s.contains("{TARGET}")) s = s.replace("{TARGET}", Bukkit.getOfflinePlayer(args[0]).getName());
                if(!s.equals("{RICHER}")) sender.sendMessage(ChatColor.translateAlternateColorCodes('&', s));
            }
        } else if(player != null) {
            if(n.equals("bump") && hasPermission(sender, "RandomPackage.bump", true)) {
                player.damage(1.0);
            } else if(n.equals("combine") && hasPermission(sender, "RandomPackage.combine", true)) {
                combine(player);
            } else if(n.equals("confirm") && hasPermission(sender, "RandomPackage.confirm", true)) {
                final RPPlayerData pdata = RPPlayerData.get(args.length == 0 ? player.getUniqueId() : Bukkit.getOfflinePlayer(args[0]).getUniqueId());
                if(pdata == null) {

                } else {
                    if(pdata.unclaimedPurchases.isEmpty()) {
                        sendStringListMessage(player, randompackage.getConfig().getStringList("confirm." + (pdata.getOfflinePlayer().equals(player) ? "self-" : "other") + "no-unclaimed-items"));
                    } else {
                        confirm(player, pdata);
                    }
                }
            } else if(n.equals("roll") && hasPermission(sender, "RandomPackage.roll", true)) {
                roll(player, args.toString());
            }
        }
        return true;
    }

    public void enable() {
        if(isEnabled) return;
        pluginmanager.registerEvents(this, randompackage);
        isEnabled = true;
        for(String string : randompackage.getConfig().getStringList("combine.combine-items")) combineitems.add(string.toUpperCase());
    }
    public void disable() {
        if(!isEnabled) return;
        isEnabled = false;
        combineitems.clear();
        HandlerList.unregisterAll(this);
    }


    @EventHandler
    private void inventoryClickEvent(InventoryClickEvent event) {
        if(!event.isCancelled() && event.getCurrentItem() != null && !event.getCurrentItem().getType().equals(Material.AIR)) {
            final Player player = (Player) event.getWhoClicked();
            final Inventory top = player.getOpenInventory().getTopInventory();
            final String t = top.getTitle();
            final int r = event.getRawSlot();
            final ItemStack c = event.getCurrentItem();
            if(t.equals(confirm)) {
                event.setCancelled(true);
                player.updateInventory();
                if(r >= top.getSize()) return;
                giveItem(player, c);
                RPPlayerData.get(player.getUniqueId()).removeUnclaimedPurchase(c);
                top.setItem(r, new ItemStack(Material.AIR));
            } else return;
            event.setCancelled(true);
            player.updateInventory();
        }
    }


    public void combine(Player player) {
        final Block tblock = player.getTargetBlock(null, 5);
        Chest chest = null;
        Inventory inventory = player.getInventory();
        if(tblock.getType().equals(Material.CHEST) || tblock.getType().equals(Material.TRAPPED_CHEST)) {
            chest = (Chest) tblock.getState();
            inventory = chest.getBlockInventory();
        }
        for(String string : randompackage.getConfig().getStringList("combine.success")) {
            if(string.equals("{SUCCESS}")) {
                for(int i = 0; i < combineitems.size(); i++) {
                    int amount = 0, amountb = 0;
                    Material material = Material.valueOf(combineitems.get(i).toUpperCase()), block = !material.name().replace("INGOT", "BLOCK").endsWith("BLOCK") ? Material.valueOf(material.name() + "_BLOCK") : Material.valueOf(material.name().replace("INGOT", "BLOCK"));
                    amount = (getTotalAmount(inventory, material, (byte) 0) / 9) * 9;
                    if(amount != 0) {
                        amountb = amount / 9;
                        player.sendMessage(ChatColor.translateAlternateColorCodes('&', randompackage.getConfig().getString("combine.format")).replace("{AMOUNT_ITEM}", "" + amount).replace("{ITEM_ORE}", material.name()).replace("{AMOUNT_BLOCK}", "" + amountb).replace("{ITEM_BLOCK}", material.name().replace("ORE", "BLOCK")));
                        for(int z = 1; z <= amount; z++) inventory.removeItem(new ItemStack(material, 1, (byte) 0));
                        inventory.addItem(new ItemStack(block, amountb));
                        if(chest != null) chest.update(); else player.updateInventory();
                    }
                }
            } else {
                player.sendMessage(ChatColor.translateAlternateColorCodes('&', string));
            }
        }
    }
    private void confirm(Player opener, RPPlayerData target) {
        opener.openInventory(Bukkit.createInventory(opener, 54, confirm));
        for(ItemStack is : target.getUnclaimedPurchases()) opener.getOpenInventory().getTopInventory().setItem(opener.getOpenInventory().getTopInventory().firstEmpty(), is);
        opener.updateInventory();
    }
    public void roll(Player player, String arg0) {
        boolean did = false, heard = false;
        int radius = randompackage.getConfig().getInt("roll.block-radius"), maxroll = getRemainingInt(arg0), roll = random.nextInt(maxroll);
        for(Entity entity : player.getNearbyEntities(radius, radius, radius)) if(!heard && entity instanceof Player) heard = true;
        if(heard) {
            for(Entity entity : player.getNearbyEntities(radius, radius, radius)) {
                if(entity instanceof Player) {
                    for(String string : randompackage.getConfig().getStringList("roll.message")) {
                        if(string.contains("{MAX_ROLL}")) string = string.replace("{MAX_ROLL}", formatInt(maxroll));
                        if(string.contains("{PLAYER}")) string = string.replace("{PLAYER}", player.getName());
                        if(string.contains("{ROLLED}")) string = string.replace("{ROLLED}", formatInt(roll));
                        entity.sendMessage(ChatColor.translateAlternateColorCodes('&', string));
                        if(!did) { did = true; player.sendMessage(ChatColor.translateAlternateColorCodes('&', string)); }
                    }
                }
            }
        } else sendStringListMessage(player, randompackage.getConfig().getStringList("roll.nobody-heard-roll"));
    }
}
