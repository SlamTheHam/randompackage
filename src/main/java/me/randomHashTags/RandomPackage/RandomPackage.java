package me.randomHashTags.RandomPackage;

import java.io.File;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import me.randomHashTags.RandomPackage.api.*;
import me.randomHashTags.RandomPackage.api.nearFinished.*;
import me.randomHashTags.RandomPackage.api.unfinished.*;
import me.randomHashTags.RandomPackage.api.ItemFilter;
import me.randomHashTags.RandomPackage.utils.*;
import me.randomHashTags.RandomPackage.utils.classes.CustomExplosion;
import me.randomHashTags.RandomPackage.utils.placeholders.Placeholders;
import me.randomHashTags.RandomPackage.utils.supported.FactionsAPI;
import me.randomHashTags.RandomPackage.utils.supported.factions.factionsUUID;
import me.randomHashTags.RandomPackage.utils.universal.UVersion;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.command.*;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.event.inventory.InventoryType.SlotType;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerItemBreakEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import me.randomHashTags.RandomPackage.api.events.player.PlayerArmorEvent;
import me.randomHashTags.RandomPackage.api.events.player.PlayerArmorEvent.ArmorEventReason;
import me.randomHashTags.RandomPackage.api.CustomBosses;
import me.randomHashTags.RandomPackage.api.Envoy;
import me.randomHashTags.RandomPackage.api.GlobalChallenges;
import me.randomHashTags.RandomPackage.utils.supported.MCMMOAPI;
import me.randomHashTags.RandomPackage.utils.supported.VaultAPI;

public class RandomPackage extends JavaPlugin implements Listener, CommandExecutor {
	
	private final String v = Bukkit.getVersion();
	public static RandomPackage getPlugin;
	private final PluginManager pm = Bukkit.getPluginManager();
	
	public static Plugin factions, spawner, mcmmo;
	public static String factionPlugin;

	public FileConfiguration config;

    private SimpleCommandMap commandMap;
    private HashMap<String, Command> knownCommands;
    private HashMap<String, PluginCommand> Y = new HashMap<>(), originalCommands = new HashMap<>();

    private AuctionHouse auctionhouse;
    private ChatEvents chatevents;
    private CoinFlip coinflip;
    private CollectionFilter collectionfilter;
    private Conquest conquest;
    private CustomArmor customarmor;
    private CustomBosses custombosses;
    private CustomEnchants customenchants;
    private CustomExplosions customexplosions;
    private Duels duels;
    private Dungeons dungeons;
    private Envoy envoy;
    private FactionAdditions factionadditions;
    private Fund fund;
    private GlobalChallenges globalchallenges;
    private Homes homes;
    private ItemFilter itemfilter;
    private Jackpot jackpot;
    private Kits kits;
    private KOTH koth;
    private Lootboxes lootboxes;
    private Masks masks;
    private MobStacker mobstacker;
    private MonthlyCrates monthlycrates;
    private ServerCrates servercrates;
    private Shop shop;
    private Showcase showcase;
    private Titles titles;
    private Trade trade;
    private XPEvents xpevents;

    private SecondaryEvents secondaryevents;
    private GivedpItem givedpitem;
    private MCMMOAPI mcmmoAPI;
    private RandomPackageAPI api;
    private RPEvents rpevents;

	public void onEnable() {
		getPlugin = this;
		final Metrics metrics = new Metrics(this);
		metrics.addCustomChart(new Metrics.SimplePie("server_version", () -> Bukkit.getVersion()));
		metrics.addCustomChart(new Metrics.SimplePie("purchaser", () -> "%%__USER__%%"));
		Bukkit.getConsoleSender().sendMessage(ChatColor.translateAlternateColorCodes('&', "&6[RandomPackage] &eUser: &f%%__USER__%%"));

		try {
			commandMap = (SimpleCommandMap) getPrivateField(getServer().getPluginManager(), "commandMap");
			knownCommands = (HashMap<String, Command>) getPrivateField(commandMap, "knownCommands");
			Y = new HashMap<>();
		} catch (Exception e) {
			e.printStackTrace();
		}

		for(String s : getDescription().getCommands().keySet()) originalCommands.put(s, getCommand(s));
		for(int i = 0; i < knownCommands.values().size(); i++) {
			final PluginCommand pc = knownCommands.values().toArray()[i] instanceof PluginCommand ? (PluginCommand) knownCommands.values().toArray()[i] : null;
			if(pc != null) Y.put(pc.getPlugin().getName() + ":" + knownCommands.keySet().toArray()[i], pc);
		}

		enable();
	}
    public void enable() {
		checkFiles();
		config = YamlConfiguration.loadConfiguration(new File(getDataFolder(), "config.yml"));
		UVersion.getInstance();

		final VaultAPI vapi = VaultAPI.getVaultAPI();
		vapi.setupEconomy();
		vapi.setupChat();
		vapi.setupPermissions();

		checkForUpdate();

		api = RandomPackageAPI.getAPI();
		api.load();

		tryLoadingSoftDepends();

		auctionhouse = AuctionHouse.getAuctionHouse();
		tryLoading(Feature.AUCTION_HOUSE);

		rpevents = RPEvents.getRPEvents();
		rpevents.enable();

		givedpitem = GivedpItem.getGivedpItem();
		givedpitem.enable();
		getCommand("givedp").setExecutor(givedpitem);
		getCommand("givedp").setTabCompleter(givedpitem);

		pm.registerEvents(this, this);

		chatevents = ChatEvents.getChatEventsAPI();
		tryLoading(Feature.CHAT_BRAG);

		coinflip = CoinFlip.getCoinFlip();
		tryLoading(Feature.COINFLIP);

		collectionfilter = CollectionFilter.getCollectionFilter();
		tryLoading(Feature.COLLECTION_FILTER);

		conquest = Conquest.getConquest();
		tryLoading(Feature.CONQUEST);

		itemfilter = ItemFilter.getItemFilter();
		tryLoading(Feature.ITEM_FILTER);

		customarmor = CustomArmor.getCustomArmor();
		tryLoading(Feature.CUSTOM_ARMOR);

		custombosses = CustomBosses.getCustomBosses();
		tryLoading(Feature.CUSTOM_BOSSES);

		customenchants = CustomEnchants.getCustomEnchants();
		tryLoading(Feature.CUSTOM_ENCHANTS);

		customexplosions = CustomExplosions.getCustomExplosions();
		tryLoading(Feature.CUSTOM_EXPLOSIONS);

		duels = Duels.getDuels();
		tryLoading(Feature.DUELS);

		dungeons = Dungeons.getDungeons();
		tryLoading(Feature.DUNGEONS);

		envoy = Envoy.getEnvoy();
		tryLoading(Feature.ENVOY);

		factionadditions = FactionAdditions.getFactionAdditions();
		tryLoading(Feature.FACTION_ADDITIONS);

		fund = Fund.getFund();
		tryLoading(Feature.FUND);

		globalchallenges = GlobalChallenges.getChallenges();
		tryLoading(Feature.GLOBAL_CHALLENGES);

		homes = Homes.getHomes();
		tryLoading(Feature.HOMES);

		titles = Titles.getTitles();
		tryLoading(Feature.TITLES);

		jackpot = Jackpot.getJackpot();
		tryLoading(Feature.JACKPOT);

		kits = Kits.getKits();
		tryLoading(Feature.KITS_EVOLUTION);
		tryLoading(Feature.KITS_GLOBAL);
		tryLoading(Feature.KITS_MASTERY);

		koth = KOTH.getKOTH();
		tryLoading(Feature.KOTH);

		masks = Masks.getMasks();
		tryLoading(Feature.MASKS);

		mobstacker = MobStacker.getMobStacker();
		tryLoading(Feature.MOB_STACKER);

		monthlycrates = MonthlyCrates.getMonthlyCrates();
		tryLoading(Feature.MONTHLY_CRATES);

		servercrates = ServerCrates.getServerCrates();
		tryLoading(Feature.SERVER_CRATES);

		lootboxes = Lootboxes.getLootboxes();
		tryLoading(Feature.LOOTBOXES);

		shop = Shop.getShop();
		tryLoading(Feature.SHOP);

		showcase = Showcase.getShowcase();
		tryLoading(Feature.SHOWCASE);

		trade = Trade.getTrade();
		tryLoading(Feature.TRADE);

		enable("outpost", "Features", "outposts.yml", Outposts.getOutposts(), true);
		enable("randompackage", null, null, api, true);

		enable("withdraw", null, null, api, true);

		xpevents = XPEvents.getXPEvents();
		tryLoading(Feature.XP_EVENTS);

		secondaryevents = SecondaryEvents.getSecondaryEvents();
		tryLoading(Feature.SECONDARY_EVENTS);

		unRegisterPluginCommands(unregisterCommands);
		if(pm.isPluginEnabled("PlaceholderAPI")) {
			new Placeholders(this).hook();
		}
	}
	private void tryLoadingSoftDepends() {
		if(pm.isPluginEnabled("Factions")) {
			factions = pm.getPlugin("Factions");
			factionPlugin = pm.getPlugin("Factions").getDescription().getAuthors().contains("ProSavage") ? "SavageFactions" : "FactionsUUID";
			pm.registerEvents(factionsUUID.getInstance(), this);
			pm.registerEvents(FactionsAPI.getFactionsAPI(), this);
		} else factionPlugin = null;

		if(pm.isPluginEnabled("mcMMO")) {
			mcmmo = pm.getPlugin("mcMMO");
			mcmmoAPI = MCMMOAPI.getMCMMOAPI();
			mcmmoAPI.enable();
		}

		final Plugin plugin;
		if(pm.isPluginEnabled("EpicSpawners")) {
		    plugin = pm.getPlugin("EpicSpawners");
			Bukkit.getConsoleSender().sendMessage(ChatColor.translateAlternateColorCodes('&', "&6[RandomPackage]&r &aEpicSpawners integration successful"));
			CustomExplosion.spawnerchance = Integer.parseInt(plugin.getConfig().getString("Spawner Drops.Chance On TNT Explosion").replace("%", ""));
			spawner = plugin;
		} else {
			CustomExplosion.spawnerchance = 100;
			Bukkit.getConsoleSender().sendMessage(ChatColor.translateAlternateColorCodes('&', "&6[RandomPackage]&r &cEpicSpawners integration unsuccessful"));
		}
	}
	public void onDisable() {
		disable(false);
	}
	public void disable(boolean checkforupgrade) {
		unregisterCommands.clear();
		RandomPackageAPI.givedpCategories.clear();
		RandomPackageAPI.givedp.clear();
		if(factionPlugin != null) {
			HandlerList.unregisterAll(factionsUUID.getInstance());
			HandlerList.unregisterAll(FactionsAPI.getFactionsAPI());
		}
		if(mcmmo != null) mcmmoAPI.disable();
        rpevents.disable(checkforupgrade);
        givedpitem.disable();
        auctionhouse.disable();
        secondaryevents.disable();
        coinflip.disable();
        collectionfilter.disable();
        conquest.disable();
        customarmor.disable();
        custombosses.disable();
        customenchants.disable();
        customexplosions.disable();
        duels.disable();
        dungeons.disable();
        envoy.disable();
        factionadditions.disable();
        fund.disable();
        globalchallenges.disable();
        homes.disable();
        itemfilter.disable();
        jackpot.disable();
        kits.disableGkits();
        kits.disableMkits();
        kits.disableVkits();
        koth.disable();
        lootboxes.disable();
        masks.disable();
        mobstacker.disable();
        monthlycrates.disable();
        servercrates.disable();
        shop.disable();
        showcase.disable();
        titles.disable();
        trade.disable();
        xpevents.disable();
        HandlerList.unregisterAll((Listener) this);
    }

    private void checkFiles() {
		saveDefaultConfig();
		save("_Data", "_player data.yml");
		save("_Data", "other.yml");
		save("", "items.yml");
		save("", "sounds.yml");
		save("", "particles.yml");
	}

	public void checkForUpdate() {
		Bukkit.getScheduler().runTaskAsynchronously(this, () -> Updater.getUpdater().checkForUpdate());
	}

	private enum Feature {
		AUCTION_HOUSE, CHAT_BRAG, COINFLIP, COLLECTION_FILTER, CONQUEST, CUSTOM_ARMOR, CUSTOM_BOSSES, CUSTOM_ENCHANTS, CUSTOM_EXPLOSIONS, DUELS, DUNGEONS, ENVOY, FACTION_ADDITIONS, FUND, GLOBAL_CHALLENGES, HOMES, ITEM_FILTER,
		JACKPOT, KITS_EVOLUTION, KITS_GLOBAL, KITS_MASTERY, KOTH, LOOTBOXES, MOB_STACKER, MASKS, MONTHLY_CRATES, SECONDARY_EVENTS, SERVER_CRATES, SHOP, SHOWCASE, TITLES, TRADE, XP_EVENTS,
	}
	public void tryLoading(Feature f) {
		try {
			tryloading(f);
		} catch (Exception e) {
			Bukkit.getConsoleSender().sendMessage(ChatColor.translateAlternateColorCodes('&', "&6[RandomPackage &cERROR&6] &c&lError trying to load feature:&r &f" + f.name()));
		}
	}
	private void tryloading(Feature f) throws Exception {
		boolean enabled = false;
		final HashMap<String, String> ali = new HashMap<>();
		final List<String> cmds = new ArrayList<>();
		CommandExecutor ce = null;
		if(f.equals(Feature.AUCTION_HOUSE)) {
			enabled = config.getBoolean("auction house.enabled");
			ce = auctionhouse;
			cmds.add("auctionhouse");
			ali.put("auctionhouse", "auction house");
			if(enabled) {
				auctionhouse.enable();
			}
		} else if(f.equals(Feature.CHAT_BRAG)) {
			enabled = config.getBoolean("chat cmds.brag.enabled");
			ce = chatevents;
			cmds.add("brag");
			if(enabled) {
				chatevents.enable();
			}
		} else if(f.equals(Feature.COINFLIP)) {
			enabled = config.getBoolean("coinflip.enabled");
			ce = coinflip;
			cmds.add("coinflip");
			if(enabled) {
				coinflip.enable();
			}
		} else if(f.equals(Feature.COLLECTION_FILTER)) {
			enabled = config.getBoolean("collection filter.enabled");
			ce = collectionfilter;
			cmds.add("collectionfilter");
			if(enabled) {
				collectionfilter.enable();
			}
		} else if(f.equals(Feature.CONQUEST)) {
			enabled = config.getBoolean("conquest.enabled");
			ce = conquest;
			cmds.add("conquest");
			if(enabled) {
				conquest.enable();
			}
		} else if(f.equals(Feature.CUSTOM_ARMOR)) {
			enabled = config.getBoolean("custom armor.enabled");
			if(enabled) {
				customarmor.enable();
			}
		} else if(f.equals(Feature.CUSTOM_BOSSES)) {
			enabled = config.getBoolean("custom bosses.enabled");
			if(enabled) {
				custombosses.enable();
			}
		} else if(f.equals(Feature.CUSTOM_ENCHANTS)) {
			enabled = config.getBoolean("custom enchants.enabled");
			ce = customenchants;
			cmds.add("alchemist");
			cmds.add("disabledenchants");
			cmds.add("enchanter");
			cmds.add("enchants");
			cmds.add("splitsouls");
			cmds.add("tinkerer");
			if(enabled) {
				customenchants.enable();
			}
		} else if(f.equals(Feature.CUSTOM_EXPLOSIONS)) {
			enabled = config.getBoolean("custom tnt.enabled");
			if (enabled) {
				customexplosions.enable();
			}
		} else if(f.equals(Feature.DUELS)) {
			enabled = config.getBoolean("duel.enabled");
			ce = duels;
			cmds.add("duel");
			if(enabled) {
				duels.enable();
			}
		} else if(f.equals(Feature.DUNGEONS)) {
			enabled = config.getBoolean("dungeon.enabled");
			ce = dungeons;
			cmds.add("dungeon");
			if(enabled) {
				dungeons.enable();
			}
		} else if(f.equals(Feature.ENVOY)) {
			enabled = config.getBoolean("envoy.enabled");
			ce = envoy;
			cmds.add("envoy");
			if(enabled) {
				envoy.enable();
			}
		} else if(f.equals(Feature.FUND)) {
			enabled = config.getBoolean("fund.enabled");
			ce = fund;
			cmds.add("fund");
			if(enabled) {
				fund.enable();
			}
		} else if(f.equals(Feature.GLOBAL_CHALLENGES)) {
			enabled = config.getBoolean("global challenges.enabled");
			ce = globalchallenges;
			cmds.add("challenge");
			ali.put("challenge", "global challenges");
			if(enabled) {
				globalchallenges.enable();
			}
		} else if(f.equals(Feature.HOMES)) {
			enabled = config.getBoolean("home.enabled");
			ce = homes;
			cmds.add("home");
			cmds.add("sethome");
			if(enabled) {
				homes.enable();
			}
		} else if(f.equals(Feature.ITEM_FILTER)) {
			enabled = config.getBoolean("filter.enabled");
			ce = itemfilter;
			cmds.add("filter");
			if(enabled) {
				itemfilter.enable();
			}
		} else if(f.equals(Feature.JACKPOT)) {
			enabled = config.getBoolean("jackpot.enabled");
			ce = jackpot;
			cmds.add("jackpot");
			if(enabled) {
				jackpot.enable();
			}
		} else if(f.equals(Feature.KITS_EVOLUTION)) {
			enabled = config.getBoolean("vkit.enabled");
			ce = kits;
			cmds.add("vkit");
			if(enabled) {
				for(String pc : cmds) {
					getCommand(pc).setTabCompleter(kits);
				}
				kits.enableVkits();
			}
		} else if(f.equals(Feature.KITS_GLOBAL)) {
			enabled = config.getBoolean("gkit.enabled");
			ce = kits;
			cmds.add("gkit");
			if(enabled) {
				for(String pc : cmds) {
					getCommand(pc).setTabCompleter(kits);
				}
				kits.enableGkits();
			}
		} else if(f.equals(Feature.KITS_MASTERY)) {
			enabled = config.getBoolean("mkit.enabled");
			ce = kits;
			cmds.add("mkit");
			if(enabled) {
				for(String pc : cmds) {
					getCommand(pc).setTabCompleter(kits);
				}
				kits.enableMkits();
			}
		} else if(f.equals(Feature.KOTH)) {
			enabled = config.getBoolean("kingofthehill.enabled");
			ce = koth;
			cmds.add("kingofthehill");
			if(enabled) {
				koth.enable();
			}
		} else if(f.equals(Feature.LOOTBOXES)) {
			enabled = config.getBoolean("lootboxes.enabled");
			ce = lootboxes;
			cmds.add("lootbox");
			if(enabled) {
				lootboxes.enable();
			}
		} else if(f.equals(Feature.MASKS)) {
			enabled = config.getBoolean("masks.enabled");
			if(enabled) {
				masks.enable();
			}
		} else if(f.equals(Feature.MOB_STACKER)) {
			enabled = config.getBoolean("mob stacker.enabled");
			if(enabled) {
				mobstacker.enable();
			}
		} else if(f.equals(Feature.MONTHLY_CRATES)) {
			enabled = config.getBoolean("monthly crates.enabled");
			ce = monthlycrates;
			cmds.add("monthlycrate");
			ali.put("monthlycrate", "monthly crates");
			if(enabled) {
				monthlycrates.enable();
			}
		} else if(f.equals(Feature.SECONDARY_EVENTS)) {
			enabled = config.getBoolean("balance.enabled") && config.getBoolean("bump.enabled") && config.getBoolean("combine.enabled") && config.getBoolean("confirm.enabled") && config.getBoolean("roll.enabled");
			ce = secondaryevents;
			cmds.add("balance");
			cmds.add("bump");
			cmds.add("combine");
			cmds.add("confirm");
			cmds.add("roll");
			if(enabled) {
				secondaryevents.enable();
			}
		} else if(f.equals(Feature.SERVER_CRATES)) {
			enabled = config.getBoolean("server crates.enabled");
			if(enabled) {
				servercrates.enable();
			}
		} else if(f.equals(Feature.SHOP)) {
			enabled = config.getBoolean("shop.enabled");
			ce = shop;
			cmds.add("shop");
			if(enabled) {
				shop.enable();
			}
		} else if(f.equals(Feature.SHOWCASE)) {
			enabled = config.getBoolean("showcase.enabled");
			ce = showcase;
			cmds.add("showcase");
			if(enabled) {
				showcase.enable();
			}
		} else if(f.equals(Feature.TITLES)) {
			enabled = config.getBoolean("title.enabled");
			ce = titles;
			cmds.add("title");
			if(enabled) {
				titles.enable();
			}
		} else if(f.equals(Feature.TRADE)) {
			enabled = config.getBoolean("trade.enabled");
			ce = trade;
			cmds.add("trade");
			if(enabled) {
				trade.enable();
			}
		} else if(f.equals(Feature.XP_EVENTS)) {
			enabled = config.getBoolean("xpbottle.enabled");
			ce = xpevents;
			cmds.add("xpbottle");
			if(enabled) {
				xpevents.enable();
			}
		}
		final boolean empty = cmds.isEmpty();
		if(enabled && !empty) {
			try {
				int v = 0;
				for(String cmd : cmds) {
					PluginCommand pc = getCommand(cmd);
					if(ce != null) {
						if(pc == null) {
							final String s = cmds.get(v);
							knownCommands.put(s, originalCommands.get(s));
							pc = (PluginCommand) knownCommands.get(s);
						}
						pc.setExecutor(ce);
					}
					final String k = pc.getName();
					if(ali.isEmpty()) addAliases(k);
					else addAliases(k, ali.get(k));
					v += 1;
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else if(!empty) {
			for(String cmd : cmds) {
				final PluginCommand pc = getCommand(cmd);
				if(pc != null) unregisterPluginCommand(pc);
			}
		}
	}
	public void reload() {
		disable(false);
		enable();
		config = YamlConfiguration.loadConfiguration(new File(getDataFolder() + File.separator, "config.yml"));
	}

	private ArrayList<Object> registeredevents = new ArrayList<>();
	private void enable(String string, String folder, String filename, Object instance, boolean hasCMD) {
		if(!config.getBoolean(string + ".enabled") && !string.equals("givedp") && !string.equals("randompackage")
				|| string.endsWith("disguise")) {
			if(hasCMD) {
				unregisterCommands.add(getCommand(string.replace("-", "")));
			}
		} else if(string.equals("givedp") || string.equals("randompackage")
				|| config.getBoolean(string + ".enabled")
				) {
			if(folder != null) {
				if(filename.contains("&&")) {
					for(String s : filename.split("&&")) save(folder, s);
				} else {
					save(folder, filename);
				}
			}
			if(hasCMD) {
				final String o = string.startsWith("chat cmds.") ? string.replace("chat cmds.", "") : string;
				getCommand(o.replace("-", "")).setExecutor((CommandExecutor) instance);
				if(config.get(o + ".aliases") != null) {
					try {
						addAliases(o);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}
			if(instance != null && !registeredevents.contains(instance)) {
				registeredevents.add(instance);
				pm.registerEvents((Listener) instance, this);
			}
		}
	}
	private void save(String folder, String file) {
		File f = null;
		if(folder != null && !folder.equals(""))
			f = new File(getDataFolder() + File.separator + folder + File.separator, file);
		else
			f = new File(getDataFolder() + File.separator, file);
		if(!f.exists()) {
			f.getParentFile().mkdirs();
			saveResource(folder != null && !folder.equals("") ? folder + File.separator + file : file, false);
		}
	}
	
	private void addAliases(String cmd) throws Exception { addAliases(cmd, cmd); }
	private void addAliases(String cmd, String path) throws Exception {
		Object result = getPrivateField(getServer().getPluginManager(), "commandMap");
		SimpleCommandMap commandMap = (SimpleCommandMap) result;
		Object map = getPrivateField(commandMap, "knownCommands");
		@SuppressWarnings("unchecked")
		HashMap<String, Command> knownCommands = (HashMap<String, Command>) map;
		for(String alias : config.getStringList(path + ".aliases")) {
			if(!alias.equals(cmd)) {
				final String c = cmd;
				knownCommands.put(alias, getCommand(c));
				final PluginCommand pc = Bukkit.getPluginCommand(c);
				if(pc != null) pc.getAliases().add(alias);
			}
		}
	}
	/*
	 * Code from "zeeveener" at https://bukkit.org/threads/how-to-unregister-commands-from-your-plugin.131808/ , edited by RandomHashTags
	 */
	private Object getPrivateField(Object object, String field) throws Exception {
		Class<?> clazz = object.getClass();
		Field objectField = field.equals("commandMap") ? clazz.getDeclaredField(field) : field.equals("knownCommands") ? v.contains("1.8") || v.contains("1.9") || v.contains("1.10") || v.contains("1.11") || v.contains("1.12") || v.equals("1.13") ? clazz.getDeclaredField(field) : clazz.getSuperclass().getDeclaredField(field) : null;
		if(objectField == null) {
			Bukkit.broadcastMessage("objectField == null!");
			return null;
		}
		objectField.setAccessible(true);
		Object result = objectField.get(object);
		objectField.setAccessible(false);
		return result;
	}
	private List<PluginCommand> unregisterCommands = new ArrayList<>();
	private void unRegisterPluginCommands(List<PluginCommand> cmds) {
        for(PluginCommand cmd : cmds) {
        	if(cmd != null) unregisterPluginCommand(cmd);
        }
	}
	private void unregisterPluginCommand(PluginCommand cmd) {
		final String c = cmd.getName();
        knownCommands.remove("randompackage:" + c);
        cmd.unregister(commandMap);
        Y.remove("RandomPackage:" + c);
        boolean hasOtherCmd = false;
        for(int i = 0; i < Y.keySet().size(); i++) {
            final String otherCmd = (String) Y.keySet().toArray()[i];
            if(!otherCmd.startsWith("RandomPackage:") && otherCmd.split(":")[1].equals(c)) { // gives the last plugin that has the cmd.getName() the command priority
                hasOtherCmd = true;
                knownCommands.replace(c, cmd, (PluginCommand) Y.values().toArray()[i]);
            }
        }
        if(!hasOtherCmd) // removes the command completely
            knownCommands.remove(c);
    }
	/*
	 * 
	 * PlayerArmorEvent Listener
	 * 
	 */
	@EventHandler
	private void inventoryClickEvent(InventoryClickEvent event) {
		if(!event.isCancelled() && !event.getClick().equals(ClickType.DOUBLE_CLICK) && event.getCurrentItem() != null && event.getCursor() != null && event.getInventory().getType().equals(InventoryType.CRAFTING)) {
			final Player player = (Player) event.getWhoClicked();
			final SlotType st = event.getSlotType();
			final ClickType ct = event.getClick();
			final ItemStack cursoritem = event.getCursor(), currentitem = event.getCurrentItem();
			final PlayerInventory inv = player.getInventory();
			final String cursor = cursoritem.getType().name(), current = currentitem.getType().name();
			if((st.equals(SlotType.QUICKBAR) || st.equals(SlotType.CONTAINER)) && ct.equals(ClickType.CONTROL_DROP)) return;
			PlayerArmorEvent a = null, b = null;
			if(st.equals(SlotType.ARMOR) && ct.equals(ClickType.NUMBER_KEY)) {
				final int rawslot = event.getRawSlot();
				final ItemStack prev = inv.getItem(event.getSlot()), hb = inv.getItem(event.getHotbarButton());
				final String t = hb != null ? hb.getType().name() : "AIR";
				if(prev != null && !prev.getType().name().equals("AIR"))
					a = new PlayerArmorEvent(player, ArmorEventReason.NUMBER_KEY_UNEQUIP, prev);
				if(canBeUsed(rawslot, t))
					b = new PlayerArmorEvent(player, ArmorEventReason.NUMBER_KEY_EQUIP, hb);
			} else if(event.isShiftClick()) {
				if(st.equals(SlotType.ARMOR))
					a = new PlayerArmorEvent(player, ArmorEventReason.SHIFT_UNEQUIP, currentitem);
				else {
					final int t = getTargetSlot(current);
					if(t == -1) return;
					final ItemStack prevArmor = inv.getArmorContents()[t == 5 ? 3 : t == 6 ? 2 : t == 7 ? 1 : 0];
					if((prevArmor == null || prevArmor.getType().equals(Material.AIR)) && canBeUsed(t, current)) a = new PlayerArmorEvent(player, ArmorEventReason.SHIFT_EQUIP, currentitem);
				}
			} else if(st.equals(SlotType.ARMOR)) {
				if(ct.name().contains("DROP") && !current.equals("AIR")) {
					a = new PlayerArmorEvent(player, ArmorEventReason.DROP, currentitem);
				} else if(ct.equals(ClickType.LEFT) || ct.equals(ClickType.RIGHT)) {
					final int rawslot = event.getRawSlot();
					if(!current.equals("AIR")) {
						final int c1 = getTargetSlot(current), c2 = getTargetSlot(cursor);
						if(c1 == c2 || rawslot == c1)
							a = new PlayerArmorEvent(player, ArmorEventReason.INVENTORY_UNEQUIP, currentitem);
					}
					if(!cursor.equals("AIR")) {
						final int c1 = getTargetSlot(current), c2 = getTargetSlot(cursor);
						if(c1 == c2 || rawslot == c2)
							b = new PlayerArmorEvent(player, ArmorEventReason.INVENTORY_EQUIP, cursoritem);
					}
				}
			}
			if(a != null) {
				pm.callEvent(a);
				final ItemStack y = a.getCurrentItem(), z = a.getCursor();
				if(y != null) event.setCurrentItem(y);
				if(z != null) event.setCursor(z);
			}
			if(b != null) {
				pm.callEvent(b);
				final ItemStack y = b.getCurrentItem(), z = b.getCursor();
				if(y != null) event.setCurrentItem(y);
				if(z != null) event.setCursor(z);
			}
		}
	}
	private int getTargetSlot(String target) {
		return target.contains("HELMET") || target.contains("SKULL") || target.contains("HEAD") ? 5
				: target.contains("CHESTPLATE") || target.contains("ELYTRA") ? 6
				: target.contains("LEGGINGS") ? 7
				: target.contains("BOOTS") ? 8
				: -1;
	}
	private boolean canBeUsed(int rawslot, String target) {
		return rawslot == 5 && (target.contains("HELMET") || target.contains("SKULL") || target.contains("HEAD"))
				|| rawslot == 6 && (target.contains("CHESTPLATE") || target.contains("ELYTRA"))
				|| rawslot == 7 && target.contains("LEGGINGS")
				|| rawslot == 8 && target.contains("BOOTS");
	}
	@EventHandler
	private void playerInteractEvent(PlayerInteractEvent event) {
		if(event.getItem() != null && event.getAction().name().contains("RIGHT")) {
			final ItemStack i = event.getItem().clone();
			final String item = event.getItem().getType().name();
			if(item.endsWith("HELMET") && event.getPlayer().getInventory().getHelmet() == null
					|| item.endsWith("CHESTPLATE") && event.getPlayer().getInventory().getChestplate() == null
					|| item.endsWith("LEGGINGS") && event.getPlayer().getInventory().getLeggings() == null
					|| item.endsWith("BOOTS") && event.getPlayer().getInventory().getBoots() == null) {
				Bukkit.getScheduler().scheduleSyncDelayedTask(this, new Runnable() {
					public void run() {
						if(!event.getPlayer().getGameMode().equals(GameMode.CREATIVE) && event.getPlayer().getItemInHand().equals(i)) return;
						PlayerArmorEvent armorevent = new PlayerArmorEvent(event.getPlayer(), ArmorEventReason.HOTBAR_EQUIP, i);
						pm.callEvent(armorevent);
					}
				}, 0);

			} else if(item.endsWith("HELMET") && event.getPlayer().getInventory().getHelmet() != null
					|| item.endsWith("CHESTPLATE") && event.getPlayer().getInventory().getChestplate() != null
					|| item.endsWith("LEGGINGS") && event.getPlayer().getInventory().getLeggings() != null
					|| item.endsWith("BOOTS") && event.getPlayer().getInventory().getBoots() != null) {
				PlayerArmorEvent armorevent = new PlayerArmorEvent(event.getPlayer(), ArmorEventReason.HOTBAR_SWAP, i);
				pm.callEvent(armorevent);
			}
		}
	}
	@EventHandler
	private void playerItemBreakEvent(PlayerItemBreakEvent event) {
		if(event.getBrokenItem().getType().name().endsWith("HELMET") || event.getBrokenItem().getType().name().endsWith("CHESTPLATE") || event.getBrokenItem().getType().name().endsWith("LEGGINGS") || event.getBrokenItem().getType().name().endsWith("BOOTS")) {
			PlayerArmorEvent armorevent = new PlayerArmorEvent(event.getPlayer(), ArmorEventReason.BREAK, event.getBrokenItem());
			pm.callEvent(armorevent);
		}
	}
}
