package me.randomHashTags.RandomPackage;

import java.io.File;
import java.util.*;

import me.randomHashTags.RandomPackage.api.*;
import me.randomHashTags.RandomPackage.api.unfinished.Dungeons;
import me.randomHashTags.RandomPackage.api.Kits;
import me.randomHashTags.RandomPackage.utils.*;
import me.randomHashTags.RandomPackage.utils.universal.UMaterial;
import me.randomHashTags.RandomPackage.utils.universal.UVersion;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.*;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.SkullMeta;

import me.randomHashTags.RandomPackage.utils.classes.customenchants.CustomEnchant;
import me.randomHashTags.RandomPackage.utils.supported.FactionsAPI;
import me.randomHashTags.RandomPackage.utils.supported.VaultAPI;
import net.milkbowl.vault.economy.Economy;

@SuppressWarnings({"deprecation", "unchecked"})
public class RandomPackageAPI extends UVersion implements CommandExecutor, Listener {
	
	private static RandomPackageAPI instance;
	public static final RandomPackageAPI getAPI() {
		if(instance == null) instance = new RandomPackageAPI();
		return instance;
	}
	public static FactionsAPI fapi;
	public static CustomEnchants customenchants;
	public static GivedpItem givedpitem;
	public static Dungeons dungeons;
	public static Envoy envoy;
	public static Kits kits;
	public static KOTH koth;
	public static ServerCrates servercrates;
	public static Economy eco;

	public final YamlConfiguration itemsConfig = YamlConfiguration.loadConfiguration(new File(randompackage.getDataFolder() + File.separator, "items.yml"));
	public static File otherdataF;
	public static YamlConfiguration otherdata;
	public final YamlConfiguration sounds = YamlConfiguration.loadConfiguration(new File(randompackage.getDataFolder(), "sounds.yml")), particles = YamlConfiguration.loadConfiguration(new File(randompackage.getDataFolder() + File.separator, "particles.yml"));
	
	public static final Inventory givedp = Bukkit.createInventory(null, 27, "Givedp Categories");
	public static final List<Inventory> givedpCategories = new ArrayList<>();
	private static final TreeMap<Integer, String> treemap = new TreeMap<>();

	public void load() {
		final long started = System.currentTimeMillis();
	    eco = VaultAPI.economy;
        fapi = FactionsAPI.getFactionsAPI();
        givedpitem = GivedpItem.getGivedpItem();
        customenchants = CustomEnchants.getCustomEnchants();
        dungeons = Dungeons.getDungeons();
        envoy = Envoy.getEnvoy();
        kits = Kits.getKits();
        koth = KOTH.getKOTH();
        servercrates = ServerCrates.getServerCrates();
        treemap.put(1000, "M"); treemap.put(900, "CM"); treemap.put(500, "D"); treemap.put(400, "CD"); treemap.put(100, "C"); treemap.put(90, "XC");
        treemap.put(50, "L"); treemap.put(40, "XL"); treemap.put(10, "X"); treemap.put(9, "IX"); treemap.put(5, "V"); treemap.put(4, "IV"); treemap.put(1, "I");
		otherdataF = new File(randompackage.getDataFolder() + File.separator + "_Data" + File.separator, "other.yml");
		otherdata = YamlConfiguration.loadConfiguration(otherdataF);
		Bukkit.getConsoleSender().sendMessage(ChatColor.translateAlternateColorCodes('&', "&6[RandomPackage] &aLoaded API &e(took " + (System.currentTimeMillis()-started) + " ms)"));
	}

	public void viewGivedp(Player player) {
		player.openInventory(Bukkit.createInventory(player, givedp.getSize(), givedp.getTitle()));
		player.getOpenInventory().getTopInventory().setContents(givedp.getContents());
		player.updateInventory();
	}
	public void addGivedpCategory(List<ItemStack> items, UMaterial m, String what, String invtitle) {
		item = m.getItemStack(); itemMeta = item.getItemMeta();
		itemMeta.setDisplayName(ChatColor.YELLOW + "" + ChatColor.BOLD + what);
		item.setItemMeta(itemMeta);
		givedp.addItem(item);
		final int size = items.size();
		final Inventory inv = Bukkit.createInventory(null, size == 9 || size == 18 || size == 27 || size == 36 || size == 45 || size == 54 ? size : ((size+9)/9)*9, invtitle);
		for(ItemStack is : items) if(is != null) inv.addItem(is);
		givedpCategories.add(inv);
	}
	public void saveOtherData() {
		try {
			otherdata.save(otherdataF);
			otherdataF = new File(randompackage.getDataFolder() + File.separator + "_Data" + File.separator, "other.yml");
			otherdata = YamlConfiguration.loadConfiguration(otherdataF);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public boolean hasPermission(CommandSender sender, String permission, boolean sendPermMsg) {
		if(!(sender instanceof Player) || sender.hasPermission(permission)) return true;
		if(sendPermMsg) sendStringListMessage(sender, randompackage.getConfig().getStringList("no-permission"));
		return false;
	}
	
	public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {
		final Player player = sender instanceof Player ? (Player) sender : null;
		final String n = cmd.getName();
		if(n.equals("randompackage")) {
			if(args.length == 0) {
				if(player != null && player.getName().equals("RandomHashTags") || hasPermission(sender, "RandomPackage.randompackage", true)) {
					for (String string : Arrays.asList(" ",
							"&6&m&l---------------------------------------------",
							"&7- Author: &6RandomHashTags",
							"&7- Plugin Version: &b" + randompackage.getDescription().getVersion(),
							"&7- Server Version: &f" + v,
							"&7- Faction Plugin: " + (RandomPackage.factions != null ? "&3" + RandomPackage.factionPlugin + " &7(&2" + RandomPackage.factions.getDescription().getVersion() + "&7)" : "&cfalse"),
							"&7- mcMMO: " + (RandomPackage.mcmmo != null ? "&atrue &7(&2" + RandomPackage.mcmmo.getDescription().getVersion() + "&7)" : "&cfalse"),
							"&7- Spawner Plugin: " + (RandomPackage.spawner != null ? "&e" + RandomPackage.spawner.getName() + " &7(&2" + RandomPackage.spawner.getDescription().getVersion() + "&7)" : "&cfalse"),
							"&7- Website: &9http://www.randomhashtags.com",
							"&7- Info: &f%%__USER__%%, &f%%__NONCE__%%",
							"&7- Purchaser: &a&nhttps://www.spigotmc.org/members/%%__USER__%%/",
							"&6&m&l---------------------------------------------",
							" "))
						sender.sendMessage(ChatColor.translateAlternateColorCodes('&', string));
				}
			} else if(args[0].equals("edit") && player != null && hasPermission(sender, "RandomPackage.randompackage.edit", true)) {
				editFeatures(player);
			} else if(args[0].equals("reload") && hasPermission(sender, "RandomPackage.randompackage.reload", true)) {
				RandomPackage.getPlugin.reload();
			} else if(args[0].equals("backup") && hasPermission(sender, "RandomPackage.randompackage.backup", true)) {
				RPEvents.getRPEvents().backup(true, true);
				sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "&3[RandomPackage] &aPlayer backup complete!"));
			}
		} else if(n.equals("withdraw") && player != null && hasPermission(sender, "RandomPackage.withdraw", true)) {
			if(args.length == 0) sendStringListMessage(player, randompackage.getConfig().getStringList("withdraw.argument-0"));
			else {
				String m = null, formattedAmount = null;
				if(parseDouble(args[0]) == null) m = "withdraw.invalid";
				else {
					double amount = getRemainingDouble(args[0]);
					formattedAmount = formatDouble(amount);
					formattedAmount = formattedAmount.contains("E") ? formattedAmount.split("E")[0] : formattedAmount;
					if(eco == null) { player.sendMessage("[RandomPackage] You need an Economy plugin installed and enabled to use this feature!"); return true; }
					else if(amount <= 0.00)                  m = "withdraw.cannot-withdraw-zero";
					else if(eco.getBalance(player) < amount) m = "withdraw.cannot-withdraw-more-than-balance";
					else if(eco.withdrawPlayer(player, amount).transactionSuccess()) {
						item = givedpitem.getBanknote(amount, player.getName());
						giveItem(player, item);
						m = "withdraw.success";
					}
				}
				for(String string : randompackage.getConfig().getStringList(m)) {
					if(string.contains("{AMOUNT}")) string = string.replace("{AMOUNT}", formattedAmount);
					if(string.contains("{BALANCE}")) string = string.replace("{BALANCE}", formatDouble(eco.getBalance(player)));
					player.sendMessage(ChatColor.translateAlternateColorCodes('&', string));
				}
			}
		} else return true;
		return true;
	}
	public void editFeatures(Player player) {
		player.openInventory(Bukkit.createInventory(player, 18, "Toggle RandomPackage Features"));
	}
	
	public void sendStringListMessage(CommandSender sender, List<String> message) {
		final Player player = sender instanceof Player ? (Player) sender : null;
		final HashMap<String, String> replacements = new HashMap<>();
		if(player != null) replacements.put("{PLAYER}", player.getName());
		sendStringListMessage(sender, message, replacements);
	}
	public void sendStringListMessage(CommandSender sender, List<String> message, HashMap<String, String> replacements) {
		if(message != null && message.size() > 0 && !message.get(0).equals(""))
			for(String s : message) {
				if(replacements != null) for(String r : replacements.keySet()) s = s.replace(r, replacements.get(r));
				sender.sendMessage(ChatColor.translateAlternateColorCodes('&', s));
			}
	}

	@EventHandler
	private void inventoryClickEvent(InventoryClickEvent event) {
		if(!event.isCancelled() && event.getCurrentItem() != null && !event.getCurrentItem().getType().equals(Material.AIR)) {
			final Player player = (Player) event.getWhoClicked();
            final Inventory top = player.getOpenInventory().getTopInventory();
			final String t = top.getTitle();
			final int r = event.getRawSlot();
			final ItemStack c = event.getCurrentItem();
			if(t.equals(givedp.getTitle()) && c != null && r < top.getSize()) {
				player.openInventory(givedpCategories.get(r));
			} else if(givedpCategories.contains(event.getClickedInventory()) && c != null && r < top.getSize()) {
				giveItem(player, c);
			} else return;
			event.setCancelled(true);
			player.updateInventory();
		}
	}

	public ItemStack d(FileConfiguration config, String path, int tier) {
		item = null;
		if(config == null && path != null || config != null && config.get(path + ".item") != null) {
			if(config == null && path.toLowerCase().startsWith("potion:")
					|| config != null && config.getString(path + ".item").toLowerCase().startsWith("potion:")) {
				ItemStack l = convertStringToPotion(config == null ? path : config.getString(path + ".item"), true);
				if(config != null && l != null) {
					itemMeta = l.getItemMeta();
					if(config.get(path + ".name") != null) itemMeta.setDisplayName(ChatColor.translateAlternateColorCodes('&', config.getString(path + ".name")));
					lore.clear();
					if(config.get(path + ".lore") != null)
						for(String s : config.getStringList(path + ".lore")) lore.add(ChatColor.translateAlternateColorCodes('&', s));
					itemMeta.setLore(lore); lore.clear();
					l.setItemMeta(itemMeta);
				}
				return l;
			} else if(config != null && config.getString(path + ".item").toLowerCase().contains("spawner") && !path.equals("mystery-mob-spawner") && !config.getString(path + ".item").toLowerCase().startsWith("mob_spawner")
						|| config == null && path.toLowerCase().contains("spawner") && !path.equals("mystery-mob-spawner") && !path.toLowerCase().startsWith("mob_spawner")) {
				return getSpawner(config != null ? config.getString(path + ".item") : path);
			}
			final ItemStack B = checkStringForRPItem(null, config == null ? path : config.getString(path + ".item"));
			if(B != null) return B.clone();
			boolean enchanted = config != null && config.getBoolean(path + ".enchanted");
			lore.clear();
			int amount = 1;
			SkullMeta m = null;
			final Material skullitem = UMaterial.PLAYER_HEAD_ITEM.getMaterial();
			if(config == null && path.toLowerCase().contains("name{")
					|| config != null && config.getString(path + ".item").toLowerCase().contains("name{")) {
				String ii = config == null ? path : config.getString(path + ".item"), it = ii.split(":")[0].toUpperCase(), na = ii.split("\\{")[1].split("}")[0];
				na = na != null ? ChatColor.translateAlternateColorCodes('&', na) : null;
				amount = Integer.parseInt(ii.split(":").length >= 2 && ii.split(":").length <= 3 ? ii.split(":")[2].split(";")[0] : "1");
				byte data = Byte.parseByte(ii.split(":")[1]);
				ItemStack M = UMaterial.valueOf(it, data);
				if(M == null) Bukkit.broadcastMessage("M==null;it=" + it + ",data=" + data);
				item = M != null ? M : new ItemStack(Material.valueOf(it), amount, data);
				itemMeta = item.getItemMeta();
				if(item.getType().equals(skullitem)) {
					m = (SkullMeta) itemMeta;
					m.setDisplayName(na);
					if(item.getData().getData() == 3) m.setOwner(ii.split(":").length == 4 ? ii.split(":")[3].split("}")[0] : "RandomHashTags");
					item.setItemMeta(m);
				} else {
					itemMeta.setDisplayName(na);
				}
			} else {
				byte data = 0;
				String it = config != null ? config.getString(path + ".item").toUpperCase() : path, name = config != null ? config.getString(path + ".name") : null;
				final int L = it.split(":").length;
				if(it.contains(":") && L >= 3 || it.contains(":") && L >= 2) data = Byte.parseByte(it.split(":")[1]);
				if(it.contains(":") && L >= 3) amount = Integer.parseInt(it.split(":")[2].split(";")[0].split("}")[0]);
				final String material = it.split(":")[0].toUpperCase();
				ItemStack M = UMaterial.valueOf(material, data);
				if(M == null)
					Bukkit.broadcastMessage("M==null;material=" + material + ",data=" + data);
				item = M != null ? M : new ItemStack(Material.valueOf(material.replace(":" + data, "").replace(":" + amount, "").toUpperCase()), amount, data); itemMeta = item.getItemMeta();
				if(item.getType().equals(skullitem)) {
					m = (SkullMeta) itemMeta;
					if(item.getData().getData() == 3) m.setOwner(it.split(":").length == 4 ? it.split(":")[3].split("}")[0] : "RandomHashTags");
				}
				if(name != null) {
					(item.getType().equals(skullitem) ? m : itemMeta).setDisplayName(ChatColor.translateAlternateColorCodes('&', name));
				}
			}
			if(enchanted) itemMeta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
			final HashMap<Enchantment, Integer> enchants = new HashMap<>();
			final boolean levelzeroremoval = customenchants.levelZeroRemoval;
			if(config != null && config.get(path + ".lore") != null) {
				lore.clear();
				for(String string : config.getStringList(path + ".lore")) {
					CustomEnchant en;
					if(string.startsWith("{") && (!string.toLowerCase().contains("reqlevel=") && string.toLowerCase().contains("chance=") || string.toLowerCase().contains("reqlevel=") && tier >= Integer.parseInt(string.toLowerCase().split("reqlevel=")[1].split(":")[0]))) {
						en = CustomEnchant.valueOf(string.split("\\{")[1].split("}")[0]);
						final boolean c = string.contains("chance=");
						if(en != null && (!c || random.nextInt(100) <= Integer.parseInt(string.split("chance=")[1]))) {
							final int lvl = random.nextInt(en.getMaxLevel()+1);
							if(lvl != 0 || !levelzeroremoval)
								lore.add(en.getRarity().getApplyColors() + en.getName() + " " + toRoman(lvl == 0 ? 1 : lvl));
						}
					} else if(string.toLowerCase().startsWith("venchants{")) {
						for(String s : string.split("\\{")[1].split("}")[0].split(";")) enchants.put(getEnchantment(s), getRemainingInt(s));
					} else if(string.toLowerCase().startsWith("rpenchants{")) {
						for(String s : string.split("\\{")[1].split("}")[0].split(";")) {
							final CustomEnchant e = CustomEnchant.valueOf(s);
							if(e != null) {
								double multiplier = tier != 0 ? config.getDouble("gui.settings.tier-custom-enchant-multiplier." + tier) : 0;
								int l = getRemainingInt(s), x = (int) (e.getMaxLevel()*multiplier);
								l = l != -1 ? l : tier != 0 ? x+random.nextInt(e.getMaxLevel()-x+1) : random.nextInt(e.getMaxLevel());
								if(l != 0 ||  !levelzeroremoval)
									lore.add(e.getRarity().getApplyColors() + e.getName() + " " + toRoman(l != 0 ? l : 1));
							}
						}
					} else if(string.toLowerCase().startsWith("{") && string.toLowerCase().contains(":reqlevel")) {
						
					} else if(string.toLowerCase().startsWith("e:"))
						enchants.put(getEnchantment(string.split(":")[1]), getRemainingInt(string));
					else
						lore.add(ChatColor.translateAlternateColorCodes('&', string));
				}
			} else if(config == null && path.toLowerCase().contains("lore{")) {
				for(String string : path.split("lore\\{")[1].split("}")[0].split("\\n")) {
					if(string.toLowerCase().startsWith("e:"))
						enchants.put(getEnchantment(string.split(":")[1]), getRemainingInt(string));
					else
						lore.add(ChatColor.translateAlternateColorCodes('&', string));
				}
			}
			(!item.getType().equals(skullitem) ? itemMeta : m).setLore(lore);
			item.setItemMeta(!item.getType().equals(skullitem) ? itemMeta : m);
			lore.clear();
			if(enchanted) item.addUnsafeEnchantment(Enchantment.ARROW_DAMAGE, 1);
			for(Enchantment enchantment : enchants.keySet())
				if(enchantment != null)
					item.addUnsafeEnchantment(enchantment, enchants.get(enchantment));
			if(itemMeta.hasDisplayName()) {
				if(itemMeta.getDisplayName().contains("{ENCHANT_SIZE}")) customenchants.applyTransmogScroll(item);
			}
		}
		return item;
	}
	/*
	 * 
	 * 
	 */
	public ItemStack checkStringForRPItem(OfflinePlayer player, String string) {
		final GivedpItem givedpitem = GivedpItem.getGivedpItem();
		int amount = 1;
		item = null;
		if(!string.startsWith("{")) {
		} else {
			if(string.toLowerCase().startsWith("{vkit_loot:")) return new ItemStack(Material.DIAMOND);
			String item = string.split("\\}\\,")[0].substring(1),
					itemname = null,
					itemlore = null;
			if(string.split("},\\{").length >= 2 && !(string.split("},\\{")[1].equals("")))
				itemname = string.split("},\\{")[1].split("},")[0];
			if(string.split("},\\{").length == 3 && !(string.split("},\\{")[2].split("}").length == 0))
				itemlore = string.split("},\\{")[2].split("}")[0];
			Material material = null;
			if(Material.matchMaterial(item.split(":")[0].toUpperCase()) != null) {
				material = Material.matchMaterial(item.split(":")[0].toUpperCase());
			} else {
				Bukkit.broadcastMessage("Invalid item --> " + string);
				return null;
			}
			this.item = new ItemStack(material, Integer.parseInt(item.split(":")[2]), Byte.parseByte(item.split(":")[1]));
			itemMeta = this.item.getItemMeta(); lore.clear();
			if(!(itemlore == null))
				for(String k : itemlore.split("\n")) lore.add(ChatColor.translateAlternateColorCodes('&', k));
			if(itemname != null) itemMeta.setDisplayName(ChatColor.translateAlternateColorCodes('&', itemname));
			itemMeta.setLore(lore); lore.clear();
			this.item.setItemMeta(itemMeta);
			this.item.setAmount(amount);
			return this.item;
		}
		//
		ItemStack potion = convertStringToPotion(string, false);
		if(potion != null) return potion;
		
		if(string.contains("||"))
			string = random.nextInt(100) < 50 ? string.split("\\|\\|")[0]  : string.split("\\|\\|")[1];
		lore.clear();
		String name;
		if(string.contains("amount=")) {
			final int min = string.split("amount=")[1].contains("-") ? Integer.parseInt(string.split("amount=")[1].split("-")[0]) : 0;
			amount = string.split("amount=")[1].contains("-")
					? min + random.nextInt(Integer.parseInt(string.split("amount=")[1].split("-")[1].split(";")[0]) - min)
					: Integer.parseInt(string.split("amount=")[1]);
			if(amount <= 0) return new ItemStack(Material.AIR);
			string = string.split(";amount=")[0];
		}
		//
		if(Material.getMaterial(string.toUpperCase()) != null) return new ItemStack(Material.getMaterial(string.toUpperCase()), amount, string.contains(":") ? Byte.parseByte(string.split(":")[1]) : (byte) 0);
		//
		final ItemStack gdpi = givedpitem.valueOf(player, string);
		if(gdpi != null) {
			ItemStack gg = gdpi.clone();
			gg.setAmount(amount);
			return gg;
		}
		if(string.endsWith("spawner")) {
			lore.clear(); lore.add(string);
			string = string.split("spawner")[0].substring(0, 1).toUpperCase() + string.split("spawner")[0].substring(1) + " Spawner";
			item = UMaterial.SPAWNER.getItemStack();
			name = ChatColor.YELLOW + string;
		} else {
			return null;
		}
		
		if(item.getItemMeta().hasDisplayName()) itemMeta.setDisplayName(item.getItemMeta().getDisplayName());
		itemMeta.setDisplayName(name);
		itemMeta.setLore(lore); lore.clear();
		item.setItemMeta(itemMeta);
		if(item != null) item.setAmount(amount);
		return item;
	}


	public double oldevaluate(String chancestring) {
		chancestring = chancestring.replace("\\p{L}", "").replaceAll("\\p{Z}", "");
		String parentheses = null;
		double prockchance = 0;
		if(chancestring.contains("(") && chancestring.contains(")")) {
			for(int z = 1; z <= 5; z++) {
				int startp = -1, endp = -1;
				for(int i = 0; i < chancestring.length(); i++) {
					if(chancestring.substring(i, i + 1).equals("(")) {
						startp = i;
					} else if(chancestring.substring(i, i + 1).equals(")")) {
						endp = i + 1;
					}
					if(startp != -1 && endp != -1) {
						parentheses = chancestring.substring(startp, endp);
						prockchance = evaluate(parentheses.substring(1, parentheses.length() - 1));
						chancestring = chancestring.replace(parentheses, "" + prockchance);
						if(chancestring.endsWith("+") || chancestring.endsWith("-") || chancestring.endsWith("*") || chancestring.endsWith("/")) {
							chancestring = chancestring.substring(0, chancestring.length() - 1);
						}
						if(chancestring.startsWith("+") || chancestring.startsWith("-") || chancestring.startsWith("*") || chancestring.startsWith("/")) {
							chancestring = chancestring.substring(1);
						}
						startp = -1; endp = -1;
					}
				}
			}
		}
		return evaluate(chancestring);
	}
	private double evaluate(String input) {
		double chance = 0.00;
		if(input.equals("-1")) return chance;
		for(int i = 1; i <= 5; i++) {
			String sign = null;
			if(input.contains("*")) {
				sign = input.split("\\*")[0] + "*" + input.split("\\*")[1];
				chance = Double.parseDouble(input.split("\\*")[0]) * Double.parseDouble(input.split("\\*")[1]);
			} else if(input.contains("/")) {
				sign = input.split("/")[0] + "/" + input.split("/")[1];
				chance = Double.parseDouble(input.split("\\/")[0]) / Double.parseDouble(input.split("\\/")[1]);
			} else if(input.contains("+")) {
				sign = input.split("\\+")[0] + "+" + input.split("\\+")[1];
				chance = Double.parseDouble(input.split("\\+")[0]) + Double.parseDouble(input.split("\\+")[1]);
			} else if(input.contains("-") && !input.startsWith("-")) {
				sign = input.split("-")[0] + "-" + input.split("-")[1];
				chance = Double.parseDouble(input.split("\\-")[0]) - Double.parseDouble(input.split("\\-")[1]);
			} else if(!input.equals("")) {
				return Double.valueOf(input);
			}
			if(sign != null) input = input.replace(sign, "" + chance);
		}
		return chance;
	}

	/*
	 * This code is from "bhlangonijr" at
	 * https://stackoverflow.com/questions/12967896/converting-integers-to-roman-numerals-java
	 */
	public final String toRoman(int number) {
		if(number <= 0) return "";
		int l = treemap.floorKey(number);
		if(number == l) return treemap.get(number);
		return treemap.get(l) + toRoman(number - l);
	}
}
